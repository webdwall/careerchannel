<?php
define('EMI_DEBUG', true);
define('BOOTSTRAP_ACTIVE', true);

if( !function_exists('dateDiff') ):
function dateDiff($time1, $time2, $precision = 6) {
	// If not numeric then convert texts to unix timestamps
	if (!is_int($time1)) {
		$time1 = strtotime($time1);
	}
	if (!is_int($time2)) {
		$time2 = strtotime($time2);
	}

	// If time1 is bigger than time2
	// Then swap time1 and time2
	if ($time1 > $time2) {
		$ttime = $time1;
		$time1 = $time2;
		$time2 = $ttime;
	}

	// Set up intervals and diffs arrays
	$intervals = array('year', 'month', 'day', 'hour', 'minute', 'second');
	$diffs = array();

	// Loop thru all intervals
	foreach ($intervals as $interval) {
		// Set default diff to 0
		$diffs[$interval] = 0;
		// Create temp time from time1 and interval
		$ttime = strtotime("+1 " . $interval, $time1);
		// Loop until temp time is smaller than time2
		while ($time2 >= $ttime) {
			$time1 = $ttime;
			$diffs[$interval] ++;
			// Create new temp time from time1 and interval
			$ttime = strtotime("+1 " . $interval, $time1);
		}
	}

	$count = 0;
	$times = array();
	// Loop thru all diffs
	foreach ($diffs as $interval => $value) {
		// Break if we have needed precission
		if ($count >= $precision) {
			break;
		}
		// Add value and interval 
		// if value is bigger than 0
		if ($value > 0) {
			// Add s if value is not 1
			if ($value != 1) {
				$interval .= "s";
			}
			// Add value and interval to times array
			$times[] = $value . " " . $interval;
			$count++;
		}
	}
	//print_r($times); 
	return $times[0];
	// Return string with times
	//return implode(", ", $times[0]);
}
endif;
/* +++++++++++++++++++++++++++++++++
  Featured jobs page
  ++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++
  adming settings
  ++++++++++++++++++++++++++++ */
//add the settings option interface
add_action('pmsc_admin_general_setting_hook', 'cc_ftjobs_page_print');

function cc_ftjobs_page_print() {
    $selected_page = get_option('cc_ftjobs_page_id');
    ?>
	<li><strong>Special Pages:</strong></li>
    <li>
        <label for='cc_fjobs_page'>'Featured Jobs' Page</label>
	<?php
	$args = array(
	    'selected' => $selected_page,
	    'name' => 'cc_fjobs_page',
	    'id' => 'cc_fjobs_page'
	);
	echo pmsc_admin_settings_pages_list($args);
	?>
        <br/><small><i>Select the page which should display 'Featured Jobs' listing.</i></small>
    </li>
    <?php
}

//save/update the settins option
add_action('pmsc_admin_setting_save_button_hook', 'cc_ftjobs_page_update');

function cc_ftjobs_page_update() {
    $selected_page = get_option('cc_ftjobs_page_id');
    //update only if it's been changed
    $new_selection = ( $_POST['cc_fjobs_page'] ? $_POST['cc_fjobs_page'] : false );
    if ($new_selection != $selected_page) {
	update_option('cc_ftjobs_page_id', $new_selection);
    }
}

/* ========================= */

/* ++++++++++++++++++++++++++++
  display content
  ++++++++++++++++++++++++++++ */
add_filter('the_content', 'cc_ftjobs_page_content');

function cc_ftjobs_page_content($content) {
    //featured job
    if (is_page()) {
	$selected_page = get_option('cc_ftjobs_page_id');
	if ($selected_page && is_page($selected_page)) {
	    $content .= '<div style="clear:both;"></div>';
	    $content .= do_shortcode('[jobs show_featured_only="yes"]');
	}
    }
    return $content;
}

/* ========================= */
/* ============================= */

/* +++++++++++++++++++++++++++++++++
  Recent jobs page
  ++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++
  adming settings
  ++++++++++++++++++++++++++++ */
//add the settings option interface
add_action('pmsc_admin_general_setting_hook', 'cc_recentjobs_page_print');

function cc_recentjobs_page_print() {
    $selected_page = get_option('cc_recentjobs_page_id');
    ?>
    <li>
        <label for='cc_recentjobs_page'>'Recent/New Jobs' Page</label>
	<?php
	$args = array(
	    'selected' => $selected_page,
	    'name' => 'cc_recentjobs_page',
	    'id' => 'cc_recentjobs_page'
	);
	echo pmsc_admin_settings_pages_list($args);
	?>
        <br/><small><i>Select the page which should display 'Recent/New Jobs' listing.</i></small>
    </li>
    <?php
}

//save/update the settins option
add_action('pmsc_admin_setting_save_button_hook', 'cc_recentjobs_page_update');

function cc_recentjobs_page_update() {
    $selected_page = get_option('cc_recentjobs_page_id');
    //update only if it's been changed
    $new_selection = ( $_POST['cc_recentjobs_page'] ? $_POST['cc_recentjobs_page'] : false );
    if ($new_selection != $selected_page) {
	update_option('cc_recentjobs_page_id', $new_selection);
    }
}

/* ========================= */

/* ++++++++++++++++++++++++++++
  display content
  ++++++++++++++++++++++++++++ */
add_filter('the_content', 'cc_recentjobs_page_content');

function cc_recentjobs_page_content($content) {
    //featured job
    if (is_page()) {
	$selected_page = get_option('cc_recentjobs_page_id');
	if ($selected_page && is_page($selected_page)) {
	    $content .= '<div style="clear:both;"></div>';
	    $content .= do_shortcode('[jobs orderby="date"]');
	}
    }
    return $content;
}

/* ========================= */
/* ============================= */

add_filter('job_manager_output_jobs_args', 'emi_jobs_filter_featured', 10, 2);
add_filter('job_manager_get_listings', 'emi_jobs_filter_featured', 10, 2);
/*
 * add the 'featured' meta query to wp_query arguments
 */

function emi_jobs_filter_featured($query_args, $post_args) {
    if (isset($post_args['show_featured_only']) && $post_args['show_featured_only'] == 'yes') {
	$feat_meta_query = array(
	    'key' => '_featured',
	    'value' => 1
	);
	if (isset($query_args['meta_query']) && !empty($query_args['meta_query'])) {
	    $query_args['meta_query'][] = $feat_meta_query;
	} else {
	    $query_args['meta_query'] = array($feat_meta_query);
	}
    }
    return $query_args;
}

/* extended/advanced search */
add_filter('job_manager_get_listings', 'emi_jobs_extended_search', 10, 2);

function emi_jobs_extended_search($query_args, $post_args) {
    //company name
    $company_name = $post_args['form_data']['txt_job_company_name'];
    if ($company_name) {
		$company_meta_query = array('key' => '_company_name', 'value' => $company_name, 'compare' => 'LIKE');
		if (isset($query_args['meta_query']) && !empty($query_args['meta_query'])) {
			$query_args['meta_query'][] = $company_meta_query;
		} else {
			$query_args['meta_query'] = array($company_meta_query);
		}
    }

    //date range query
    $start_date = $post_args['form_data']['dt_start_date'];
    $end_date = $post_args['form_data']['dt_end_date'];
    if ($start_date || $end_date) {
		$date_query = array('inclusive' => true,);
		if ($start_date) {
			list( $day, $month, $year ) = explode("/", $start_date);
			$date_query['after'] = array('year' => $year, 'month' => $month, 'day' => $day);
		}
		if ($end_date) {
			list( $day, $month, $year ) = explode("/", $end_date);
			$date_query['before'] = array('year' => $year, 'month' => $month, 'day' => $day);
		}
		$query_args['date_query'] = array($date_query);
    }

	//job region query
	$job_region = $post_args['form_data']['search_region'];
	if( $job_region ){
		$field = is_numeric( $job_region ) ? 'term_id' : 'slug';
		$query_args['tax_query'][] = array(
			'taxonomy' => 'job_listing_region',
			'field'    => $field,
			'terms'    => $job_region
		);
	}
	
    return $query_args;
}




/* ========================================================= */
/*
 * load custom stylesheets and scripts
 */
add_action( 'wp_enqueue_scripts', 'emi_add_css_js' );
function emi_add_css_js(){
	$theme_root_url = get_bloginfo('template_directory');
	wp_enqueue_style( 'jobify-emedia-custom', $theme_root_url.'/css/custom.css' );
	wp_enqueue_style( 'bootstrap', $theme_root_url.'/_inc/bootstrap/css/bootstrap.min.css' );
	wp_enqueue_script( 'bootstrap', $theme_root_url.'/_inc/bootstrap/js/bootstrap.min.js' );
}

add_action( 'wp_footer', 'emi_print_js' );
function emi_print_js(){
	?>
	<script>
	jQuery(document).ready(function($){
		$(".show-tooltip[disabled!='disabled']").tooltip({});
	});
	jQuery(window).load(function(){
		jQuery(".oneall_social_login, .oneall_social_link").wrap("<div style='height: 40px; overflow:hidden'></div>");
	});
	</script>
	<?php 
}

/*
 * Redirect users to appropriate pages
 * if not authorized to view the requested url
*/
add_action( 'template_redirect', 'cc_authorization_redirect' );
function cc_authorization_redirect(){
	if( ( is_page('post-a-job') || is_page('employer-dashboard') || is_page('candidate-dashboard') ) && !is_user_logged_in() ){
		auth_redirect();
		exit();
	}
}

if( !function_exists('emi_get_option') ):
/**
 * A wrapper for the get_option() method.
 * All the requests to get_option call through this method are cached for later retrieval.
 * 
 * @param string $option_name name of the option
 * @param mixed $default_value default false
 * @param bool $bypass_cache whether to refresh cache and retrieve again from database
 * @return mixed option value
 */
function emi_get_option( $option_name, $default_value=false, $bypass_cache=false ){
	$option_value = wp_cache_get( 'emi' . $option_name  );
	if( $option_value ===false || $bypass_cache ){
		$option_value = get_option($option_name, $default_value);
		wp_cache_set( 'emi'.$option_name, $option_value);
	}
	return $option_value;
}
endif;

if( !function_exists('emi_update_option') ):
/**
 * A wrapper for the update_option() method.
 * A call to update_option is made and cache is updated.
 * 
 * @param string $option_name name of the option
 * @param mixed $option_value 
 * 
 * @return void
 */
function emi_update_option( $option_name, $option_value ){
	update_option($option_name, $option_value);
	wp_cache_set( 'emi'.$option_name, $option_value);
}
endif;
