<?php 
/* 
 * the header in candiate's dashboard
 */

$candidate = new WP_User( get_current_user_id() );
?>
<div class='dashboard-header container dashboard-header-candidate'>
	<div class="row clear text-info member-header">
		<div class="row clear">
			<div class='col span_2'>
				<?php add_filter( 'get_avatar', 'add_responsive_class_to_avatar' );?>
				<a class="pull-left member-dp" href="<?php echo home_url('/candidates/').$candidate->ID;?>" title='view profile'>
					<?php echo get_avatar( $candidate->ID, 150 );?>
				</a>
				<?php remove_filter( 'get_avatar', 'add_responsive_class_to_avatar' );?>
			</div>
			<div class="col span_10">
				<h1 class="media-heading">
					<?php echo $candidate->data->display_name; ?>
				</h1>

				<div class='member-badges pull-right text-right'>
					<ul class='list-unstyled inline clearfix'><?php do_action( 'cc_member_badges', 'candidate', $candidate );?></ul>
				</div>

				<ul class='member-meta list-unstyled'>
					<li class='member-since'>
						<span>Member Since - </span>
						<?php echo date( 'M Y', strtotime($candidate->data->user_registered));?>
					</li>
					<?php
					$fields_to_display = array(
						'user_cor'				=> 'Residence',
						'skill'					=> 'Specialization',
						'experience_in_months'	=> 'Experience'
					);
					foreach( $fields_to_display as $meta_key=>$label ){
						if( ($meta_value=$candidate->get($meta_key))!=false && can_view_candidate_info( $candidate, 'meta-'.$meta_key ) ){
							$meta_value = maybe_unserialize($meta_value);
							if(is_array($meta_value) ){
								$meta_value = implode( ', ', $meta_value );
							}

							echo "<li class='" . esc_attr( $meta_key ) ."'><span>$label - </span>";
							echo apply_filters( 'ccuserinfo_'.$meta_key, $meta_value ) . "</li>";
						}
					}
					?>
				</ul>

			</div>
		</div><!-- .media -->
		<div class='pull-right member-actions'>
			<div class='pull-right' style='margin-left: 5px;'><?php echo cc_resume_button($candidate->ID);?></div>
			<span class="btn-group pull-right">
				<?php 
				echo "<a class='btn btn-info btn-small show-tooltip' href='". home_url( '/candidates/' ). $candidate->ID ."' title='view profile'><span class='glyphicon glyphicon-zoom-in'></span> view profile</a>";
				echo "<a class='btn btn-default btn-small show-tooltip' href='". home_url( '/your-profile/' ) ."' title='edit profile'><span class='glyphicon glyphicon-cog'></span> edit profile</a>";
				echo "<a class='btn btn-default btn-small show-tooltip' href='". home_url( '/your-profile/#pass1' ) ."' title='change password'><span class='glyphicon glyphicon-cog'></span> change password</a>";
				?>
			</span>
		</div>
	</div>
</div><!-- .dashboard-header -->