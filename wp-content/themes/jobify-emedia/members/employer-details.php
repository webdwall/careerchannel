<?php
/* $employer is the wp_user object whose info we have to display, $employer variable is already accessible here */
global $employer;
//$employer_meta  = get_user_meta($employer->ID);
?>
<div class="row clear text-info member-header">
	<div class='member-badges text-right'>
		<ul class='list-unstyled inline clearfix'><?php do_action( 'cc_member_badges', 'employer', $employer );?></ul>
	</div>
	
	<div class="media">
		<?php add_filter( 'get_avatar', 'add_responsive_class_to_avatar' );?>
		<a class="pull-left member-dp" href="<?php echo home_url('/employers/').$employer->ID;?>" title="view profile">
			<?php echo get_avatar( $employer->ID, 150 );?>
		</a>
		<?php remove_filter( 'get_avatar', 'add_responsive_class_to_avatar' );?>
		
		<div class="media-body">
			<h1 class="media-heading">
				<?php if( can_view_employer_info( $employer, 'display_name' ) ){
					echo $employer->data->display_name;
				} else {
					echo 'candidate ' . $employer->ID;
				}?>
			</h1>
			
			<ul class='member-meta list-unstyled'>
				<li class='member-since'>
					<span>Member Since - </span>
					<?php echo date( 'M Y', strtotime($employer->data->user_registered));?>
				</li>
				<?php
				$fields_to_display = array(
					'user_cor'				=> 'Residence',
				);
				foreach( $fields_to_display as $meta_key=>$label ){
					if( ($meta_value=$employer->get($meta_key))!=false && can_view_employer_info( $employer, 'meta-'.$meta_key ) ){
						$meta_value = maybe_unserialize($meta_value);
						if(is_array($meta_value) ){
							$meta_value = implode( ', ', $meta_value );
						}

						echo "<li class='" . esc_attr( $meta_key ) ."'><span>$label - </span>";
						echo apply_filters( 'ccuserinfo_'.$meta_key, $meta_value ) . "</li>";
					}
				}
				?>
			</ul>
			
		</div>
	</div><!-- .media -->
	<div class='pull-right member-actions'>
		<?php do_action( 'cc_member_action_buttons', 'employer', $employer, 'profile' );?>
	</div>
</div>
<div class="row clear member-content">
	<div class='col span_6 member-bio'>
		<h2>
			About Me
			<?php 
			if( $employer->ID ==  get_current_user_id() ){
				echo "<small><a href='". home_url( '/your-profile/#description' ) ."' class='show-tooltip' title='Edit this'><span class='glyphicon glyphicon-pencil'></span></a></small>";
			}
			?>
		</h2>
		<?php echo $employer->get('description');?>
		<div class="employer_about_container" >
		    <?php 
		    $temp_desc = explode("\n", $employer->get('description') );
		    foreach ( $temp_desc as $desc ){
			echo '<p>'.$desc.'</o>';
		    }
		    ?>
		</div>
	</div>
	<div class='col span_6 member-info'>
		<table class='table table-striped'>
			<?php
			$fields_to_display = array(
				'user_cor'				=> 'Country of residence',
				'skill'					=> 'Specialization',
				'experience_in_months'	=> 'Experience'
			);
			foreach( $fields_to_display as $meta_key=>$label ){
				if( ($meta_value=$employer->get($meta_key))!=false && can_view_employer_info( $employer, 'meta-'.$meta_key ) ){
					$meta_value = maybe_unserialize($meta_value);
					if(is_array($meta_value) ){
						$meta_value = implode( ', ', $meta_value );
					}
					
					echo "<tr><th>$label</th>";
					echo "<td>" . apply_filters( 'ccuserinfo_'.$meta_key, $meta_value ) . "</td></tr>";
				}
			}
			?>
		</table>
	</div>
</div>