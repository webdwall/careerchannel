<?php
/* $candidate is the wp_user object whose info we have to display, $candidate variable is already accessible here */
global $candidate; 
$candidate_meta  = get_user_meta($candidate->ID);
?>
<div class="panel panel-default candidate candidate-<?php echo $candidate->ID;?>">
	<?php 
	$candidate_name = '';
	//only premium users should see candidate's name
	if( can_view_candidate_info( $candidate, 'name' ) ){
		$candidate_name .= $candidate->display_name;
	}
	?>
	<div class="panel-heading">
		<h4 class='panel-title'><?php echo get_avatar($candidate->data->user_email, 20);?> <a href='<?php echo home_url( '/candidates/' ). $candidate->ID;?>'><?php echo $candidate_name;?></a>
			<?php do_action( 'cc_member_action_buttons', 'candidate', $candidate, 'loop' );?>
		</h4>
	</div>
	<div class='panel-body row clear'>
		<div class="col span_6">
			<table class='table table-condensed table-responsive'>
				<tbody>
				<?php if( can_view_candidate_info( $candidate->ID, 'email') ):?>
					<tr><td><span class='glyphicon glyphicon-envelope'> </span>&nbsp;<?php echo $candidate->data->user_email;?><br/></td></tr>
				<?php endif;?>
				
				<?php if( can_view_candidate_info( $candidate->ID, 'meta-nationality') ):?>
					<tr><td><span class='glyphicon glyphicon-map-marker'> </span>Nationality: <?php echo $candidate_meta['nationality'][0];?></td></tr>
				<?php endif;?>
				</tbody>
			</table>
		</div>
		<div class="col span_6">
			<table class='table table-condensed table-responsive'>
				<tbody>
					<?php /*
					<?php if( can_view_candidate_info( $candidate->ID, 'meta-expected_salary') ):?>
						<tr>
						<td>Expected Salary:</td>
						<td><?php echo $candidate_meta['salary'][0] . ' ' . $candidate_meta['expected_salary'][0];?></td>
						</tr>
					<?php endif;?>

					<?php if( can_view_candidate_info( $candidate->ID, 'meta-position') ):?>
						<tr>
						<td>Desired Position:</td>
						<td>
							<?php if( $candidate_meta['position'][0] ): foreach( unserialize( $candidate_meta['position'][0] ) as $dd ):?>
								<?php echo $dd;?><br/>
							<?php endforeach; endif;?>
						</td>
						</tr>
					<?php endif;?>
					 */?>
				
				<?php if( can_view_candidate_info( $candidate->ID, 'meta-skill') ):?>
					<tr>
						<td>
							<span class='glyphicon glyphicon-list'></span> Skills/Expertise: 
							<?php 
							if( $candidate_meta['skill'][0] ){
								$meta_value = maybe_unserialize($candidate_meta['skill'][0]);
								if(is_array($meta_value) ){
									$meta_value = implode( ', ', $meta_value );
								}
								echo apply_filters( 'ccuserinfo_skill', $meta_value );
							}
							else{
								echo "<span class='label label-default'>Not provided</span>";
							}
							?>
						</td>
					</tr>
				<?php endif;?>
					
				<?php if( can_view_candidate_info( $candidate->ID, 'meta-education') ):?>
					<tr>
						<td>
							<span class='glyphicon glyphicon-list-alt'></span> Education: <?php echo $candidate_meta['education'][0];?>
						</td>
					</tr>
				<?php endif;?>
				</tbody>
			</table>
		</div>
	</div>
</div><!-- .candidate -->