<?php 
/* 
 * the header in employers's dashboard
 */

$employer = new WP_User( get_current_user_id() );
?>
<div class='dashboard-header container dashboard-header-employer'>
	<div class="row clear text-info member-header">
		<div class="row clear">
			<div class='col span_2'>
				<?php add_filter( 'get_avatar', 'add_responsive_class_to_avatar' );?>
				<a class="pull-left member-dp" href="<?php echo home_url('/employers/').$employer->ID;?>" title="view profile">
					<?php echo get_avatar( $employer->ID, 150 );?>
				</a>
				<?php remove_filter( 'get_avatar', 'add_responsive_class_to_avatar' );?>
			</div>
			<div class="col span_10">
				<h1 class="media-heading">
					<?php echo $employer->data->display_name;?>
				</h1>

				<div class='employer_packagae_details_container'>
					<?php 
					if( function_exists('member_package_details') ){
						$package_details = member_package_details( get_current_user_id() );
						?>
					<table class='table table-bordered table-condensed table-small'>
						<thead>
							<tr class='warning'><th colspan='100%'>Package Details</th></tr>
						</thead>
						<tbody>
							<tr class='active'>
								<td>Membership</td>
								<td>
								    <span class='label label-primary show-tooltip' title='<?php echo $package_details['package_label'];?>' ><?php echo substr( $package_details['package_label'] , 0, 50)."...";?></span>
									<?php if( $package_details["package"] != "free" ):?>
									<small><i> ( valid till - <?php echo Date("F j, Y, g:i a", strtotime( $package_details["expiry_date"] ) ); ?> )</i></small>
									<?php endif;?>
									<?php 
									$membership_page_id = get_option("emi_jl_package_info",0);
									$membership_page_url = get_permalink( $membership_page_id['membership_page'] );
									?>
									<a class='btn btn-xs btn-info show-tooltip' href='<?php echo $membership_page_url;?>' title='Upgrade/Extend membership'>Upgrade</a>
								</td>
							</tr>
							<tr class='active'>
								<td rowspan="2">Job listings remaining</td>
								<td>
									Free listings: <span class='label label-info'><?php echo $package_details['free_job_listing_remaining'];?></span>
									<small><i> ( <?php echo emi_get_option('emi_free_listing_per_month', 3);?> free listings are given every month. Free listings, if not used, will NOT be carried over to next month. )</i></small>
								</td>
							</tr>
							<tr class='active'>
								<td>
									Paid listings: 
									<?php if( $package_details["package"] != "free" ):?>
										<span class='label label-info'><?php echo $package_details['featured_job_listing_remaining'];?></span>
										
										<small><i> ( available till - <?php echo Date("F j, Y, g:i a", strtotime( $package_details["expiry_date"] ) ); ?> )</i></small>
										<?php if( $package_details['featured_job_listing_remaining']==0 ):?>
											<br/><br/><a class='btn btn-info show-tooltip' href='<?php echo $membership_page_url;?>' title='Purchase additional listings'> Purchase additional listings</a>
										<?php endif;?>
									<?php else:?>
										<span class='label label-danger'>N/A</span>  &nbsp;&nbsp;
										<a class='btn btn-xs btn-info show-tooltip' href='<?php echo $membership_page_url;?>' title='Upgrade your membership to avail of this facility.'>Upgrade</a>
									<?php endif;?>
								</td>
							</tr>
						</tbody>
					</table>
						<?php 
					}
					?>
				</div>
			</div>
		</div><!-- .media -->
		<div class='pull-right member-actions'>
			<span class="btn-group pull-right">
				<?php 
				echo "<a class='btn btn-info btn-small show-tooltip' href='". home_url( '/employers/' ). $employer->ID ."' title='view profile'><span class='glyphicon glyphicon-zoom-in'></span> view profile</a>";
				echo "<a class='btn btn-default btn-small show-tooltip' href='". home_url( '/your-profile/' ) ."' title='edit profile'><span class='glyphicon glyphicon-cog'></span> edit profile</a>";
				echo "<a class='btn btn-default btn-small show-tooltip' href='". home_url( '/your-profile/#pass1' ) ."' title='change password'><span class='glyphicon glyphicon-cog'></span> change password</a>";
				?>
			</span>
		</div>
	</div>
</div><!-- .dashboard-header -->