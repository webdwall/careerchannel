<?php
/**
 * Single Post
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Jobify
 * @since Jobify 1.0
 */

get_header(); ?>

<?php
global $slug_lists;
$slug_lists = array();

$terms = array(
	'Accounting/Auditing' => array(
		'Accounting',
		'Audit & Taxation',
		'Financial Controller'
	),
	'Architect/Interior Design' => array(
		'Architecture',
		'Interior Design',
		'Landscape'
	),
	'Banking/Finance' => array(
		'Consumer Banking',
		'Corporate Banking',
		'Corporate Finance',
		'Credit & Loan',
		'Dealer/Trader',
		'Investments/Fund Management'
	),
	'Building Construction' => array(
		'Building/Construction',
		'Quantity Surveyor'
	),
	'Call Centre' => array(
		'Call Centre'
	),
	'Civil Service/Government' => array(
		'Government Sector/Civil Service'
	),
	'Consulting' => array(
		'Consulting'
	),
	'Corp Comms/PR' => array(
		'Corporate Communications',
		'Public Relations'
	),
	'Creative Design/Arts' => array(
		'Fashion Design' ,
		'Graphic/Print',
		'Multimedia/Web'
	),

	'Customer Service' => array(
		'Customer Service',
		'Receptionist',
		'Sales Support/Coordination'
	),

	'Car Rental' => array(
		'Car Rental'
	),

	'Rental Sale' => array(
		'Rantal Sale'
	),

	'Education/Training' => array(
		'Early Childhood',
		'Teaching/Tutoring',
		'Training'
	),

	'Engineering' => array(
		'Aerospace',
		'Automotive',
		'Chemical',
		'Electrical/Electronics',
		'Environmental/Clean Energy',
		'Industrial & Production',
		'Maintenance',
		'Mechanica',
		'Mechatronics',
		'Oil & Gas',
		'Principal/Management',
		'Structural/Civil'
	),

	'General Worker' => array(
		'Driver',
		'General Worker'
	),

	'Healthcare/Pharmaceutical' => array(
		'Clinical/Medical',
		'Nursing/Support',
		'Pharmaceutical'
	),
	
	'Hotels/Integrated Resorts' => array(
		'Hotels/Integrated Resorts'
	),

	'Human Resources' => array(
		'Compensation & Benefits',
		'Generalis',
		'Training/Development'
	),

	'Computer//IT' => array(
		'IT-Administration',
		'IT-Hardware',
		'IT-Management',
		'IT-Software/Development'
	),

	'Insurance' => array(
		'Gen Insurance/Underwriting/Claims',
		'Insurance Agents/Brokers'
	),

	'Law/Legal' => array(
		'Law/Legal'
	),

	'Logistics/Supply Chain' => array(
		'Freight Services',
		'Supply Chain',
		'Transportation/Shipping',
		'Warehouse/Store'
	),

	'Management Trainee' => array(
		'Management Trainee'
	),

	'Manufacturing' => array(
		'Assembly/Process/Production',
		'Product Development/Management'
	),

	'Maritime' => array( 
		'Maritime'
	),

	'Marketing/Branding' => array(
		'Advertising/Media Buying',
		'Brand/Product Management',
		'Digital/Online Marketing',
		'Events Management',
		'General Marketing'
	),

	'Media/Entertainment' => array(
		'Editorial/Journalism',
		'Media Production',
		'Music/Theatre/Dance',
		'Publishing'
	),

	'Office Admin' => array(
		'Clerical/Admin',
		'Data Entry',
		'Office Operations',
		'Secretarial'
	),

	'Health & Fitness/Beauty' => array(
		'Trainer',
		'Consultant',
		'Instructor',
		'Beauty Therapist'
	),

	'Procurement/Purchasing' => array(
		'Procurement/Purchasing'
	),

	'Professional Services' => array(
		'Corporate services',
		'Consumer services'
	),

	'Quality Control/Assurance' => array(
	'Quality Control/Assurance'
	),

	'Real Estate/Propety Management' => array(
		'Property/Facility Management',
		'Sales/Leasing',
		'Valuation'
	),

	'Research & Development' => array(
		'Research & Development'
	),

	'Restaurant/F&B' => array(
		'Restaurant/F&B'
	),

	'Retail' => array(
		'Retail',
		'Supermarket'
	),

	'Sales' => array(
		'Account Management',
		'Business Development',
		'Channel/Distribution',
		'Consumer Sales',
		'Corporate Sales',
		'Merchandising',
		'Online/e-Commerce',
		'Telesales/Telemarketing'
	),

	'Sciences' => array(
		'Agriculture/Horticulture',
		'Environmental/Geosciences',
		'Medical/Life Science',
		'Sciences'
	),

	'Security' => array(
		'Security'
	),

	'Senior Management' => array(
		'CEO/MD/GM'
	),

	'Survey' => array(
		'Survey'
	),

	'Technician' => array(
		'Troubleshoot',
		'Technician'
	),

	'Trading/Wholesale' => array(
		'Trading/Wholesale'
	),

	'Travel/Tourism' => array(
		'Travel/Tourism'
	),
);

global $wpdb;

$last_term_id = 0;
$taxonomy_name = "job_listing_category";
$last_term_id = $wpdb->get_var("SELECT max( term_id ) FROM ".$wpdb->prefix."terms");

echo "<br/>Last term id is : $last_term_id<br/>";
foreach ($terms as $term_parent => $term ) {

	/* this mnay keywords will be replaced if found in the terms string */
	$slug = get_term_slug( $term_parent );
	$wpdb->query(" INSERT INTO ".$wpdb->prefix."terms( name, slug, term_group ) VALUES ( '$term_parent', '$slug', 0 ); ");
}

$term_taxonomy_data = $wpdb->get_results(" SELECT term_id FROM ".$wpdb->prefix."terms WHERE term_id > $last_term_id ");
echo '<pre>';
var_dump( $term_taxonomy_data );
echo '</pre>';
foreach ($term_taxonomy_data as $term_id ) {

	$wpdb->query(" INSERT INTO ".$wpdb->prefix."term_taxonomy ( term_id, taxonomy, description, parent, count) VALUES ( ".$term_id->term_id.", '$taxonomy_name', '', 0 , 0); ");

}

foreach ($terms as $term_parent => $term_childs ) {
	
	foreach ($term_taxonomy_data as $term_id ) {
		
		$term_name = $wpdb->get_var("SELECT name FROM ".$wpdb->prefix."terms WHERE term_id = ".$term_id->term_id );

		if( $term_parent == $term_name ){

			foreach ($term_childs as $value) {
					
				$slug = get_term_slug( $value );
				$wpdb->query(" INSERT INTO ".$wpdb->prefix."terms( name, slug, term_group ) VALUES ( '$value', '$slug', 0 ); ");

				$temp_last_term_id = $wpdb->get_var("SELECT max( term_id ) FROM ".$wpdb->prefix."terms");

				$wpdb->query(" INSERT INTO ".$wpdb->prefix."term_taxonomy ( term_id, taxonomy, description, parent, count) VALUES ( ".$temp_last_term_id.", '$taxonomy_name', '', $term_id->term_id , 0); ");

			}
		}
	}
}




function get_term_slug( $term_name ){
	
	global $slug_lists;

	$replace_keys = array( " ", ",","/","+","?","&" );
	$slug = strtolower( str_replace( $replace_keys , "-", $term_name ) );
	if( in_array( $slug , $slug_lists ) ){
		$slug = get_term_slug( $term_name." ".rand(1,10) );
	}

	$slug_lists[] = $slug;

	return $slug;

}
?>
<?php get_footer(); ?>