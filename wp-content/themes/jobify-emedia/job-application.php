<?php if ( $apply = get_the_job_application_method() ) :
	?>
	<div class="application">
	    
		<?php 
		$user_id = get_current_user_id();
		$user_details = get_userdata( $user_id );
		$user_role = $user_details->roles[0];
		
		if( $user_role != "employer" ) : ?>
		<input class="application_button" type="button" value="<?php esc_attr_e( 'Apply', 'jobify' ); ?>" />
		<?php endif; ?>
		
		<div class="application_details animated">
			<h2 class="modal-title"><?php _e( 'Apply', 'jobify' ); ?></h2>
			
			<div class="application-content">
			    <?php
			    /* if user is not logged in will return false directly */
			    if( !is_user_logged_in() ):
				    echo '<p>You are not allowed to Apply for current job.. please <a href="'.  get_site_url().'/login" >Login</a> </p>';
			    else:
			    ?>
				
				<?php cc_job_apply_alternate_method();?>
				<?php
					switch ( $apply->type ) {
						case 'email' :

							if ( class_exists( 'Astoundify_Job_Manager_Apply' ) ) :
								echo do_shortcode( '[gravityform id="' . get_option( 'job_manager_gravity_form' ) . '" title="false" ajax="true"]' );
							else :
							    $post_detials = get_post(get_the_ID());
							    if(check_employer_email_credentails( $post_detials->post_author , get_the_ID() ) ){
								echo '<p>' . sprintf( __( 'Additionaly, you can <strong>email your details to</strong> <a class="job_application_email" href="mailto:%1$s%2$s">%1$s</a>', 'jobify' ), $apply->email, '?subject=' . rawurlencode( $apply->subject ) ) . '</p>';
							    }
							endif;

						break;
						case 'url' :
							echo '<p>' . sprintf( __( 'Additionaly, you can visit the following URL: <a href="%1$s">%1$s &rarr;</a>', 'jobify' ), $apply->url ) . '</p>';
						break;
					}
				?>
			    <?php endif; ?>
			</div>
		</div>
	</div>
<?php endif; ?>