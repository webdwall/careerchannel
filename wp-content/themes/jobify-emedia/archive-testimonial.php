<?php
/**
 * Testimonials
 *
 * @package Jobify
 * @since Jobify 1.0
 */

get_header(); ?>

	<header class="page-header">
		<h1 class="page-title"><?php echo apply_filters( 'jobify_testimonial_page_title', post_type_archive_title( '', false ) ); ?></h1>
	</header>

	<div id="primary" class="content-area">
	    <?php if(is_user_logged_in() ): ?>
		<?php
		if( isset( $_POST["test_submit_btn"] ) ){
		    if( isset( $_POST["test_name"] ) && $_POST["test_name"] != "" && isset( $_POST["test_email"] ) && $_POST["test_email"] != "" && isset( $_POST["test_desc"] ) && $_POST["test_desc"] != "" && isset( $_POST["test_company_name"] ) && $_POST["test_company_name"] != "" &&  isset( $_POST["test_contact_num"] ) && $_POST["test_contact_num"] != "" ){
			
			if(filter_var(trim( $_POST["test_email"] ) , FILTER_VALIDATE_EMAIL) ){
			    
			    $message = "<h3>Name : ".trim( $_POST["test_name"] )."</h3>";
			    $message .= "<h3>Email : ".trim( $_POST["test_email"] )."</h3>";
			    $message .= "<h3>Company Name : ".trim( $_POST["test_email"] )."</h3>";
			    $message .= "<h3>Contact Number : ".trim( $_POST["test_email"] )."</h3>";
			    $message .= "<p>Review : ".trim( $_POST["test_desc"] )."</p>";
			    
			    mail("info@careerchannel.sg", "Reg - CareerChannel Testimonials", $message);
			    
			    echo '<p class="text-info" >Thank you for reviewing us!</p>';
			}else{
			    echo '<p class="text-error" >Email id is Invalid!</p>';
			}
		    }else{
			echo '<p class="text-error" >Empty field are not allowed!</p>';
		    }
		}
		
		?>
		<div class="testimonails_form_container" style="width: 50%;margin: 20px;" >
		    <form action="" method="post" >
			<label>Name : </label>
			<input type="text" name="test_name" />
			<label>Email : </label>
			<input type="text" name="test_email" />
			<label>Company Name : </label>
			<input type="text" name="test_company_name" />
			<label>Contact Number : </label>
			<input type="text" name="test_contact_num" />
			<label>Testimonial : </label>
			<textarea name="test_desc" ></textarea><br/><br/>
			<input type="submit" name="test_submit_btn" value="Send Testimonial" />
		    </form>
		</div>
	    <?php endif; ?>
	    
		<div id="content" class="site-content full" role="main">
			<?php 
				the_widget( 
					'Jobify_Widget_Testimonials', 
					array(
						'title'       => null,
						'description' => null,
						'number'      => 8,
						'background'  => null,
						'animations'  => 0
					),
					array(
						'widget_id'     => 'widget-area-front-page',
						'before_widget' => '<section class="homepage-widget jobify_widget_testimonials">',
						'after_widget'  => '</section>',
						'before_title'  => '<h3 class="homepage-widget-title">',
						'after_title'   => '</h3>',
					) 
				); 
			?>
		</div><!-- #content -->

	</div><!-- #primary -->

<?php get_footer(); ?>