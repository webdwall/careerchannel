<?php 
wp_enqueue_script( 'wp-job-manager-ajax-filters' );
wp_enqueue_script('jquery-ui-datepicker');
wp_enqueue_style(
    'jquery-ui-smoothness', 
    get_stylesheet_directory_uri() . "/_inc/jquery-ui-smoothness.css", array(), '1.8.2'
);
?>
<script type="text/javascript">
jQuery(document).ready(function($){
    $("#dt_start_date, #dt_end_date").datepicker({
        showOtherMonths     : true,
        selectOtherMonths   : true,
		dateFormat			: 'dd/mm/yy',
		maxDate             : "+0D"
    });
    $("#lnk_toggle_extended_search").click(function(){
		if($(this).hasClass('minimized')){
			$(this).html('- less options');
		}
		else{
			$(this).html('+ more options');
		}
		$(this).toggleClass('minimized');

		var target = $(this).attr('href');
		$(target).slideToggle('fast').toggleClass('minimized');
		return false;
    });
	$("#sl_jobs_per_page").change(function(){
		$(this).closest('.job_listings').attr('data-per_page', $(this).val());
	});
});
</script>
<form class="job_filters">

	<div class="search_jobs">
		<?php do_action( 'job_manager_job_filters_search_jobs_start', $atts ); ?>

		<div class="search_keywords">
			<label for="search_keywords"><?php _e( 'Keywords', 'job_manager' ); ?></label>
			<input type="text" name="search_keywords" id="search_keywords" placeholder="<?php _e( 'All Jobs', 'job_manager' ); ?>" />
		</div>

		<div class="search_location">
			<?php 
			/*
			<label for="search_location"><?php _e( 'Location', 'job_manager' ); ?></label>
			<input type="text" name="search_location" id="search_location" placeholder="<?php _e( 'Any Location', 'job_manager' ); ?>" />
			*/
			/*
			 * Location should be a drodown field instead, with values entered in backend as 'job regions' taxonomy
			 */
			?>
			<label for="search_region"><?php _e( 'Location', 'job_manager' ); ?></label>
			<select name='search_region' id='search_region'>
				<option value=''>Location..</option>
				<?php 
				$job_regions = get_terms( 'job_listing_region');
				if( $job_regions && !empty( $job_regions ) ){
					foreach ($job_regions as $job_region) {
						echo "<option value='{$job_region->term_id}'>{$job_region->name}</option>";
					}
				}
				?>
			</select>
		</div>

		<?php if ( $categories ) : ?>
			<?php foreach ( $categories as $category ) : ?>
				<input type="hidden" name="search_categories[]" value="<?php echo sanitize_title( $category ); ?>" />
			<?php endforeach; ?>
		<?php elseif ( $show_categories && get_option( 'job_manager_enable_categories' ) && ! is_tax( 'job_listing_category' ) ) : ?>
			<div class="search_categories">
				<label for="search_categories"><?php _e( 'Category', 'job_manager' ); ?></label>
				<?php show_custom_specialization_select_tag(); ?>
			</div>
		<?php endif; ?>

		<?php do_action( 'job_manager_job_filters_search_jobs_end', $atts ); ?>
	</div>
	
	<?php $toggle_class = ( isset( $_GET['extended_search'] ) ? 'maximized' : 'minimized' );?>
    
	<?php if ( ! is_tax( 'job_listing_type' ) && empty( $job_types ) ) : ?>
		<ul class="job_types">
			<?php foreach ( get_job_listing_types() as $type ) : ?>
				<li><label for="job_type_<?php echo $type->slug; ?>" class="<?php echo sanitize_title( $type->name ); ?>"><input type="checkbox" name="filter_job_type[]" value="<?php echo $type->slug; ?>" <?php checked( 1, 1 ); ?> id="job_type_<?php echo $type->slug; ?>" /> <?php echo $type->name; ?></label></li>
			<?php endforeach; ?>
			<li class="more-options"><a href='#search_extended' id='lnk_toggle_extended_search' class='<?php echo $toggle_class;?>' >+ more options</a></li>
		</ul>
		<div id="search_extended" class="search_jobs" style="display: none;" >
		    <div class="search_keywords">
				<label for="txt_job_company_name">Search By Company Name</label>
				<input type="text" name="txt_job_company_name" id="txt_job_company_name" placeholder="company name" />
		    </div>
		    <div style='width:20%'>
				<label for="dt_start_date">Start Date:</label>
				<input type="text" name="dt_start_date" id="dt_start_date" placeholder="start date" />
		    </div>
		    <div style='width:20%'>
				<label for="dt_end_date">End Date:</label>
				<input type="text" name="dt_end_date" id="dt_end_date" placeholder="end date" />
		    </div>
			<div style='width:20%'>
				<label for="sl_jobs_per_page">Jobs Per Page:</label>
				<select name='sl_jobs_per_page' id='sl_jobs_per_page'>
					<option value='<?php echo esc_attr( get_option( 'job_manager_per_page', 10 ) );?>' selected ><?php echo get_option( 'job_manager_per_page', 10 );?> at a time</option>
					<option value='2'>2 at a time</option>
					<option value='25'>25 at a time</option>
					<option value='50'>50 at a time</option>
				</select>
			</div>
		</div>
	<?php elseif ( $job_types ) : ?>
		<?php foreach ( $job_types as $job_type ) : ?>
			<input type="hidden" name="filter_job_type[]" value="<?php echo sanitize_title( $job_type ); ?>" />
		<?php endforeach; ?>
	<?php endif; ?>

	<div class="showing_jobs"></div>
</form>