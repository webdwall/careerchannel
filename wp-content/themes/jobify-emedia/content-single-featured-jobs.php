<?php
/**
 * Featured Job
 *
 * @package Jobify
 * @since Jobify 1.0
 */
?>


				<li class="post-2092 job_listing type-job_listing status-publish hentry job_listing job-type-<?php echo the_job_type(); ?>">
				   <a href="<?php the_permalink(); ?>" rel="bookmark">
		<img src="<?php echo get_the_company_logo(); ?>" alt="Logo"/>
				  <div class="position">
			         <h3><?php the_title(); ?></h3>
			           <div class="company">
				<strong><?php the_company_name(); ?></strong> 
				 <span class="tagline"><?php the_company_tagline(); ?></span>
				 			</div>
		</div>
		     <div class="location">
			<?php the_job_location(false); ?>	</div>
			<ul class="meta">
			<li class="job-type <?php echo get_the_job_type() ? sanitize_title( get_the_job_type()->slug ) : ''; ?>"><?php the_job_type(); ?></li>
			<li class="date"><date>
			<?php 
		       $date1 = get_the_date();
		       $time1 = get_the_time();
			$date2 = date('Y-m-d');
			$time2 = date('h:i:s');

			 $convert_date = date("F j, Y",strtotime( $date2 )); 
			$convert_time = date('g:i a',strtotime( $time2 ));  

			$newdate1 = $date1.$time1;
			$newdate2 = $convert_date.$convert_time;
		
			echo  $show_time =  dateDiff($newdate1,$newdate2)." ago";
			?></date></li>
		</ul>
		                 </a>
				 </li>
              
            