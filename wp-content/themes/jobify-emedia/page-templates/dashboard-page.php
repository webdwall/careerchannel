<?php
/**
 * Template Name: Dashboard Page
 *
 */

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>

	<header class="page-header">
		<?php 
		if(function_exists('cc_user_dashboard_header') ){
			cc_user_dashboard_header();
		}
		?>
	</header>

	<div id="primary" class="content-area">
		<div id="content" class="site-content full" role="main">
			<?php get_template_part( 'content', 'page' ); ?>
		</div><!-- #content -->

		<?php do_action( 'jobify_loop_after' ); ?>
	</div><!-- #primary -->

	<?php endwhile; ?>

<?php get_footer(); ?>