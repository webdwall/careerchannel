jQuery(document).ready(function($){
    //candidate-loop-template - add remove candidate to/from bookmarks
    $(".ccbmc_collection input[type='checkbox']").change(function(e){
	e.preventDefault();
	$elem = $(this);
	var collection_name = $elem.val();
	var candidate_id = $elem.attr('data-candidate_id');
	if( typeof( collection_name )!=='undefined' && typeof( candidate_id )!=='undefined' ){
	    var data = {
		action: CCBMC_.action.remove_bookmark,
		_wpnonce: CCBMC_.nonce.remove_bookmark,
		candidate_id: candidate_id,
		collection_name: collection_name
	    };
	    if( $elem.is(':checked') ){
		//add to collection
		data.action = CCBMC_.action.add_bookmark;
		data._wpnonce = CCBMC_.nonce.add_bookmark;
	    }
	    
	    $.ajax({
		type: "POST",
		url: CCBMC_.ajaxurl,
		data: data,
		success: function (response) {
		    ccbmc_after_toggle_bookmark($elem, response);
		},
		error: function(){
		    //do nothing
		}
	    });
	}
    });

    //dashboard-bookmarks screen - delete bookmarked candidate
    $(".delete_bookmark_candidate").click(function(e){
	e.preventDefault();
	$elem = $(this);
	var collection_name = $elem.attr('data-collection');
	var candidate_id = $elem.attr('data-candidate_id');
	if( typeof( collection_name )!=='undefined' && typeof( candidate_id )!=='undefined' ){
	    $elem.parent().css('background', 'rgb(213, 45, 3)');
	    var data = {
		action: CCBMC_.action.remove_bookmark,
		_wpnonce: CCBMC_.nonce.remove_bookmark,
		candidate_id: candidate_id,
		collection_name: collection_name
	    };
	    
	    $.ajax({
		type: "POST",
		url: CCBMC_.ajaxurl,
		data: data,
		success: function (response) {
		    $elem.parent().remove();
		},
		error: function(){
		    //do nothing
		}
	    });
	}
    });
    
    //dashboard-bookmarks screen - delete bookmark collection
    $(".delete_bookmark_collection").click(function(e){
	e.preventDefault();
	$elem = $(this);
	var collection_name = $elem.attr('data-collection');
	if( typeof( collection_name )!=='undefined' ){
	    $elem.closest('tr').addClass('danger');
	    var data = {
		action: CCBMC_.action.delete_collection,
		_wpnonce: CCBMC_.nonce.delete_collection,
		collection_name: collection_name
	    };
	    
	    $.ajax({
		type: "POST",
		url: CCBMC_.ajaxurl,
		data: data,
		success: function (response) {
		    $elem.closest('tr').remove();
		},
		error: function(){
		    //do nothing
		}
	    });
	}
    });
});

function ccbmc_after_toggle_bookmark($elem, response){
    var $parent_list = $elem.closest('.ccbmc_collection');
    checked_count = $parent_list.find("input[type='checkbox']:checked").length;
    if( checked_count>0 ){
	$parent_list.prev('.ccbmc_link').find('span.glyphicon').removeClass('glyphicon-bookmark').addClass('glyphicon-saved');
    }
    else{
	$parent_list.prev('.ccbmc_link').find('span.glyphicon').addClass('glyphicon-bookmark').removeClass('glyphicon-saved');
    }
}