<?php 
/**
 * Prints the html for 'add to bookmark' link.
 * The html is suitable to be used inside a button group and might not render as good when used otherwise.
 * 
 * @param int $candidate_id id of the candidate
 * @return void
 */
function cc_candidate_bookmark_link( $candidate_id ){
	echo cc_get_candidate_bookmark_link( $candidate_id );
}

function cc_get_candidate_bookmark_link( $candidate_id ){
	$obj = CCBookmarkCandidates::get_instance();
	return $obj->get_bookmark_link( $candidate_id );
}

/**
 * Print the html content for 'manage bookmarks' page.
 * Prints a form to add new collection and prints the existing collections and their respective bookmarks 
 * with option to remove them.
 * 
 * @return void
 */
function employer_candidates_bookmarks(){
	$obj = CCBookmarkCandidates::get_instance();
	$obj->print_manage_bookmarks_content();
}