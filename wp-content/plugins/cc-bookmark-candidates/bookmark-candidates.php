<?php 
/**
* this function is to create collection setting page
*/
function employer_candidates_bookmarks(){
    $user_id = get_current_user_id();
    $collection = get_user_meta($user_id,'cc_members_collection',true);
    $collection_name_array = array();
	if( isset( $collection ) && !empty( $collection ) ){
		foreach ($collection as $collection_name => $member_ids) {
			$collection_name_array[]=$collection_name;
		}
	}
	
    if(isset($_POST['add_new_collection'])){    
        //if collection name is not there
        if(isset($_POST['collection_name']) && $_POST['collection_name'] !==""){
            //check is this collection is already in database.
            $collection = get_user_meta($user_id,'cc_members_collection',true);
            
            if($_POST['collection_name']  !=="" && in_array($_POST['collection_name'] ,$collection_name_array)){
                echo "<p class='text-error'>Error - Collection already exists!</p>";
            }
            else{
                $collection[$_POST['collection_name']] = array();
                update_user_meta($user_id , 'cc_members_collection' ,$collection);
            }
        }
        else{
            echo "<p class='text-error'>Error - Enter the new collection name first!</p>";
        }
    }
    ?>
	
	<div class="well">
		<form  class="add_collection" method="post" >
			<div class="input-group">
				<input type="text" class="form-control collection_name" name="collection_name" placeholder="add new collection..">
				<span class="input-group-btn">
					<input class="btn btn-default button button-small" type="submit" id="add_collection" name="add_new_collection" value="Add"/>
				</span>
			</div><!-- /input-group -->
		</form>
	</div>
    
    <table class='table table-striped'>
		<thead><tr><th>Folders</th><th>Candidates</th></tr></thead>
		<tbody>
        <?php $current_user = wp_get_current_user(); ?>
        <?php 
		$collection= get_user_meta($user_id,'cc_members_collection',true);
		if(empty ($collection)){
			 $collection['other'] = array();
			 update_user_meta($user_id , 'cc_members_collection' ,$collection);
		 }
		 
		foreach ($collection as $collection_name => $member_ids) :
			?>
			<tr data-value='<?php echo esc_attr( $collection_name );?>'>
				<td>
					<span class="label label-default">
						<button type="button" class="close pull-left delete_bookmark delete_bookmark_collection" aria-hidden="true" data-collection="<?php echo esc_attr( $collection_name );?>" title="Delete">&times;</button>
						<strong><?php echo $collection_name;?></strong>
					</span>
				</td>
				<td>
					<?php if(is_array($collection[$collection_name])):?>
						<ul class='inline bookmarked-candidates'>
						<?php foreach ($collection[$collection_name] as $key => $member_id):?>
							<li>
								<button type="button" aria-hidden="true" 
										class="close pull-left delete_bookmark delete_bookmark_collection" 
										data-collection='<?php echo esc_attr( $collection_name );?>' 
										data-member_id ='<?php echo esc_attr( $member_id );?>' title='Remove'>&times;
								</button>
								<a href='<?php echo home_url( '/candidates/'.$member_id );?>' title='View Profile'>Member <?php echo $member_id;?></a>
							</li>
						<?php endforeach;?>
						</ul>
					<?php endif;?>
				</td>
			</tr>
		<?php endforeach;?>
		</tbody>
	</table>
	<?php 
}

/**
 * Adds the given candidate to given employers bookmark collection
 * 
 * @param int $candidate_id the candidate to bookmark
 * @param int $employer_id the employer who is adding the candidate to his collection
 * @param int $collection_id the collection which the bookmark should be added to
 *          AJAx type Method 
 * @return bool
 */
function cc_candidate_bookmark_add( $candidate_id, $employer_id, $collection_name ){
            
    if($candidate_id !=="" && $collection_name !=="" && $employer_id !=="" ){
        $collection = get_user_meta($employer_id,'cc_members_collection',true);
        
		foreach ($collection as $collection_name_as_id => $member_ids) {
		   $collection_name_array[]=$collection_name_as_id;
		}

		if(is_array($collection[$collection_name]) && in_array($candidate_id, $collection[$collection_name])){
			//if this user is already in collection .
			$msg = "user is already in collection ".$collection_name;
			return $msg;
		}
		else{
			//if it's a new user to be entered in collection.
			$collection[$collection_name][]= $candidate_id;
			update_user_meta($employer_id ,'cc_members_collection',$collection);
			return true;
		}
    }
}

/**
 * Removes the given candidate frp, given employers bookmark collection
 * 
 * @param int $candidate_id the candidate to bookmark
 * @param int $employer_id the employer who is adding the candidate to his collection
 * @param int $collection_id the collection which the bookmark should be added to
 * 
 * @return bool
 */
function cc_candidate_bookmark_remove( $employer_id, $collection_name, $candidate_id ){
	$collection = get_user_meta($employer_id,'cc_members_collection',true);

    if(!empty($collection)){
		if(($key = array_search($candidate_id, $collection[$collection_name])) !== false) {
			unset($collection[$collection_name][$key]);
		}
        update_user_meta($employer_id ,'cc_members_collection',$collection);
        $msg = "successfully removed";
        return $msg;
    }
}

/*
* this is when employee want to delete his whole collection
*/
function cc_candidate_collection_remove($employer_id, $collection_name ){
    $collection = get_user_meta($employer_id,'cc_members_collection',true);
    if($collection_name !==""){
        unset($collection[$collection_name]);
        update_user_meta($employer_id ,'cc_members_collection',$collection);
        $msg = "successfully deleted";
        return $msg;
    }
}

/**
 * Prints the html for 'add to bookmark' link
 * 
 * @param WP_User $candidate
 */
function cc_candidate_bookmark_link( $candidate ){
	if( !is_object($candidate) ){
		$candidate = new WP_User($candidate);
	}
    $candidate = $candidate->ID;
    
    $collection = get_user_meta(get_current_user_id(),'cc_members_collection',true);

    //$collection = get_user_meta(1,'cc_members_collection',true);
    //check if candidate is added to any of the collections
    $candidate_added = true;
    
    $glypicon = "collection_gly glyphicon glyphicon-heart-empty";
    if( $candidate_added ){
    	//$glypicon = "glyphicon glyphicon-heart";
    }

    $html  = "";
    //$html  = "<div class='btn-group'>";
    $html .=    "<button type='button' class='btn btn-default btn-small dropdown-toggle' data-toggle='dropdown' title='add/remove to/from bookmarks'>";
    $html .=        "<span class='$glypicon'></span>";
    $html .=    "</button>";
    $html .=    "<ul class='dropdown-menu'>";

        if(is_array($collection)){
            $candidate_id = strval($candidate);
            foreach ($collection as $collection_name => $member_ids) {

                if(is_array($member_ids)){
                    
                    if(!empty($member_ids)){
                        if( in_array($candidate_id ,$member_ids) ){

                            $checked = 'checked';
                        }
                       
                    }
                    else{
                        $checked = '';

                    }
                   
                }
                $key = " ";
                if(!empty($collection[$collection_name])){

                    $candidate_key = array_search(strval($candidate),$collection[$collection_name]);
                }
                $html.= $candidate_key;
                $html .= "<li><a><label><input type='checkbox' class ='checked_to_add' data-member =".$candidate." data-candidate ='".$candidate_key."' value='". esc_attr( $collection_name ) ."' $checked/>$collection_name</label></a></li>";     
                
            }
       
        }

	$html .=        "<li class='divider'></li>";
    $html .=        "<li><a href='". esc_attr( cc_user_dashboard_url('employer', 'candidates', 'bookmarks') ) ."' title='Add Collection'>Collection settings</a></li>";
    $html .=    "</ul>";
    //$html .= "</div>";
    echo $html;
}