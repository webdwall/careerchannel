<?php 
/* 
Plugin Name: CareerChannel Bookmark Candidates 
Description: Bookmark candidates functionality for CareerChannel.
Version: 1.1
Author: E Media Identity
Author URI: http://emediaidentity.com/
Network: true
*/
require_once( 'functions.php' );
//include_once('bookmark-candidates.php');
add_action( 'init', 'add_to_collection_init_plugin' );

function add_to_collection_init_plugin(){
	CCBookmarkCandidates::get_instance();
}
class CCBookmarkCandidates{
    private static $instance;
	private $noncekey = 'lkn76g';
	private $bookmarks;
    
    public static function get_instance(){
        if(!isset(self::$instance)){
            self::$instance=new self();
        }
        return self::$instance;
    }

    private function __construct() {
		add_action( 'wp_enqueue_scripts',				array( $this,'add_js' ) );
        add_action( 'wp_ajax_ccbmc_add_bookmark',		array( $this, 'ajax_add_bookmark' ) );
        add_action( 'wp_ajax_ccbmc_remove_bookmark',	array( $this, 'ajax_remove_bookmark' ) );
        add_action( 'wp_ajax_ccmbc_delete_collection',	array( $this, 'ajax_delete_collection' ) );
    }

	function ajax_add_bookmark(){
		$retval = array(
			'status'	=> false,
			'message'	=> ''
		);
		check_admin_referer( $this->noncekey.'add_bookmark' );//_wpnonce should be the name of the $_POST varialble containing the nonce
		
		//if( isset( $_POST['candidate_id'] ) )
		$candidate_id = (int)( isset( $_POST['candidate_id'] ) ? $_POST['candidate_id'] : 0 );
		$employer_id = get_current_user_id();
		$collection_name = ( isset( $_POST['collection_name'] ) ? strip_tags($_POST['collection_name']) : '' );
		//$this->add_bookmark($candidate_id, $employer_id, $collection_name)
		$retval['status'] = $this->add_bookmark($candidate_id, $employer_id, $collection_name);
		die( json_encode( $retval ) );
	}
	
	function ajax_remove_bookmark(){
		$retval = array(
			'status'	=> false,
			'message'	=> ''
		);
		check_admin_referer( $this->noncekey.'remove_bookmark' );//_wpnonce should be the name of the $_POST varialble containing the nonce
		
		//if( isset( $_POST['candidate_id'] ) )
		$candidate_id = (int)( isset( $_POST['candidate_id'] ) ? $_POST['candidate_id'] : 0 );
		$employer_id = get_current_user_id();
		$collection_name = ( isset( $_POST['collection_name'] ) ? strip_tags($_POST['collection_name']) : '' );
		//$this->add_bookmark($candidate_id, $employer_id, $collection_name)
		$retval['status'] = $this->remove_bookmark($employer_id, $collection_name, $candidate_id);
		die( json_encode( $retval ) );
	}
	
	function ajax_delete_collection(){
		$retval = array(
			'status'	=> false,
			'message'	=> ''
		);
		check_admin_referer( $this->noncekey.'delete_collection' );//_wpnonce should be the name of the $_POST varialble containing the nonce
		
		$employer_id = get_current_user_id();
		$collection_name = ( isset( $_POST['collection_name'] ) ? $_POST['collection_name'] : '' );
		//$this->add_bookmark($candidate_id, $employer_id, $collection_name)
		$retval['status'] = $this->delete_collection($employer_id, $collection_name);
		die( json_encode( $retval ) );
	}
	
    function add_js(){
        wp_enqueue_script(
            "cc_bookmark_candidates",
            path_join( WP_PLUGIN_URL, basename( dirname( __FILE__ ) )."/js/script.js" ),
            array( 'jquery' )
        );
		
		$data = array(
			'ajaxurl'				=> admin_url('/admin-ajax.php'),
			'action'				=> array(
				'add_bookmark'		=> 'ccbmc_add_bookmark',
				'remove_bookmark'	=> 'ccbmc_remove_bookmark',
				'delete_collection'	=> 'ccmbc_delete_collection'
			),
			'nonce'					=> array(
				'add_bookmark'		=> wp_create_nonce($this->noncekey.'add_bookmark'),
				'remove_bookmark'	=> wp_create_nonce($this->noncekey.'remove_bookmark'),
				'delete_collection'	=> wp_create_nonce($this->noncekey.'delete_collection'),
			)
		);
		
		wp_localize_script('cc_bookmark_candidates', 'CCBMC_', $data);
    }
	
	/**
	 * Retrieve the bookmarks of the current loggedin user.
	 * Saves the info in class level variable and fetches from it from the second time onwards.
	 * 
	 * @return array
	 */
	private function get_bookmarks(){
		if( !$this->bookmarks ){
			$user_id = get_current_user_id();
			$collection = get_user_meta($user_id,'cc_members_collection',true);
			if( !$collection || empty($collection) ){
				$collection = array( 'default'=>array() );
			}
			$this->bookmarks = $collection;
		}
		return $this->bookmarks;
	}
	
	/**
	 * Updates the user's bookmark into database, and updates the local copy as well.
	 * @param array $new_bookmarks
	 * @return void
	 */
	private function update_bookmarks( $new_bookmarks ){
		$this->bookmarks = $new_bookmarks;
		update_user_meta(get_current_user_id(), 'cc_members_collection', $this->bookmarks);
	}
	
	/**
	 * Print the html content for 'manage bookmarks' page.
	 * Prints a form to add new collection and prints the existing collections and their respective bookmarks 
	 * with option to remove them.
	 * 
	 * @return void
	 */
	function print_manage_bookmarks_content(){
		$collection = $this->get_bookmarks();
		$collection_name_array = array();
		if( isset( $collection ) && !empty( $collection ) ){
			foreach ($collection as $collection_name => $member_ids) {
				$collection_name_array[]=$collection_name;
			}
		}

		if(isset($_POST['add_new_collection'])){    
			//if collection name is not there
			if(isset($_POST['collection_name']) && $_POST['collection_name'] !==""){
				if($_POST['collection_name']  !=="" && in_array($_POST['collection_name'], $collection_name_array)){
					echo "<p class='text-error'>Error - Collection already exists!</p>";
				}
				else{
					$collection[$_POST['collection_name']] = array();
					$this->update_bookmarks($collection);
				}
			}
			else{
				echo "<p class='text-error'>Error - Enter the new collection name first!</p>";
			}
		}
		?>

		<div class="well">
			<form  class="add_collection" method="post" >
				<div class="input-group">
					<input type="text" class="form-control collection_name" name="collection_name" placeholder="add new collection..">
					<span class="input-group-btn">
						<input class="btn btn-default button button-small" type="submit" id="add_collection" name="add_new_collection" value="Add"/>
					</span>
				</div><!-- /input-group -->
			</form>
		</div>

		<table class='table'>
			<thead><tr><th>Folders</th><th>Candidates</th></tr></thead>
			<tbody>
			<?php $current_user = wp_get_current_user(); ?>
			<?php 
			foreach ($collection as $collection_name => $member_ids) :
				?>
				<tr data-value='<?php echo esc_attr( $collection_name );?>'>
					<td>
						<span class="label label-default">
							<button type="button" class="close pull-left delete_bookmark delete_bookmark_collection" aria-hidden="true" data-collection="<?php echo esc_attr( $collection_name );?>" title="Delete">&times;</button>
							<strong><?php echo $collection_name;?></strong>
						</span>
					</td>
					<td>
						<?php if( $member_ids && !empty( $member_ids ) ):?>
							<ul class='inline bookmarked-candidates'>
							<?php foreach( $member_ids as $member_id):?>
								<li>
									<button type="button" aria-hidden="true" 
											class="close pull-left delete_bookmark delete_bookmark_candidate" 
											data-collection='<?php echo esc_attr( $collection_name );?>' 
											data-candidate_id ='<?php echo esc_attr( $member_id );?>' title='Remove'>&times;
									</button>
									<?php
									$member_name = get_the_author_meta( "display_name", $member_id );
									?>
									<a href='<?php echo home_url( '/candidates/'.$member_id );?>' title='View Profile'><?php echo $member_name;?></a>
								</li>
							<?php endforeach;?>
							</ul>
						<?php endif;?>
					</td>
				</tr>
			<?php endforeach;?>
			</tbody>
		</table>
		<?php 
	}

	/**
	 * Adds the given candidate to given employers bookmark collection
	 * 
	 * @param int $candidate_id the candidate to bookmark
	 * @param int $employer_id the employer who is adding the candidate to his collection
	 * @param string $collection_name the collection which the bookmark should be added to
	 * @return boolean : true on sucess and false on error.
	 */
	private function add_bookmark( $candidate_id, $employer_id, $collection_name ){
		$collection_name = strip_tags($collection_name);
		$candidate_id = (int)$candidate_id;
		$employer_id = (int)$employer_id;
		
		if( $candidate_id !=="" && $collection_name !=="" && $employer_id !=="" ){
			$collection = $this->get_bookmarks();

			foreach ($collection as $collection_name_as_id => $member_ids) {
			   $collection_name_array[]=$collection_name_as_id;
			}

			if(is_array($collection[$collection_name]) && in_array($candidate_id, $collection[$collection_name])){
				//if this user is already in collection .
				return false;
			}
			else{
				//if it's a new user to be entered in collection.
				$collection[$collection_name][]= $candidate_id;
				$this->update_bookmarks($collection);
				return true;
			}
		}
	}
	
	/**
	 * Removes the given candidate from given employers bookmark collection
	 * 
	 * @param int $candidate_id the candidate to remove
	 * @param int $employer_id the employer id
	 * @param string $collection_name the collection which the bookmark should be removed from
	 * 
	 * @return boolean
	 */
	private function remove_bookmark( $employer_id, $collection_name, $candidate_id ){
		$collection = $this->get_bookmarks();

		if(!empty($collection)){
			if(($key = array_search($candidate_id, $collection[$collection_name])) !== false) {
				unset($collection[$collection_name][$key]);
			}
			$this->update_bookmarks($collection);
			return true;
		}
		return false;
	}

	/**
	 * remove an entire collection/bookmarks-folder.
	 * 
	 * @param int $employerid
	 * @param string $collection_name
	 * 
	 * @return boolean
	 */
	private function delete_collection($employer_id, $collection_name ){
		$collection = $this->get_bookmarks();
		if($collection_name !==""){
			unset($collection[$collection_name]);
			$this->update_bookmarks($collection);
			return true;
		}
		return false;
	}

	/**
	 * Returns the html for 'add to bookmark' link.
	 * The html is suitable to be used inside a button group and might not render as good when used otherwise.
	 * 
	 * @param int $candidate_id id of the candidate
	 * @return string html generated for the add-to-bookmark link
	 */
	function get_bookmark_link( $candidate_id ){
		$collection = $this->get_bookmarks();

		//if candidate is added to any of the collections or not
		$candidate_added = false;

		$html  = "";
		$html .= "<ul class='dropdown-menu ccbmc_collection'>";

			if(is_array($collection)){
				foreach ($collection as $collection_name => $member_ids) {
					$checked = '';
					if( !empty($member_ids) && is_array($member_ids) && in_array($candidate_id, $member_ids) ){
						$checked = 'checked';
						$candidate_added = true;//candidate has been added to at least one collection
					}
					
					$html .= "<li><label><input type='checkbox' data-candidate_id='".$candidate_id."'  value='". esc_attr( $collection_name ) ."' $checked/>$collection_name</label></li>";
				}
			}

		$html .=    "<li class='divider'></li>";
		$html .=	"<li><a href='". esc_attr( cc_user_dashboard_url('employer', 'candidates', 'bookmarks') ) ."' title='Add/remove collections and manage your bookmarks.'>Manage Bookmarks</a></li>";
		$html .= "</ul>";
		
		$glyphicon = "glyphicon-bookmark";
		if( $candidate_added ){
			$glyphicon = "glyphicon-saved";
		}
		
		$button  =    "<button type='button' class='ccbmc_link btn btn-default btn-small show-tooltip dropdown-toggle' data-toggle='dropdown' title='add/remove to/from bookmarks'>";
		$button .=        "<span class='glyphicon $glyphicon'></span>";
		$button .=    "</button>";
		
		return $button . $html;
	}
}