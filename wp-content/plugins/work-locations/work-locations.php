<?php
/*
Plugin Name: Manage Work Locations Plugin
Plugin URI: http://eminsoftwaresolutions.com
Description: This plugin is to manage work locations of site . 
Version: 2.1
Author: Gurmeet Badwal .
Author URI: gbadwal@eminsoftwaresolutions.com 
*/
add_action('admin_menu', 'WorkLocations');
// Table creation code start here
register_activation_hook(__FILE__,'table_work_location_install');

function table_work_location_install ()
 	{
   		     category_country();
  			 work_location();
	}
function category_country()
  {
  global $wpdb;
   $table_name = $wpdb->prefix ."country_cat";
   if($wpdb->get_var("show tables like '$table_name'") != $table_name) {
      
      $sql = "CREATE TABLE " . $table_name . " (
	  id int(11) NOT NULL AUTO_INCREMENT,
	  cat_name VARCHAR(255) NOT NULL,
	  added_on TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
	  PRIMARY KEY id (id)
	);";
	
      require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
      dbDelta($sql);
  	}
  }
  function work_location() {
   global $wpdb;

   $table_name = $wpdb->prefix ."work_location";
   if($wpdb->get_var("show tables like '$table_name'") != $table_name) {
      
      $sql = "CREATE TABLE " . $table_name . " (
	  id int(11) NOT NULL AUTO_INCREMENT,
	  location_name VARCHAR(255) NOT NULL,
	  location_cat varchar(255) NOT NULL ,
	  added_on TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
	  status BOOLEAN NOT NULL ,
	  PRIMARY KEY id (id)
	);";
	
      require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
      dbDelta($sql);
	}
	
   }
// Table creation code end here

function WorkLocations() {
add_options_page('Manage Locations', __('Manage Locations'), 0,  __FILE__, 'manage_locations');
}

////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////
function manage_locations()
{

   global $wpdb;
   		$wpu = $_GET['wpu'];
		switch($wpu) {
			case 'add_location':
				add_location();
			break;
			case 'add_location_query':
				add_location_query();
			break;
			case 'status':
				update_status_location();
			break;
			case 'delete_location':
				delete_location();
			break; 
			case 'add_cat_country':
				add_cat_country();
			break;
			case 'delete_country':
				delete_country();
			break; 
			case 'location_admin':
				showlocation_adminside();
			break;
			default:
				showcat_country_adminside();
			break; 
		}
      }
	  ?>
<?php function showlocation_adminside($msg="")
{
global $wpdb;
$message = $msg ;
if($_GET['msg'] != "")
{
	$message = $_GET['msg'];
}
if($_GET['id'] != "")
{
	 $id = $_GET['id'];
}

			
$resultsno = 10;
$table_name_cat = $wpdb->prefix ."country_cat";
$query_cat = "select * from $table_name_cat where id=$id  ";
$sqlquery=mysql_query($query_cat); 
$fecth_query=mysql_fetch_array($sqlquery);
$table_name = $wpdb->prefix ."work_location";
$query = "select * from $table_name where location_cat=$id order by added_on desc ";
$row_count = $wpdb->get_results($query);
// pagination code //
	$page = $_REQUEST['page_num']; 
	if ($_REQUEST["page_num"]) { $page  = $_REQUEST["page_num"]; } else { $page=1; };
	$start_from = ($page-1) * $resultsno; 
	$query.=" LIMIT $start_from, $resultsno";
	
$row = $wpdb->get_results($query);
$res_count = sizeof($row_count);
?>

  <table border="0" align="center" cellpadding="10" cellspacing="10" style="width:600px;margin-left:20px; border:solid 1px #999999; margin-top:50px;">
      
     <tr>
	 <td colspan="3"><b> <?php echo __('Manage work locations of ' .$fecth_query['cat_name'].'');  ?> </b> </td> 
	 <td align="right"><a href="?page=<?php echo dirname(plugin_basename(__FILE__)); ?>/work-locations.php&wpu=add_location&id=<?php echo"$id"?>"><b>Add New</b></a>&nbsp;
	 <a href="?page=<?php echo dirname(plugin_basename(__FILE__)); ?>/work-locations.php"><b>< Go Back</b></a></td>
      </tr>
	 <tr><td colspan="4">&nbsp;</td></tr>  
	 <?php if($message!="") { ?>    
	 <tr>
	 <td colspan="4"><b> <?php echo $message ; ?> </b> </td> 
	
	 </tr>
	 <tr><td colspan="4">&nbsp;</td></tr>  
	 <?php } ?>	  
	 <?php
   
	  if(!empty($row))
      {
	 ?>
	 <tr>
	 	<td> <b>Work Location</b> </td>
		<td> <b>Status</b> </td>
		<td> <b>Option</b> </td>
	 </tr>
	 <?php
	 								
	 foreach($row as $data)
	   {   ?>
	 <tr>
	 	<td><?php echo $data->location_name; ?></td>
	<td ><?php  if($data->status == 1) { echo "Published |"." <a href='?page=".dirname(plugin_basename(__FILE__))."/work-locations.php&wpu=status&set=unpublish&id=$id&pid=".$data->id."'>Unpublish</a>"; } else { echo "Unpublished |"." <a href='?page=".dirname(plugin_basename(__FILE__))."/work-locations.php&wpu=status&set=publish&id=$id&pid=".$data->id."'>Publish</a>"; } ?></td>
		<td><a href="?page=<?php echo dirname(plugin_basename(__FILE__)); ?>/work-locations.php&wpu=delete_location&pid=<?php echo $data->id ;?>&id=<?php echo $id; ?>">Delete</a>
  </td>
	</tr>
	<?php
														
	    }
		?>
	<tr><td height="10"></td></tr>
	<tr>
		<td colspan="4" align="center">
		<?php 	
				
                       $total_rows = ceil($res_count / $resultsno); 
 
                     for ($i=1; $i<=$total_rows; $i++) {
			         if($page == $i)
			           {
				          echo "<b style='color:#999999;'>[".$i."] </b>";
			           }
                         else
			           {
				          ?><a href="?page=<?php echo dirname(plugin_basename(__FILE__)); ?>/MusicUpload.php&page_num=<?php echo $i; ?>" style="text-decoration:none;" >[<?php echo $i; ?>]</a>
		<?php 
				        }
                }?>
		</td>
	</tr>
	<?php
	  }
	  else
	  {
	?>
	  <tr>
	 	<td colspan="4" align="center"> <b>No Records Found.</b> </td>
		
	 </tr>
	<?php
	  }
	?>
	       
</table>
<?php
}
function add_location_query()
{
if($_GET['id'] != "")
{
	 $id = $_GET['id'];
}
global $wpdb;
if(isset($_POST['submit']))
{
		$location_title = $_POST['location_title'];
		
		$table_name = $wpdb->prefix ."work_location";

		$get_old_files = "select * from $table_name where location_name = '".$location_title."'";
			
		$row_count = mysql_num_rows(mysql_query($get_old_files));
			
		if($row_count > 0)
		{
			$msg =  "<p><strong>"." Error: Location already exist. "."</strong></p>";
			Showlocation_adminside($msg) ;
		}
		else
		{
			$insert =  "INSERT INTO ".$table_name." (location_name,location_cat, status) VALUES ('".$location_title."','".$id."', '1')";
					
			if($wpdb->query($insert)){
			$msg =  "<p><strong>"." New Location Added Successfully "."</strong></p>";
			Showlocation_adminside($msg) ;
			}
		}
		
}
}
function add_location()
{
if($_GET['id'] != "")
{
	 $id = $_GET['id'];
}
?>

<script>
function show_loading()
{
 
  if(document.getElementById('location_title').value=='')
  {
   alert("Enter Location");
   return false;
  }
  
}
</script>

  <table border="0" cellpadding="10" cellspacing="10" align="center" style="width:600px;margin-left:20px; border:solid 1px #999999; margin-top:50px;">
     <tr>
	 <td colspan="3"><b> <?php echo __('Add Work Location'); ?> </b> </td> 
	    <td align="right"><a href="?page=<?php echo dirname(plugin_basename(__FILE__)); ?>/work-locations.php"><b>< Go Back</b></a></td>
      </tr> 
     <tr><td colspan="4">&nbsp;</td></tr>  
	 <?php if($message!="") { ?>    
	 <tr>
	 <td colspan="4"><b> <?php echo $message ; ?> </b> </td> 
	
	 </tr>
	 <tr><td colspan="4">&nbsp;</td></tr>  
	 <?php } ?>
	 <form method="post" action="?page=<?php echo dirname(plugin_basename(__FILE__)); ?>/work-locations.php&wpu=add_location_query&id=<?php echo $id; ?>" name="form1" id="form1">
	 <tr>
	 	<td colspan="2" align="right" width="30%">Work Location</td>
		<td colspan="2" align="left"><input type="text" name="location_title" id="location_title" style="width:195px;" /></td>
	 </tr>
	

	 <tr>
	 	<td colspan="4" align="center"><input type="submit" name="submit" value="Submit" onclick="return show_loading();" /></td>
	 </tr>
	 </form>
	
	       
</table>
<?php
}
function delete_location() //function is used to delete records
{
   global $wpdb;
   $pid=$_GET['pid'];
   $id=$_GET['id'];
   
  $table_name = $wpdb->prefix ."work_location";
  
    if($wpdb->query("DELETE FROM $table_name WHERE id = '$pid'"))
    {
    $msg = "<p><strong>".__('Record deleted successfully')."</strong></p>" ;
	showlocation_adminside($msg) ;
    }	
  
  else 
  {
  echo  "<p><strong>".__('There is an error in deleting record. Please check the data.')."</strong></p>" ;
  showlocation_adminside($msg) ;
     
  }
 
}

function update_status_location() //function is used to update record status
{
   global $wpdb;
   $table_name = $wpdb->prefix ."work_location";
   
   $query="select * from $table_name where status = 1";
   $affected_query = mysql_query($query);
   $total_rows = mysql_affected_rows();
   //echo $total_rows ;
  
   $pid=$_GET['pid'];
   if($_GET['set'] == "publish")
   {
   		$status = 1;
   }
   else
   {
   		$status = 0;
   }
  	if($wpdb->query("update $table_name set status = '$status' WHERE id = $pid"))
    {
    $msg = "<p><strong>".__('Record updated successfully')."</strong></p>" ;
	showlocation_adminside($msg) ;
    }	
   	else 
  	{
  
	  echo  "<p><strong>".__('There is an error in Updating record.')."</strong></p>" ;
	  showlocation_adminside($msg) ;
     }
 
}
////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////cat////////////////////////////////////////////////////////////
?>
<?php function showcat_country_adminside($msg="")
{

global $wpdb;

$message = $msg ;
if($_GET['msg'] != "")
{
	$message = $_GET['msg'];
}
//results per page;
$resultsno = 10;
$table_name = $wpdb->prefix ."country_cat";
$query = "select * from $table_name order by added_on desc ";
$row_count = $wpdb->get_results($query);
// pagination code //

	$page = $_REQUEST['page_num']; 
	if ($_REQUEST["page_num"]) { $page  = $_REQUEST["page_num"]; } else { $page=1; };
	$start_from = ($page-1) * $resultsno; 
	$query.=" LIMIT $start_from, $resultsno";
	
$row = $wpdb->get_results($query);
$res_count = sizeof($row_count);
?>

  <table border="0" align="center" cellpadding="10" cellspacing="10" style="width:600px;margin-left:20px; border:solid 1px #999999; margin-top:50px;">
      
      <tr>
	 <td colspan="3"><b> <?php echo __('Manage Country '); ?> </b> </td> 
	 <td align="right"><a href="?page=<?php echo dirname(plugin_basename(__FILE__)); ?>/work-locations.php&wpu=add_cat_country"><b>Add New</b></a></td>
	    
      </tr>
	 <tr><td colspan="4">&nbsp;</td></tr>  
	 <?php if($message!="") { ?>    
	 <tr>
	 <td colspan="4"><b> <?php echo $message ; ?> </b> </td> 
	
	 </tr>
	 <tr><td colspan="4">&nbsp;</td></tr>  
	 <?php } ?>
	 <?php
   
	  if(!empty($row))
      {
	 ?>
	 <tr>
	 	<td> <b>Country</b> </td>
		<td> <b>View Work Locations</b> </td>
		<td> <b>Option</b> </td>
	 </tr>
	 <?php
	 								
	 foreach($row as $data)
	   {   ?>
	 <tr>
	 	<td><?php echo $data->cat_name; ?></td>
		<td><a href="?page=<?php echo dirname(plugin_basename(__FILE__)); ?>/work-locations.php&wpu=location_admin&id=<?php echo $data->id ;?>">View locations of <?php echo $data->cat_name; ?></a></td>
		<td><a href="?page=<?php echo dirname(plugin_basename(__FILE__)); ?>/work-locations.php&wpu=delete_country&id=<?php echo $data->id ;?>">Delete</a>
 		</td>
	</tr>
	<?php
	   }
		?>
	<tr><td height="10"></td></tr>
	<tr>
		<td colspan="4" align="center">
		<?php 	
				
                       $total_rows = ceil($res_count / $resultsno); 
 
                     for ($i=1; $i<=$total_rows; $i++) {
			         if($page == $i)
			           {
				          echo "<b style='color:#999999;'>[".$i."] </b>";
			           }
                         else
			           {
				          ?><a href="?page=<?php echo dirname(plugin_basename(__FILE__)); ?>/work-locations.php&page_num=<?php echo $i; ?>" style="text-decoration:none;" >[<?php echo $i; ?>]</a>
		<?php 
				        }
                }?>
		</td>
	</tr>
	<?php
	  }
	  else
	  {
	?>
	  <tr>
	 	<td colspan="4" align="center"> <b>No Records Found.</b> </td>
		
	 </tr>
	<?php
	  }
	?>
	       
</table>
<?php
}


function add_cat_country()
{
?>
<script>
function show_loading()
{
 
  if(document.getElementById('cat_name').value=='')
  {
   alert("Enter Country");
   return false;
  }
  
}
</script>
<?php
global $wpdb;
if(isset($_POST['submit']))
{
		$cat_name = $_POST['cat_name'];
		$table_name = $wpdb->prefix ."country_cat";
        
		$get_old_files = "select * from $table_name where cat_name = '$cat_name'";
	
		$row_count = mysql_num_rows(mysql_query($get_old_files));
			
		if($row_count > 0)
		{
			$message = "Error: Country already exist.";
		}
		else
		{
			$insert =  "INSERT INTO ".$table_name." (cat_name) VALUES ('".$cat_name."')";
			if($wpdb->query($insert)){
			$msg =  "<p><strong>"." Country Added Successfully "."</strong></p>";
			Showcat_country_adminside($msg) ;			
		}
}
		
					
}

?>
  <table border="0" cellpadding="10" cellspacing="10" align="center" style="width:600px;margin-left:20px; border:solid 1px #999999; margin-top:50px;">
     <tr>
	 <td colspan="3"><b> <?php echo __('Add Country '); ?> </b> </td> 
	 <td align="right"><a href="?page=<?php echo dirname(plugin_basename(__FILE__)); ?>/work-locations.php"><b>< Go Back</b></a></td>
      </tr> 
     <tr><td colspan="4">&nbsp;</td></tr>  
	 <?php if($message!="") { ?>    
	 <tr>
	 <td colspan="4"><b> <?php echo $message ; ?> </b> </td> 
	 </tr>
	 <tr><td colspan="4">&nbsp;</td></tr>  
	 <?php } ?>
	 <form method="post" action="" enctype="multipart/form-data" name="form1" id="form1">
	 <tr>
	 	<td colspan="2" align="right" width="30%">Country Name </td>
		<td colspan="2" align="left"><input type="text" name="cat_name" id="cat_name" style="width:195px;" /></td>
	 </tr>
	  
	 <tr>
	 	<td colspan="4" align="center"><input type="submit" name="submit" value="Submit" onclick="return show_loading();" /></td>
	 </tr>
	 </form>
</table>
<?php
}

function delete_country() //function is used to delete records
{
  global $wpdb;
  $id=$_GET['id'];   
  $table_name = $wpdb->prefix ."country_cat";
  $row = $wpdb->get_results("select * from $table_name where id = '$id'");
		  foreach($row as $data)
		  {
			$file_name = $data->music_file_name; 
		  }
			  
if($wpdb->query("DELETE FROM $table_name WHERE id = '$id'"))
    {
// delete and unset all the music files for category
				  
	$wpdb->query("delete from wp_work_location where location_cat='$id'");			
	$msg = "<p><strong>".__('Record deleted successfully')."</strong></p>" ;
	showcat_country_adminside($msg) ;
    }	
  else 
  {
  echo  "<p><strong>".__('There is an error in deleting record. Please check the data.')."</strong></p>" ;
  showcat_country_adminside($msg) ;
  }
}
?>