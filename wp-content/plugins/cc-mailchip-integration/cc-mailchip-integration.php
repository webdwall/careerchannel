<?php 
/* 
Plugin Name: CareerChannel MailChimp Integration
Description: Syncs the Users with the mailchimp list
Version: 0.1
Author: E Media Identity
Network: true
*/

//the mailchimp api class
include_once( "inc/MCAPI.class.php" );
add_action( 'admin_menu', 'cmmi_adminsettings_menu' );
function cmmi_adminsettings_menu(){
	add_options_page( 'Mailchimp configuration', 'Mailchimp config', 'manage_options', 'cmmi_config', 'cmmi_adminsettings' );
}

function cmmi_adminsettings(){
	?>
	<div class="wrap">
		<div id="icon-options-general" class="icon32"><br></div>
		<h2>Mailchimp Configuration</h2>
		<?php 
		global $wp_roles;
		$roles = $wp_roles->get_names();
		if( $_POST['btn_saveOptions'] ){
			$key = $_POST['txt_apikey'];
			
			$options = array(
				'apikey'		=> $key
			);
			
			foreach( $roles as $roleslug => $role ){
			    $options['list'.$roleslug] = $_POST['sl_groups_'.$roleslug];
			}

			
			update_option( 'CMMI_OPTIONS', $options );

			echo "<div class='updated fade'><p>Settings updated</p></div>";
		}
		$options = get_option( 'CMMI_OPTIONS' );
		if( isset( $options['apikey'] ) ){
			$api = new MCAPI_13( $options['apikey'] );
			$myLists = $api->lists();

			$cmmi_mylists = array();
			if( !empty($myLists) ){
				foreach ($myLists['data'] as $list) {
					$cmmi_mylists[$list['id']] = $list['name'] ;
				}
			}
		}
		?>
		<form method="POST">
			<table class="form-table">
				<tfoot>
					<tr>
						<td colspan="100%">
							<input type="submit" name="btn_saveOptions" id="btn_saveOptions" value="Save" class="button button-primary" />
						</td>
					</tr>
				</tfoot>
				<tbody>
					<tr valign="top">
						<th scope="row"><label for="blogname">Enter your mailchimp api key:</label></th>
						<td>
							<input type="text" name='txt_apikey' id='txt_apikey' placeholder='mailchimp api key' value='<?php echo ( isset( $options["apikey"] ) ? $options['apikey'] : "" );?>'/>
						</td>
					</tr>
					<?php if( isset( $options['apikey'] ) ): ?>
						<?php if( $cmmi_mylists && !empty( $cmmi_mylists ) ):?>
						    <?php foreach ( $roles as $roleslug => $role ) : ?>
							<tr valign="top">
							    <th scope="row"><label for="blogname">Mailchimp list for <?php echo $role; ?>:</label></th>
								<td>
									<select name='sl_groups_<?php echo $roleslug ?>'>
										<option value=''>Select..</option>
										<?php 
										foreach ($cmmi_mylists as $key => $value){
											$selected = '';
											if( isset( $options['list'.$roleslug] ) && $key == $options['list'.$roleslug] )	
												$selected = 'selected';
											echo "<option value='$key' $selected>$value</option>";
										}
										?>
									</select>
								</td>
							</tr>
						    <?php endforeach; ?>
							
						<?php else:?>
							<tr valign="top">
								<th scope="row">Error!</th>
								<td>
									Please double check your api key and make sure you have created mailing lists in your mailchimp account.
								</td>
							</tr>
						<?php endif;?>
					<?php endif;?>
				</tbody>
			</table>
		</form>
	</div>
	<?php 
}

/*
 * add the checkbox for subscribing to the mailchimp newsletter
*/
add_action("show_user_profile", "cmmi_optin_checkbox_your_profile", 98);
add_action("register_form","cmmi_optin_checkbox_register",98);
function cmmi_optin_checkbox_your_profile(){
	$user_id = get_current_user_id();
	$subscribe_to_maillist = get_user_meta( $user_id, "subscribe_to_maillist", true );
	?>
	    <div class="text-info" >
		<label><input type='checkbox' value='yes' name='cmmi_optin_checkbox' <?php if( $subscribe_to_maillist ){ echo 'checked'; } ?> />Subscribe me to Career Channel Mailing list</label>
	    </div>
	<?php 
}
function cmmi_optin_checkbox_register(){
	?>
	    <div class="text-info register_form_bottom_message" >
		<label><input type='checkbox' value='yes' name='cmmi_optin_checkbox' <?php echo 'checked'; ?> />Subscribe me to Career Channel Mailing list</label>
	    </div>
	<?php 
}

/*
 * add user role after user registers as business member
*/
function cmmi_subscribe( $user_id, $user_email ){
    
	if( isset( $_POST['cmmi_optin_checkbox'] ) && 'yes'==$_POST['cmmi_optin_checkbox'] ){
		$options = get_option( 'CMMI_OPTIONS' );
		if( isset( $options['apikey'] ) ){
			$api = new MCAPI_13( $options['apikey'] );
			//$myLists = $api->lists();
			//check if the current user is a business member
			//else she is a mum
			if( user_can( $user_id, 'employer' ) ){
				//subscribe the user to business member group
				if( isset( $options['list_employer'] ) ){
					$user = new WP_User( $user_id );
					$merge_vars = array( 'FNAME'=>$user->first_name, 'LNAME'=>$user->last_name );
					$retval = $api->listSubscribe( $options['list_employer'], $user_email, $merge_vars, 'html' );
				}
			}
			else{
				//subscribe the user to free memeber group
				if( isset( $options['list_subscriber'] ) ){
					$user = new WP_User( $user_id );
					$merge_vars = array( 'FNAME'=>$user->first_name, 'LNAME'=>$user->last_name );
					$retval = $api->listSubscribe( $options['list_subscriber'], $user_email, $merge_vars, 'html' );
				}
			}
			update_user_meta( $user_id, "subscribe_to_maillist", true );
		}
	}
}

/*
 * Removes user role after user registers as business member
*/
function cmmi_unsubscribe( $user_id, $user_email ){
    
	if( isset( $_POST['cmmi_optin_checkbox'] ) && 'yes'==$_POST['cmmi_optin_checkbox'] ){
		$options = get_option( 'CMMI_OPTIONS' );
		if( isset( $options['apikey'] ) ){
			$api = new MCAPI_13( $options['apikey'] );
			//$myLists = $api->lists();
			//check if the current user is a business member
			//else she is a mum
			if( user_can( $user_id, 'employer' ) ){
				//subscribe the user to business member group
				if( isset( $options['list_employer'] ) ){
					$user = new WP_User( $user_id );
					$merge_vars = array( 'FNAME'=>$user->first_name, 'LNAME'=>$user->last_name );
					$retval = $api->listUnsubscribe( $options['list_employer'], $user_email, $merge_vars, 'html' );
				}
			}
			else{
				//subscribe the user to free memeber group
				if( isset( $options['list_subscriber'] ) ){
					$user = new WP_User( $user_id );
					$merge_vars = array( 'FNAME'=>$user->first_name, 'LNAME'=>$user->last_name );
					$retval = $api->listUnsubscribe( $options['list_subscriber'], $user_email, $merge_vars, 'html' );
					
				}
			}
		}
	}
}