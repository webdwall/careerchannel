<?php 
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * Resume management 
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
add_action( 'cc_dashboard_nav',						'candidate_resume_nav' );
add_action( 'cc_dashboard_subnav',					'candidate_resume_subnav' );
add_action( 'cc_dashboard_title',					'candidate_resume_title' );
add_action( 'cc_dashboard_content',					'candidate_resume_content' );
add_filter( 'cc_dashboard_default_subcomponent',	'candidate_resume_default_subcomponent', 10, 2 );

function candidate_resume_nav( $user_type ){
	if( $user_type!='employer' ){
		$active_class = '';
		if( cc_user_dashboard_is_current_component('resume') ){
			$active_class = 'active';
		}
		
		//must be all small letters, capitalization will be done with css
		echo "<li class='$active_class'><a href='". cc_user_dashboard_url( $user_type, 'resume') ."'>resume</a></li>";
	}
}

function candidate_resume_title( $user_type ){
	if( $user_type!='employer' && cc_user_dashboard_is_current_component('resume') ){
		//must be all small letters, capitalization will be done with css
		$title = cc_user_dashboard_current_component() . ' - ' . cc_user_dashboard_current_subcomponent();
		
		if( 'edit'==cc_user_dashboard_current_subcomponent() && !$_GET['resume_id'] ){
			$title = cc_user_dashboard_current_component() . ' - ' . 'add new';
		}
		echo $title;
	}
}

function candidate_resume_subnav( $user_type ){
	if( $user_type!='employer' && cc_user_dashboard_is_current_component('resume') ){
		if(cc_user_dashboard_is_current_subcomponent('my-resumes') ){
			echo "<li class='active'><a href='". cc_user_dashboard_url($user_type, 'resume') ."'>my resumes</a></li>";
		}
		else{
			echo "<li><a href='". cc_user_dashboard_url($user_type, 'resume') ."'>my resumes</a></li>";
		}
		if(cc_user_dashboard_is_current_subcomponent('edit') ){
			echo "<li class='active'><a href='". cc_user_dashboard_url($user_type, 'resume', 'edit') ."'>add/edit resume</a></li>";
		}
		else{
			echo "<li><a href='". cc_user_dashboard_url($user_type, 'resume', 'edit') ."'>add/edit resume</a></li>";
		}
		
	}
}

function candidate_resume_default_subcomponent( $default_subcomponent, $component ){
	if( $component=='resume' ){
		$default_subcomponent = 'my-resumes';
	}
	return $default_subcomponent;
}

function candidate_resume_content( $user_type ){
	if( $user_type!='employer' && cc_user_dashboard_is_current_component('resume') ){
		$sub_component = cc_user_dashboard_current_subcomponent();
		switch ($sub_component){
			case 'edit':
				echo do_shortcode('[resumeForm]');
				break;
			default:
				echo do_shortcode('[resumeDisplay]');
				break;
		}
	}
}

/* ================================================================ */

/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * Jobs
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
add_action( 'cc_dashboard_nav',						'candidate_jobs_nav', 9 );
add_action( 'cc_dashboard_title',					'candidate_jobs_title' );
add_action( 'cc_dashboard_content',					'candidate_jobs_content' );

function candidate_jobs_nav( $user_type ){
	if( $user_type=='candidate' ){
		$active_class = '';
		if( cc_user_dashboard_is_current_component('jobs') ){
			$active_class = 'active';
		}
		
		//must be all small letters, capitalization will be done with css
		echo "<li class='$active_class'><a href='". cc_user_dashboard_url( $user_type, 'jobs') ."'>jobs</a></li>";
	}
}

function candidate_jobs_title( $user_type ){
	if( $user_type=='candidate' && cc_user_dashboard_is_current_component('jobs') ){
		//must be all small letters, capitalization will be done with css
		$title = cc_user_dashboard_current_component();
		echo $title;
	}
}

function candidate_jobs_content( $user_type ){
	if( $user_type=='candidate' && cc_user_dashboard_is_current_component('jobs') ){
		echo do_shortcode('[jobs]');
	}
}

/* ================================================================ */