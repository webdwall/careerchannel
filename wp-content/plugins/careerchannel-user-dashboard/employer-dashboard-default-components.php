<?php 
add_action( 'template_redirect', 'load_employer_default_components' );
function load_employer_default_components(){
	add_action( 'cc_dashboard_nav',						'employer_jobs_nav', 9 );
	add_action( 'cc_dashboard_subnav',					'employer_jobs_subnav' );
	add_action( 'cc_dashboard_title',					'employer_jobs_title' );
	add_action( 'cc_dashboard_content',					'employer_jobs_content' );
	add_filter( 'cc_dashboard_default_subcomponent',	'employer_jobs_default_subcomponent', 10, 2 );
	
	/* purchase history - woocommerce shortcode */
	add_action( 'cc_dashboard_nav',						'employer_purchase_history_nav', 11 );
	//add_action( 'cc_dashboard_subnav',					'employer_purchase_history_subnav' );
	add_action( 'cc_dashboard_title',					'employer_purchase_history_title' );
	add_action( 'cc_dashboard_content',					'employer_purchase_history_content' );
	//add_filter( 'cc_dashboard_default_subcomponent',	'employer_purchase_history_default_subcomponent', 10, 2 );
}

function employer_jobs_nav( $user_type ){
	if( $user_type=='employer' ){
		$active_class = '';
		if( cc_user_dashboard_is_current_component('jobs') ){
			$active_class = 'active';
		}
		
		//must be all small letters, capitalization will be done with css
		echo "<li class='$active_class'><a href='". cc_user_dashboard_url( $user_type, 'jobs') ."'>job Post</a></li>";
	}
}

function employer_jobs_subnav( $user_type ){
 	$my_component = 'jobs';
	if( 'employer'==$user_type && cc_user_dashboard_is_current_component($my_component) ){
 		$subcomponents = array( 'my-jobs', 'post-a-job' );
 		foreach( $subcomponents as $subcomponent ){
 			$active_class = '';
			if( cc_user_dashboard_is_current_subcomponent($subcomponent) ){
				$active_class = 'active';
			}

			//must be all small letters, capitalization will be done with css
			echo "<li class='$active_class'><a href='". cc_user_dashboard_url($user_type, $my_component, $subcomponent) ."'>". str_replace('-', ' ', $subcomponent) ."</a></li>";	
 		}
  }
}

function employer_jobs_title( $user_type ){
	if( $user_type=='employer' && cc_user_dashboard_is_current_component('jobs') ){
		//must be all small letters, capitalization will be done with css
		echo cc_user_dashboard_current_component() . ' - ' . cc_user_dashboard_current_subcomponent();
	}
}

function employer_jobs_content( $user_type ){
	if( $user_type=='employer' && cc_user_dashboard_is_current_component('jobs') ){
		switch( cc_user_dashboard_current_subcomponent() ){
			case 'post-a-job':
				echo do_shortcode('[submit_job_form]');
				break;
 			case 'my-jobs':
			default:
 				echo do_shortcode('[job_dashboard]');
 				break;
 		}
	}
}

function employer_jobs_default_subcomponent( $default_subcomponent, $component ){
	if( !is_user_a_candidate() && $component=='jobs' ){
		$default_subcomponent = 'my-jobs';
 	}
	return $default_subcomponent;
}


function employer_purchase_history_nav( $user_type ){
	if( $user_type=='employer' ){
		$active_class = '';
		if( cc_user_dashboard_is_current_component('purchase-history') ){
			$active_class = 'active';
		}
		
		//must be all small letters, capitalization will be done with css
		echo "<li class='$active_class'><a href='". cc_user_dashboard_url( $user_type, 'purchase-history') ."'>purchase history</a></li>";
	}
}

function employer_purchase_history_subnav( $user_type ){
 	$my_component = 'purchase-history';
	if( 'employer'==$user_type && cc_user_dashboard_is_current_component($my_component) ){
 		$subcomponents = array( 'purchase-history' );
 		foreach( $subcomponents as $subcomponent ){
 			$active_class = '';
			if( cc_user_dashboard_is_current_subcomponent($subcomponent) ){
				$active_class = 'active';
			}

			//must be all small letters, capitalization will be done with css
			echo "<li class='$active_class'><a href='". cc_user_dashboard_url($user_type, $my_component, $subcomponent) ."'>". str_replace('-', ' ', $subcomponent) ."</a></li>";	
 		}
  }
}

function employer_purchase_history_title( $user_type ){
	if( $user_type=='employer' && cc_user_dashboard_is_current_component('purchase-history') ){
		//must be all small letters, capitalization will be done with css
		//echo cc_user_dashboard_current_component() . ' - ' . cc_user_dashboard_current_subcomponent();
		echo 'purchase history';
	}
}

function employer_purchase_history_content( $user_type ){
	if( $user_type=='employer' && cc_user_dashboard_is_current_component('purchase-history') ){
		echo do_shortcode('[woocommerce_my_account]');
	}
}

function employer_purchase_history_default_subcomponent( $default_subcomponent, $component ){
	if( !is_user_a_candidate() && $component=='purchase-history' ){
		$default_subcomponent = 'purchase-history';
 	}
	return $default_subcomponent;
}