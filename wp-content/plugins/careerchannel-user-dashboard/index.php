<?php 
/* 
Plugin Name: CareerChannel User Dashboard
Description: Dashboard component for candidates and employers
Author: E Media Identity
Version: 0.2
*/

require_once( plugin_dir_path( __FILE__ ) . 'functions.php' );
require_once( plugin_dir_path( __FILE__ ) . 'commom-default-components.php' );
require_once( plugin_dir_path( __FILE__ ) . 'employer-dashboard-default-components.php' );
require_once( plugin_dir_path( __FILE__ ) . 'candidate-dashboard-default-components.php' );
/**
 * returns the current/active component of the dashboard
 * 
 * @since 0.2
 * @return string current component name/slug
 */
function cc_user_dashboard_current_component(){
	$obj = CCUserDashboard::get_instance();
	return $obj->current_component();
}

/**
 * returns the current/active subcomponent of the dashboard
 * 
 * @since 0.2
 * @return string current subcomponent name/slug
 */
function cc_user_dashboard_current_subcomponent(){
	$obj = CCUserDashboard::get_instance();
	return $obj->current_subcomponent();
}

/*
 * check if the current active tab/component of the user dashboard is the given component
 * e.g: if( cc_user_dashboard_is_current_component( 'inbox' ) ){ ..display inbox layout... }
 * 
 * @return bool
 */
function cc_user_dashboard_is_current_component( $component ){
	$obj = CCUserDashboard::get_instance();
	return $obj->is_current_component( $component );
}

/*
 * check if the current active tab/component of the user dashboard is the given component
 * e.g: if( cc_user_dashboard_is_current_component( 'inbox' ) ){ ..display inbox layout... }
 * 
 * @return bool
 */
function cc_user_dashboard_is_current_subcomponent( $subcomponent ){
	$obj = CCUserDashboard::get_instance();
	return $obj->is_current_subcomponent( $subcomponent );
}

/**
 * Get the user dashboard url of selected component and subcomponent.
 * @param string $user_type 'candidate'|'employer'
 * @param string $component the component which the url shold point to. E.g: 'jobs'. Optional - will take default component if not passed.
 * @param string $subcomponent the subcomponent which the url should point to. E.g: 'add-job'.  Optional - will take default sub-component if not passed.
 * @param boolean $add_default_hash_link : whether to add #dnav to url or not. Default true
 * 
 * @return string the complete url to dashboard page component
 */
function cc_user_dashboard_url( $user_type, $component='', $subcomponent='', $add_default_hash_link=true ){
	$obj = CCUserDashboard::get_instance();
	return $obj->dashboard_url( $user_type, $component, $subcomponent, $add_default_hash_link );
}

function cc_user_dashboard( $type='employer', $theme='vertical' ){
	?>
	<div class='row clear dashboard dashboard-<?php echo esc_attr($type) . ' ' . $theme;?> '>
		
		<div class='dashboard-navigation <?php echo $theme=='vertical' ? '' : 'col span_2';?>' id='dnav'>
			<h4>Navigation</h4>
			<ul class='nav <?php echo $theme=='vertical' ? 'nav-tabs nav-justified' : 'nav-pills nav-stacked';?>' >
				<?php do_action( 'cc_dashboard_nav', $type, cc_user_dashboard_current_component() );?>
			</ul>
		</div>
		<div class='dashboard-content <?php echo $theme=='vertical' ? '' : 'col span_10';?>' id='dcontent'>
			<div class="dashboard-subnav">
				<ul class='subnav nav nav-pills'>
					<?php do_action( 'cc_dashboard_subnav', $type, cc_user_dashboard_current_component() );?>
				</ul>
			</div>
			<div class='inner'>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title"><?php do_action( 'cc_dashboard_title', $type );?></h3>
					</div>
					<div class="panel-body">
						<?php do_action( 'cc_dashboard_content', $type );?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php 
}

function candidate_dashboard(){
	cc_user_dashboard( 'candidate' );
}

function employer_dashboard(){
	cc_user_dashboard( 'employer' );
}

function cc_user_dashboard_header(){
	if( is_user_a_candidate() ){
		get_template_part('members/candidate', 'dashboard-header');
	}
	else{
		get_template_part('members/employer', 'dashboard-header');
	}
}

/*
 * sample use
 * adding the 'inbox' tab for 'employers'
 * we need to hook into three actions : nav, title, content.
 */


/*#########################...DEMO...###############################

add_action( 'cc_dashboard_nav',						'employer_messages_nav' );
add_action( 'cc_dashboard_subnav',					'employer_messages_subnav' );
add_action( 'cc_dashboard_title',					'employer_messages_title' );
add_action( 'cc_dashboard_content',					'employer_messages_content' );
add_filter( 'cc_dashboard_default_subcomponent',	'employer_messages_default_subcomponent', 10, 2 );

function employer_messages_nav( $user_type ){
	if( $user_type=='employer' ){
		$active_class = '';
		if( cc_user_dashboard_is_current_component('messages') ){
			$active_class = 'active';
		}
		
		//must be all small letters, capitalization will be done with css
		echo "<li class='$active_class'><a href='". cc_user_dashboard_url( $user_type, 'messages') ."'>messages</a></li>";
	}
}

function employer_messages_subnav( $user_type ){
 	$my_component = 'messages';
	if( 'employer'==$user_type && cc_user_dashboard_is_current_component($my_component) ){
 		$subcomponents = array( 'inbox', 'sentbox', 'compose' );
 		foreach( $subcomponents as $subcomponent ){
 			$active_class = '';
			if( cc_user_dashboard_is_current_subcomponent($subcomponent) ){
				$active_class = 'active';
			}

			//must be all small letters, capitalization will be done with css
			echo "<li class='$active_class'><a href='". cc_user_dashboard_url($user_type, $my_component, $subcomponent) ."'>$subcomponent</a></li>";
 		}
  }
}

function employer_messages_title( $user_type ){
	if( $user_type=='employer' && cc_user_dashboard_is_current_component('messages') ){
		//must be all small letters, capitalization will be done with css
		echo cc_user_dashboard_current_component() . ' - ' . cc_user_dashboard_current_subcomponent();
	}
}

function employer_messages_content( $user_type ){
	if( $user_type=='employer' && cc_user_dashboard_is_current_component('messages') ){
		switch( cc_user_dashboard_current_subcomponent() ){
 			case 'inbox':
 				employer_inbox_content();//define this function
 				break;
 			case 'sentbox':
 				employer_sentbox_content();//define this function
 				break;
 			case 'inbox':
 				employer_compose_message_content();//define this function
 				break;
 		}
	}
}

function employer_messages_default_subcomponent( $default_subcomponent, $component ){
	if( $component=='messages' ){
		$default_subcomponent = 'inbox';
 	}
	return $default_subcomponent;
}
##################################################################*/



/* you need be worried about the code inside this class, all the functions
 * exposed in global scope above is sufficient.
 */
add_action('init', 'cc_user_dashboard_init');
function cc_user_dashboard_init() {
    CCUserDashboard::get_instance();
}

class CCUserDashboard {
    private $pages = array();
    
	// static function only use static function hence this var is used in  get_instance() method.
    private static $instance;

    private function __construct() {
		$this->pages['employer']  = get_option( 'cc_employer_dashboard_page_id' );
		$this->pages['candidate'] = get_option( 'cc_candidate_dashboard_page_id' );
		
		add_action('pmsc_admin_general_setting_hook',		array( $this, 'employer_dashboard_page_print' ) );
		add_action('pmsc_admin_setting_save_button_hook',	array( $this, 'employer_dashboard_page_update' ) );
		
		add_action('pmsc_admin_general_setting_hook',		array( $this, 'candidate_dashboard_page_print' ) );
		add_action('pmsc_admin_setting_save_button_hook',	array( $this, 'candidate_dashboard_page_update' ) );
		
		add_filter( 'the_content',							array( $this, 'dashboard_pages_content' ) );
		add_action( 'wp_footer',							array( $this, 'footer_script' ) );
    }

    public static function get_instance() {
		if (!isset(self::$instance)) {
			self::$instance = new self();
		}
		return self::$instance;
    }
	
	function employer_dashboard_page_print(){
		$selected_page = $this->pages['employer'];
		?>
		<li>
			<label for='cc_employer_dashboard_page'>'Employer Dashboard' Page</label>
		<?php
		$args = array(
			'selected' => $selected_page,
			'name' => 'cc_employer_dashboard_page',
			'id' => 'cc_employer_dashboard_page'
		);
		echo pmsc_admin_settings_pages_list($args);
		?>
			<br/><small><i>Select the page which should display 'Employer Dashboard'.</i></small>
		</li>
		<?php
	}
	
	function employer_dashboard_page_update(){
		$selected_page = $this->pages['employer'];
		//update only if it's been changed
		$new_selection = ( $_POST['cc_employer_dashboard_page'] ? $_POST['cc_employer_dashboard_page'] : false );
		if ($new_selection != $selected_page) {
			update_option('cc_employer_dashboard_page_id', $new_selection);
			$this->pages['employer'] = $new_selection;
		}
	}
	
	function candidate_dashboard_page_print(){
		$selected_page = $this->pages['candidate'];
		?>
		<li>
			<label for='cc_candidate_dashboard_page'>'Candidate Dashboard' Page</label>
		<?php
		$args = array(
			'selected' => $selected_page,
			'name' => 'cc_candidate_dashboard_page',
			'id' => 'cc_candidate_dashboard_page'
		);
		echo pmsc_admin_settings_pages_list($args);
		?>
			<br/><small><i>Select the page which should display 'Candidate Dashboard'.</i></small>
		</li>
		
		<?php
	}
	
	function candidate_dashboard_page_update(){
		$selected_page = $this->pages['candidate'];
		//update only if it's been changed
		$new_selection = ( $_POST['cc_candidate_dashboard_page'] ? $_POST['cc_candidate_dashboard_page'] : false );
		if ($new_selection != $selected_page) {
			update_option('cc_candidate_dashboard_page_id', $new_selection);
			$this->pages['candidate'] = $new_selection;
		}
	}
	
	public function current_component(){
		//jobs is the default component
		return isset( $_GET['component'] ) ? $_GET['component'] : 'jobs';
	}
	
	public function is_current_component( $component ){
		return strtolower( $this->current_component() )==strtolower($component);
	}
	
	public function current_subcomponent(){
		//messages is the default component
		$sub_component = isset( $_GET['subcomponent'] ) ? $_GET['subcomponent'] : '';
		
		if( !$sub_component ){
			//we should return the default sub-component of the current component
			$sub_component = apply_filters( 'cc_dashboard_default_subcomponent', '', $this->current_component() );
		}
		return $sub_component;
	}
	
	public function is_current_subcomponent( $component ){
		return strtolower( $this->current_subcomponent() )==strtolower($component);
	}

	public function dashboard_url( $user_type, $component='', $subcomponent='', $add_default_hash_link=true ){
		/*
		 * user type can be either employer or candidate
		*/
		if( isset( $this->pages[$user_type] ) ){
			$base_link = wp_cache_get('dshurlbase_'.$user_type);
			if( $base_link==false ){
				$base_link = get_permalink( $this->pages[$user_type] );
				wp_cache_set('dshurlbase_'.$user_type, $base_link);
			}
			
			if( $component ){
				$base_link = add_query_arg( 'component', $component, $base_link );
			}
			if( $subcomponent ){
				$base_link = add_query_arg( 'subcomponent', $subcomponent, $base_link );
			}
			
			if( $add_default_hash_link ){
				$base_link .= '#dnav';
			}
			return $base_link;
		}
		return '';
	}
	
	function dashboard_pages_content( $content ){
		if (is_page() && is_main_query() && !is_admin() ) {
			if( $this->pages['employer'] && is_page( $this->pages['employer'] ) && $this->pages['employer'] == get_the_ID() ) {
				if( is_user_a_candidate() ){
					$content .= "<h2>Error - Restricted Content!!</h2>";
					$content .= "<p class='text-error'>Your account desn't have access to this content</p>";
				}
				else{
					ob_start();
					cc_user_dashboard( 'employer', 'horizontal' );
					$content .= ob_get_contents();
					ob_end_clean();
				}
			}
			else if( $this->pages['candidate'] && is_page( $this->pages['candidate'] ) ) {
				if( is_user_a_candidate() || current_user_can( 'level_10' ) ){
					ob_start();
					cc_user_dashboard( 'candidate', 'horizontal' );
					$content .= ob_get_contents();
					ob_end_clean();
				}
				else{
					$content .= "<h2>Error - Restricted Content!!</h2>";
					$content .= "<p class='text-error'>Your account desn't have access to this content</p>";
				}
			}
		}
		return $content;
	}
	
	function footer_script(){
		$is_our_page = false;
		if (is_page()) {
			if( $this->pages['employer'] && is_page( $this->pages['employer'] ) ) {
				$is_our_page = true;
			}
			else if( $this->pages['candidate'] && is_page( $this->pages['candidate'] ) ) {
				$is_our_page = true;
			}
			if( $is_our_page ){
				?>
				<script type='text/javascript'>
					jQuery(document).ready(function($){
						$(".dashboard.horizontal .dashboard-navigation li.active a").append( '<span class="glyphicon glyphicon-chevron-right pull-right"></span>' );
					});
				</script>
				<?php 
			}
		}
	}
}