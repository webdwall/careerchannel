<?php 
add_action( 'template_redirect', 'load_common_default_components' );
function load_common_default_components(){
	add_action( 'cc_dashboard_nav',		'invite_friends_nav', 15 );
	add_action( 'cc_dashboard_title',	'invite_friends_title' );
	add_action( 'cc_dashboard_content',	'invite_friends_content' );
	
	$is_chat_available = true;
	if( !is_user_a_candidate() ){
		//should only be available to paid employers
		$membership_details = member_package_details();
		if( $membership_details['package']=='free' ){
			$is_chat_available = false;
		}
	}
	if( $is_chat_available ){
		add_action( 'cc_dashboard_nav',		'cc_chat_nav', 14 );
		add_action( 'cc_dashboard_title',	'cc_chat_title' );
		add_action( 'cc_dashboard_content',	'cc_chat_content' );
	}
}

/* ++++++++++++++++ INVITE FRIENDS ++++++++++++++ */
function invite_friends_nav( $user_type ){
	//should be added to all user types
	$active_class = '';
	if( cc_user_dashboard_is_current_component('invite-friends') ){
		$active_class = 'active';
	}

	//must be all small letters, capitalization will be done with css
	echo "<li class='$active_class'><a href='". cc_user_dashboard_url( $user_type, 'invite-friends') ."'>invite friends</a></li>";
}

function invite_friends_title( $user_type ){
	if( cc_user_dashboard_is_current_component('invite-friends') ){
		echo "invite friends";
	}
}

function invite_friends_content( $user_type ){
	if( cc_user_dashboard_is_current_component('invite-friends') ){
		echo do_shortcode('[invite_friends]');
	}
}
/* =============================================== */

/* ++++++++++++++++ CHAT ++++++++++++++ */
function cc_chat_nav( $user_type ){
	//should be added to all user types
	$active_class = '';
	if( cc_user_dashboard_is_current_component('chat') ){
		$active_class = 'active';
	}

	//must be all small letters, capitalization will be done with css
	//echo "<li class='$active_class'><a href='". home_url('/chat') ."'>online chat</a></li>";
	echo "<li class='$active_class'><a href='". cc_user_dashboard_url( $user_type, 'chat') ."'>online chat</a></li>";
}

function cc_chat_title( $user_type ){
	if( cc_user_dashboard_is_current_component('chat') ){
		echo "online chat";
	}
}

function cc_chat_content( $user_type ){
	if( cc_user_dashboard_is_current_component('chat') ){
		echo do_shortcode('[avchat]');
	}
}
/* =============================================== */