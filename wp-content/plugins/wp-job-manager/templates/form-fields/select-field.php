<?php 
/* emi_career_channel_custom_changes : formatting select tag */ 
if( $key != "job_category" ):
?>
<select name="<?php echo esc_attr( isset( $field['name'] ) ? $field['name'] : $key ); ?>" id="<?php echo esc_attr( $key ); ?>">
	<option value="" >--select <?php echo $field["label"]; ?> --</option>
	<?php foreach ( $field['options'] as $key => $value ) : ?>
		<option value="<?php echo esc_attr( $key ); ?>" <?php if ( isset( $field['value'] ) ) selected( $field['value'], $key ); ?>><?php echo esc_html( $value ); ?></option>
	<?php endforeach; ?>
</select>
<?php else: ?>
<?php show_custom_specialization_select_tag( "post_job", $key, $field['value'] ); ?>
<?php endif; ?>
