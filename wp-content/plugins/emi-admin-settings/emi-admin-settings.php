<?php
/*
  Plugin name: General Admin settings
  Description: Admin Settings module
  version: 0.2
  Author: E Media Identity
  Author URI: http://emediaidentity.com/
  Text Domain: emi-admin-settings
 */
add_action('init', 'emi_pmsc_init_admin_setting_module');

function emi_pmsc_init_admin_setting_module() {
    EMI_PMSC_Admin_Setting_Module_Core::get_instance();
}

/*
 * function to return the html for a select list containing all the 'pages'
 * all other plugins using this plugin should make use of this function instead of generating page lists individually
 * the results are cached, so no multiple sql queries are performed.
 * 
 * @author @ckchaudhary
 * @since 0.2
 * @param mixed $args : default false
 * @return string html (html for the <select> tag)
 */
function pmsc_admin_settings_pages_list( $args = '' ) {
    $obj = EMI_PMSC_Admin_Setting_Module_Core::get_instance();
    return $obj->pages_list($args);
}

class EMI_PMSC_Admin_Setting_Module_Core {
    
    /*
     * hold the result of get_pages function.
     * @since 0.2
     */
    private $pages;
    // static function only use static function hence this var is used in  get_instance() method.
    private static $instance;

    private function __construct() {
	add_action('admin_menu', array($this, 'pmsc_admin_setting_menu'));
    }

    /*
     * this function will check if the object is already present or not
     * this function is static therefore no need to create the object of it.
     */

    public static function get_instance() {
	if (!isset(self::$instance)) {
	    self::$instance = new self();
	}
	return self::$instance;
    }

    function pmsc_admin_setting_menu() {
	add_options_page('Emedia General Config', 'EMI General Configuartion', 'manage_options', 'emi_pmsc_config', array($this, 'emi_pmsc_setting_menu_page'));
    }

    function emi_pmsc_setting_menu_page() {
	if (isset($_POST['save_admin_general_setting_button'])) {
	    do_action("pmsc_admin_setting_save_button_hook");
	}
	?>
	<style type='text/css'>
		.emi_admin_settings form > ul > li label{
			display:inline-block; width: 300px;
		}
		.emi_admin_settings form > ul > li.devider{
			border-bottom: 1px inset #ddd;
			padding: 5px 0;
			margin-bottom: 10px;
		}
	</style>
	<div class="emi_admin_settings">
	    <form name="admin_general_setting_form" method="post" action="" >
		<h3>Miscellaneous settings:</h3>
		<ul>

	<?php /* this action is to append other setting controls */ ?>
	<?php do_action("pmsc_admin_general_setting_hook"); ?>
			<li class="devider"></li>
			<li>
			<input type="submit" name="save_admin_general_setting_button" value="Save Settings" class="button-primary" />
		    </li>
		</ul>
	    </form>
	</div>
	<?php
    }

    /*
    * function to return the html for a select list containing all the 'pages'
    * all other plugins using this plugin should make use of this function instead of generating page lists individually
    * the results are cached, so no multiple sql queries are performed.
    * 
    * @author @ckchaudhary
    * @since 0.2
    * @param mixed $args : default false
    * @return string html (html for the <select> tag)
    */
    function pages_list( $args='' ){
	$html = '';
	$defaults = array(
	    'selected'	=> false,
	    'name'	=> '',
	    'id'	=> '',
	    'multiple'	=> false
	);
	
	$args = wp_parse_args($args, $defaults);
	extract($args);
	
	if( !$this->pages ){
	    $this->pages = get_pages();
	}

	$pages_to_print = apply_filters( 'pmsc_admin_settings_pages_list', $this->pages );
	
	$html .= "<select name='". esc_attr($name) ."' id='". esc_attr($id) ."' " ;
	if( $multiple ){
	    $html .= 'multiple';
	}
	$html .= '>';
	
	$html.= "<option value=''>". esc_attr( __( 'Select page' ) ) ."</option>";
	
	foreach ( $pages_to_print as $page ) {
	    $option = "<option value='". $page->ID ."'";
		if( $selected && $selected==$page->ID ){
		    $option .= ' selected';
		}
		$option .= ">";
		
	    $option .= $page->post_title;
	    $option .= '</option>';
	    
	    $html .= $option;
	}
	
	$html .= "</select>";
	
	return $html;
    }
}

/* EMI_PMSC_Admin_Setting_Module_Core */

