<?php 
/* 
Plugin Name: CareerChanel messaging system.
Description: Plugin to facilitate messaging system between members.
Version: 1.2
Author: E Media Identity
Author URI: http://emediaidentity.com/
Network: true
*/
require_once( plugin_dir_path( __FILE__ ) . 'functions.php' );
require_once( plugin_dir_path( __FILE__ ) . 'employer-dashboard.php' );
require_once( plugin_dir_path( __FILE__ ) . 'candidate-dashboard.php' );
require_once( plugin_dir_path( __FILE__ ) . 'class.CCMembersMessagesTemplate.php' );

add_action( 'init', 'members_messages_init_plugin' );

function members_messages_init_plugin(){
    $instance = CCMembersMessages::get_instance();
}

class CCMembersMessages{
    private static $instance;
    
    public static function get_instance(){
        if(!isset(self::$instance)){
            self::$instance=new self();
        }
        return self::$instance;
    }

    function __construct() {
		//add_shortcode( 'CC_members_inbox',			array($this,'show_members_notification'));
		//add_shortcode( 'CC_members_sentbox',		array($this,'members_sentbox'));
		
		add_action(	'wp_enqueue_scripts',			array($this,'add_js'));
		add_action(	'wp_enqueue_scripts',			array($this,'add_css'));
		add_action( 'wp_ajax_delete_message_ajax',	array($this,'delete_message'));
		
		add_action( 'wp_ajax_compose_message_ajax', 'cc_compose_message_ajax');
		add_action( 'wp_ajax_cc_messages_reply',	array( $this, 'ajax_save_reply' ) );
		
		add_filter( 'cc_user_notifications',		array( $this, 'get_notifications_threads' ), 10, 3 );
		add_filter( 'cc_user_notifications',		array( $this, 'get_notifications_replies' ), 10, 3 );
		
		add_action( 'wp_ajax_cc_messages_delete',	array( $this, 'ajax_cc_messages_delete' ) );
		add_action( 'wp_ajax_cc_candidate_messages_delete',	array( $this, 'ajax_cc_candidate_messages_delete' ) );
    }

    function add_js(){
	wp_enqueue_script(
	    "cc_message_deshboard_script",
	    path_join( WP_PLUGIN_URL, basename( dirname( __FILE__ ) )."/JS/cc_message_script.js" ),
	    array('jquery')
	);
	$data = array(
	    'ajaxurl' => admin_url('/admin-ajax.php'),
	);
	wp_localize_script( 'cc_message_deshboard_script', 'CC_MSG', $data );
    }

    function add_css(){
        wp_enqueue_style(
          "cc_deshboard_style",
           path_join( WP_PLUGIN_URL, basename( dirname( __FILE__ ) )."/CSS/CC_deshboard_style.css" )
        );
    }
        
    function delete_message(){
        $id = $_POST['msg_id'];
        global $wpdb;
        $query = "UPDATE ".get_cc_meesage_table()." 
                   SET status='deleted'
                   WHERE ID =".$id."";
        $wpdb->query($query);
    }
    /*
     * one method is to create user message. user message wil be created
     * with default content or with custom content
    */
    
    /*
     *  one method is to display user inbox message this is a sql query 
     *  which  will show all message with pending status with some alert type.
     */
    
    /*
     * one method with action and filter in it. which is cutomisable from anywhere.
     * just pass parameter and save your message.
     * 1 method will do his work and will print alert for that in user inbox.
     */
    function cc_messages_send( $args ){
        $retval = array(
            'status'    => true,
            'message'   => '',
            'message_id'    => false
        );

        $defaults = array (
			'subject'       => false,
			'content'       => false ,
			'sender_id'     => false,
			'reciver_id'    => false,
			'reply_of'      => false ,
			'type'          => 'pm',
			'primary_obj'   => false,
			'secondry_obj'  => false
        );

        $args = wp_parse_args( $args, $defaults );
        
		/* validation */
        if( $args['type']=='reply' &&!$args['reply_of'] ){
            $retval['status'] = false;
            $retval['message']='Message reply must have parent message id';
        }
		
        if( $args['sender_id'] == false || trim($args['sender_id']) == "" ){    
            $retval['status'] = false;
            $retval['message'] = "access denied";
        }
		
        do_action_ref_array( 'before_cc_messages_send', $retval, $args );
                
		global $wpdb;
		$content = $args['content'];
		if($retval['status'] == true){
			//save message data
			if( $args['type']=='reply' ){
				$wpdb->insert(
					get_cc_replies_table(),
					array(
						'thread_id'	=>  $args['reply_of'],
						'sender_id' =>  $args['sender_id'],
						'message'   =>  $content,
						'msg_date'  => current_time('mysql'),
					),
					array(
						'%d','%d','%s','%s'
					)
				);
			}
			else{
				$wpdb->insert(
					get_cc_meesage_table(),
					array(
						'subject'       => $args['subject'],
						'message'       => $content,
						'sender_id'     => $args['sender_id'],
						'reciver_id'    => $args['reciver_id'],
						'type'          => $args['type'],
						'primary_obj'   => $args['primary_obj'],
						'secondry_obj'  => $args['secondry_obj'],
						'msg_date'      => current_time('mysql'),
					),
					array(
						'%s','%s','%d','%d','%s','%s','%s','%s'
					)
				);
			}
			$retval['message'] = 'message successfuly sent to reciver.';
			$retval['message_id'] = $wpdb->insert_id;
		}
        do_action_ref_array( 'after_cc_messages_send', $retval, $args );

        return $retval;
    }
	
	public function print_reply_form( $thread_details ){
		?>
		<ul class='list-unstyled thread-replies'>
			<li class='sender-me'>
			<form method='POST' id='frm_reply_thread'>
				<input type='hidden' name='tid' value='<?php echo $thread_details->ID;?>' />
				<?php wp_nonce_field( 'ccthreadreply' );?>
				<textarea name='reply_message' id='reply_message' placeholder='REPLY - type your reply here..'></textarea>
				
				<input type='submit' class="button button-small" value='Reply' style="margin-top:5px;" />
			</form>
			<script>
				jQuery(document).ready(function($){
					$('form#frm_reply_thread').submit(function(e){
						e.preventDefault();
						var $form = $(this);
						var message = $.trim( $form.find('#reply_message').val() );
						if( message=='' ){
							alert('enter your message first');
							$form.find('#reply_message').focus();
							return false;
						}

						$form.find('input[type="submit"]').attr('disabled', 'disabled').val('wait..');
						var data = {
							action : 'cc_messages_reply',
							message: message,
							thread_id: $form.find('input[name="tid"]').val(),
							_wpnonce: $form.find('input[name="_wpnonce"]').val()
						};

						$.ajax({
							type: "POST",
							url: '<?php echo admin_url( "/admin-ajax.php" );?>',
							data: data,
							success: function (response) {
								response = $.parseJSON(response);
								if( response.status ){
								    if ( $("#thread-replies-list")[0] ){
									$('#thread-replies-list').append(response.html);
								    }else{
									$('.thread-replies').before("<ul class='list-unstyled thread-replies' id='thread-replies-list'>"+response.html+"</ul>");
								    }
								}
								else{
									alert(response.messaage);
								}
								$form.find('input[type="submit"]').removeAttr('disabled').val('Reply');
								$form.find('#reply_message').val("");
								//ccbmc_after_toggle_bookmark($elem, response);
							},
							error: function(){
								//do nothing
							}
						});
					});
				});
			</script>
			</li>
		</ul>
		<?php
	}
	
	function ajax_save_reply(){
		$retval= array(
			'status'	=> false,
			'message'	=> ''
		);
		
		check_admin_referer( 'ccthreadreply' );
		$thread_id = (int)( isset( $_POST['thread_id'] ) ? $_POST['thread_id'] : 0 );
		$message = isset( $_POST['message'] ) ? $_POST['message'] : '';
		if( !$thread_id || !$message ){
			$retval['message'] = 'Error - some fields are empty!';
			die( json_encode($retval) );
		}
		if( ( $reply_id=$this->_save_reply( $thread_id, $message ) )!=false ){
			$retval['status']=true;
			$retval['message'] = 'Done';
			$retval['reply_id'] = $reply_id;
			//also generate the html for new reply to be displayed on front end
			global $wpdb;
			$user_details = $wpdb->get_results( "SELECT user_email, display_name FROM " . $wpdb->prefix . "users WHERE ID=".get_current_user_id() );
			if( $user_details && !empty( $user_details ) ){
				foreach( $user_details as $user_detail ){
					$html  = "<li class='sender-me reply-$reply_id'>";
					$html .=	"<div class='row clear'>";
					$html .=		"<div class='user'>";
					$html .=			get_avatar( $user_detail->user_email, 30 );
					$html .=		"</div>";
					$html .=		"<div class='message'>";
					$html .=			"<strong>" . $user_detail->display_name . "</strong> | ";
					$html .=			"<i>" . current_time('mysql') . "</i><hr/>";
					$html .=			"<p class='message-text'>$message</p>";
					$html .=		"</div>";
					$html .=	"</div>";
					$html .= "</li>";
					$retval['html'] = $html;
				}
			}
		}
		else{
			$retval['message'] = 'Some error occured. Please try again later.';
		}
		die( json_encode( $retval ) );
	}
	
	protected function _save_reply( $thread_id, $message ){
		global $wpdb;
		
		$update_query = " update ".get_cc_meesage_table()." set status = 'read' where ID = $thread_id";
		$wpdb->query( $update_query );
		
		$query = "INSERT INTO " . get_cc_replies_table() . " ( thread_id, message, sender_id, msg_date, status ) "
				. " VALUES ( %d, %s, %d, %s, 'pending' ) ";
		
		$wpdb->query( $wpdb->prepare( $query, $thread_id, $message, get_current_user_id(), current_time('mysql') ) );
		
		
		return mysql_insert_id();
	}
    /*
        * this is to show member's notifications with status "other then Delete" in inbox of member's.
        * this is simply do one sql query to retrive data from wp_cc_members_messages table 
        * then this method will print all data regarding current logged in user.
        * type      :   sortcode 
        * return    :   String
        * parameter :   none
    */
    function show_members_notification(){
        /*
        * do one sql query to retrive data 
        * from wp_cc_members_messages table  
        * where message status should not be delete
        * and reciver_id = logged_in_user_id 
        */
        $data = get_recived_message();
        $total_posts = count($data);
        $posts_per_page = 2;
        $paged =( isset( $_GET['list_count'] ) ? $_GET['list_count'] : 1 );
        $previous = $paged - 1; 
        $remaining = $previous * $posts_per_page;
        $index = $paged*$posts_per_page;
        $new_total = $total_posts - $remaining;

            $newdata = array();
            for ($i=$remaining; $i < $index; $i++) { 
                $newdata[] = $data[$i];
            }
            if(empty($data)){
                echo "no message to show.";   
            }

            if($_GET['type']=='single' && !empty($_GET['msg_id'])){
                $this->show_single_message_page($data);
            }
            else{
                $this->member_inbox_html($data);
            }
    }
    
    function members_sentbox(){
            $data = get_sent_message();
            $total_posts = count($data);
            $posts_per_page = 5;
            $paged =( isset( $_GET['list_count'] ) ? $_GET['list_count'] : 1 );
            $previous = $paged - 1; 
            $remaining = $previous * $posts_per_page;
            $index = $paged*$posts_per_page;
            $new_total = $total_posts - $remaining;

                $newdata = array();
                for ($i=$remaining; $i < $index; $i++) { 
                    $newdata[] = $data[$i];
                }
                if(!empty($data)){
                    $data = $data;
                }
                else{
                    echo "no message to show";   
                }
             ?>
             <div class="message_container">
                 <table id="message-threads" class="messages-notices">
                     <tbody>
                            <?php foreach($data as $key => $message){?>

                            <tr id="m-<?php echo $message->ID;?>" class=" read">
                                     <td width="1%" class="thread-count">
                                             <span class="unread-count"></span>
                                     </td>
                                     <td width="1%" class="thread-avatar">
                                             <!-- user profile pic-->
                                             <?php echo get_avatar( $message->reciver_id, 32 );?>
                                     </td>
                                     <td width="27%" class="thread-from">
                                            To: <span class="user_id"><?php echo "Member ".$message->reciver_id; ?></span><br/>
                                            <span class="activity"><?php echo $message->msg_date; ?></span>
                                     </td>
                                     <td width="51%" class="thread-info">
                                             <p>message sent : <a href="#" title="View Message"><?php echo "".$message->subject ;?></a></p>
                                             <p class="thread-excerpt"><?php //echo $message->message; ?></p>
                                     </td>
                                     <td width="15%" class="thread-options">
                    <a class="button delete_msg" id="delete_msg-<?php echo $message->ID; ?>" href="#" data_id='<?php echo $message->ID; ?>' title="Delete Message">Delete</a> &nbsp;
                                    </td>
                             </tr>
                            <?php }?>
                     </tbody>
                    <tfoot>
                    <?php emi_generate_paging_param($total_posts, $posts_per_page, $paged, "inbox", "", "list_count" ); ?>
                    </tfoot>
                </table>
             </div>
             <?php
    }

    function show_single_message_page($message_data){
        $message_id =$_GET['msg_id'];
        //get all record of current user and without delete status

        //check if job id is valid or not
        foreach ($message_data as $key => $value) {
            if($message_id == $value->ID)
            {
              $status = "vald";
              //get only this message data and pass to action below.
              $message_data = $message_data[$key];
              break;
            }
            else{
                $status = "invalid";
            }
        }
        if($status == "invalid"){
            echo "not a valid job page.";
        }
        
        do_action('cc_messages_display_message',$message_data);

    }
	
	/**
	 * Checks if the given user has any new messages after the given time and if yes then appends it to the list of notifications.
	 * 
	 * @param array $notifications
	 * @param int $user_id
	 * @param string $last_time_checked
	 * @return array
	 */
	function get_notifications_threads( $notifications, $user_id, $last_time_checked ){
		/* check if given user has any messages('pm') after the given time */
		global $wpdb; 
		$query = "SELECT * FROM " . get_cc_threads_table() . " WHERE reciver_id=%d AND msg_date>%s AND type='pm'";
		$query = $wpdb->prepare( $query, $user_id, $last_time_checked );
		
		$messages = $wpdb->get_results( $query );
		if( $messages && !empty( $messages ) ){
			$user_type = 'candidate';
			if( !is_user_a_candidate() ){
				$user_type = 'employer';
			}
			
			/* collective notification for more than one message */
			$notification = array(
				'type'	=> 'message',
				'url'	=> cc_user_dashboard_url($user_type, 'messages', 'others', true),
				'text'	=> 'You have '.count($messages). ' new message(s).'
			);
			
			//instead of displaying multiple notifcation for individual messages, display a collective notification for all messages
			if( count($messages) == 1 ){
				$notification['url'] = add_query_arg( array( 'tid'=>$messages[0]->ID ), $notification['url'] );
			}
			$notifications[] = $notification;
		}
		
		return $notifications;
	}
	
	/**
	 * Checks if the given user has any new message replies after the given time and if yes then appends it to the list of notifications.
	 * 
	 * @param array $notifications
	 * @param int $user_id
	 * @param string $last_time_checked
	 * @return array
	 */
	function get_notifications_replies( $notifications, $user_id, $last_time_checked ){
		global $wpdb;
		$query_replies = "SELECT t.ID AS 'thread_id', t.type, COUNT(r.ID) AS 'replies' FROM " . get_cc_replies_table() . " r JOIN ". get_cc_threads_table() ." t ON r.thread_id=t.ID "
				. " WHERE ( t.sender_id=%d OR t.reciver_id=%d) AND r.sender_id!=%d "
				. " AND r.msg_date>%s GROUP BY r.thread_id ";
		
		$query_replies = $wpdb->prepare( $query_replies, $user_id, $user_id, $user_id, $last_time_checked );
		
		$replies = $wpdb->get_results( $query_replies );
		
		if( $replies && !empty( $replies ) ){	
			$user_type = 'candidate';
			if( !is_user_a_candidate() ){
				$user_type = 'employer';
			}
			
			//instead of displaying multiple notifcation for individual messages, display a collective notification for all messages
			if( count($replies) == 1 ){
				$sub_component = '';
				switch( $replies[0]->type ){
					case 'jobapplication':
						$sub_component = 'job-application';
						break;
					case 'jobappointment':
						$sub_component = 'job-appointment';
						break;
					case 'pm':
					default:
						if( $user_type=='candidate' ){
							$sub_component = 'others';
						}
						else{
							$sub_component = 'sentbox';
						}
						break;
				}
				
				$notification = array(
					'type'	=> 'message',
					'url'	=> cc_user_dashboard_url($user_type, 'messages', $sub_component, true),
					'text'	=> 'You have '. $replies[0]->replies . ' new replies to your message.'
				);
			}
			else{
				$total_replies = 0;
				foreach ($replies as $reply){
					$total_replies += $reply->replies;
				}
				
				/* collective notification for more than one message */
				$notification = array(
					'type'	=> 'message',
					'url'	=> cc_user_dashboard_url($user_type, 'messages'),
					'text'	=> 'You have '. $total_replies . ' new replies to your message.'
				);
			}
			$notifications[] = $notification;
		}
		
		return $notifications;
	}
	
	function ajax_cc_messages_delete(){
	    $message_ids = $_POST["mid"];
	    
	    global $wpdb;

	    $update_query = " delete from ".get_cc_meesage_table()." where ID = $message_ids";
	    $update_query_replies = " delete from ".  get_cc_replies_table()." where thread_id = $message_ids ";


	    $results = $wpdb->query($update_query);
	    $results_replies = $wpdb->query($update_query_replies);
	    
	    die("ok");
	}
	
	function ajax_cc_candidate_messages_delete(){
	    $message_ids = $_POST["mid"];
	    
	    global $wpdb;

	    $update_query = " update ".get_cc_meesage_table()." set status = 'deleted' where ID = $message_ids";
	    $update_query_replies = " update ".  get_cc_replies_table()." set status = 'deleted' where thread_id = $message_ids ";


	    $results = $wpdb->query($update_query);
	    $results_replies = $wpdb->query($update_query_replies);
	    
	    die("ok");
	}
	
}

register_activation_hook( __FILE__, 'emi_members_messages' );
/*
* this method is to create table in database to store message data.
*/

function emi_members_messages(){
    $sql1 ="CREATE TABLE ". get_cc_threads_table() ."(
        ID bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        subject varchar(255),
        message text,
        sender_id bigint(20),
        reciver_id bigint(20),
        type varchar(50) DEFAULT 'pm',
        primary_obj varchar(100),
        secondry_obj varchar(100),
        msg_date datetime,
        status VARCHAR(50) DEFAULT 'pending'
        );";
	
    $sql2 ="CREATE TABLE " . get_cc_replies_table() . "(
        ID bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        thread_id bigint(20),
        message text,
        sender_id bigint(20),
        msg_date datetime,
        status VARCHAR(50) DEFAULT 'pending'
        );";
    
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql1);
    dbDelta( $sql2);
}