<?php
//add_action( 'plugins_loaded', array( 'CCMembersMessagesTemplate', 'get_instance' ) );
class CCMembersMessagesTemplate{
    private static $instance;
	
    public static function get_instance(){
        if(!isset(self::$instance)){
            self::$instance=new self();
        }
        return self::$instance;
    }
	
	private function __construct() {
		add_action( 'cc_print_thread',	array( $this, 'print_appointment_thread' ), 10, 4 );
	}
	
	/**
	 * Prints the html for given type of threads.
	 * 
	 * @param int $user_id get the messages for given user
	 * @param string $user_type
	 * @param string $messages_type the type of messages to be printed.
	 *		e.g: jobapplication|jobappointments|sentbox
	 * 
	 * @return void
	 */
	public function list_threads( $user_id, $user_type, $messages_type ){
		if( $user_type=='employer' ){
			switch( $messages_type ){
				case 'jobappointment':
					$this->_employer_jobappointments( $user_id );
					break;
				
				case 'sentbox':
					$this->_employer_sentbox( $user_id );
					break;
				
				case 'jobapplication':
				default:
					$this->_employer_jobapplications( $user_id );
					break;
			}
		}
		else{
			switch( $messages_type ){
				case 'jobappointment':
					$this->_candidate_jobappointments( $user_id );
					break;

				case 'others':
					$this->_candidate_othermessages( $user_id );
					break;

				case 'jobapplication':
				default:
					$this->_candidate_jobapplications( $user_id );
					break;
			}
		}
	}
	
	/**
	 * Prints the html for given type of threads.
	 * 
	 * @param int $thread_id id of the thread whose details are to be displayed
	 * @param string $user_type
	 * @param string $messages_type the type of messages to be printed.
	 *		e.g: jobapplication|jobappointments|sentbox
	 * 
	 * @return void
	 */
	public function thread_details( $thread_id, $user_type, $message_type ){
		
		//fetch thread details
		global $wpdb;
		$query = " SELECT thread.*, "
				. "sender.user_email AS 'sender_email', sender.display_name AS 'sender_name', "
				. "receiver.user_email AS 'receiver_email', receiver.display_name AS 'receiver_name' "
				
				. "FROM " . get_cc_threads_table() . " thread "
				. "JOIN " . $wpdb->prefix . "users sender ON thread.sender_id=sender.ID "
				. "JOIN " . $wpdb->prefix . "users receiver ON thread.reciver_id=receiver.ID "
				
				. "WHERE thread.ID=%d AND thread.type=%s "
				. "AND ( thread.sender_id=%d OR thread.reciver_id=%d )";
		
		
		$thread_details = $wpdb->get_results( $wpdb->prepare( $query, $thread_id, $message_type, get_current_user_id(), get_current_user_id() ) );
		if( $thread_details && !empty( $thread_details ) ){
			foreach( $thread_details as $thread_detail ){
				//validation complete, lets move on to fetch its replies
				$replies_query = " SELECT * FROM " . get_cc_replies_table() . " WHERE thread_id=%d ORDER BY msg_date ASC";
				$replies = $wpdb->get_results( $wpdb->prepare( $replies_query, $thread_id ) );

				//replies fetched Now move on to print the thread and replies
				//different type of threads will be printed differently
				switch( $message_type ){
					case 'jobappointment':
						$this->_thread_details_appointment( $thread_detail );
						break;
					case 'pm':
						$this->_thread_details_sentbox( $thread_detail );
						break;
					case 'jobapplication':
					default:
						$this->_thread_details_jobapplication( $thread_detail );
						break;
				}

				//print replies - replies for all type of thread will be printed in the same way
				$this->_print_replies( $replies, $thread_detail );

				//print reply form 
				$message_controller = CCMembersMessages::get_instance();
				$message_controller->print_reply_form( $thread_detail );
			}
		}
		else{
			echo "<p class='text-error'>Nothing found!</p>";
		}
	}
	
	/* ++++++++++++++++++++++ appointments ++++++++++++++ */
	protected function _employer_jobappointments( $employer_id ){
		$this->_print_search_form();
	
		/* Bulk action handler */
		$this->employer_action_handler();
		
		$search_message_attr = "";
		if( isset( $_GET["message_filter_button"] ) && ( isset( $_GET["jobname"] ) || isset( $_GET["candidatename"] )  ) ){
		    if( $_GET["candidatename"] != "" ){
			$search_message_attr .= " and u.display_name like '%".esc_attr( $_GET["candidatename"] )."%' ";
		    }
		    if( $_GET["jobname"] != "" ){
			$search_message_attr .= " and  coalesce( p.post_title, '' ) like '%".esc_attr( $_GET["jobname"] )."%' ";
		    }
		}
		
		$current_page = isset( $_GET['list'] ) ? (int)$_GET['list'] : 1;
		$per_page = 10;
		$lower_limit = ( $per_page * $current_page ) - $per_page;
		global $wpdb;
		
		$hide_user_id_str = $this->get_hide_user_id_string();
		$hide_profile_attr = $hide_user_id_str != "" ? "AND t.reciver_id NOT IN ( $hide_user_id_str )" : "";

		
		$main_query  = "SELECT t.*, coalesce( MAX(r.msg_date), t.msg_date ) AS 'last_date', coalesce( COUNT(r.ID), 0 ) AS 'replies', 
						a.app_date, a.app_time, 
						u.display_name AS 'candidate_name', 
						coalesce( p.post_title, '' ) AS 'job_title' 
						FROM " . get_cc_threads_table() . " t 
						JOIN " . $wpdb->prefix . "employer_appointment_details a ON t.primary_obj=a.id 
						JOIN " . $wpdb->prefix ."users u ON t.reciver_id=u.ID 
						LEFT JOIN ". get_cc_replies_table() ." r ON t.ID=r.thread_id 
						LEFT JOIN ". $wpdb->prefix ."posts p ON a.job_id=p.ID 

						WHERE t.type='jobappointment' AND t.sender_id=$employer_id
						$hide_profile_attr
						".$search_message_attr."
						GROUP BY t.ID 
						ORDER BY last_date DESC";
		

		$total_messages = $wpdb->get_results( $main_query );
		$total_posts = count($total_messages) > 0 ? count($total_messages) : 0;
		
		$messages = $wpdb->get_results( $main_query." LIMIT $lower_limit, $per_page" );
		if( $messages && !empty( $messages ) ){
			$retrieved_thread_ids = array();
			$thread_unread_count = array();

			//first loop to only pull out thread ids
			foreach( $messages as $message ){
				$retrieved_thread_ids[] = $message->ID;
				$thread_unread_count[$message->ID] = 0;
			}

			//GET UNREAD message count for all threads
			$unread_count_query = "SELECT t.ID, coalesce( COUNT(r.ID), 0 ) AS 'unread' 
									FROM ".get_cc_threads_table()." t LEFT JOIN ".get_cc_replies_table()." r ON r.thread_id=t.ID 
									WHERE r.sender_id !=%d AND r.status='pending' 
									AND t.ID IN ( ". implode(',', $retrieved_thread_ids) ." )
									GROUP BY t.ID ";
			$unread_results = $wpdb->get_results( $wpdb->prepare( $unread_count_query, $employer_id ) );
			if( $unread_results && !empty($unread_results) ){
				foreach( $unread_results as $unread_result ){
					$thread_unread_count[ $unread_result->ID ] = $unread_result->unread;
				}
			}

			//now print the threads data 
			echo '<form class="bulk_action_form" action="" method="post" >';
			    echo "<table class='threads threads-jobappointment table '>";
			    ?>
			    <tr><td>
			    <div class="bulk_action_wrapper" style="margin:15px 0px 15px 0px;float: right;" >
				    <div class="input-group maxwidth200">
					    <select name="bulk_action_drop_down" class="" >
						    <option value="" >--Bulk Actions--</option>
						    <option value="delete" >Delete</option>
						    <option value="read" >Mark read</option>
					    </select>
					    <span class="input-group-btn">
						    <input type="submit" class="button button-small" name="action_apply_button" id="action_apply_button" value="Apply"  style="margin-left:4px;" />
					    </span>
				    </div><!-- /input-group -->
			    </div>
			    </td></tr>
			    <?php
			    foreach( $messages as $message ){
				    $this->_print_appointment_loop( $message, 'jobappointment', $thread_unread_count[$message->ID], $employer_id );
			    }
			    echo "</table><!-- .threads -->";
				?>
				<div class="action_button_container pagination-nomargin">
					<div class="row clear">
						<div class="col span_6">
							<?php emi_generate_paging_param( $total_posts , $per_page, $current_page, "employer-dashboard", 2 ); ?>
						</div>
					</div>
				</div><!-- .action_button_container -->

			    <?php
			echo '</form>';
		}
		else{
			echo 'Nothing found!';
		}
	}
	
	protected function _print_appointment_loop( $message, $thread_type, $thread_unread_count, $employer_id ){
		?>
		<tr id='thread-<?php echo $message->ID;?>' class='<?php if( $thread_unread_count > 0 ){echo "warning";} ?>'>
			<td>
			    <span><input type="checkbox" name="message_bulk_action_checkbox[]" value="<?php echo $message->ID; ?>" /></span>
			<?php 
			$base_link = cc_user_dashboard_url( 'employer', 'messages', 'job-appointment' );
			$single_msg_url = add_query_arg( 'tid', $message->ID, $base_link );

			$candidate_name = $message->candidate_name;
			?>
			<h3  style="display:inline;"  ><a href="<?php echo $single_msg_url;?> " title='view details'>
				Appointment scheduled with <strong><?php echo $candidate_name;?></strong>.
			</a></h3>
			<div class='thread-meta clear'>
				<ul class='inline clear'>
					<li><span class='label label-default'>Scheduled : <?php echo date( 'd M \'y', strtotime($message->app_date) ) . ' ' . $message->app_time;?></span></li>
					<?php if( $message->job_title ):?>
						<li><span class='label label-default'><?php echo stripslashes($message->job_title);?></span></li>
					<?php endif;?>
					<?php /*<li><span>Added on: </span><?php echo date( 'd M \'y g:i A', strtotime( $message->msg_date ) );?></li>*/?>
				</ul>
			</div>
			<div class="thread-actions clear">
				( <?php echo $message->replies;?> ) Replies. 
				<?php if( $thread_unread_count > 0 ): ?>
					<span class="badge">( <?php echo $thread_unread_count;?> )</span> New
				<?php endif;?>
				<ul class='inline pull-right clear'>
					<li><a class="btn btn-xs btn-default" href='<?php echo $single_msg_url;?>' title="view details">Details</a></li>
					<li>
					    <a id="<?php echo $message->ID ?>" class="glyphicon glyphicon-remove show-tooltip delete_thread_link" title="delete"  ></a>
					</li>
				</ul>
			</div>
			</td>
		</tr>
		<?php 
	}

	protected function _thread_details_appointment( $thread_details ){
	    
	    global $wpdb;
	    $appointment_id = $thread_details->primary_obj;
	    $appointment_details = $wpdb->get_results("select * from ".$wpdb->prefix."employer_appointment_details where id = ".$appointment_id);
	    $app_date = date( "F j, Y, g:i a", strtotime( $appointment_details[0]->app_date ) );
	    $app_time = $appointment_details[0]->app_time;
	    
	    $user_id = get_current_user_id();
	    $sender_id = $thread_details->sender_id;
	    $reciver_id = $thread_details->reciver_id;
	    $message_date = date( "F j, Y, g:i a", strtotime($thread_details->msg_date) );
	    $sender_details = new WP_User($sender_id);
	    $reciver_details = new WP_User($reciver_id);
	    $employer_name = $sender_details->display_name;
	    
	    $job_details = array();
	    
	    if( isset( $appointment_details[0]->job_id ) && is_numeric($appointment_details[0]->job_id) ){
		    $job_details_query = new WP_Query( array( 'post_type' => 'job_listing', 'p'=>$appointment_details[0]->job_id ) );
		    if( $job_details_query->have_posts() ){
			    while( $job_details_query->have_posts() ){
				    $job_details_query->the_post();
				    $job_details['title'] = get_the_title();
				    $job_details['link'] = get_permalink();
				    $job_details['id'] = get_the_ID();
			    }
		    }
		    wp_reset_postdata();
	    }
	    
	    $member_name = "";
	    if( is_user_a_candidate( $reciver_details ) ){
		$candidate_name = '';
		//only premium users should see candidate's name
		if( can_view_candidate_info( $reciver_details, 'name' ) ){
			$candidate_name .= $reciver_details->display_name;
		}
		$member_name = $candidate_name;
	    }
	    
	    ?>
		
		<?php if( $user_id == $sender_id ) : ?>
			<div class="singlle_thred_details" >
				<div>
					<strong>To: </strong>
					<span class="btn-group">
						<a href="<?php echo get_site_url() . '/candidates/'.$reciver_id;?>" type="button" class="btn btn-default"><?php echo $member_name; ?></a>
						<a class="btn btn-default btn-small dropdown-toggle" href="#" data-toggle="dropdown"><span class="caret"></span></a>

						<ul class="dropdown-menu" role="menu">
							<li><a href="<?php echo get_site_url()."/candidates/".$reciver_id; ?>">View Profile</a></li>
							
							<?php 
							$candidate_resume_url = cc_members_get_resume_url( $reciver_id );
							if( $candidate_resume_url ) : ?>
							<li class="divider"></li>
							<li><a href="<?php echo $candidate_resume_url; ?>" >View Resume</a></li>
							<?php endif; ?>
						</ul>
					</span><!-- /btn-group -->
					<?php echo get_candidate_tag_dropdown( $reciver_id, $appointment_details[0]->job_id ); ?>
					<span class="candidate_tag" >
					    <?php get_current_candidate_tag( $reciver_id, $appointment_details[0]->job_id );?>
					</span>
					<br/>
					<strong>Sent: </strong><?php echo $message_date; ?>
					<p><strong>Appointment Date: </strong><?php echo date( "F j, Y", strtotime( $app_date ) ) .' <strong>'. $app_time . '</strong>'; ?></p>
					<?php if( isset( $job_details['title'] ) && isset( $job_details['link'] ) ):?>
						<span class='label label-default'>Appointed for</span> 
						<a href='<?php echo $job_details['link'];?>' title='view job details'><?php echo $job_details['title'];?></a>
					<?php endif;?>
					
					<?php /* if( isset( $job_details['title'] ) && isset( $job_details['link'] ) ):?>
						<br/><span class='label label-default'>Applied for</span> 
						<a href='<?php echo $job_details['link'];?>' title='view job details'><?php echo $job_details['title'];?></a>
					<?php endif;*/?>
				</div>
				
				<strong>About the candidate</strong>
				<ul class="inline clear">
					<li><span class="label label-default">Highest Qualification</span> : <?php echo $reciver_details->get('education'); ?></li>
					<li>
						<span class="label label-default">Position</span> : 
						<?php 
							$candiate_positions = $reciver_details->get('position');
							if( isset( $candiate_positions ) && !empty( $candiate_positions ) ) {
								echo implode(", ", $candiate_positions);
							}
						?>
					</li>
					<li><span class="label label-default">Webcam</span> : <?php echo $reciver_details->get('webcam_check'); ?></li>
					
					
				</ul>
				<?php if( $thread_details->message ):?>
					<p class='thread-message'><?php echo $thread_details->message; ?></p>
				<?php endif;?>
			</div>
		<?php else :?>
			<div class="singlle_thred_details" >
				<div>
					<strong>From: </strong>
					<a href="<?php echo get_site_url() . '/employers/'.$sender_id;?>" type="button" title='view profile'><?php echo $employer_name; ?></a>
					<br/>
					<strong>Received: </strong><?php echo $message_date; ?>
					<p><strong>Appointment Date: </strong><?php echo date( "F j, Y", strtotime( $app_date ) ) .' <strong>'. $app_time . '</strong>'; ?></p>
					<?php if( isset( $job_details['title'] ) && isset( $job_details['link'] ) ):?>
						<br/><span class='label label-default'>Applied for</span> 
						<a href='<?php echo $job_details['link'];?>' title='view job details'><?php echo $job_details['title'];?></a>
					<?php endif;?>
				</div>
				<?php if( $thread_details->message ):?>
					<p class='thread-message'><?php echo stripslashes( $thread_details->message ); ?></p>
				<?php endif;?>
			</div>
		
		<?php endif; ?>
		
	    <?php
	}
	/* ================= appointments ================== */
	
	/* +++++++++++++++++++ job applications ++++++++++++++ */
	protected function _employer_jobapplications( $employer_id ){
	    $this->_print_search_form();
	    
	    /* Bulk action handler */
	    $this->employer_action_handler();
	    
	    $search_message_attr = "";
	    if( isset( $_GET["message_filter_button"] ) && ( isset( $_GET["jobname"] ) || isset( $_GET["candidatename"] )  ) ){
			if( $_GET["candidatename"] != "" ){
				$search_message_attr .= " and u2.display_name like '%".esc_attr( $_GET["candidatename"] )."%' ";
			}
			if( $_GET["jobname"] != "" ){
				$search_message_attr .= " and  coalesce( p.post_title, '' ) like '%".esc_attr( $_GET["jobname"] )."%' ";
			}
	    }
	    
	    $current_page = isset( $_GET['list'] ) ? (int)$_GET['list'] : 1;
	    $per_page = 10;
	    $lower_limit = ( $per_page * $current_page ) - $per_page;
	    global $wpdb;
	    
	    $hide_user_id_str = $this->get_hide_user_id_string();
	    $hide_profile_attr = $hide_user_id_str != "" ? "AND t.sender_id NOT IN ( $hide_user_id_str )" : "";
	    
	    $main_query  = "SELECT t.*, coalesce( MAX(r.msg_date), t.msg_date ) AS 'last_date', coalesce( COUNT(r.ID), 0 ) AS 'replies', 
					    u2.display_name AS 'candidate_name', 
					    coalesce( p.post_title, '' ) AS 'job_title' 
					    FROM " . get_cc_threads_table() . " t 
					    JOIN " . $wpdb->prefix ."users u ON t.reciver_id=u.ID 
					    JOIN " . $wpdb->prefix ."users u2 ON t.sender_id=u2.ID
					    LEFT JOIN ". get_cc_replies_table() ." r ON t.ID=r.thread_id 
					    LEFT JOIN ". $wpdb->prefix ."posts p ON t.primary_obj=p.ID 
					    WHERE t.type='jobapplication' 
					    AND t.reciver_id=$employer_id 
					    $hide_profile_attr
					    ".$search_message_attr."
					    GROUP BY t.ID 
					    ORDER BY last_date DESC";

	    $total_messages = $wpdb->get_results( $main_query );
	    $total_posts = count($total_messages) > 0 ? count($total_messages) : 0;
	    
	    $temp_query = $main_query." LIMIT $lower_limit, $per_page";
	    $messages = $wpdb->get_results( $temp_query );
	    if( $messages && !empty( $messages ) ){
		    $retrieved_thread_ids = array();
		    $thread_unread_count = array();

		    //first loop to only pull out thread ids
		    foreach( $messages as $message ){
			    $retrieved_thread_ids[] = $message->ID;
			    $thread_unread_count[$message->ID] = 0;
		    }

		    //GET UNREAD message count for all threads
		    $unread_count_query = "SELECT t.ID, coalesce( COUNT(r.ID), 0 ) AS 'unread' 
								    FROM ".get_cc_threads_table()." t LEFT JOIN ".get_cc_replies_table()." r ON r.thread_id=t.ID 
								    WHERE r.sender_id!=%d AND r.status='pending' 
								    AND t.ID IN ( ". implode(',', $retrieved_thread_ids) ." )
								    GROUP BY t.ID ";

		    $unread_results = $wpdb->get_results( $wpdb->prepare( $unread_count_query, $employer_id ) );
		    if( $unread_results && !empty($unread_results) ){
			    foreach( $unread_results as $unread_result ){
				    $thread_unread_count[ $unread_result->ID ] = $unread_result->unread;
			    }
		    }
		    echo '<form class="bulk_action_form" action="" method="post" >';
			//now print the threads data 
			echo "<table class='threads threads-jobappointment table '>";
			
			?>
			<tr><td>
			<div class="bulk_action_wrapper" style="margin:15px 0px 15px 0px;float: right;" >
				<div class="input-group maxwidth200">
					<select name="bulk_action_drop_down" class="" >
						<option value="" >--Bulk Actions--</option>
						<option value="delete" >Delete</option>
						<option value="read" >Mark read</option>
					</select>
					<span class="input-group-btn" >
						<input type="submit" class="button button-small" name="action_apply_button" id="action_apply_button" value="Apply"  style="margin-left:4px;" />
					</span>
				</div><!-- /input-group -->
			</div>
			</td></tr>
			<?php
			
			foreach( $messages as $message ){
				$this->_print_application_loop( $message, 'jobapplication', $thread_unread_count[$message->ID], $employer_id );
			}
			echo "</table><!-- .threads -->";
			?>
			<div class="action_button_container pagination-nomargin">
				<div class="row clear">
					<div class="col span_6">
						<?php emi_generate_paging_param( $total_posts , $per_page, $current_page, "employer-dashboard", 2 ); emi_gene ?>
					</div>
				</div>
			</div><!-- .action_button_container -->
			<?php
		    echo '</form>';
	    }
	    else{
		    echo 'Nothing found!';
	    }
	}
	
	protected function _print_application_loop( $message, $thread_type, $thread_unread_count, $employer_id ){
	    ?>
	    <tr id='thread-<?php echo $message->ID;?>' class='<?php if( $thread_unread_count || $message->status == 'pending' ){ echo 'warning'; }?>'>
		<td>
		    <span><input type="checkbox" name="message_bulk_action_checkbox[]" value="<?php echo $message->ID; ?>" /></span>
		    <?php 
		    $base_link = cc_user_dashboard_url( 'employer', 'messages', 'job-application' );
		    $single_msg_url = add_query_arg( 'tid', $message->ID, $base_link );

		    $candidate_name = $message->candidate_name;
		    ?>
		    <h3 style="display:inline;" >
			<a href="<?php echo $single_msg_url;?> " title='view details'>
				Job Application by <strong><?php echo $candidate_name;?></strong>.
			</a>
		    </h3>
		    <span class="tag_candidate" >
			<?php get_current_candidate_tag( $message->sender_id, $message->primary_obj ); ?>
		    </span>
		    <div class='thread-meta clear'>
			    <ul class='inline clear'>
				    <li><span class='label label-default'>Applied on : <?php echo date( 'd M \'y h:i A', strtotime($message->msg_date) );?></span></li>
				    <?php if( $message->job_title ):?>
					    <li><span class='label label-default'><?php echo stripslashes($message->job_title);?></span></li>
				    <?php endif;?>
			    </ul>
		    </div>
		    <div class="thread-actions clear">
			    ( <?php echo $message->replies;?> ) Replies. 
			    <?php if( $thread_unread_count > 0 ): ?>
				    <span class="badge">( <?php echo $thread_unread_count;?> )</span> New
			    <?php endif;?>
			    <ul class='inline pull-right clear'>
					<li><a class="btn btn-xs btn-default" href='<?php echo $single_msg_url;?>' title="view details">Details</a></li>
					<li>
					    <a id="<?php echo $message->ID ?>" class="glyphicon glyphicon-remove show-tooltip delete_thread_link" title="delete"  ></a>
					</li>
			    </ul>
		    </div>
		</td>
	</tr>
	<?php 
    }

	protected function _thread_details_jobapplication( $thread_detail ){
	    //echo "<p>Message from Candidate : ".$thread_detail->message."</p>";
	    $user_id = get_current_user_id();
	    $sender_id = $thread_detail->sender_id;
	    $reciver_id = $thread_detail->reciver_id;
	    $message_date = date( "F j, Y, g:i a", strtotime($thread_detail->msg_date) );
	    $webcam = get_user_meta( $sender_id , "webcam_check", true);
	    $education = get_user_meta( $sender_id , "education", true);
	    $position = get_user_meta( $sender_id , "position", true);
	    $position_str = implode(",", $position);
	    $sender_details = new WP_User($sender_id);
	    $reciver_details = new WP_User($reciver_id);
	    $employer_name = $reciver_details->display_name;
	    
		$job_details = array();
		if( isset( $thread_detail->primary_obj ) && is_numeric($thread_detail->primary_obj) ){
			$job_details_query = new WP_Query( array( 'post_type' => 'job_listing', 'p'=>$thread_detail->primary_obj ) );
			if( $job_details_query->have_posts() ){
				while( $job_details_query->have_posts() ){
					$job_details_query->the_post();
					$job_details['title'] = get_the_title();
					$job_details['link'] = get_permalink();
					$job_details['id'] = get_the_ID();
				}
			}
			wp_reset_postdata();
		}
		
	    $member_name = "";
	    if( is_user_a_candidate( $sender_details ) ){
			$candidate_name = '';
			//only premium users should see candidate's name
			if( can_view_candidate_info( $sender_details, 'name' ) ){
				$candidate_name .= $sender_details->display_name;
			}
			$member_name = $candidate_name;
	    }
	    
	    if( $user_id == $sender_id ) : ?>
			<div class="singlle_thred_details" >
				<div>
					<strong>To: </strong>
					<a href="<?php echo get_site_url() . '/employers/'.$reciver_id;?>" type="button" title='view profile'><?php echo $employer_name; ?></a>
					<br/>
					<strong>Sent: </strong><?php echo $message_date; ?>
					<?php if( isset( $job_details['title'] ) && isset( $job_details['link'] ) ):?>
						<br/><span class='label label-default'>Applied for</span> 
						<a href='<?php echo $job_details['link'];?>' title='view job details'><?php echo $job_details['title'];?></a>
					<?php endif;?>
				</div>
				<?php if( $thread_detail->message ):?>
					<p class='thread-message'><?php echo $thread_detail->message; ?></p>
				<?php endif;?>
			</div>
	
	    <?php else: ?>
	
		<div class="singlle_thred_details" >
		    <div>
				<strong>From: </strong>
				<span class="btn-group">
					<a href="<?php echo get_site_url() . '/candidates/'.$sender_id;?>" type="button" class="btn btn-default"><?php echo $member_name; ?></a>
					<a class="btn btn-default btn-small dropdown-toggle" href="#" data-toggle="dropdown"><span class="caret"></span></a>
				
					<ul class="dropdown-menu" role="menu">
						<li><a href="<?php echo get_site_url()."/candidates/".$sender_id; ?>">View Profile</a></li>
						
						<?php 
						$candidate_resume_url = cc_members_get_resume_url( $sender_id );
						if( $candidate_resume_url  ) : ?>
						<li class="divider"></li>
						<li>
						    <a href="<?php echo $candidate_resume_url; ?>" >View Resume</a>
						</li>
						<?php endif; ?>
					</ul>
				</span><!-- /btn-group -->
				<span class="view_candidate_dashboard_link" >
				    <a href="<?php echo get_site_url(); ?>/employer-dashboard/?component=appointments&subcomponent=add&member_id=<?php echo $sender_id; ?>" >
					<button type="button" class="btn btn-default dropdown-toggle show-tooltip"><span class="">Add Appointment</span></button>
				    </a>
				</span>
				<span>
				    <?php echo get_candidate_tag_dropdown( $sender_id, $job_details['id'] ); ?>
				</span>
				<span class="candidate_tag" >
				    <?php get_current_candidate_tag( $sender_id, $job_details['id'] );?>
				</span>
				<br/>
				<strong>Received: </strong><?php echo $message_date; ?>
				<?php if( isset( $job_details['title'] ) && isset( $job_details['link'] ) ):?>
					<br/><span class='label label-default'>Applied for</span> 
					<a href='<?php echo $job_details['link'];?>' title='view job details'><?php echo $job_details['title'];?></a>
				<?php endif;?>
		    </div>
			
			<br/>
			<strong>About the candidate</strong>
			<ul class="inline clear">
				<li><span class="label label-default">Highest Qualification</span> : <?php echo $education; ?></li>
				<li><span class="label label-default">Position</span> : <?php echo $position_str; ?></li>
				<li><span class="label label-default">Webcam</span> : <?php echo $webcam; ?></li>
			</ul>
			<?php if( $thread_detail->message ):?>
				<p class='thread-message'><?php echo stripslashes( $thread_detail->message ); ?></p>
			<?php endif;?>
		</div>
		
	    <?php endif;
	}
	/* ================= job applications ================== */
	
	/* +++++++++++++++++++ sentbox ++++++++++++++ */
	
	protected function _employer_sentbox( $employer_id ){
	    
	    /* Bulk action handler */
	    $this->employer_action_handler();
	    
	    $current_page = isset( $_GET['list'] ) ? (int)$_GET['list'] : 1;
	    $per_page = 10;
	    $lower_limit = ( $per_page * $current_page ) - $per_page;
	    
	    $hide_user_id_str = $this->get_hide_user_id_string();
	    $hide_profile_attr = $hide_user_id_str != "" ? "AND t.reciver_id NOT IN ( $hide_user_id_str )" : "";
	    
	    global $wpdb;
	    $main_query  = "SELECT t.*, coalesce( MAX(r.msg_date), t.msg_date ) AS 'last_date', coalesce( COUNT(r.ID), 0 ) AS 'replies', 
					    u.display_name AS 'candidate_name' 
					    FROM " . get_cc_threads_table() . " t 
					    JOIN " . $wpdb->prefix ."users u ON t.reciver_id=u.ID 
					    LEFT JOIN ". get_cc_replies_table() ." r ON t.ID=r.thread_id 
					    WHERE t.type='pm'
					    $hide_profile_attr
					    AND t.sender_id=%d 
					    GROUP BY t.ID 
					    ORDER BY last_date DESC"; 

	    $total_messages = $wpdb->get_results( $wpdb->prepare( $main_query, $employer_id ) );
	    $total_posts = count($total_messages) > 0 ? count($total_messages) : 0;
	    
	    $temp_query = $wpdb->prepare( $main_query." LIMIT $lower_limit, $per_page", $employer_id );
	    $messages = $wpdb->get_results( $temp_query);

	    if( $messages && !empty( $messages ) ){
		    $retrieved_thread_ids = array();
		    $thread_unread_count = array();

		    //first loop to only pull out thread ids
		    foreach( $messages as $message ){
			    $retrieved_thread_ids[] = $message->ID;
			    $thread_unread_count[$message->ID] = 0;
		    }
		    $unread_results = array();
		    //GET UNREAD message count for all threads
		    $unread_count_query = "SELECT t.ID, coalesce( COUNT(r.ID), 0 ) AS 'unread' 
								    FROM ".get_cc_threads_table()." t LEFT JOIN ".get_cc_replies_table()." r ON r.thread_id=t.ID 
								    WHERE r.sender_id!=%d AND r.status='pending' 
								    AND t.ID IN ( ". implode(',', $retrieved_thread_ids) ." )
								    GROUP BY t.ID ";
		    $unread_results = $wpdb->get_results( $wpdb->prepare( $unread_count_query, $employer_id ) );
			 
		    if( $unread_results && !empty($unread_results) ){
			    foreach( $unread_results as $unread_result ){
				    $thread_unread_count[ $unread_result->ID ] = $unread_result->unread;
			    }
		    }

		    echo '<form class="bulk_action_form" action="" method="post" >';
			//now print the threads data 
			echo "<table class='threads threads-sentbox table '>";
			?>
			<tr><td>
			<div class="bulk_action_wrapper" style="margin:15px 0px 15px 0px;float: right;" >
				<div class="input-group maxwidth200">
					<select name="bulk_action_drop_down" class="" >
						<option value="" >--Bulk Actions--</option>
						<option value="delete" >Delete</option>
						<option value="read" >Mark read</option>
					</select>
					<span class="input-group-btn">
						<input type="submit" class="button button-small" name="action_apply_button" id="action_apply_button" value="Apply"  style="margin-left:4px;" />
					</span>
				</div><!-- /input-group -->
			</div>
			</td></tr>
			<?php
			foreach( $messages as $message ){
				$this->_print_sentbox_loop( $message, 'sentbox', $thread_unread_count[$message->ID], $employer_id );
			}
			echo "</table><!-- .threads -->";
			?>
			<div class="action_button_container pagination-nomargin">
				<div class="row clear">
					<div class="col span_6">
						<?php emi_generate_paging_param( $total_posts , $per_page, $current_page, "employer-dashboard", 2 ); ?>
					</div>
				</div>
			</div><!-- .action_button_container -->
			<?php
		    echo '</form>';
	    }
	    else{
		    echo 'Nothing found!';
	    }
	}
	
	protected function _print_sentbox_loop( $message, $thread_type, $thread_unread_count, $employer_id ){
	    ?>
	    <tr id='thread-<?php echo $message->ID;?>' class='<?php if( $thread_unread_count > 0 ){echo "warning";} ?>'>

		    <td>
			<span><input type="checkbox" name="message_bulk_action_checkbox[]" value="<?php echo $message->ID; ?>" /></span>
		    <?php 

		    $base_link = cc_user_dashboard_url( 'employer', 'messages', 'sentbox' );
		    $single_msg_url = add_query_arg( 'tid', $message->ID, $base_link );

		    $candidate_name = $message->candidate_name;
		    ?>
		    <h3 style="display:inline;" ><a href="<?php echo $single_msg_url;?> " title='view details'>
			     <?php echo stripslashes( $message->subject ) ." - "?> <strong><?php echo $candidate_name;?></strong>.
		    </a></h3>
		    <span><?php get_current_candidate_tag( $message->reciver_id );?></span>
		    <div class='thread-meta clear'>
			    <ul class='inline clear'>
				    <li><span class='label label-default'>Sent on : <?php echo date( 'd M \'y', strtotime($message->app_date) ) . ' ' . $message->app_time;?></span></li>
			    </ul>
		    </div>
		    <div class="thread-actions clear">
			    ( <?php echo $message->replies;?> ) Replies. 
			    <?php if( $thread_unread_count > 0 ): ?>
				    <span class="badge">( <?php echo $thread_unread_count;?> )</span> New
			    <?php endif;?>
			    <ul class='inline pull-right clear'>
				    <li><a class="btn btn-xs btn-default" href='<?php echo $single_msg_url;?>' title="view details">Details</a></li>
				    <li>
					<a id="<?php echo $message->ID ?>" class="glyphicon glyphicon-remove show-tooltip delete_thread_link" title="delete"  ></a>
				    </li>
				    
			    </ul>
		    </div>
		    </td>
	    </tr>
	    <?php 
	}

	protected function _thread_details_sentbox( $thread_details ){
	    $message_date = date( "F j, Y, g:i a", strtotime($thread_details->msg_date) );
	    $employer_name = $thread_details->sender_name;
	    
	    $candidate_name = '';
	    if( can_view_candidate_info( $thread_details->reciver_id, 'name' ) ){
			$candidate_name .= $thread_details->receiver_name;
	    }
	    
	    ?>
	    <div class="singlle_thred_details" >
			<?php if( get_current_user_id() == $thread_details->sender_id ) : ?>
			    <div>
				<strong>To: </strong>
				<span class="btn-group">
					<a href="<?php echo get_site_url() . '/candidates/'.$thread_details->reciver_id;?>" type="button" class="btn btn-default"><?php echo $candidate_name; ?></a>
					<a class="btn btn-default btn-small dropdown-toggle" href="#" data-toggle="dropdown"><span class="caret"></span></a>

					<ul class="dropdown-menu" role="menu">
						<li><a href="<?php echo get_site_url()."/candidates/".$thread_details->reciver_id; ?>">View Profile</a></li>
						
						<?php 
						$candidate_resume_url = cc_members_get_resume_url( $sender_id );
						if( $candidate_resume_url ) : ?>
						<li class="divider"></li>
						<li>
						    <a href="<?php echo $candidate_resume_url; ?>" >View Resume</a>
						</li>
						<?php endif; ?>
					</ul>
				</span><!-- /btn-group -->
				<?php echo get_candidate_tag_dropdown( $thread_details->reciver_id ); ?>
				<span class="candidate_tag" >
				    <?php get_current_candidate_tag( $thread_details->reciver_id );?>
				</span>
				<br/>
				
			    </div>
				
				
				<strong>Sent: </strong><?php echo $message_date; ?><br/>

			<?php else: ?>
				<strong>From: </strong><a href='<?php echo home_url('/employers'). '/'. $thread_details->sender_id;?>/'><?php echo $thread_details->sender_name; ?></a><br/>
				<strong>Received: </strong><?php echo $message_date; ?><br/>
			<?php endif; ?>
			
			<p class="thread-message"><?php echo stripslashes( $thread_details->message ) ?></p>
	    </div>
	    <?php
	}
	
	/* ================= sentbox ================== */
	
	/* ++++++++++++++++ replies ++++++++++++++++++++++++ */
	protected function _print_replies( $replies, $thread_details ){
		$loggedin_user_id = get_current_user_id();
		
		
		$this->update_top_level_message_status( $loggedin_user_id, $thread_details->ID );
		
		$my_name = ( ( $loggedin_user_id==$thread_details->sender_id ) ? $thread_details->sender_name : $thread_details->receiver_name );
		$other_person_id = ( ( $loggedin_user_id==$thread_details->sender_id ) ? $thread_details->reciver_id : $thread_details->sender_id );
		
		$other_persons_name = '';
		if( is_user_a_candidate( $other_person_id ) ){
			if(can_view_candidate_info($other_person_id, 'display_name') ){
				$other_persons_name = ( ( $loggedin_user_id==$thread_details->sender_id ) ? $thread_details->receiver_name : $thread_details->sender_name );
			}
			else{
				$other_persons_name = 'Candidate ' . $other_person_id;
			}
		}
		else{
			$other_persons_name = ( ( $loggedin_user_id==$thread_details->sender_id ) ? $thread_details->receiver_name : $thread_details->sender_name );
		}
		
		$sender_avatar = get_avatar( $thread_details->sender_email, 30 );
		$reciver_avatar = get_avatar( $thread_details->receiver_email, 30 );
		
		if( $replies && !empty( $replies ) ){
			echo '<ul class="list-unstyled thread-replies" id="thread-replies-list">';
				
				foreach( $replies as $reply ){
					/* Here i will change the status of replies to read */
					$this->update_message_rply_status( $reply->ID, $loggedin_user_id );
					
					$class = 'reply-'.$reply->ID;
					$class .= ' sender-' . ( ( $reply->sender_id==$loggedin_user_id ) ? 'me' : 'other' );
					
					echo "<li class='$class'>";
						echo "<div class='row clear'>";
							echo "<div class='user'>";
								echo ( $reply->sender_id==$thread_details->sender_id ) ? $sender_avatar : $reciver_avatar;
							echo "</div>";
							echo "<div class='message'>";
								echo "<strong>" . ( ( $reply->sender_id == $loggedin_user_id ) ? $my_name : $other_persons_name ) . "</strong> | ";
								echo "<i>" . date( 'd M \'y g:i A', strtotime( $reply->msg_date ) ) . "</i><hr/>";
								echo "<p class='message-text'>" . stripslashes($reply->message). "</p>";
							echo "</div>";
						echo "</div>";
					echo "</li>";
				}
			
			echo "</ul>";
		}
	}
	
	protected function update_message_rply_status( $reply_id, $loggedin_user_id ){
	    global $wpdb;
	    $table_name = $wpdb->prefix."cc_messages_replies";
	    $wpdb->query("update ".$wpdb->prefix."cc_messages_replies set status='read' where ID = $reply_id and sender_id != ".$loggedin_user_id );
	}
	protected function update_top_level_message_status( $loggedin_user_id, $thread_id ){
	    global $wpdb;
	    $table_name = $wpdb->prefix."cc_messages_threads";
	    $wpdb->query("update ".$wpdb->prefix."cc_messages_threads set status='read' where ID = ".$thread_id." and  reciver_id = ".$loggedin_user_id );
	}
	
	/* ================ replies ======================== */
	
	protected function _print_search_form(){
		$membership_details = member_package_details();
		?>
		<form method='GET' >
			<input type='hidden' name='component' value='messages' />
			<input type='hidden' name='subcomponent' value='<?php echo cc_user_dashboard_current_subcomponent(); ?>' />
			
			<div class='text-info messages-search'>
				
				<div class='row clear'>
					<div class='col <?php echo $membership_details['package']=='free' ? 'span_6' : 'span_4';?>'>
						<label for='jobname'>Search by job name</label>
						<input type='text' id='jobname' name='jobname' value="<?php echo isset( $_GET['jobname'] ) ? esc_attr( $_GET['jobname'] ) : '';?>" />
					</div>

					<?php if ( $membership_details['package']!='free'):?>
						<div class='col span_4'>
							<label for='candiatename'>Search by candidate name</label>
							<input type='text' id='candidatename' name='candidatename' value="<?php echo isset( $_GET['candidatename'] ) ? esc_attr( $_GET['candidatename'] ) : '';?>" />
						</div>
					<?php endif;?>

					<div class='col <?php echo $membership_details['package']=='free' ? 'span_6' : 'span_4';?> text-right'>
						<input type='submit' class='button button-small' name="message_filter_button" value='search' />
					</div>
				</div>
				
			</div>
		</form>
		<?php 
	}
	
	protected function employer_action_handler(){
	    /* Bulk action handler will write here */
	    if( isset( $_POST["action_apply_button"] ) ){
		if( isset( $_POST["bulk_action_drop_down"] ) && $_POST["bulk_action_drop_down"] != "" && isset( $_POST["message_bulk_action_checkbox"] ) ){
		    $message_id_arr = $_POST["message_bulk_action_checkbox"];
		    $message_ids = implode(",", $message_id_arr);

		    global $wpdb;
		    $update_query_replies = "";
		    if( $_POST["bulk_action_drop_down"] == "delete" ){
			
			$update_query = " delete from ".get_cc_meesage_table()." where ID = $message_ids";
			$update_query_replies = " delete from ".  get_cc_replies_table()." where thread_id = $message_ids ";

		    }else if( $_POST["bulk_action_drop_down"] == "read" ){

			$update_query = " update ".get_cc_meesage_table()." set status = 'read' where ID in ($message_ids)";
			$update_query_replies = " update ".  get_cc_replies_table()." set status = 'read' where thread_id in ($message_ids) ";

		    }
		    
		    $results = $wpdb->query($update_query);
		    $results_replies = $wpdb->query($update_query_replies);
		}
	    }
	}

	protected function candidate_action_handler(){
	    /* Bulk action handler will write here */
	    if( isset( $_POST["action_apply_button"] ) ){
		if( isset( $_POST["bulk_action_drop_down"] ) && $_POST["bulk_action_drop_down"] != "" && isset( $_POST["message_bulk_action_checkbox"] ) ){
		    $message_id_arr = $_POST["message_bulk_action_checkbox"];
		    $message_ids = implode(",", $message_id_arr);

		    global $wpdb;
		    $update_query_replies = "";
		    if( $_POST["bulk_action_drop_down"] == "delete" ){
			
			$update_query = " update ".get_cc_meesage_table()." set status = 'deleted' where ID in ($message_ids)";
			$update_query_replies = " update ".  get_cc_replies_table()." set status = 'deleted' where thread_id in ($message_ids) ";

		    }else if( $_POST["bulk_action_drop_down"] == "read" ){

			$update_query = " update ".get_cc_meesage_table()." set status = 'read' where ID in ($message_ids)";
			$update_query_replies = " update ".  get_cc_replies_table()." set status = 'read' where thread_id in ($message_ids) ";

		    }
		    
		    $results = $wpdb->query($update_query);
		    $results_replies = $wpdb->query($update_query_replies);
		}
	    }
	}
	
	/*========== Candidate Module ==========*/
	
	protected function _candidate_jobapplications( $candidate_id ){
	    
	    $this->candidate_action_handler();
	    
	    $current_page = isset( $_GET['list'] ) ? (int)$_GET['list'] : 1;
	    $per_page = 10;
	    $lower_limit = ( $per_page * $current_page ) - $per_page;
	    global $wpdb;

	    $main_query  = "SELECT t.*, coalesce( MAX(r.msg_date), t.msg_date ) AS 'last_date', coalesce( COUNT(r.ID), 0 ) AS 'replies', 
					u.display_name AS 'candidate_name' 
					FROM " . get_cc_threads_table() . " t 
					JOIN " . $wpdb->prefix ."users u ON t.reciver_id=u.ID 
					LEFT JOIN ". get_cc_replies_table() ." r ON t.ID=r.thread_id 
					WHERE t.status !='deleted' AND t.type='jobapplication'
					AND t.sender_id=%d 
					GROUP BY t.ID 
					ORDER BY last_date DESC";

		

	    $total_messages = $wpdb->get_results( $wpdb->prepare( $main_query, $candidate_id ) );
	    $total_posts = count($total_messages) > 0 ? count($total_messages) : 0;
	    
	    $temp_query = $wpdb->prepare( $main_query." LIMIT $lower_limit, $per_page", $candidate_id );
	    $messages = $wpdb->get_results( $temp_query);

	    if( $messages && !empty( $messages ) ){
		    $retrieved_thread_ids = array();
		    $thread_unread_count = array();

		    //first loop to only pull out thread ids
		    foreach( $messages as $message ){
			    $retrieved_thread_ids[] = $message->ID;
			    $thread_unread_count[$message->ID] = 0;
		    }
		    $unread_results = array();
		    //GET UNREAD message count for all threads
		    $unread_count_query = "SELECT t.ID, coalesce( COUNT(r.ID), 0 ) AS 'unread' 
								    FROM ".get_cc_threads_table()." t LEFT JOIN ".get_cc_replies_table()." r ON r.thread_id=t.ID 
								    WHERE r.sender_id!=%d AND r.status='pending' 
								    AND t.ID IN ( ". implode(',', $retrieved_thread_ids) ." )
								    GROUP BY t.ID ";
		    $unread_results = $wpdb->get_results( $wpdb->prepare( $unread_count_query, $candidate_id ) );
			 
		    if( $unread_results && !empty($unread_results) ){
			    foreach( $unread_results as $unread_result ){
				    $thread_unread_count[ $unread_result->ID ] = $unread_result->unread;
			    }
		    }

		    //now print the threads data 
		    echo '<form class="bulk_action_form" action="" method="post" >';
			echo "<table class='threads threads-sentbox table '>";
			?>
			<tr><td>
			<div class="bulk_action_wrapper" style="margin:15px 0px 15px 0px;float: right;" >
				<div class="input-group maxwidth200">
					<select name="bulk_action_drop_down" class="" >
						<option value="" >--Bulk Actions--</option>
						<option value="delete" >Delete</option>
						<option value="read" >Mark read</option>
					</select>
					<span class="input-group-btn">
						<input type="submit" class="button button-small" name="action_apply_button" id="action_apply_button" value="Apply"  style="margin-left:4px;" />
					</span>
				</div><!-- /input-group -->
			</div>
			</td></tr>
			<?php
			foreach( $messages as $message ){
				$this->_print_jobapplication_loop_candidate( $message, 'others', $thread_unread_count[$message->ID], $candidate_id );
			}
			echo "</table><!-- .threads -->";
			echo '<div class="action_button_container" style="width: 30%;float:left;" >';
				    emi_generate_paging_param( $total_posts , $per_page, $current_page, "candidate-dashboard", 2);
			echo "</div>";
		    echo '</form>';
	    }
	    else{
		    echo 'Nothing found!';
	    }
	    
	}

	protected function _print_jobapplication_loop_candidate( $message, $thread_type, $thread_unread_count, $candidate_id ){
	    ?>
	    <tr id='thread-<?php echo $message->ID;?>' class='<?php if( $thread_unread_count > 0 ){echo "warning";} ?>'>
		    <td>
			<span><input type="checkbox" name="message_bulk_action_checkbox[]" value="<?php echo $message->ID; ?>" /></span>
		    <?php 
		    $base_link = cc_user_dashboard_url( 'candidate', 'messages', 'job-application' );
		    $single_msg_url = add_query_arg( 'tid', $message->ID, $base_link );

		    $candidate_name = $message->candidate_name;

		    ?>
		    <h3 style="display:inline;" ><a href="<?php echo $single_msg_url;?> " title='view details'>
			     Job Application to <strong><?php echo $candidate_name;?></strong>.
		    </a></h3>
		    <div class='thread-meta clear'>
			    <ul class='inline clear'>
				    <li><span class='label label-default'>Applied on : <?php echo date( 'd M \'y', strtotime($message->app_date) ) . ' ' . $message->app_time;?></span></li>
				    <?php if( $message->job_title ):?>
					<li><span class='label label-default'><?php echo stripslashes($message->job_title);?></span></li>
				    <?php endif;?>
			    </ul>
		    </div>
		    <div class="thread-actions clear">
			    ( <?php echo $message->replies;?> ) Replies. 
			    <?php if( $thread_unread_count > 0 ): ?>
				    <span class="badge">( <?php echo $thread_unread_count;?> )</span> New
			    <?php endif;?>
			    <ul class='inline pull-right clear'>
				    <li><a class="btn btn-xs btn-default" href='<?php echo $single_msg_url;?>' title="view details">Details</a></li>
				    <li>
					<a id="<?php echo $message->ID ?>" class="glyphicon glyphicon-remove show-tooltip candidate_delete_thread_link" title="delete"  ></a>
				    </li>
			    </ul>
		    </div>
		    </td>
	    </tr>
	    <?php 
	}

	
	protected function _candidate_jobappointments( $candidate_id ){
	    
	    $this->candidate_action_handler();
	    
	    $current_page = isset( $_GET['list'] ) ? (int)$_GET['list'] : 1;
	    $per_page = 10;
	    $lower_limit = ( $per_page * $current_page ) - $per_page;
	    global $wpdb;

	    $main_query  = "SELECT t.*, coalesce( MAX(r.msg_date), t.msg_date ) AS 'last_date', coalesce( COUNT(r.ID), 0 ) AS 'replies', 
					a.app_date, a.app_time, 
					coalesce( p.post_title, '' ) AS 'job_title', 
					u.display_name AS 'employer_name' 
					FROM " . get_cc_threads_table() . " t 
					JOIN " . $wpdb->prefix . "employer_appointment_details a ON t.primary_obj=a.id 
					JOIN " . $wpdb->prefix ."users u ON t.sender_id=u.ID 
					LEFT JOIN ". get_cc_replies_table() ." r ON t.ID=r.thread_id 
					LEFT JOIN ". $wpdb->prefix ."posts p ON a.job_id=p.ID 
						
					WHERE t.status !='deleted' AND t.type='jobappointment'
					AND t.reciver_id=%d 
					GROUP BY t.ID 
					ORDER BY last_date DESC";
		
	    $total_messages = $wpdb->get_results( $wpdb->prepare( $main_query, $candidate_id ) );
	    $total_posts = count($total_messages) > 0 ? count($total_messages) : 0;
	    
	    $temp_query = $wpdb->prepare( $main_query." LIMIT $lower_limit, $per_page", $candidate_id );
	    $messages = $wpdb->get_results( $temp_query);

	    if( $messages && !empty( $messages ) ){
		    $retrieved_thread_ids = array();
		    $thread_unread_count = array();

		    //first loop to only pull out thread ids
		    foreach( $messages as $message ){
			    $retrieved_thread_ids[] = $message->ID;
			    $thread_unread_count[$message->ID] = 0;
		    }
		    $unread_results = array();
		    //GET UNREAD message count for all threads
		    $unread_count_query = "SELECT t.ID, coalesce( COUNT(r.ID), 0 ) AS 'unread' 
								    FROM ".get_cc_threads_table()." t LEFT JOIN ".get_cc_replies_table()." r ON r.thread_id=t.ID 
								    WHERE r.sender_id != %d AND r.status='pending' 
								    AND t.ID IN ( ". implode(',', $retrieved_thread_ids) ." )
								    GROUP BY t.ID ";
		    
		    $unread_results = $wpdb->get_results( $wpdb->prepare( $unread_count_query, $candidate_id ) );
		    if( $unread_results && !empty($unread_results) ){
			    foreach( $unread_results as $unread_result ){
				    $thread_unread_count[ $unread_result->ID ] = $unread_result->unread;
			    }
		    }
		    
		    //now print the threads data 
		    echo '<form class="bulk_action_form" action="" method="post" >';
			echo "<table class='threads threads-sentbox table '>";
			?>
			<tr><td>
			<div class="bulk_action_wrapper" style="margin:15px 0px 15px 0px;float: right;" >
				<div class="input-group maxwidth200">
					<select name="bulk_action_drop_down" class="" >
						<option value="" >--Bulk Actions--</option>
						<option value="delete" >Delete</option>
						<option value="read" >Mark read</option>
					</select>
					<span class="input-group-btn">
						<input type="submit" class="button button-small" name="action_apply_button" id="action_apply_button" value="Apply"  style="margin-left:4px;" />
					</span>
				</div><!-- /input-group -->
			</div>
			</td></tr>
			<?php
			foreach( $messages as $message ){
				$this->_print_jobappointment_loop_candidate( $message, 'jobappointment', $thread_unread_count[$message->ID], $candidate_id );
			}
			echo "</table><!-- .threads -->";
			echo '<div class="action_button_container" style="width: 30%;float:left;" >';
				    emi_generate_paging_param( $total_posts , $per_page, $current_page, "candidate-dashboard", 2);
			echo "</div>";
		    echo '</form>';
	    }
	    else{
		    echo 'Nothing found!';
	    }
	    
	}
	
	protected function _print_jobappointment_loop_candidate( $message, $thread_type, $thread_unread_count, $candidate_id ){
	    ?>
	    <tr id='thread-<?php echo $message->ID;?>' class='<?php if( $thread_unread_count || $message->status == 'pending' ){ echo 'warning'; }?>'>
		    <td>
			<span><input type="checkbox" name="message_bulk_action_checkbox[]" value="<?php echo $message->ID; ?>" /></span>
		    <?php 
		    $base_link = cc_user_dashboard_url( 'candidate', 'messages', 'job-appointment' );
		    $single_msg_url = add_query_arg( 'tid', $message->ID, $base_link );

		    $employer_name = $message->employer_name;

		    ?>
		    <h3 style="display:inline;" ><a href="<?php echo $single_msg_url;?> " title='view details'>
			     <strong><?php echo $employer_name;?></strong> scheduled an appointment
		    </a></h3>
		    <div class='thread-meta clear'>
			    <ul class='inline clear'>
				    <li><span class='label label-default'>Scheduled on : <?php echo date( 'd M \'y', strtotime($message->app_date) ) . ' ' . $message->app_time;?></span></li>
				    <?php if( $message->job_title ):?>
					<li><span class='label label-default'><?php echo stripslashes($message->job_title);?></span></li>
				    <?php endif;?>
			    </ul>
		    </div>
		    <div class="thread-actions clear">
			    ( <?php echo $message->replies;?> ) Replies. 
			    <?php if( $thread_unread_count > 0 ): ?>
				    <span class="badge">( <?php echo $thread_unread_count;?> )</span> New
			    <?php endif;?>
			    <ul class='inline pull-right clear'>
				    <li><a class="btn btn-xs btn-default" href='<?php echo $single_msg_url;?>' title="view details">Details</a></li>
				    <li>
					<a id="<?php echo $message->ID ?>" class="glyphicon glyphicon-remove show-tooltip candidate_delete_thread_link" title="delete"  ></a>
				    </li>
			    </ul>
		    </div>
		    </td>
	    </tr>
	    <?php 
	}

	
	protected function _candidate_othermessages( $candidate_id ){
	    
	    $this->candidate_action_handler();
	    
	    $current_page = isset( $_GET['list'] ) ? (int)$_GET['list'] : 1;
	    $per_page = 10;
	    $lower_limit = ( $per_page * $current_page ) - $per_page;
	    global $wpdb;

		$main_query  = "SELECT t.*, coalesce( MAX(r.msg_date), t.msg_date ) AS 'last_date', coalesce( COUNT(r.ID), 0 ) AS 'replies', 
					    u.display_name AS 'candidate_name' 
					    FROM " . get_cc_threads_table() . " t 
					    JOIN " . $wpdb->prefix ."users u ON t.reciver_id=u.ID 
					    LEFT JOIN ". get_cc_replies_table() ." r ON t.ID=r.thread_id 
					    WHERE t.status !='deleted' AND t.type='pm'
					    AND t.reciver_id=%d 
					    GROUP BY t.ID 
					    ORDER BY last_date DESC";

		

	    $total_messages = $wpdb->get_results( $wpdb->prepare( $main_query, $candidate_id ) );
	    $total_posts = count($total_messages) > 0 ? count($total_messages) : 0;
	    
	    $temp_query = $wpdb->prepare( $main_query." LIMIT $lower_limit, $per_page", $candidate_id );
	    $messages = $wpdb->get_results( $temp_query);

	    if( $messages && !empty( $messages ) ){
		    $retrieved_thread_ids = array();
		    $thread_unread_count = array();

		    //first loop to only pull out thread ids
		    foreach( $messages as $message ){
			    $retrieved_thread_ids[] = $message->ID;
			    $thread_unread_count[$message->ID] = 0;
		    }
		    $unread_results = array();
		    //GET UNREAD message count for all threads
		    $unread_count_query = "SELECT t.ID, coalesce( COUNT(r.ID), 0 ) AS 'unread' 
								    FROM ".get_cc_threads_table()." t LEFT JOIN ".get_cc_replies_table()." r ON r.thread_id=t.ID 
								    WHERE r.sender_id!=%d AND r.status='pending' 
								    AND t.ID IN ( ". implode(',', $retrieved_thread_ids) ." )
								    GROUP BY t.ID ";
		    $unread_results = $wpdb->get_results( $wpdb->prepare( $unread_count_query, $candidate_id ) );
			 
		    if( $unread_results && !empty($unread_results) ){
			    foreach( $unread_results as $unread_result ){
				    $thread_unread_count[ $unread_result->ID ] = $unread_result->unread;
			    }
		    }

		    //now print the threads data 
		    echo '<form class="bulk_action_form" action="" method="post" >';
		    echo "<table class='threads threads-sentbox table '>";
		    ?>
		    <tr><td>
		    <div class="bulk_action_wrapper" style="margin:15px 0px 15px 0px;float: right;" >
			    <div class="input-group maxwidth200">
				    <select name="bulk_action_drop_down" class="" >
					    <option value="" >--Bulk Actions--</option>
					    <option value="delete" >Delete</option>
					    <option value="read" >Mark read</option>
				    </select>
				    <span class="input-group-btn">
					    <input type="submit" class="button button-small" name="action_apply_button" id="action_apply_button" value="Apply"  style="margin-left:4px;" />
				    </span>
			    </div><!-- /input-group -->
		    </div>
		    </td></tr>
		    <?php
		    foreach( $messages as $message ){
			    $this->_print_othermessages_loop_candidate( $message, 'others', $thread_unread_count[$message->ID], $candidate_id );
		    }
		    echo "</table><!-- .threads -->";
		    echo '<div class="action_button_container" style="width: 30%;float:left;" >';
				emi_generate_paging_param( $total_posts , $per_page, $current_page, "candidate-dashboard", 2);
		    echo "</div>";
		    echo '</form>';
	    }
	    else{
		    echo 'Nothing found!';
	    }
	}
	
	protected function _print_othermessages_loop_candidate( $message, $thread_type, $thread_unread_count, $candidate_id ){
	    ?>
	    <tr id='thread-<?php echo $message->ID;?>' class='<?php if( $thread_unread_count || $message->status == 'pending' ){ echo 'warning'; }?>'>
		    <td>
			<span><input type="checkbox" name="message_bulk_action_checkbox[]" value="<?php echo $message->ID; ?>" /></span>
		    <?php 
		    $base_link = cc_user_dashboard_url( 'candidate', 'messages', 'others' );
		    $single_msg_url = add_query_arg( 'tid', $message->ID, $base_link );

		    $candidate_name = 'Candidate' . $message->reciver_id;

		    ?>
		    <h3 style="display:inline;" ><a href="<?php echo $single_msg_url;?> " title='view details'>
			     <?php echo $message->subject ." - "?> <strong><?php echo $candidate_name;?></strong>.
		    </a></h3>
		    <div class='thread-meta clear'>
			    <ul class='inline clear'>
				    <li><span class='label label-default'>Received on : <?php echo date( 'd M \'y', strtotime($message->app_date) ) . ' ' . $message->app_time;?></span></li>
			    </ul>
		    </div>
		    <div class="thread-actions clear">
			    ( <?php echo $message->replies;?> ) Replies. 
			    <?php if( $thread_unread_count > 0 ): ?>
				    <span class="badge">( <?php echo $thread_unread_count;?> )</span> New
			    <?php endif;?>
			    <ul class='inline pull-right clear'>
				    <li><a class="btn btn-xs btn-default" href='<?php echo $single_msg_url;?>' title="view details">Details</a></li>
				    <li>
					<a id="<?php echo $message->ID ?>" class="glyphicon glyphicon-remove show-tooltip candidate_delete_thread_link" title="delete"  ></a>
				    </li>
			    </ul>
		    </div>
		    </td>
	    </tr>
	    <?php 
	}
	
	/*========== Candidate Module ==========*/
	
	protected function get_hide_user_id_string(){
	    global $wpdb;
	    $hide_user_ids = $wpdb->get_results( 'select user_id from '.$wpdb->prefix.'usermeta where meta_key = "cc_hide_profile"' );
	    $hide_user_id_arr = array();
	    foreach ( $hide_user_ids as $u_id ){
		$hide_user_id_arr[] = $u_id->user_id;
	    }
	    $hide_user_id_str = implode(",", $hide_user_id_arr);
	    return $hide_user_id_str;
	}
	
}