<?php 
add_action( 'template_redirect', 'load_employer_dashboard_candidate_component' );
function load_employer_dashboard_candidate_component(){
	$pacakge_details = member_package_details();
	if( 'free'!=$pacakge_details['package'] ){
		add_action( 'cc_dashboard_nav',						'employer_candidates_nav' );
		add_action( 'cc_dashboard_subnav',					'employer_candidates_subnav' );
		add_action( 'cc_dashboard_title',					'employer_candidates_title' );
		add_action( 'cc_dashboard_content',					'employer_candidates_content' );
		add_filter( 'cc_dashboard_default_subcomponent',	'employer_candidates_default_subcomponent', 10, 2 );
	}
}

function employer_candidates_nav( $user_type ){
	if( $user_type=='employer' ){
		$active_class = '';
		if( cc_user_dashboard_is_current_component('candidates') ){
			$active_class = 'active';
		}
		
		//must be all small letters, capitalization will be done with css
		echo "<li class='$active_class'><a href='". cc_user_dashboard_url('employer', 'candidates') ."'>candidates</a></li>";
	}
}

function employer_candidates_subnav( $user_type ){
	$my_component = 'candidates';
	if( $user_type=='employer' && cc_user_dashboard_is_current_component($my_component) ){
		$subcomponents = array( 'search', 'saved-searches', 'bookmarks' );
 		foreach( $subcomponents as $subcomponent ){
 			$active_class = '';
			if( cc_user_dashboard_is_current_subcomponent($subcomponent) ){
				$active_class = 'active';
			}

			//must be all small letters, capitalization will be done with css
			echo "<li class='$active_class'><a href='". cc_user_dashboard_url($user_type, $my_component, $subcomponent) ."'>$subcomponent</a></li>";
 		}
	}
}

function employer_candidates_default_subcomponent( $default_subcomponent, $component ){
	if( $component=='candidates' ){
		$default_subcomponent = 'search';
 	}
	return $default_subcomponent;
}

function employer_candidates_title( $user_type ){
	if( $user_type=='employer' && cc_user_dashboard_is_current_component('candidates') ){
		//must be all small letters, capitalization will be done with css
		echo 'candidates - ' . cc_user_dashboard_current_subcomponent();
	}
}

function employer_candidates_content( $user_type ){
	if( $user_type=='employer' && cc_user_dashboard_is_current_component('candidates') ){
		 switch( cc_user_dashboard_current_subcomponent() ){ 
			case 'saved-searches':
				employer_candidates_savedsearches_content();
				break;
			case 'bookmarks':
				//the function is there inside cc-bookmark-candidates plugin
				if(function_exists('employer_candidates_bookmarks') ){
					employer_candidates_bookmarks();
				}
				break;
			case 'search':
			default:
				employer_candidates_search_content();
				break;
		 }
	}
}

	function employer_candidates_search_content(){
		$controller = CandidatesSearch::get_instance();
		
		$users_per_page = ( $_GET["users_per_page_txt"] != "" ) ? $_GET["users_per_page_txt"] : 10;
		$current_page = isset( $_GET['list'] ) ? $_GET['list'] : 1;
		$offset = ( $current_page * $users_per_page) - $users_per_page;
		$args = $controller->candidate_search_args();
		$args["number"] = $users_per_page;
		$args["offset"] = $offset;
		
		$candidate_query = new WP_User_Query( $args );
		?>
		<script type='text/javascript'>
			jQuery(document).ready(function($){
				$("#lnk_toggle_extended_search").click(function(){
					if($(this).hasClass('minimized')){
						$(this).html('- less options');
					}
					else{
						$(this).html('+ more options');
					}
					$(this).toggleClass('minimized');

					var target = $(this).attr('href');
					$(target).slideToggle('fast').toggleClass('minimized');
					return false;
				});
				$("#frm_reset_btn").click(function(){
				    $(".standard-form").find("select,input[type='text'],input[type='number']").val("");
				    return false;
				});
			});
		</script>
		<div class='search-filters text-info'>
		    <form class='standard-form' method='GET' action="<?php echo cc_user_dashboard_url("employer", "candidates", "search", false); ?>" >
				<input type='hidden' name='component' value='<?php echo esc_attr( cc_user_dashboard_current_component() );?>' />
				<input type='hidden' name='action' value='<?php echo esc_attr(cc_user_dashboard_current_subcomponent() );?>' />
				<?php /* search by education, skills/specialization, desired position, salary range  */ ?>
				<div class='row clear'>
					<div class='col span_3 specialization'>
						<label for='specialization'>Specialization</label>
						<select name='specialization' id='specialization' >
							<?php echo $controller->specialization_options( $_GET['specialization'] );?>
						</select>
					</div>
					<div class='col span_3 education'>
						<label for='education'>Education</label>
						<select name='education' id='education' >
							<?php echo $controller->education_options( $_GET['education'] );?>
						</select>
					</div>
					<div class='col span_3 desired_position'>
						<label for='position'>Desired Position</label>
						<select name='position' id='position' >
							<?php echo $controller->position_options( $_GET['position'] );?>
						</select>
					</div>
					<div class='col span_3 more_options'>
						<a href="#" class="save_search_info show-tooltip" title='save this search'><span class="glyphicon glyphicon-save"></span></a>
						
						<div class="save_search_popup"  id="dialog"  style="display:none">
						    <label for="search_title">
							    Give a Title to this search
						    </label>
						    <input type="text" name="search_title" id="searcc_title_id"/>
						    <input type="submit" name="save_this" id="set_title"/>
						</div>

					    
						<input type='submit' value='Fiilter' class='button button-medium btn-block' /><br/>
						<div class='row clear' style='margin-top: 10px'>
							<div class='col span_6'>
								<a href="#search_extended" id="lnk_toggle_extended_search" class="minimized btn btn-info btn-xs btn-block">+ more options</a></span>
							</div>
							<div class='col span_6'>
								<a href="#" id="frm_reset_btn" class='btn btn-warning btn-xs btn-block'>Reset</a>
							</div>
						</div>
					</div>
				</div>
				<div id='search_extended' style='display:none'>
				    <div class="row clear separator" >
						<div class='col span_3'>
							<strong>Filter by expected salary</strong>
						</div>
						<div class='col span_3'>
							<label for='currency'>Currency</label>
							<select name='currency' id='currency'>
								<?php echo $controller->currency_options( $_GET['currency'] );?>
							</select>
						</div>
						<div class='col span_3'>
							<label for='currency'>Min. Salary</label>
							<input type='text' name='min_salary' id='min_salary' value='<?php echo isset( $_GET['min_salary'] ) ? esc_attr( $_GET['min_salary'] ): "";?>' />
						</div>
					
						<div class='col span_3'>
							<label for='currency'>Max. Salary</label>
							<input type='text' name='max_salary' id='max_salary' value='<?php echo isset( $_GET['max_salary'] ) ? esc_attr( $_GET['max_salary'] ): "";?>' />
						</div>
				    </div>
					<div class="row clear filter_by_experience separator" >
						<div class="col span_6" >
							<strong>Filter by Experience</strong><br/>
							<select name="experience" >
								<option value="">All..</option>
								<?php echo $controller->experience_options( $_GET["experience"] ) ?>
							</select>
						</div>
						<div class="col span_6" >
							<strong>Members Per Page</strong><br/>
							<input type="number" name="users_per_page_txt" id="users_per_page_txt" value="<?php echo $_GET["users_per_page_txt"]; ?>" />
						</div>
				    </div>
				</div>
				
			</form>
		</div>
		<div class='search-results'>
			<?php
			
			if ( ! empty( $candidate_query->results ) ) {
				$total_users = $candidate_query->get_total();
				foreach ( $candidate_query->results as $user ) {
					global $candidate;
					$candidate = $user;
					get_template_part('members/candidate', 'loop');
				}
			} else {
				echo 'No users found.';
			}
			?>
		</div>
		<div class="standard-form-pagination" >
		    <?php
		    emi_generate_paging_param( $total_users, $users_per_page, $curr_page, "employer-dashboard");
		    ?>
		</div>
		<?php
	}
	
	function employer_candidates_savedsearches_content(){
	    
	    echo 'this is coming';
	    $user_id = get_current_user_id();
	    $saved_search_data = get_user_meta( $user_id, "search_saved_data", true );
	    if( $saved_search_data && !empty( $saved_search_data ) ){
		$anchor_attr = "";
		?>
		<div class="panel-group" id="accordion">
		
		    <?php foreach( $saved_search_data as $search_key => $search ) : ?>
			    <div class="panel panel-default">
					<div class="panel-heading">
					    <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $search_key; ?>" class="block-level">
								<?php echo $search["title"]; ?>
								<span class='glyphicon glyphicon-sort pull-right'></span>
							</a>
					    </h4>
					</div>
					<div id="collapse<?php echo $search_key; ?>" class="panel-collapse collapse">
					    <div class="panel-body">
							
							<table class="saved_search_table" >
								<tr>
									<th>Title</th>
									<td><?php echo $search["title"]; ?></td>
								</tr>
								<tr>
									<th>Specialization</th>
									<?php
									$specialization = $search["specialization"];
									?>
									<td>
										<?php 
										echo $specialization != "" ? $specialization: "-"; 
										$anchor_attr .= "&specialization=".$specialization;
										?>
									</td>
								</tr>
								<tr>
									<th>Education</th>
									<?php
									$education = $search["education"];
									?>
									<td>
										<?php 
										    echo $education != "" ? $education : "-"; 
										    $anchor_attr .= "&education=".$education;
										?>
									</td>
								</tr>
								<tr>
									<th>Position</th>
									<?php
									$position = $search["position"];
									?>
									<td>
									    <?php 
										echo $position != "" ? $position : "-"; 
										$anchor_attr .= "&position=".$position;
									    ?>
									</td>
								</tr>
								<tr>
									<th>Currency</th>
									<td>
										<?php 
										echo $search["currency"] == "" ? "-" : $search["currency"]; 
										$anchor_attr .= "&currency=".$search["currency"];
										?>
									</td>
								</tr>
								<tr>
									<th>Min Salary</th>
									<td>
									<?php 
									echo $search["min_salary"] == "" ? "-" : $search["min_salary"]; 
									$anchor_attr .= "&min_salary=".$search["min_salary"];
									?>
									</td>
								</tr>
								<tr>
									<th>Max Salary</th>
									<td>
										<?php 
										echo $search["max_salary"] == "" ? "-" : $search["max_salary"]; 
										$anchor_attr .= "&max_salary=".$search["max_salary"];
										?>
									</td>
								</tr>
								<tr>
									<th>Users per page</th>
									<td>
										<?php 
										echo $search["users_per_page_txt"] == "" ? "-" : $search["users_per_page_txt"]; 
										$anchor_attr .= "&users_per_page_txt=".$search["users_per_page_txt"];
										?>
									</td>
								</tr>
								<tr>
									<th>Experience</th>
									<td>
										<?php 
										echo $search["experience"] == "" ? "-" : $search["experience"]; 
										$anchor_attr .= "&experience=".$search["experience"];
										?>
									</td>
								</tr>
								<tr  style="border-bottom:1px solid #ededed;margin-bottom: 10px"  >
									<th><a href="?component=candidates&subcomponent=search<?php echo $anchor_attr; ?>" >Search</a></th>
									<th><a href="#" id="<?php echo $search_key; ?>" class="delete_search_filter" >Delete</a></th>
								</tr> 
						    </table>
					    </div>
					</div>
			    </div>
		    <?php endforeach; ?>
		</div>
		<?php
	    }else{
		echo '<p class="text-info" >No candidate search filter parameter has been saved yet!! </p>';
	    }
	    
	}
