<?php 
add_action( 'plugins_loaded', 'instantiate_emp_messages', 'get_instance' );
function instantiate_emp_messages(){
	if( function_exists( 'is_user_a_candidate' ) ){
	    if( !is_user_a_candidate() ){
		    CCEmployerMessages::get_instance();
	    }
	}
}

class CCEmployerMessages{
    private static $instance;
    private $unread_messages = array();
	
    public static function get_instance(){
        if(!isset(self::$instance)){
            self::$instance=new self();
        }
        return self::$instance;
    }

    function __construct(){
		$this->_fetch_unread_msg_counts();
		add_action( 'cc_dashboard_nav',						array( $this, 'dashboard_nav' ) );
		add_action( 'cc_dashboard_subnav',					array( $this, 'dashboard_subnav' ) );
		add_action( 'cc_dashboard_title',					array( $this, 'dashboard_title' ) );
		add_action( 'cc_dashboard_content',					array( $this, 'dashboard_content' ) );
		add_filter( 'cc_dashboard_default_subcomponent',	array( $this, 'dashboard_default_subcomponent' ), 10, 2 );
	}
	
	/*
	 * fetches the unread message count for different types of messages and saves locally to be used later
	 */
	protected function _fetch_unread_msg_counts(){
		global $wpdb;
		$employer_id = get_current_user_id();
		//SELECT `type`, COUNT( ID ) AS 'unread' FROM `demo_cc_members_messages` WHERE status='pending' GROUP BY type
		$query  = "SELECT t.type, COUNT( r.ID ) AS 'unread' FROM ". get_cc_replies_table() ." r";
		$query .= " JOIN ". get_cc_threads_table() . " t ON r.thread_id=t.ID ";
		$query .= " WHERE r.status='pending' AND ( t.reciver_id=%d OR t.sender_id=%d ) AND r.sender_id!=%d GROUP BY t.type";
		$results = $wpdb->get_results( $wpdb->prepare( $query, $employer_id, $employer_id, $employer_id ) );
		if( $results && !empty( $results ) ){
			foreach ($results as $result) {
				$this->unread_messages[$result->type] = $result->unread;
			}
		}
		$query2 = "SELECT type, COUNT( ID ) AS 'unread' FROM ".get_cc_threads_table()." WHERE status='pending' AND ( reciver_id = %d ) GROUP BY type";
		$results2 = $wpdb->get_results( $wpdb->prepare( $query2, $employer_id ) );
		
		if( $results2 && !empty( $results2 ) ){
			foreach ($results2 as $result) {
				$this->unread_messages[$result->type] = $this->unread_messages[$result->type] > 0 ? $this->unread_messages[$result->type] : 0;
				$this->unread_messages[$result->type] += $result->unread;
			}
		}
	}
	
	/**
	 * returns the number of unread messages of any given type (or all)
	 * @param string $message_type Which type of method. 
	 * Default blank, will return sum of unread count of all type of messages
	 * 
	 * @return int count
	 */
	protected function _get_unread_message_count( $message_type='' ){
		$al_count = 0;
		if( !$this->unread_messages || empty( $this->unread_messages ) ){
			return 0;
		}
		
		if( !$message_type ){
			//count of all type of messages
			foreach ($this->unread_messages as $type => $count) {
				$allcount += $count;
			}
		}
		else{
			$allcount = isset( $this->unread_messages[$message_type] ) ? $this->unread_messages[$message_type] : 0;
		}
		
		return $allcount;
	}
	
	public function dashboard_nav($user_type ){
		if( $user_type=='employer' ){
			$active_class = '';
			if( cc_user_dashboard_is_current_component('messages') ){
				$active_class = 'active';
			}

			$count = $this->_get_unread_message_count();
			$count_attr = "";
			if( $count > 0 ){
				$count_attr = "<span class='badge' >".$count."</span>";
			}

			//must be all small letters, capitalization will be done with css
			echo "<li class='$active_class'><a href='". cc_user_dashboard_url( $user_type, 'messages') ."'>inbox $count_attr</a></li>";
		}
	}

	public function dashboard_subnav( $user_type ){
		$my_component = 'messages';
		if( 'employer'==$user_type && cc_user_dashboard_is_current_component($my_component) ){
			$subcomponents = array( 'job-application','job-appointment', 'sentbox', 'compose' );
			foreach( $subcomponents as $subcomponent ){
				$active_class = '';
				if( cc_user_dashboard_is_current_subcomponent($subcomponent) ){
					$active_class = 'active';
				}

				$count_attr = "";
				if( $subcomponent == "job-application" ){
					$count = $this->_get_unread_message_count("jobapplication");
					if( $count > 0 ){
						$count_attr = "<span class='badge' >$count</span>";
					}
				}else if( $subcomponent == "job-appointment"  ){
					$count = $this->_get_unread_message_count("jobappointment");
					if( $count > 0 ){
						$count_attr = "<span class='badge' >$count</span>";
					}
				}else if( $subcomponent == "sentbox"  ){
				    $count = $this->_get_unread_message_count("pm");
				    if( $count > 0 ){
					    $count_attr = "<span class='badge' >$count</span>";
				    }
				}

				//must be all small letters, capitalization will be done with css
				echo "<li class='$active_class'><a href='". cc_user_dashboard_url($user_type, $my_component, $subcomponent) ."'>" . $subcomponent ."&nbsp;" .$count_attr. "</a></li>";
			}
		}
	}
	
	public function dashboard_title( $user_type ){
		if( $user_type=='employer' && cc_user_dashboard_is_current_component('messages') ){
			//must be all small letters, capitalization will be done with css
			echo 'messages - ' . str_replace( '-', ' ', cc_user_dashboard_current_subcomponent() );
		}
	}
	
	public function dashboard_content( $user_type ){
		if( $user_type=='employer' && cc_user_dashboard_is_current_component('messages') ){
			$controller = CCMembersMessagesTemplate::get_instance();
			switch( cc_user_dashboard_current_subcomponent() ){
				case 'compose':
					employer_compose_message_content();//define this function
					break;
				
				case 'sentbox':
					//echo do_shortcode( '[CC_members_sentbox]' );
					if( isset( $_GET['tid'] ) ){
						$controller->thread_details( $_GET['tid'], 'employer', 'pm' );
					}
					else{
						$controller->list_threads( get_current_user_id(), 'employer', 'sentbox' );
					}
					break;
				
				case 'job-appointment':
					if( isset( $_GET['tid'] ) ){
						$controller->thread_details( $_GET['tid'], 'employer', 'jobappointment' );
					}
					else{
						$controller->list_threads( get_current_user_id(), 'employer', 'jobappointment' );
					}
					//employer_job_specific_message_list( "jobappointment" );
					break;
				
				case 'job-application':
				default:
					//employer_job_specific_message_list( "jobapplication" );
					if( isset( $_GET['tid'] ) ){
						$controller->thread_details( $_GET['tid'], 'employer', 'jobapplication' );
					}
					else{
						$controller->list_threads( get_current_user_id(), 'employer', 'jobapplication' );
					}
					break;
			}
		}
	}
	
	public function dashboard_default_subcomponent( $default_subcomponent, $component ){
		if( $component=='messages' ){
			$default_subcomponent = 'job-application';
		}
		return $default_subcomponent;
	}
}