<?php 
/* use the more meaninigful get_cc_threads_table() instead. */
function get_cc_meesage_table(){
    return get_cc_threads_table();
}

function get_cc_threads_table(){
	global $wpdb;
    return $wpdb->prefix."cc_messages_threads";
}

function get_cc_replies_table(){
	global $wpdb;
    return $wpdb->prefix."cc_messages_replies";
}

/*
 * Check if the any message (mathching the conditions specified in args), 
 * exists in messages table or not.
 * 
 * @todo : incomplete
 * 
 * @param mixed $args
 * @return boolean
 */
function cc_messages_does_message_exist( $args ){
    /*
     * do select query from demo_cc_members_messages table 
     * and get all record with delete status 
     * check if logged in user is already applied for job or not.
     * if yes the return true.
    */
    $data = check__message_existance();
    if(!empty($data) && isset($data)){
		return true;
    }
    else{
		return false;
    }
}

/* get all  from database 
* select * from table wher sender_id = loggedinuserid and reciver_id = author_id;
*/
function check__message_existance(){
    global $wpdb;
    $query ="select * from ".get_cc_meesage_table()." where sender_id =".get_current_user_id()." And primary_obj = ".get_the_ID();
    $data = $wpdb->get_results($query);
    return $data;
}

function get_recived_message( $type ){
    global $wpdb;
    $query = "select * from ".get_cc_meesage_table()." where (reciver_id = ".get_current_user_id()." || sender_id = ".get_current_user_id().") AND status != 'delete' and type = '$type' ";
    $messages = $wpdb->get_results($query);
    
    return $messages;
}
function get_sent_message(){
    global $wpdb;
    $query ="select * from ".get_cc_meesage_table()." where sender_id =".get_current_user_id()." AND status !='delete'";
    $data = $wpdb->get_results($query);
    return $data;
}

function get_paged_recived_message(){
    global $wpdb;
    $query ="select * from ".get_cc_meesage_table()." where reciver_id =".get_current_user_id()." AND status !='delete' LIMIT 0 , 3";
    $data = $wpdb->get_results($query);
    return $query;
}

function cc_member_inbox(){
    $instance = CCMembersMessages::get_instance();
    $instance->show_members_notification();
}
function cc_send_message($args){
    $instance = CCMembersMessages::get_instance();
    $responce = $instance->cc_messages_send($args);
    return $responce;
}

/*##################################################################
 * Employer Message Tab
 ##################################################################*/
/*add_action( 'cc_dashboard_nav',						'employer_messages_nav' );
add_action( 'cc_dashboard_subnav',					'employer_messages_subnav' );
add_action( 'cc_dashboard_title',					'employer_messages_title' );
add_action( 'cc_dashboard_content',					'employer_messages_content' );
add_filter( 'cc_dashboard_default_subcomponent',	'employer_messages_default_subcomponent', 10, 2 );
*/
function employer_messages_nav( $user_type ){
	if( $user_type=='employer' ){
		$active_class = '';
		if( cc_user_dashboard_is_current_component('messages') ){
			$active_class = 'active';
		}
		
		$count = get_unread_message_count("");
		$count_attr = "";
		if( $count > 0 ){
		    $count_attr = "<span class='badge' >".$count."</span>";
		}
		
		
		//must be all small letters, capitalization will be done with css
		echo "<li class='$active_class'><a href='". cc_user_dashboard_url( $user_type, 'messages') ."'>$count_attr&nbsp;messages</a></li>";
	}
}

function employer_messages_subnav( $user_type ){
 	$my_component = 'messages';
	if( 'employer'==$user_type && cc_user_dashboard_is_current_component($my_component) ){
 		$subcomponents = array( 'job-application','job-appointment', 'sentbox', 'compose' );
 		foreach( $subcomponents as $subcomponent ){
 			$active_class = '';
			if( cc_user_dashboard_is_current_subcomponent($subcomponent) ){
				$active_class = 'active';
			}
			
			
			$count_attr = "";
			if( $subcomponent == "job-application" ){
			    $count = get_unread_message_count("jobapplication");
			    if( $count > 0 ){
				$count_attr = "<span class='badge' >".get_unread_message_count( "jobapplication" )."</span>";
			    }
			}else if( $subcomponent == "job-appointment"  ){
			    $count = get_unread_message_count("jobappointment");
			    if( $count > 0 ){
				$count_attr = "<span class='badge' >".get_unread_message_count( "jobappointment" )."</span>";
			    }
			}
			
			//must be all small letters, capitalization will be done with css
			echo "<li class='$active_class'><a href='". cc_user_dashboard_url($user_type, $my_component, $subcomponent) ."'>$count_attr&nbsp$subcomponent</a></li>";
 		}
	}
}

function employer_messages_title( $user_type ){
	if( $user_type=='employer' && cc_user_dashboard_is_current_component('messages') ){
		//must be all small letters, capitalization will be done with css
		echo 'messages - ' . cc_user_dashboard_current_subcomponent();
	}
}

function employer_messages_content( $user_type ){
    if( $user_type=='employer' && cc_user_dashboard_is_current_component('messages') ){
	switch( cc_user_dashboard_current_subcomponent() ){
	    case 'compose':
		employer_compose_message_content();//define this function
		break;
	    case 'sentbox':
		echo do_shortcode( '[CC_members_sentbox]' );
		break;
	    case 'job-application':
		employer_job_specific_message_list( "jobapplication" );
		break;
	    case 'job-appointment':
		employer_job_specific_message_list( "jobappointment" );
		break;
	    default:
		employer_job_specific_message_list( "jobapplication" );
		break;
	}
    }
}

function employer_messages_default_subcomponent( $default_subcomponent, $component ){
	if( $component=='messages' ){
		$default_subcomponent = 'job-application';
 	}
	return $default_subcomponent;
}

/**
* this is a form for composeing message.
* must have member_id in url.
* otherwise show error message to select member first.
*/
function employer_compose_message_content() {
	$member_id = $_GET['member_id'];

	if (isset($member_id) && !empty($member_id)) {
		$user = new WP_User($member_id);
		//is it a valid user 
		if (isset($user->data) && !empty($user->data)) {
			$member_name = 'Member ' . $member_id;
			if( is_user_a_candidate( $user ) ){
				$candidate_name = 'Candidate ' . $user->ID;
				//only premium users should see candidate's name
				if( can_view_candidate_info( $user, 'name' ) ){
					$candidate_name .= ' ( ' . $user->display_name . ' )';
				}
				$member_name = $candidate_name;
			}
			?>
			<div class = "compose_message_form message_form">
				<div class="message_status">
				</div>
				<label>To : <span class="label label-default"><?php echo $member_name;?></span></label><br/>
				
				<label for='message_subj'>Subject</label>
				<input type="text" value="" class="subject" id= "message_subj"/> 
				
				<label for="message_content">Message</label>
				<textarea id="message_content"></textarea>
				
				<input type="submit" class="sent_notification button button-medium" id="sent_notification_to_member-<?php echo $member_id; ?>" value = "send" data-reciverid = "<?php echo $member_id; ?>" data-senderid = "<?php echo get_current_user_id(); ?>"/>
				<span class="show_status"></span>
			</div>
			<?php
		} else {
			echo "<p class='text-error'>Not a valid member!</p>";
		}
	} else {
	    
	    $package_details = member_package_details( get_current_user_id() );
	    if( $package_details && !empty( $package_details ) &&  $package_details["package"] != "free" ){
		
		get_candidate_search_box("messages","compose","send message");
		
	    }else{
		echo "<p class='text-info'>Please select a member first!</p>";
	    }
	}
}

/**
 * 
 * @param string $type - specify which type of messge to list
 */
function employer_job_specific_message_list( $type ){
    
    
    
    if($_GET['type']=='single' && !empty($_GET['msg_id'])){
	
	show_single_message_page( $type );
	
    }
    else{
	
	/* Bulk action handler will write here */
	if( isset( $_POST["action_apply_button"] ) ){
	    if( isset( $_POST["bulk_action_drop_down"] ) && $_POST["bulk_action_drop_down"] != "" && isset( $_POST["message_bulk_action_checkbox"] ) ){
		$message_id_arr = $_POST["message_bulk_action_checkbox"];
		$message_ids = implode(",", $message_id_arr);
		
		global $wpdb;
		$update_query = "";
		if( $_POST["bulk_action_drop_down"] == "delete" ){

		    $update_query = " update ".get_cc_meesage_table()." set status = 'delete' where ID in ($message_ids) ";

		}else if( $_POST["bulk_action_drop_down"] == "read" ){

		    $update_query = " update ".get_cc_meesage_table()." set status = 'read' where ID in ($message_ids) ";
		    
		}
		$wpdb->query( $update_query );
		$results = $wpdb->query($update_query);
	    }
	}
	
	$job_messages = get_recived_message( $type );
	if( empty( $job_messages ) ){
	    echo "<p class='text-info' >No messages found!</p>";
	}else{
	    member_inbox_html( $job_messages );
	}
    }
    
}

function member_inbox_html( $job_messages ){
    ?>
    <div class="message_container">
	<form class="bulk_action_form" action="" method="post" >
	    <table id="message-threads" class="messages-notices">
		<tbody>
		    <?php foreach( $job_messages as $key => $message ):?>
		       <?php
		       $message_class_attr = "";
		       if( $message->status == "pending" ){
			    $message_class_attr = "text-info";
		       }
		       ?>
			<tr id="m-<?php echo $message->ID;?>" class="member_message <?php echo $message_class_attr; ?>">

			    <td width="5%" class="thread-checkbox">
				<input type="checkbox" name="message_bulk_action_checkbox[]" value="<?php echo $message->ID; ?>" />
			    </td>

			    <td width="10%" class="thread-avatar">
				<?php echo get_avatar( $message->sender_id, 32 );?>
			    </td>

			    <td width="25%" class="thread-from">
				<label>From: member </label>
				<span class="user_id"><?php echo $message->sender_id; ?></span><br/>
				<span class="activity"><?php echo $message->msg_date; ?></span>
			    </td>

			    <td width="51%" class="thread-info">
				<?php 
				/*
				 * if user is employ print employe page else 
				 * candidate page
				 * using get_page_link(get_the_ID());
				*/
				$base_url = cc_user_dashboard_url('employer', 'messages', 'inbox');
				$message_details_url = add_query_arg( array('type'=>'single', 'msg_id'=>$message->ID) );
				?>
				<p>
				    <a href="<?php echo $message_details_url; ?>" title="View Message"><?php echo $message->subject; ?></a>
				</p>
				<p class="thread-excerpt"><?php echo $message->message; ?></p>
			    </td>

			    <td width="15%" class="thread-options">
				<a class="button delete_msg" id="delete_msg-<?php echo $message->ID; ?>" href="#" data_id='<?php echo $message->ID; ?>' title="Delete Message">Delete</a> &nbsp;
			    </td>

			</tr>
		    <?php endforeach; ?>
			<tr>
			    <td  colspan="5"  >
				<div class="action_button_container">

				    <select name="bulk_action_drop_down" >
					<option value="" >--Bulk Actions--</option>
					<option value="delete" >Delete</option>
					<option value="read" >Mark read</option>
				    </select>
				    <input type="submit" name="action_apply_button" id="action_apply_button" value="Apply" />

				</div>
			    </td>
			</tr>
		</tbody>
		<tfoot>
		   <?php //emi_generate_paging_param($total_posts, $posts_per_page, $paged, "employer-dashboard/?operation=inbox", " ", "list_count" ); ?>
		</tfoot>
	   </table>
       </form>
    </div>
    <?php
}

function show_single_message_page( $type ){
    $message_id = $_GET['msg_id'];
    
    global $wpdb;
    $query = "select * from ".get_cc_meesage_table()." where ID = $message_id ";
    $message = $wpdb->get_results( $query );
    if( $message ){
	single_jobapplication_page( $message, $type );
    }else{
	echo '<p class="text-error" >Invalid message selected!! </p>';
    }

}

/**
 * 
 * @global type $wpdb
 * @param string $type = show count of specified type of message and if empty that means overall count is returned of current user
 * @return $count - count of job listing unread message
 */
function get_unread_message_count( $type ){
    global $wpdb;
    $type_attr = ( $type != "" ) ? " and type = '$type' " : "";
    $status_attr = "";
    if( $type == "jobapplication" ){
	$status_attr = "status='read'";
    }else{
	
    }
    $emp_id = get_current_user_id();
    $count  = $wpdb->get_var("SELECT count(*) FROM ".  get_cc_meesage_table()." WHERE status = 'pending' and ( sender_id = $emp_id || reciver_id = $emp_id ) $type_attr ");
    $count = ( $count > 0 ) ? $count : 0;
    return $count;
}

function update_message_status_read( $type ){
    
    global $wpdb;
    $message_id = $_GET['msg_id'];
    
    $status = $wpdb->get_var("select status from  ".get_cc_meesage_table()." where ID = $message_id ");
    $update_query = "";
    if( $status == 'pending' ){
	if( $type == "jobapplication" ){
	    $update_query = " update ".get_cc_meesage_table()." set status = 'read_r' where ID = $message_id ";
	}else if( $type == "jobappointment" ){
	    $update_query = " update ".get_cc_meesage_table()." set status = 'read_s' where ID = $message_id ";
	}
    }else if( $status == 'read_r' || $status == 'read_s' ){
	$update_query = " update ".get_cc_meesage_table()." set status = 'read_s_r' where ID = $message_id ";
    }
    echo $update_query;
    
    $results = $wpdb->query($update_query);
    
}


//this is ajax request operation for compose message from.
function cc_compose_message_ajax(){

     $args = array (
            'subject'       =>  $_POST['subject'],

            'content'       =>  $_POST['content'],

            'sender_id'     =>  $_POST['sender_id'],

            'reciver_id'    =>  $_POST['reciver_id'],

            'reply_of'      =>  $_POST['reciver_id'],

            'type'          => 'pm',

            'primary_obj'   => $_POST['primary_obj'],

            'secondry_obj'  =>false
             );

    $instance = CCMembersMessages::get_instance();
    $response = $instance->cc_messages_send($args);
    if($response['status']==true){
        $response['message'] = "your message was sent suceccfully";
    }
    die(json_encode($response));
}



function single_jobapplication_page( $message_data, $type ){
    /*
    * get msg_id from url and show data of that message.
    * check mssage data if staus == jobapplication
    */
    $message_data = $message_data[0];
    $candiate = new WP_User( $message_data->sender_id );
    
    ?>
    <div class="single_message_container ccsingle">
	<div class="message_detail_container">
	    <ul class='msg_details'>
		<li>From: <?php echo "member ".$message_data->sender_id;?>
		    <?php 
		    $candidate_name = 'member ' . $candiate->ID;
		    //only premium users should see candidate's name
		    if( can_view_candidate_info( $candidate, 'name' ) ){
			$candidate_name .= '( ' . $candiate->display_name . ' )';        
		    }
		    ?>
		    <a href='<?php echo home_url( '/candidates/' ). $candiate->ID;?>'><?php echo $candidate_name;?></a>
		</li>
		<li>Sent: <?php echo $message_data->msg_date;?></li>
		<li>Job Applied for: 
		    <a href='<?php echo esc_url( get_permalink( $message_data->primary_obj ) );?>' title='view job details'>
			<?php echo get_the_title( $message_data->primary_obj );?>
		    </a>
		</li>
		<?php $candidate_meta = get_user_meta( $candidate->ID ); ?>
		<li>Web cam: 
		    <?php if( isset( $candidate_meta['web_cam'][0] ) && $candidate_meta['web_cam'][0]=='yes' ){
			echo '<span class="label label-info">Available</span>';
		    }
		    else{
			echo '<span class="label label-info">Not available</span>';
		    }
		    ?>
		</li>
		<li>Highest Qualification: 
		    <?php 
		    if( isset( $candidate_meta['education'][0] ) ){
			echo $candidate_meta['education'][0];
		    }
		    ?>
		</li>
	    </ul>
	</div>
	<div class="message_container">
	    Message :
	    <p class="message_content">
		<?php echo $message_data->message;?>
	    </p>
	</div>
    </div>
    <?php
    update_message_status_read( $type );
}

function get_candidate_search_box( $component, $subcomponent, $button_label ){
    
    $args = array(
	'role' => 'Subscriber'
    );
    $candidate_query = new WP_User_Query( $args );
    
    $candidates_detail_arr = array();
    
    if ( ! empty( $candidate_query->results ) ) {
	foreach ( $candidate_query->results as $user ) {
	    $candidate_details["label"] = $user->display_name;
	    $candidate_details["value"] = $user->ID;
	    $candidates_detail_arr[] = $candidate_details;
	}
    } 
    
    $candidate_details_string = json_encode($candidates_detail_arr);
    
    echo '<div class="employer_candidae_searchbox_container" >';
	echo '<form method="get" action="" >';
	    echo '<input type="hidden" name="component" value="'.$component.'" />';
	    echo '<input type="hidden" name="subcomponent" value="'.$subcomponent.'" />';
	    echo '<input type="hidden" id="candidate_search_member_id" name="member_id" value="" />';
	    echo '<input type="text"  name="candidate_search_text_box" id="candidate_search_text_box" placeholder="Search Candidates here..." />';
	    echo '<input type="submit" style="margin-top: 10px;" name="candidate_send_message_button" id="candidate_send_message_button" class="button button-medium" value="'.$button_label.'" />';
	echo '</form>';
	echo '<input type="hidden" name="candidate_details_arr" id="candidate_details_arr" value="'.  esc_attr( $candidate_details_string ).'" />';
    echo '</div>';
    
}