/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
jQuery(document).ready(function(){
    var doing_ajax = false;
    jQuery(".delete_thread_link").click(function(){
	
	if( doing_ajax ){
	    alert("Please wait your request is getting processed...");
	    return false;
	}
	
	if( !confirm( "Are you sure you want to delete this message??" ) ){
	    return false;
	}
	
	var curr_obj = jQuery(this);
	var mid = curr_obj.attr("id");
	
	var data = {
	    action: 'cc_messages_delete',
	    mid: mid
	}
	doing_ajax = true;
	jQuery.ajax({
	    type: 'POST',
	    url: CC_MSG.ajaxurl,
	    data: data,
	    success:function( response ){
		doing_ajax = false;
		curr_obj.parents("td").remove();
	    },
	    error:function( response ){
		doing_ajax = false;
	    }
	});
	
	return false;
    });
    
    jQuery(".candidate_delete_thread_link").click(function(){
	
	if( doing_ajax ){
	    alert("Please wait your request is getting processed...");
	    return false;
	}
	
	if( !confirm( "Are you sure you want to delete this message??" ) ){
	    return false;
	}
	
	var curr_obj = jQuery(this);
	var mid = curr_obj.attr("id");
	
	var data = {
	    action: 'cc_candidate_messages_delete',
	    mid: mid
	}
	doing_ajax = true;
	jQuery.ajax({
	    type: 'POST',
	    url: CC_MSG.ajaxurl,
	    data: data,
	    success:function( response ){
		doing_ajax = false;
		curr_obj.parents("td").remove();
	    },
	    error:function( response ){
		doing_ajax = false;
	    }
	});
	
	return false;
    });
    
    
});