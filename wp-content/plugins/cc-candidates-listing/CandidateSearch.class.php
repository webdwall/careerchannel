<?php 
class CandidatesSearch {
    private static $instance;
	public $per_page = 10;
	
    private function __construct() {
		
    }
	
    public static function get_instance() {
		if (!isset(self::$instance)) {
			self::$instance = new self();
		}
		return self::$instance;
    }
	
	/**
	 * returns the arguments for WP_User_Query for candidate search.
	 * 
	 * @return array
	 */
	function candidate_search_args(){
		$current_page = isset( $_GET['list'] ) ? $_GET['list'] : 1;
		$offset = ( $current_page * $this->per_page ) - $this->per_page;
		
		$meta_query = array(
			'relation'	=> 'AND'
		);
		
		$meta_query_params = array(
			'specialization'	=> 'skill',
			'education'		=> 'education',
			'currency'		=> 'salary',
			'min_salary'		=> 'expected_salary',
			'max_salary'		=> 'expected_salary',
			'position'		=> 'position',
			'experience'		=> 'experience_in_months'
		);
		
		foreach( $meta_query_params as $urlparam=>$meta_key ){
			if( isset( $_GET[$urlparam] ) && $_GET[$urlparam]!='' ){
				
				$meta_compare_key = ( $urlparam == "experience" || $urlparam == "min_salary" || $urlparam == "max_salary" ) ? ( $urlparam == "max_salary" ) ? '<' : '>' :'LIKE';
				
				if( $urlparam == "currency" ){
				    if( $_GET["min_salary"] == "" && $_GET["max_salary"] == "" ){
					continue;
				    }
				}
				
				if( $urlparam == "specialization" || $urlparam == "education" || $urlparam == "position" ){
					
				    $meta_values = $_GET[$urlparam];
				    $meta_compare_key = "LIKE";
				    
				}else{
				    if( $urlparam == "currency" ){
					$meta_values = $_GET[$urlparam];
				    }else{
					$meta_values = (int)$_GET[$urlparam];
				    }
				}
				if( $urlparam == "min_salary" || $urlparam == "max_salary" || $urlparam == "experience" ){
				    $meta_query[] = array(
					    'key'     => $meta_key,
					    'value'   => $meta_values,
					    'type'    => 'numeric',
					    'compare' => $meta_compare_key
				    );
				}else{
				    $meta_query[] = array(
					    'key'     => $meta_key,
					    'value'   => $meta_values,
					    'compare' => $meta_compare_key
				    );
				}
			}
		}
		/* This is to hide candidate Profile according to the setting made by the candidates */
		$meta_query[] = array(
			'key'     => "cc_hide_profile",
			'value'   => "yes",
			'compare' => "NOT EXISTS"
		);
			
		$args = array(
			'role'			=> 'Subscriber',
			'fields'		=> 'all_with_meta',
			'offset'		=> $offset,
			'number'		=> $this->per_page,
			'orderby'		=> 'ID',//display new candidaets first
			'order'			=> 'DESC',
			'meta_query'		=> $meta_query
		);
		return $args;
	}

	function specialization_options( $selected ){
		if( $selected ){
			$selected = explode(",", $selected);
		}
		
		$skills = get_transient('cc_specialization_options');
		if( $skills===false ){
			global $wpdb;
			$query  = "SELECT s.id, s.skill_name, sc.cat_name FROM " . $wpdb->prefix ."skills s ";
			$query .= "	JOIN " . $wpdb->prefix ."skills_cat sc ON s.skill_cat = sc.id ";
			$query .= "	WHERE s.status=true ORDER BY sc.id";
			$skills = $wpdb->get_results( $query );
			
			//store in transient cache
			if( $skills && !empty( $skills ) ){
				set_transient( 'cc_specialization_options', $skills, CC_TRANSIENT_TIME );
			}
		}
		
		$options_html = "";
		if( $skills && !empty( $skills ) ){
			$attr_selected = ( empty($selected) || $selected == "" ) ? ' selected' : '';
			$options_html .= "<option $attr_selected value=''>All</option>";
			foreach ($skills as $skill) {
				$attr_selected = ( !empty($selected) && in_array($skill->skill_name, $selected) ) ? ' selected' : '';
				$options_html .= "<option $attr_selected value='". $skill->skill_name ."'>". $skill->skill_name ."</option>";
			}
		}
		
		return $options_html;
	}
	
	function education_options( $selected ){
		if( $selected ){
			$selected = explode(",", $selected);
		}
		$skills = get_transient('cc_education_options');
		if( $skills === false ){
			global $wpdb;
			$query  = "SELECT id, name FROM " . $wpdb->prefix ."educational_qualification ";
			$query .= "	WHERE status=true ORDER BY name";
			$skills = $wpdb->get_results( $query );
			
			//store in transient cache
			if( $skills && !empty( $skills ) ){
				set_transient( 'cc_education_options', $skills, CC_TRANSIENT_TIME );
			}
		}
		
		$options_html = "";
		if( $skills && !empty( $skills ) ){
			$attr_selected = ( empty($selected) || $selected == "" ) ? ' selected' : '';
			$options_html .= "<option $attr_selected value=''>All</option>";
			foreach ($skills as $skill) {
				$attr_selected = (!empty($selected) && in_array($skill->name, $selected) ) ? ' selected' : '';
				$options_html .= "<option $attr_selected value='". $skill->name ."'>". $skill->name ."</option>";
			}
		}
		
		return $options_html;
	}
	
	function position_options( $selected ){
		$options = array(
			''					=> 'All',
			'Senior Manager'			=> 'Senior Manager',
			'Manager'				=> 'Manager',
			'Senior Executive'			=> 'Junior Executive',
			'Fresh Entry/Level'			=> 'Fresh Entry/Level',
			'Non Executive'				=> 'Non Executive',
			'Student Seeking Intership'		=> 'Student Seeking Intership'
		);
		
		$options_html = "";
		foreach( $options as $option_val=>$option_label ){
			$attr_selected = ( $selected==$option_val ) ? ' selected' : '';
			$options_html .= "<option $attr_selected value='". esc_attr($option_val) ."'>$option_label</option>";
		}
		
		return $options_html;
	}
	
	function currency_options( $selected ){
		if( !$selected ){
			$selected = 'SGD';
		}
		$options = array(
			'SGD'	=> 'SGD',
			'USD'	=> 'USD'
		);
		
		$options_html = "";
		foreach( $options as $option_val=>$option_label ){
			$attr_selected = ( $selected==$option_val ) ? ' selected' : '';
			$options_html .= "<option $attr_selected value='". esc_attr($option_val) ."'>$option_label</option>";
		}
		
		return $options_html;
	}
	
	function experience_options( $selected ){
	    $options = array(
			"6"		=> '6 Months',
			"12"	=> '1 Year',
			"24"	=> '2 Years',
			"36"	=> '3 Years',
			"60"	=> '5 Years'
	    );
	    
	    $options_html = "";
	    foreach( $options as $option_val => $option_label ){
		    $attr_selected = ( $selected==$option_val ) ? ' selected' : '';
		    $options_html .= "<option $attr_selected value='". esc_attr($option_val) ."'>$option_label</option>";
	    }

	    return $options_html;
	    
	}
}