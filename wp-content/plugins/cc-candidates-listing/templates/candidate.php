<?php
/**
 * The public profile of individual candidate
 *
 */

get_header(); ?>
	
	<div id="primary" class="content-area">
		<div id="content" class="site-content full user-details candidate-details" role="main">
			<?php 
			$is_accesible = true;
			if( !is_user_logged_in() ){
				$is_accesible = false;
				auth_redirect();
				exit();
			}
			else if( is_user_a_candidate() && get_current_user_id()!=get_query_var('member_id') ){
				$is_accesible = false;
				echo "<p class='text-error'><strong>Error!</strong> Restricted content.</p>";
			}
			
			if( $is_accesible ){
				$candidate = get_user_by('id', get_query_var('member_id'));
				if( !$candidate ){
					echo "<p class='text-error'>No such candidate found!</p>";
				}
				else{
					get_template_part('members/candidate', 'details');
				}
			}
			?>
		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>