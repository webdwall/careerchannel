<?php
/**
 * The public profile of individual employer
 *
 */
get_header(); ?>
	
	<div id="primary" class="content-area">
		<div id="content" class="site-content full user-details employer-details" role="main">
			<?php 
			$is_accesible = true;
			if( !is_user_logged_in() ){
				$is_accesible = false;
				auth_redirect();
				exit();
			}
			
			if( $is_accesible ){
				$employer = get_user_by('id', get_query_var('member_id'));
				if( !$employer ){
					echo "<p class='text-error'>No such employer found!</p>";
				}
				else{
					get_template_part('members/employer', 'details');
				}
			}
			?>
		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>