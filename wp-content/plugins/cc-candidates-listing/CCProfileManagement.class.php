<?php 
class CCProfileManagement {
    private static $instance;
    public $per_page = 10;
	
    private function __construct() {
	    add_filter( 'ccuserinfo_experience_in_months', array( $this, 'convert_months_to_year_and_months' ) );
	    add_action( 'cc_member_badges', array( $this, 'print_member_badges' ), 10, 2 );
	    add_action( 'cc_member_action_buttons', array( $this, 'print_member_action_buttons' ), 10, 3 );		
	    add_filter( 'ccuserinfo_skill', array( $this, 'get_candidate_skills_names' ) );

	    add_action( 'wp_enqueue_scripts',array( $this,'add_js' ) );
	    add_action( 'wp_ajax_save_search_ajax',array( $this, 'ajax_save_search' ) );
	    add_action( 'wp_ajax_delete_search_ajax',array( $this, 'ajax_delete_search' ) );
    }
	
    /**
     * Formats the given number of months in the form of number of years and number of months
     * @param type int $number_of_months 
     * @return string formatted value example: 20 (months) will be converted to 1yr 8months
     */
    function convert_months_to_year_and_months( $number_of_months ){
	    $formatted_val = '';
	    $number_of_months = (int)$number_of_months;

	    if( $number_of_months==12 ){
		    $formatted_val = '1yr';
	    }
	    else if( $number_of_months<12 ){
		    $formatted_val = $number_of_months . 'months';
	    }
	    else{
		    $quotient = $number_of_months / 12;
		    $years = (int) ($quotient < 0 ? ceil($quotient) : floor($quotient));
		    $months = $number_of_months % 12;
		    $formatted_val = $years .( ( $years>1 ) ? 'yrs' : 'yr' );
		    if( $months ){
			    $formatted_val .= ' ' . $months . ( ( $months>1 ) ? 'months' : 'month' );
		    }
	    }

	    return $formatted_val;
    }

    /**
     * 
     * @param type string $skills
     * @return $skills
     */
    function  get_candidate_skills_names( $skills ){
		return $skills;
    }

    /**
     * Prints the html for user badges
     * @param string $user_type either 'candidate' or 'employer'
     * @param object(WP_User) $user
     * 
     * @return void
     */
    function print_member_badges( $user_type, $user ){
	    echo "<li class='user_type'><span class='label label-primary'>$user_type</span></li>";

	    //badges for employers
	    if( 'employer'==$user_type ){
		    $membership_details = member_package_details($user->ID);

		    if( ''==$membership_details['package'] ){
			    $membership_details['package']=='free';
		    }
		    echo "<li class='employer-type employer-type-". esc_attr( $membership_details['package'] ) ."'><span class='glyphicon glyphicon-certificate'></span> " . $membership_details['package'] . " member</li>";
	    }
    }

    /**
     * Prints the html for member action buttons
     * @param string $user_type either 'candidate' or 'employer'
     * @param object(WP_User) $user
     * @param string $content e.g: 'loop' or 'profile'. Default 'profile'
     * Differentiates if these action buttons are being printed on user profile page or user-loop template
     * 
     * @return void
     */
    function print_member_action_buttons( $user_type, $user, $context ){
	    if( !$context ){
		    $context = 'profile';
	    }

	    $buttons = $this->get_member_action_buttons( $user_type, $user, $context );
	    if( empty( $buttons ) ){
		    return;
	    }
	    echo '<span class="btn-group member_action_buttons">';
	    foreach( $buttons as $button ){
		    echo $button;
	    }
	    echo '</span>';
    }

    function get_member_action_buttons( $user_type, $user, $context ){
	    $buttons = array();
	    if( $user_type=='candidate' ){
		    //view profile link
		    if( $context=='loop' ){
			    $buttons[] = "<a class='btn btn-default btn-small show-tooltip' href='". home_url( '/candidates/' ). $user->ID ."' title='view profile'><span class='glyphicon glyphicon-zoom-in'></span></a>";
		    }

		    //message link
		    $buttons[] = "<a class='btn btn-default btn-small show-tooltip' href='". add_query_arg( 'member_id', $user->ID, cc_user_dashboard_url('employer', 'messages', 'compose') ) ."' title='send message'><span class='glyphicon glyphicon-envelope'></span></a>";

		    //schedule appointment link
		    $buttons[] = "<a class='btn btn-default btn-small show-tooltip' href='". add_query_arg( 'member_id', $user->ID, cc_user_dashboard_url('employer', 'appointments', 'add') ) ."' title='Schedule appointment'><span class='glyphicon glyphicon-calendar'></span></a>";

		    //bookmark link
		    if( function_exists( 'cc_get_candidate_bookmark_link' ) && !is_user_a_candidate(get_current_user_id()) ){
			    $buttons[] = cc_get_candidate_bookmark_link( $user->ID );
		    }
		    
		    
	    }
	    return apply_filters( 'get_member_action_buttons', $buttons, $user_type, $user, $context );
    }
	
    public static function get_instance() {
		if (!isset(self::$instance)) {
			self::$instance = new self();
		}
		return self::$instance;
    }
    
    function add_js(){
	wp_enqueue_script(
            "cc_save_search",
            path_join( WP_PLUGIN_URL, basename( dirname( __FILE__ ) )."/JS/save_search.js" ),
            array( 'jquery' )
        );
    }
    
    function ajax_save_search(){
    	
    	$specialization = $_POST['specialization'];
	$education = $_POST['education'];
	$position  = $_POST['position'];
	$currency  = $_POST['currency'];
	$min_salary = $_POST['min_salary'];
	$max_salary = $_POST['max_salary'];
	$users_per_page_txt = $_POST['users_per_page_txt'];
	$experience = $_POST['experience'];
	
	$search_title = $_POST["title"];
	/*
	*
	* save this data in user meta  

	$save_search['specialization'];
	$save_search['education'];
	$save_search['position'];
	*/
	//update_user_meta('search_saved_data',);
	$save_search = get_user_meta(get_current_user_id(),'search_saved_data',true);
	//var_dump($save_search);

	$args = array(
	    'title'		=> $search_title,
	    'specialization'	=> $specialization,
	    'education'		=> $education,
	    'position'		=> $position,
	    'currency'		=> $currency,
	    'min_salary'	=> $min_salary,
	    'max_salary'	=> $max_salary,
	    'users_per_page_txt'=> $users_per_page_txt,
	    'experience'	=> $experience
	);
	$save_search[] = $args;
	update_user_meta(get_current_user_id(),'search_saved_data',$save_search);
	die("ok");
    }
    
    function ajax_delete_search(){
	$user_id = get_current_user_id();
	$array_id = $_POST["arr_id"];
	$save_search = get_user_meta($user_id,'search_saved_data',true);
	array_splice($save_search, $array_id, 1);
	update_user_meta( $user_id ,'search_saved_data',$save_search);
	die("ok");
    }
    
}
CCProfileManagement::get_instance();//instantiate, just in case