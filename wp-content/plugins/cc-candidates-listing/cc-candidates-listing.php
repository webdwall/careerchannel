<?php
/*
Plugin name: CareerChannel Members Listing
Description: Handles candidate/employer profile display, candidate search etc.
version: 0.2
Author: E Media Identity
Author URI: http://emediaidentity.com/
*/
define( 'CC_CANDIDATES_LISTING_PATH', plugin_dir_path( __FILE__ ) );
include_once ( CC_CANDIDATES_LISTING_PATH . '/functions.php' );
include_once ( CC_CANDIDATES_LISTING_PATH . '/template.php' );
include_once ( CC_CANDIDATES_LISTING_PATH . '/CandidateSearch.class.php' );
include_once ( CC_CANDIDATES_LISTING_PATH . '/CCProfileManagement.class.php' );
//include_once ( CC_CANDIDATES_LISTING_PATH . '/bookmark-candidates.php' );

add_action('init', 'cc_candidates_listing_init');
function cc_candidates_listing_init() {
    CCCandidatesListing::get_instance();
}

class CCCandidatesListing {
    // static function only use static function hence this var is used in  get_instance() method.
    private static $instance;

    private function __construct() {
		add_filter('user_contactmethods', array( $this, 'remove_extra_contact_info' ) );
		$this->rewrite_rules();
		add_filter( 'query_vars', array( $this, 'register_query_var' ) );
		//add_filter( 'the_content', array( $this, 'test' ) );
		add_action( 'template_redirect', array( $this, 'url_rewrite_templates' ) );
    }

	function test($content){
		if( is_page('candidates') ){
			$content .= 'member_id is :' . get_query_var( 'member_id' );
		}
		return $content;
	}
	
    public static function get_instance() {
		if (!isset(self::$instance)) {
			self::$instance = new self();
		}
		return self::$instance;
    }
	
	function remove_extra_contact_info($contactmethods) {
		unset($contactmethods['aim']);
		unset($contactmethods['yim']);
		unset($contactmethods['jabber']);
		
		return $contactmethods;
	}
	
	function rewrite_rules(){
		//author/([^/]+)/?$	index.php?author_name=$matches[1]
		add_rewrite_rule( 'candidates/([0-9/]+)', 'index.php?pagename=candidates&member_id=$matches[1]', 'top' );
		add_rewrite_rule( 'employers/([0-9/]+)', 'index.php?pagename=employers&member_id=$matches[1]', 'top' );
	}
	
	function register_query_var( $vars ) {
		$vars[] = 'member_id';

		return $vars;
	}
	
	function url_rewrite_templates() {
		if ( get_query_var( 'member_id' ) && is_page( 'candidates' ) ) {
			add_filter( 'template_include', function() {
				return CC_CANDIDATES_LISTING_PATH . 'templates/candidate.php';
			});
		}
		
		if ( get_query_var( 'member_id' ) && is_page( 'employers' ) ) {
			add_filter( 'template_include', function() {
				return CC_CANDIDATES_LISTING_PATH . 'templates/employer.php';
			});
		}
	}
}
