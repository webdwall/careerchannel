<?php
/**
 * checks if the loggedin user should be allowed to view the given info about the given candidate
 * 
 * @param int|object(WP_User) the user(id) or user(object)
 * @param string $field
 * 
 * @return bool
 */
function can_view_candidate_info( $candidate_in_question, $field ){
	if( !is_object($candidate_in_question) ){
		$candidate_in_question = new WP_User($candidate_in_question);
	}
	//visitors wont be able to see any detail, whatsoever.
	if( !is_user_logged_in() ){
		return false;
	}
	
	$is_visible=true;
	//candidates shouldn't be allowed to view details of any other candidate(except himself/herself)
	if( is_user_a_candidate($candidate_in_question) && is_user_a_candidate( get_current_user_id() ) ){
		if( get_current_user_id()!=$candidate_in_question->ID ){
			$is_visible = false;
		}
	}
	return $is_visible;
}

/**
 * checks if the loggedin user should be allowed to view the given info about the given employer
 * 
 * @param object(WP_User) $employer_in_question
 * @param string $field
 * 
 * @return bool
 */
function can_view_employer_info( $employer_in_question, $field ){
	//visitors wont be able to see any detail, whatsoever.
	if( !is_user_logged_in() ){
		return false;
	}
	
	//dummy code, returning always true, replace with acutal condition checks
	return true;
}

/**
 * Generates html for link to given candidates resume
 * 
 * @param int $candidate_id the id of candidate whose resume link is required
 * @return string the html for resume link(button)
 */
function cc_resume_button( $candidate_id ){
	$resume_button = '';
	//resume link
	if( function_exists( 'cc_members_get_resume_url' ) ){
		$resume_link = cc_members_get_resume_url( $candidate_id );

		$glyphicon = 'glyphicon glyphicon-folder-open';
		$title = 'View Resume';
		$class = 'btn btn-default btn-small' ;
		if( !$resume_link ){
			$glyphicon = 'glyphicon glyphicon-folder-close';
			$title = 'resume not uploaded';
			$class .= ' disabled';

			$resume_button  = "<div class='show-tooltip' title='$title'>";
			$resume_button .=	"<a class='$class' href='#'><span class='$glyphicon'></span> View Resume</a>";
			$resume_button .= "</div>";
		}
		else{
			$class .= ' show-tooltip dropdown-toggle';
			$resume_button  = "<span class='btn-group'>";
			$resume_button .=	"<a class='$class' href='". esc_attr( $resume_link ) ."' title='$title'><span class='$glyphicon'></span> View Resume</a>";
			$resume_button .=	"<a class='btn btn-default btn-small dropdown-toggle' href='#' data-toggle='dropdown'><span class='caret'></span></a>";
			$resume_button .=	"<ul class='dropdown-menu'>";
			$resume_button .=		"<li><a target='_blank' href='". esc_attr( $resume_link ) ."'>Open in new tab</a></li>";
			$resume_button .=	"<ul>";
			$resume_button .= "</span>";
		}
	}
	
	return $resume_button;
}

if( !function_exists('emi_generate_paging_param') ):
/**
 * Prints pagination links for given parameters with pagination value as a querystring parameter.
 * Instead of printing http://yourdomain.com/../../page/2/, it prints http://yourdomain.com/../../?list=2
 * 
 * If your theme uses twitter bootstrap styles, define a constant :
 * define('BOOTSTRAP_ACTIVE', true)
 * and this function will generate the bootstrap-pagination-compliant html. 
 * 
 * @author @ckchaudhary
 * @param int $total_items total number of items(grand total)
 * @param int $items_per_page number of items displayed per page
 * @param int $curr_paged current page number, 1 based index
 * @param string $slug part of url to be appended after the home_url and before the '/?ke1=value1&...' part. withour any starting or trailing '/'
 * @param string $hashlink Optional, the '#' link to be appended ot url, optional
 * @param int $links_on_each_side Optional, how many links to be displayed on each side of current active page. Default 2.
 * @param string $param_key the name of the queystring paramter for page value. Default 'list'
 *
 * @return void
 */
function emi_generate_paging_param($total_items, $items_per_page, $curr_paged, $slug, $links_on_each_side=2, $hashlink="", $param_key="list"){
	$use_bootstrap = false;
	if( defined( 'BOOTSTRAP_ACTIVE' ) ){
		$use_bootstrap = true;
	}
	
    $s = $links_on_each_side; //no of tabs to show for previos/next paged links
    if($curr_paged == 0){$curr_paged=1;}
    /*$elements : an array of arrays; each child array will have following structure
    $child[0] = text of the link
    $child[1] = page no of target page
    $child[2] = link type :: link|current|nolink
    */
    $elements = array(); 
    $no_of_pages = ceil($total_items/$items_per_page);
    //prev lik
    if($curr_paged > 1){$elements[] = array('&laquo; Previous', $curr_paged-1, 'link');}
    //generating $s(2) links before the current one
    if($curr_paged > 1){
        $rev_array = array();//paged in reverse order
        $i = $curr_paged-1;
        $counter = 0;
        while($counter<$s && $i>0){
            $rev_array[] = $i;
            $i--;
            $counter++;
        }
        $arr = array_reverse ($rev_array);
        if($counter==$s){$elements[] = array(' ... ', '', 'nolink');}
        foreach($arr as $el){
            $elements[] = array($el, $el, 'link');
        }
        unset($rev_array); unset($arr); unset($i); unset($counter);
    }
    
    //generating $s+1(3) links after the current one (includes current)
    if($curr_paged <= $no_of_pages){
        $i = $curr_paged;
        $counter = 0;
        while($counter<$s+1 && $i<=$no_of_pages){
            if($i==$curr_paged){
                $elements[] = array($i, $i, 'current');
            }
            else{
                $elements[] = array($i, $i, 'link');
            }
            $counter++; $i++;
        }
        if($counter==$s+1){$elements[] = array(' ... ', '', 'nolink');}
        unset($i); unset($counter);
    }
    //next link
    if($curr_paged < $no_of_pages){$elements[] = array('Next &raquo;', $curr_paged+1, 'link');}
    /*enough php, lets echo some html*/
    if(isset($elements) && count($elements) > 1){?>
        <div class="navigation">
			<?php if( $use_bootstrap ):?>
				<ul class='pagination'>
			<?php else: ?>
				<ol class="wp-paginate">
			<?php endif;?>
                <?php
                foreach($elements as $e){
                    $link_html = "";
					$class = "";
                    switch($e[2]){
                        case 'link':
                            unset($_GET[$param_key]);
                            $base_link = get_bloginfo('url')."/$slug?";
                            foreach($_GET as $k=>$v){
                                $base_link .= "$k=$v&";
                            }
                            $base_link .= "$param_key=$e[1]";
                            if(isset($hashlink) && $hashlink!=""){
                                $base_link .="#$hashlink";
                            }
                            $link_html = "<a href='$base_link' title='$e[0]' class='page-numbers'>$e[0]</a>";
                            break;
                        case 'current':
							$class = "active";
							if( $use_bootstrap ){
								$link_html = "<span>$e[0] <span class='sr-only'>(current)</span></span>";
							} else {
								$link_html = "<span class='page-numbers current'>$e[0]</span>";
							}
                            break;
                        default: 
							if( $use_bootstrap ){
								$link_html = "<span>$e[0]</span>";
							} else {
								$link_html = "<span class='page-numbers'>$e[0]</span>";
							}
                            break;
                    }
					
					$link_html = "<li class='". esc_attr($class) ."'>" . $link_html . "</li>";
					echo $link_html;
                }
                ?>
            <?php if( $use_bootstrap ):?>
				</ul>
			<?php else:?>
				</ol>
			<?php endif;?>
        </div>
    <?php
    }
}
endif;

add_action('wp_footer','user_menu_name');
function user_menu_name(){
    
    if(is_user_logged_in() ){
	$user = get_userdata( get_current_user_id() );
	$username = $user->display_name;
	echo "<input type='hidden' name='emi_ng_user_menu_name'  id='emi_ng_user_menu_name' value='$username' />";
    }
}