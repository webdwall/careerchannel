var ajaxcount = true;
var title = "";
var specialization = "";
var education = "";
var position = "";
var currency = "";
var min_salary = "";
var max_salary = "";
var users_per_page_txt = "";
var experience = "";
jQuery(document).ready(function($){
	
	set_curr_username_in_header_menu();
    
	$(".standard-form  .save_search_info ").click(function(){
	    
	    specialization = $(".standard-form #specialization").val();
	    education = $(".standard-form #education").val();
	    position = $(".standard-form #position").val();
	    currency = $("#search_extended #currency").val();
	    min_salary = $("#search_extended #min_salary").val();
	    max_salary = $("#search_extended #max_salary").val();
	    users_per_page_txt = $("#search_extended #users_per_page_txt").val();
	    experience = $("#search_extended select[name='experience']").val();

	    //console.log( specialization + "  ::  " + education + "  ::  " + position + "  ::  " + min_salary + "  ::  " + max_salary + "  ::  " + users_per_page_txt + "  ::  " + experience );

	    if( specialization != "" || education != "" || position != "" || min_salary != "" || max_salary != "" || users_per_page_txt != "" && experience > 0 ){
            	jQuery("#dialog").dialog({ modal: true, draggable: false, height: 270, width: 400  });
            	return false;
            }
            else{
            	alert( "Please select filter parameter first!!" );
            }

	    return false;
	});

	$(".save_search_popup #set_title").click(function(){
	    var doing_ajax = false;
	    title = jQuery(".save_search_popup #searcc_title_id").val();
	    
	    if( title == "" ){
		alert("Title is requuired!!");
		return false;
	    }
	    var curr_obj = jQuery(this);
	    //console.log( specialization + "  ::  " + education + "  ::  " + position + "  ::  " + min_salary + "  ::  " + max_salary + "  ::  " + users_per_page_txt + "  ::  " + experience + "  :: "+title );
	    
            /*
	     * if all fields are empty then dont allow to save empty data.
	     * check here if all conditions are valid then show a popup.
	     */
	    doing_ajax = true;
	    jQuery(this).after("<p class='text-info search_form_message' >Processing request..</p>");
	    jQuery.ajax({ 
                type: 'POST',  
                url: CCBMC_.ajaxurl,  
                data: {  
		    specialization: specialization,
		    education: education,
		    position: position,
		    currency: currency,
		    min_salary: min_salary,
		    max_salary: max_salary,
		    users_per_page_txt: users_per_page_txt,
		    experience: experience,
		    title: title,
		    action: 'save_search_ajax'
                },
                success:function(response){
		    doing_ajax = false;
		    jQuery(".save_search_popup .search_form_message").remove();
		    curr_obj.after("<p class='text-success' >Added Sunccessfully...</p>");
		    jQuery(".save_search_popup #searcc_title_id").val("");
                },
		error:function( response ){
		    alert("some problem occured please try again!!");
		    doing_ajax = false;
		}
            });
	});
	
	jQuery(".saved_search_table .delete_search_filter").click(function(){
	    var array_id = jQuery(this).attr("id");
	    var data = {
		arr_id: array_id,
		action: 'delete_search_ajax'
	    };
	    jQuery.ajax({
		type: 'POST',
		url: CCBMC_.ajaxurl,
		data: data,
		success:function( response ){
		    location.reload();
		},
		error:function( response ){}
	    });
	    return false;
	});
	
});

function set_curr_username_in_header_menu(){
    var username = jQuery("#emi_ng_user_menu_name").val();
    
    jQuery(" #menu-item-148 > a, #menu-item-160 > a ").text( username );
}