<?php 
/**
 * Adds the given candidate to given employers bookmark collection
 * 
 * @param int $candidate_id the candidate to bookmark
 * @param int $employer_id the employer who is adding the candidate to his collection
 * @param int $collection_id the collection which the bookmark should be added to
 * 
 * @return bool
 */
function cc_candidate_bookmark_add( $candidate_id, $employer_id, $collection_id ){
	
}

/**
 * Removes the given candidate frp, given employers bookmark collection
 * 
 * @param int $candidate_id the candidate to bookmark
 * @param int $employer_id the employer who is adding the candidate to his collection
 * @param int $collection_id the collection which the bookmark should be added to
 * 
 * @return bool
 */
function cc_candidate_bookmark_remove( $candidate_id, $employer_id, $collection_id ){
	
}

/**
 * Prints the html for 'add to bookmark' link
 * 
 * @param WP_User $candidate
 */
function cc_candidate_bookmark_link( $candidate ){
	if( !is_object($candidate) ){
		$candidate = new WP_User($candidate);
	}
	
	//cehck if candidate is already added and generate html accordingly
}