<?php 
/* 
Plugin Name: CareerChanel notification system.
Description: Plugin for instant notification to loggedin members about events of their concern.
Version: 1.1
Author: E Media Identity
Author URI: http://emediaidentity.com/
*/
//require_once( plugin_dir_path( __FILE__ ) . 'functions.php' );

add_action( 'init', array( 'CCNotifications', 'get_instance' ) );

class CCNotifications{
    private static $instance;
    protected $umeta_name = 'cc_notf_last_checked';
	
    public static function get_instance(){
        if(!isset(self::$instance)){
            self::$instance=new self();
        }
        return self::$instance;
    }

    function __construct() {
		if( is_user_logged_in() ){
			add_action( 'wp_footer',					array( $this, 'udpate_notification_last_checked_time' ) );
			add_action(	'wp_enqueue_scripts',			array( $this, 'add_js' ) );
			add_action( 'wp_ajax_cc_get_notifications', array( $this, 'ajax_get_user_notifications' ) );
			add_action( 'wp_footer',					array( $this, 'print_css' ) );
		}
    }

	function udpate_notification_last_checked_time(){
		update_user_meta( get_current_user_id(), $this->umeta_name, current_time('mysql') );
	}
	
    function add_js(){
		if( is_user_logged_in() ){
			wp_enqueue_script(
				"cc_notifications",
				path_join( WP_PLUGIN_URL, basename( dirname( __FILE__ ) )."/script.js" ),
				array( 'jquery' )
			);
			
			$data = array(
				'action'=>'cc_get_notifications',
				'nonce'	=> wp_create_nonce('ccnotifications_get'),
				'ajaxurl' => admin_url('/admin-ajax.php'),
				'wavfile_url' => path_join( WP_PLUGIN_URL, basename( dirname( __FILE__ ) )."/ding.wav" ),
			);
			wp_localize_script( 'cc_notifications', 'CC_NOTIFY', $data );
		}
    }
	
	function print_css(){
		?>
		<style type='text/css'>
			#ccnotifications_wrapper{ position: fixed; top: 10px; right: 20px; z-index: 1001;}
			#ccnotifications_wrapper .notification{ position:relative; width: 300px; margin: 0 0 10px 0; border: 2px solid #d6e9c6; background: #dff0d8; color:#468847; }
			#ccnotifications_wrapper .notification a.close_notification{ position: absolute; top: -5px; right: -5px; border: 1px solid #aaa; background: #eee; color:#333; text-decoration: none; font-size: 10px; padding: 0px 3px; border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px; }
			#ccnotifications_wrapper .notification p.notification_content{ padding: 10px; margin: 0; }
			#ccnotifications_wrapper .notification p.notification_content a{ color: #000; font-style: italic; }
		</style>
		<?php 
	}
	
	/**
	 * Get new notifications for given user.
	 * @param type $user_id
	 * @return array list of notifications
	 */
	function get_notifications( $user_id=false ){
		if( !$user_id ){
			$user_id = get_current_user_id();
		}
		
		if( ($last_checked = get_user_meta($user_id, $this->umeta_name, true))==false ){
			$last_checked = current_time( 'mysql' );
		}
		
		/**
		 * This is where all the other modules should hook into to register a new notification.
		 * 
		 * One notification corresponds to one array, of the following structure
		 *	'text'=> 'xyz replied to your message',
		 *  'type'=> 'message'
		 *  'url' => 'http://domain.com/dashboard/?component=messages&subcomponent=inbox&thread_id=154#reply-7881'
		 */
		return apply_filters( 'cc_user_notifications', array(), $user_id, $last_checked  );
	}
	
	function ajax_get_user_notifications(){
		check_admin_referer( 'ccnotifications_get' );
		$retval = array(
			'status' => false,
			'notifications'=>array()
		);
		$notifications = $this->get_notifications();
		if( $notifications && !empty( $notifications ) ){
			$retval['status'] = true;
			$retval['notifications'] = $notifications;
		}
		
		//update the last checked time
		$this->udpate_notification_last_checked_time();
		
		die( json_encode( $retval ) );
	}
}