jQuery(document).ready(function($){
    CC_NOTIFY.debug = false;//@todo set to false once testing done
    CC_NOTIFY.doingajax = false;
    CC_NOTIFY.time = 30000;//30 seconds
    CC_NOTIFY.wrapperid = 'ccnotifications_wrapper';
    CC_NOTIFY.audio_element_id = 'cc_notification_player';
    
    t = setTimeout( cc_notifications_check, CC_NOTIFY.time );
    
    //lets preload the 'sound' played on notification
    jQuery('body').append('<audio id="'+CC_NOTIFY.audio_element_id+'" src="'+ CC_NOTIFY.wavfile_url +'" style="display:none;" controls preload="auto"></audio>');
});

function cc_notifications_check(){
    if (CC_NOTIFY.doingajax){
	t = setTimeout(cc_notifications_check, CC_NOTIFY.time);
	return false;
    }
    
    var data = {
	action: CC_NOTIFY.action,
	_wpnonce: CC_NOTIFY.nonce
    };

    CC_NOTIFY.doingajax = true;
    if( CC_NOTIFY.debug )
	console.log("started ajax request");

    jQuery.ajax({
	type: "POST",
	url: CC_NOTIFY.ajaxurl,
	data: data,
	success: function (response) {
	    if( CC_NOTIFY.debug )
		console.log( "ajax request completed, aborting. Here's the response: %o", response );
	    cc_notifications_callback(response);
	    CC_NOTIFY.doingajax = false; //reset it so that next ajax request can process
	}
    });
    t = setTimeout(cc_notifications_check, CC_NOTIFY.time);
}

function cc_notifications_callback( response ){
    response = jQuery.parseJSON( response );
    if( response.status ){
	if( jQuery('#'+CC_NOTIFY.wrapperid).length == 0 ){
	    jQuery('body').append("<div id='"+ CC_NOTIFY.wrapperid +"'></div>");
	}
	for( var i=0; i<response.notifications.length; i++ ){
	    cc_notifications_print_notification( response.notifications[i] );
	}
	
	cc_notification_play_audio();
	cc_notifications_rebind_events();
    }
}

function cc_notifications_print_notification( notification ){
    if( CC_NOTIFY.debug ){
	console.log('adding new notification %o', notification);
    }
    var html  = "<div class='notification notification-"+notification.type+"'>";
	html +=	    "<a class='close_notification' href='#'>X</a>";
	html +=	    "<p class='notification_content'>";
	html +=		"<a href='"+notification.url+"'>"+notification.text+"</a>";
	html +=	    "</p>";
	html += "</div>";
    
    jQuery('#'+CC_NOTIFY.wrapperid).append(html);
}

function cc_notifications_rebind_events(){
    //rebind the close notification event
    jQuery('#'+CC_NOTIFY.wrapperid+' a.close_notification').unbind('click').click(function(e){
	e.preventDefault();
	//close this div 
	jQuery(this).parent('.notification').remove();
	
	//if this was the last one, remove the wrapper as well
	if( jQuery('#'+CC_NOTIFY.wrapperid).html()=='' ){
	    jQuery('#'+CC_NOTIFY.wrapperid).remove();
	}
    });
}

function cc_notification_play_audio(){
    if( typeof jQuery( '#'+CC_NOTIFY.audio_element_id ).get(0).play === 'function' ){
	jQuery( '#'+CC_NOTIFY.audio_element_id ).get(0).play();
    }
}