<?php 
function get_reorder_table_name(){
	global $wpdb;
	return $wpdb->prefix."cc_jobs_reorder";
}

/**
 * these are filter to be added  before wp_Query for adding custom code in  wp_Query jobs listing
 */
function add_filters_for_jobs(){
	add_filter('posts_join', 'cc_custom_directory_join'); //join post table with cc_jobs_reorder;
	add_filter( 'posts_orderby', 'cc_custom_directory_order' ); // order by updated date from cc_jobs_reorder
}

/**
 * these are filters to be added after wp_Query to remove filters from wp_Query for jobs listing
 */
function remove_filters_for_jobs(){
	remove_filter('posts_join', 'cc_custom_directory_join');
	remove_filter( 'posts_orderby', 'cc_custom_directory_order' );
}


function cc_custom_directory_join( $join ) {
	global $wpdb;
	$join .= " LEFT JOIN ".$wpdb->prefix."cc_jobs_reorder ON ( ".$wpdb->prefix."posts.ID = ".$wpdb->prefix."cc_jobs_reorder.post_id ) ";
	return $join;
}

function cc_custom_directory_order(){
	global $wpdb; 
	return $wpdb->prefix."cc_jobs_reorder.reorder_date desc,".$wpdb->prefix."posts.post_date desc";
}

/**
 * Dummy function to test proper working of filters.
 * this is wp_Query later on replace by job_Query.
 */
function reorder_jobs_by_current_date(){
	$args= array(
		'post_type'	=> 'post',
		);
	$xquery=new wp_query($args);
	if($xquery->have_posts()){
		echo "<div class='Artticles_list'>";
		while ( $xquery->have_posts() ){
			$xquery->the_post();
			?>
			<div class="article_thumbnail">
				<?php the_post_thumbnail();?>
			</div>
			</div class="articles_title">
				<h1><a href="<?php the_permalink();?>"><?php the_title()?></a></h1>
			</div>
			</div class="articles_content">
			<?php //the_excerp();?>
			</div>

			<?php
		}
		echo "</div>";
	}
	wp_reset_postdata();
}

/**
 * this function is to show reorder button 
 * save data in cc_reorder_data table
 */
function get_reorder_button($user_id){
	//if user have privilage or his reorder chance in less then his ability to reorder
	//var_dump(cc_current_user_have_privilage());
	if(cc_current_user_have_privilage()){
		$post = get_post(get_the_ID());
		
		if($post->post_author == get_current_user_ID()){
			echo "<form  method='post'>";
				echo "<input type='submit' name='reorder_my_posts' value='Re Post this job'>";
			echo "</form>";
		}

		if(isset($_POST['reorder_my_posts'])){

			//save reorder record
			$reorder_no = cc_current_user_have_privilage();//next reorder no.
			//echo "<p class='text-success'>successfully Reordered your jobs. now your jobs are up in list</p>";
			
			$posts = get_the_ID();

			global $wpdb;
			
			$wpdb->insert(
				get_reorder_table_name(),
				array(
					'user_id' =>$user_id,
					'post_id' =>get_the_ID(),
					'reorder_date'=>date("Y-m-d H:i:s"),
					'status' =>'pending'
					),
				array(
				'%d','%d','%s','%s'
				)
			);		
		}
	}
	else{
		echo "your privilege to reorder post have been finished. upgrade your package to get it back.";
	}
}

//check whether user have ability to reorder the post or not
function cc_current_user_have_privilage(){

	//get what is user package type how much he can reorder 
	// then check  whether  package_reorder >= current reorder 
	//if yes save next order with current_order++  ;
	global $wpdb;
	
	//user package is silver and can reorder three times only.
	$user_can_reorder = user_can_reorder_no_tm();//3 times
	
	$current_reorder_no = get_user_post_count(get_current_user_ID())->{'count(*)'};
	
	$current_reorder_no = intval($current_reorder_no);
	
	if( $current_reorder_no < $user_can_reorder){
		//save data
		return $current_reorder_no+1;
	}
	else{
		return false;
	}
}


/*
* this is the function (uncompleted)
* this function return a number which 
* is calulated based on user package 
* Ex. if user have gold package the can reorder upto 3 times 
* if have silver then reorder upto 2 times.
*/
function user_can_reorder_no_tm(){
	return 3;
}

function get_cc_reorder_post_table(){
	global $wpdb;
	$table_pointmaster = $wpdb->prefix."cc_jobs_reorder";
	return $table_pointmaster;
}

/*
* this is maximum reorder user have done 
* if user clicked that button for 
* first time  =  1
* second time  = 2 
* thired time  = 3
*/
function get_user_post_count($user_id){
	global $wpdb;
	$query="SELECT count(*) FROM ".get_cc_reorder_post_table()." where user_id=".$user_id;
	$data = $wpdb->get_results($query);
	return $data[0];	
}
