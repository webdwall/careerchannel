jQuery(document).ready(function($){
    CCJOBSREPOST.debug = false;
    
    $(".btn_repost[disabled!='disabled']").click(function(){
	
	var $btn = this;
	if( $($btn).hasClass('processing') ){
	    return false;
	}
	$($btn).addClass('processing');
	$($btn).find('>span').removeClass('glyphicon-arrow-up').addClass('glyphicon-repeat');
	
	var data={
	    'job_id': $($btn).attr('data-job_id'),
	    'action': CCJOBSREPOST.action
	};
	
	$.ajax({
            type: "POST",
            url: CCJOBSREPOST.ajaxurl,
            data: data,
            success: function (response) {
		console.log( response );
		//for some reason, unknown to poor me, a '0' is added at the end of json string. Lets remove it
		var pos = response.lastIndexOf("}");
		if(pos!= -1){
		    response = response.substring(0, pos+1);
		}
		$($btn).removeClass('processing');
		$($btn).find('>span').addClass('glyphicon-arrow-up').removeClass('glyphicon-repeat');
		
            	response = $.parseJSON(response);
            	if( 'undefined'!==typeof( response.status ) ){
		    ccjobsreorder_show_modal( response.status, response.message );
            	}
		else{
		    ccjobsreorder_show_modal( false, 'Request can not be completed at the moment' );
		}
		
		var repost_count_html = $( $btn ).html();
		var repost_count = repost_count_html.substring( repost_count_html.length-2, repost_count_html.length-1 );
		repost_count--;
		$( $btn ).html('<span class="glyphicon glyphicon-arrow-up"></span> Repost('+repost_count+')');
		
	    },
	    error: function(){
		$($btn).removeClass('processing');
		$($btn).find('>span').addClass('glyphicon-sort').removeClass('glyphicon-repeat');
		ccjobsreorder_show_modal( false, 'Request can not be completed at the moment' );
	    }
        });
	return false;
    });
});

function ccjobsreorder_show_modal( type, message ){
    var cssclass = 'success';
    var modaltitle = 'Success';
    if( type==false ){
	cssclass = 'error';
	modaltitle = 'Error';
    }
    jQuery('#ccjobsrepost_modal').remove();
    var html  = '<div class="modal fade" id="ccjobsrepost_modal"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
        html += '<h4 class="modal-title">'+ modaltitle +'</h4>';
	html += '</div><div class="modal-body">';
        html += '<p class="text-'+ cssclass +'">'+ message +'</p>';
	html += '</div><div class="modal-footer"></div></div><!-- /.modal-content --></div><!-- /.modal-dialog --></div><!-- /.modal -->';
    
    jQuery('body').append(html);
    jQuery('#ccjobsrepost_modal').modal();
}