<?php 
add_action('pmsc_admin_general_setting_hook', 'cc_jobs_repost_settings_print');
function cc_jobs_repost_settings_print() {
    $settings = emi_get_option( 'package_reposts_limit', false );
	if( !$settings ){
		$settings = array( 'bronze'=> 3,'silver'=>6, 'gold'=>9, 'platinum'=>15, 'per_post'=>3 );
	}
    ?>
	<li class="devider"></li>
    <li>
	<p><strong>Jobs Reposting Limit:</strong><br/><small><i>Maximum number of 'reposts' that different types of members can do in a month.</i></small></p>
	<p>
	    <label for='cc_jobs_repost_bronze'>'Bronze' Package</label>
	    <input type='text' id='cc_jobs_repost_bronze' name='cc_jobs_repost_bronze' value='<?php echo esc_attr( $settings['bronze'] );?>' />
	    <br/>
	    <label for='cc_jobs_repost_silver'>'Silver' Package</label>
	    <input type='text' id='cc_jobs_repost_silver' name='cc_jobs_repost_silver' value='<?php echo esc_attr( $settings['silver'] );?>' />
	    <br/>
	    <label for='cc_jobs_repost_gold'>'Gold' Package</label>
	    <input type='text' id='cc_jobs_repost_gold' name='cc_jobs_repost_gold' value='<?php echo esc_attr( $settings['gold'] );?>' />
	    <br/>
	    <label for='cc_jobs_repost_platinum'>'Platinum' Package</label>
	    <input type='text' id='cc_jobs_repost_platinum' name='cc_jobs_repost_platinum' value='<?php echo esc_attr( $settings['platinum'] );?>' />

	</p>
	<p>
	    <label for="cc_jobs_repost_max_per_post">Max reposts per job listing</label>
	    <input type='text' id='cc_jobs_repost_max_per_post' name='cc_jobs_repost_max_per_post' value='<?php echo esc_attr( $settings['per_post'] );?>' />
	    <br/>
	    <small><i>The maximum number of times any job listing could be reposted.</i></small>

	</p>
    </li>
	<li class="devider"></li>
    <?php
}

//save/update the settins option
add_action('pmsc_admin_setting_save_button_hook', 'cc_jobs_repost_settings_update');
function cc_jobs_repost_settings_update() {
    $settings = emi_get_option( 'package_reposts_limit', false );
	if( !$settings ){
		$settings = array( 'bronze'=> 3,'silver'=>6, 'gold'=>9, 'platinum'=>15, 'per_post'=>3 ); 
	}
	
    //update only if it's been changed
    $new_selection = array(
		'bronze'	=> ( isset( $_POST['cc_jobs_repost_bronze'] ) && !empty( $_POST['cc_jobs_repost_bronze'] ) ? $_POST['cc_jobs_repost_bronze'] : 3 ),
		'silver'	=> ( isset( $_POST['cc_jobs_repost_silver'] ) && !empty( $_POST['cc_jobs_repost_silver'] ) ? $_POST['cc_jobs_repost_silver'] : 6 ),
		'gold'		=> ( isset( $_POST['cc_jobs_repost_gold'] ) && !empty( $_POST['cc_jobs_repost_gold'] ) ? $_POST['cc_jobs_repost_gold'] : 9 ),
		'platinum'	=> ( isset( $_POST['cc_jobs_repost_platinum'] ) && !empty( $_POST['cc_jobs_repost_platinum'] ) ? $_POST['cc_jobs_repost_platinum'] : 15 ),
		'per_post'	=> ( isset( $_POST['cc_jobs_repost_max_per_post'] ) && !empty( $_POST['cc_jobs_repost_max_per_post'] ) ? $_POST['cc_jobs_repost_max_per_post'] : 3 ),
	);
			
    if ($new_selection != $settings) {
		emi_update_option('package_reposts_limit', $new_selection);
    }
}