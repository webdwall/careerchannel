<?php 
/* 
Plugin Name: CareerChannel Jobs Repost
Description: Provides the 'Repost' functionality for jobs and all associated tweaks.
Author: E Media Identity
Author URI: http://emediaidentity.com/
Version: 0.2
*/

include( 'admin-settings.php' );
add_action('init', 'cc_jobs_repost_init');
function cc_jobs_repost_init() {
    CCJobsRepost::get_instance();
}

class CCJobsRepost {
    private $table_name;
    private static $instance;
    private $user_reposts;
	private $job_reposts_count;
    private $user_reposts_count_for_this_month = 0;
    public $user_reposts_count_for_this_job = 0;
    public $package_repost_limits;
    private $queries = array();
    private $include_featured_query = false;

    public static function get_instance() {
		if (!isset(self::$instance)) {
			self::$instance = new self();
		}
		return self::$instance;
    }
	
	private function __construct() {
		global $wpdb;
		$this->table_name = $wpdb->prefix."cc_jobs_reposts";
		$this->fetch_current_user_reposts();
		$this->package_repost_limits = emi_get_option( 'package_reposts_limit', false );
		if( !$this->package_repost_limits ){
			$this->package_repost_limits = array( 'bronze'=> 3,'silver'=>6, 'gold'=>9, 'platinum'=>15, 'per_post'=>3 ); 
		}
		
		add_action( 'before_get_job_listings',	array( $this, 'addargs_orderby_repostdate_new' ),	20, 1 );
		add_action( 'after_get_job_listings',	array( $this, 'removeargs_orderby_repostdate_new' ),20, 1 );
		
		/* Custom Action on home page */
		add_action( 'custom_before_get_job_listings',	array( $this, 'addargs_orderby_repostdate_new' ),	20, 1 );
		add_action( 'custom_after_get_job_listings',	array( $this, 'removeargs_orderby_repostdate_new' ),20, 1 );
		
		
		add_action( 'wp_ajax_cc_jobs_repost',	array( $this, 'ajax_save_repost_data' ) );
		add_action( 'wp_enqueue_scripts',		array( $this, 'load_css_js' ) );
    }
	
	function addargs_orderby_repostdate_new( $query_args ){
		//if( !$query_args['orderby'] || ( $query_args['orderby']=='meta_key' && $query_args['meta_key']=='_featured' ) ){
			//we have to alter this query
			if( $query_args['orderby']=='meta_key' && $query_args['meta_key']=='_featured' ){
				$this->include_featured_query = true;
			}else{
			    $this->include_featured_query = false;
			}
			
			add_filter( 'posts_join',		array( $this, 'join' ),		11 ); //join post table with cc_jobs_reorder;
			add_filter( 'posts_orderby',	array( $this, 'order' ),	11 ); // order by updated date from cc_jobs_reorder
			add_filter( 'posts_fields',		array( $this, 'fields' ),	11 );
			add_filter( 'posts_groupby',	array( $this, 'groupby' ),	11 );
		//}
	}
	
	function removeargs_orderby_repostdate_new( $query_args ){
		remove_filter( 'posts_join',	array( $this, 'join' ),		11 ); //join post table with cc_jobs_reorder;
		remove_filter( 'posts_orderby',	array( $this, 'order' ),	11 ); // order by updated date from cc_jobs_reorder
		remove_filter( 'posts_fields',	array( $this, 'fields' ),	11 );
		remove_filter( 'posts_groupby',	array( $this, 'groupby' ),	11 );
		$this->include_featured_query = false;
	}
	
	function join( $join ) {
		global $wpdb;
		$join .= " LEFT JOIN ". $this->table_name ." ON ( ". $wpdb->prefix ."posts.ID = ". $this->table_name .".post_id ) ";
		return $join;
	}
	
	function fields( $fields ){
		global $wpdb;
		return $fields . ", coalesce( MAX(" . $this->table_name . ".reorder_date), ". $wpdb->prefix ."posts.post_date ) AS 'last_reorder_date' ";
	}
	
	function order( $order ){
	    
		global $wpdb; 
		if( $this->include_featured_query ){
			//demo_postmeta.meta_value+0 DESC, demo_posts.post_date DESC
			return $wpdb->prefix . "postmeta.meta_value+0 DESC, last_reorder_date DESC, " .$order;
		}
		else{
			return "last_reorder_date DESC, " .$order;
		}
	}
	
	function groupby( $groupby ){
		if( !$groupby || $groupby=='' ){
			global $wpdb;
			$groupby = $wpdb->posts.".ID";
		}
		return $groupby;
	}
	
	function load_css_js(){
		if( is_user_logged_in() && !is_user_a_candidate() ){
		    wp_enqueue_script(
			"cc_jobs_repost",
			path_join( WP_PLUGIN_URL, basename( dirname( __FILE__ ) )."/script.js" ),
			array( 'jquery' )
		    );

		    wp_localize_script('cc_jobs_repost', 'CCJOBSREPOST', array( 'ajaxurl'=>  admin_url('/admin-ajax.php'), 'action'=> 'cc_jobs_repost' ) );
		}
	}
	
	/**
	 * Retrieves all the reposts done by current user.
	 * Fetches only one record per post because we are only interested 
	 * in the last date when any particular job listing was reposted.
	 * Additionally, in the same process, also calculates how many reposts the user has done this month.
	 * 
	 * @return void
	 */
	private function fetch_current_user_reposts(){
		if( !is_user_logged_in() )
			return;
		global $wpdb;
		$query  = "SELECT post_id, MAX( reorder_date) AS 'last_date', COUNT( post_id ) AS 'repost_count' FROM " . $this->table_name;
		$query .= " WHERE user_id=%d GROUP BY post_id ORDER BY post_id DESC";
		
		$reposts = $wpdb->get_results( $wpdb->prepare( $query, get_current_user_id() ) );
		if( $reposts && !empty( $reposts ) ){
			foreach( $reposts as $repost ){
				$this->user_reposts[$repost->post_id] = $repost->last_date;
				$repostdatetime = strtotime( $repost->last_date );

				$this->job_reposts_count[$repost->post_id] = $repost->repost_count;
				
				//if it is current month
				if( date('Y')==date( 'Y', $repostdatetime ) && date('n')==date( 'n', $repostdatetime ) ){
					$this->user_reposts_count_for_this_month++;
				}
			}
		}
		else{
			$reposts['status'] = 'no reposts';
		}
	}
	
	/**
	 * The function checks if logged in user can do more reposts or not.
	 * This capability depends upon user's subscription package.
	 * Gold members get more reposts per month than silver members and likewise.
	 * So if a user has exceeded the quota for the month, this function will return false, else true.
	 * 
	 * @return boolean true|false
	 */
	public function can_do_more_reposts(){
		$can_do_more_reposts = false;
		$user_package = member_package_details();
		/*
		 * ["purchase_date"]=>"2014-01-15 15:58:32" ["package"]=>"gold" ["quota_exceeded"]=>false) ["expiry_date"]=>"2014-02-14 15:58:32"
		 */
		//free (or non-loggedin) user's can't do reposts
		if( isset( $user_package['package'] ) && 'free'!=$user_package['package'] ){
			//number of reposts done by user should be less than the quota for his package
			if( $this->user_reposts_count_for_this_month < $this->package_repost_limits[$user_package['package']] ){
				$can_do_more_reposts = true;
			}
		}
		return $can_do_more_reposts;
	}
	
	/**
	 * Returns the (formatted) last date when the given job was reposted.
	 * 
	 * @param int $job_id job id whose last repost date is required
	 * @return string formatted date and time
	 */
	function get_jobs_last_repost_date($job_id){
		if( !$this->user_reposts ){
			$this->fetch_current_user_reposts();
		}
		if( isset( $this->user_reposts[$job_id] ) ){
			//@todo: format before returning
			return $this->user_reposts[$job_id];
		}
		return 'never';
	}
	
	/**
	 * Returns the total number of times the given job was reposted
	 * 
	 * @param int $job_id job id whose repost count is required
	 * @return int count
	 */
	function get_jobs_total_repost_count( $job_id ){
		if( !isset( $this->job_reposts_count[$job_id] ) ){
			//job reposts count was either not fetched from db or the given job has never been reposted yet
			if( !empty( $this->job_reposts_count ) ){
				//job reposts count are already fetched from database
				//its just that this job hasn't been reposted yet
			}
			else{
				//fetch the job reposts count from db
				$this->fetch_current_user_reposts();
			}
		}
		$reposts_remaining_count = ( isset( $this->package_repost_limits['per_post'] ) ? (int)$this->package_repost_limits['per_post'] : 0 ) - ( isset( $this->job_reposts_count[$job_id] ) ? (int)$this->job_reposts_count[$job_id] : 0 );
		return $reposts_remaining_count;
	}
	
	/**
	 * Prints the repost button.
	 * @param int $job_id the id of the job which is to be reposted. Default current post id if inside 'the loop'.
	 * @param boolean $show_anchor_text whether to display text in respost button or not. Default false, only display icon.
	 */
	function repost_button( $job_id=false, $show_anchor_text=false ){
		echo $this->get_repost_button( $job_id, $show_anchor_text );
	}
	
	/**
	 * Returns the html for repost button.
	 * @param int $job_id the id of the job which is to be reposted. Default current post id if inside 'the loop'.
	 * @param boolean $show_anchor_text whether to display text in respost button or not. Default false, only display icon.
	 * 
	 * @return string the html for button
	 */
	function get_repost_button( $job_id=false, $show_anchor_text=false ){
		if( !$job_id ){
			$job_id = get_the_ID();
		}

		$button = array(
			'class'			=> 'btn btn-default btn-small btn_repost',
			'title'			=> 'Last repost done on - ' . $this->get_jobs_last_repost_date($job_id),
			'data-job_id'	=> $job_id,
			'href'			=> '#'
		);
		
		$icon = "<span class='glyphicon glyphicon-arrow-up'></span>REPOST";
		if( $show_anchor_text ){
			$icon = "<span class='glyphicon glyphicon-arrow-up'></span> Repost";
			$button['class']='btn btn-link btn-xs btn_repost';
		}
		
		$icon .= "(". $this->get_jobs_total_repost_count($job_id) .")";
		
		$can_do_more_reposts = $this->can_do_more_reposts();
		if( $can_do_more_reposts==false ){
			$button['disabled'] = 'disabled';
			$button['title'] = 'Upgrade your membership to avail of more reposts per month.';
		}
		else{
			//if the job has already been reposted 3 times(max number set in settings), it shouldn't be allowed to repost again
			if( $this->get_jobs_total_repost_count($job_id) <= 0 ){
				$button['disabled'] = 'disabled';
				$button['title'] = 'Already been reposted maximum number of times!';
				$can_do_more_reposts = false;
			}
			else{
				$button['class'] .= ' show-tooltip';
			}
		}

		$attr = "";
		foreach( $button as $attrname=>$attrval ){
			$attr .= " $attrname='". esc_attr($attrval) ."'";
		}

		//showting tooltip for disabled items requires to wrap it inside a div and set tooltip on wrapper div
		if( $can_do_more_reposts==false ){
			return "<div class='show-tooltip' title='". esc_attr( $button['title'] ) ."'>" . "<a" . $attr . ">" . $icon . "</a>" . "</div>";
		}
		else{
			return "<a" . $attr . ">" . $icon . "</a>";
		}
	}
		
	function ajax_save_repost_data(){
	    
		$retval = array(
			'status'	=> false,
			'message'	=> 'Something went wrong!'
		);
		
		$job_id = $_POST['job_id'];
		if( !$job_id ){
			$retval['message'] = 'Invalid job id provided!';
			die( json_encode($retval) );
		}
		
		if( !$this->can_do_more_reposts() ){
			$retval['mes`sage'] = 'Quota exceeded. Upgrade your membership to avail of more reposts per month.';
			die( json_encode($retval) );
		}
		global $wpdb;
		$wpdb->insert(
			$this->table_name,
			array(
				'user_id'		=> get_current_user_id(),
				'post_id'		=> $job_id,
				'reorder_date'	=> current_time('mysql'),
				'status'		=> 'pending'
				),
			array(
				'%d','%d','%s','%s'
			)
		);
		$retval['status'] = true;
		$retval['message'] = 'Repost done successfully.';
		die( json_encode( $retval ) );
	}
	
	function get_table_name(){
		return $this->table_name;
	}
}

register_activation_hook( __FILE__, 'cc_jobs_repost_setup_db' );
function cc_jobs_repost_setup_db(){		
	global $wpdb;
	$table_pointmaster = $wpdb->prefix."cc_jobs_reposts";

	$sql1 ="CREATE TABLE $table_pointmaster(
		ID bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
		post_id bigint(20),
		user_id bigint(20),
		reorder_date DATETIME,
		status VARCHAR(50) DEFAULT 'pending'
	);";
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql1);
}