<?php

/*
Plugin Name: OIOpublisher Ad Manager 2.55
Plugin URI: http://download.oiopublisher.com
Description: Control your Ad Space with OIOpublisher.
Version: 2.55
Author: Simon Emery
Author URI: http://www.simonemery.co.uk
*/


//no direct access
if(reset(get_included_files()) === __FILE__) die();

//check install directory
if(strpos(__FILE__, "/plugins/" . basename(__FILE__)) !== false) {
	echo 'It looks like you have uploaded OIO directly to the Wordpress "plugins" folder.' . "\n";
	echo '<br />' . "\n";
	echo 'OIO must be uploaded inside its own directory (eg. oiopub-direct) within the "plugins" folder to function correctly.' . "\n";
	die();
}

//define vars
if(!defined('OIOPUB_PLATFORM')) {
	define('OIOPUB_PLATFORM', 'wordpress');
}

//include plugin
include_once(str_replace('\\', '/', dirname(__FILE__)) . "/index.php");