<?php

/*
if(isset($_GET['debug'])) {
	include_once("include/debug.php");
	$oiopub_debug = new oiopub_debug();
	declare(ticks=1);
	register_shutdown_function(array( &$oiopub_debug, 'shutdown_handler' ));
	register_tick_function(array( &$oiopub_debug, 'tick_handler' ));
}
*/

//debug (start)
if(function_exists('microtime')) {
	define('OIOPUB_TIME_START', microtime(true));
}
if(function_exists('memory_get_usage')) {
	define('OIOPUB_MEM_START', memory_get_usage());
}

//error level
error_reporting(E_ERROR);

//set environment
if(function_exists('ini_set')) {
	if((int) @ini_get('memory_limit') < 32) {
		@ini_set('memory_limit', '32M');
	}
	@ini_set('magic_quotes_sybase', 0);
}

//define oiopub
if(!defined('oiopub')) {
	define('oiopub', 1);
}

//WP 2.5+ activation
global $oiopub_cache, $oiopub_db, $oiopub_set, $oiopub_hook, $oiopub_cron, $oiopub_alerts, $oiopub_version, $wpdb;

//oiopub version
$oiopub_version = "2.55";

//clear settings
$oiopub_set = '';

//csrf protection?
$oiopub_set->csrf = false;

//demo mode?
$oiopub_set->demo = ($_SERVER['HTTP_HOST'] == "demo.oiopublisher.com");

//test site?
$oiopub_set->test_site = ($_SERVER['HTTP_HOST'] == "test.oiopublisher.com");

//normalise $_SERVER variables
//adapted from Wordpress 2.6
if(empty($_SERVER['REQUEST_URI'])) {
	if(isset($_SERVER['HTTP_X_ORIGINAL_URL'])) {
		$_SERVER['REQUEST_URI'] = $_SERVER['HTTP_X_ORIGINAL_URL'];
	} elseif(isset($_SERVER['HTTP_X_REWRITE_URL'])) {
		$_SERVER['REQUEST_URI'] = $_SERVER['HTTP_X_REWRITE_URL'];
	} else {
		if(!isset($_SERVER['PATH_INFO']) && isset($_SERVER['ORIG_PATH_INFO'])) {
			$_SERVER['PATH_INFO'] = $_SERVER['ORIG_PATH_INFO'];
		}
		if(isset($_SERVER['PATH_INFO'])) {
			if($_SERVER['PATH_INFO'] == $_SERVER['SCRIPT_NAME']) {
				$_SERVER['REQUEST_URI'] = $_SERVER['PATH_INFO'];
			} else {
				$_SERVER['REQUEST_URI'] = $_SERVER['SCRIPT_NAME'] . $_SERVER['PATH_INFO'];
			}
		}
		if(isset($_SERVER['QUERY_STRING']) && !empty($_SERVER['QUERY_STRING'])) {
			$_SERVER['REQUEST_URI'] .= '?' . $_SERVER['QUERY_STRING'];
		}
	}
}

//script folder
$cwd = trim(str_replace('\\', '/', dirname(__FILE__)));
$exp = explode('/', $cwd);

//set directories
$oiopub_set->folder_dir = $cwd;
$oiopub_set->folder_name = $exp[count($exp)-1];
$oiopub_set->parent_dir = dirname($cwd);
$oiopub_set->parent_name = $exp[count($exp)-2];
$oiopub_set->cache_dir = $oiopub_set->folder_dir . "/cache";

//load core libs
include_once($oiopub_set->folder_dir . "/include/functions.php");
include_once($oiopub_set->folder_dir . "/include/hooks.php");
include_once($oiopub_set->folder_dir . "/include/config.php");
include_once($oiopub_set->folder_dir . "/include/cache.php");
include_once($oiopub_set->folder_dir . "/include/database.php");
include_once($oiopub_set->folder_dir . "/include/settings.php");
include_once($oiopub_set->folder_dir . "/include/cron.php");
include_once($oiopub_set->folder_dir . "/include/api.php");
include_once($oiopub_set->folder_dir . "/include/modules.php");

//load hooks class
$oiopub_hook = new oiopub_hooks();

//init config class
$oiopub_config = new oiopub_config;

//load wordpress?
if(isset($oiopub_set->wp_load) && $oiopub_set->wp_load == 1) {
	if(!defined('WPINC')) {
		$oio_cwd = getcwd();
		chdir(dirname($oiopub_set->wp_config));
		include_once($oiopub_set->wp_config);
		chdir($oio_cwd);
	}
}

//set extra vars
$oiopub_config->set_extras();

//display errors
$oiopub_config->display_errors();

//unset config
unset($oiopub_config);

//load cache class
$oiopub_cache = new oiopub_cache($oiopub_set->cache_args);

//load db class
$oiopub_db = new oiopub_db($oiopub_set->db_args, $oiopub_set->dbh, $oiopub_cache);

//load settings class
$oiopub_set = new oiopub_settings();

//load api class
$oiopub_api = new oiopub_api();

//load cron class?
if(!defined('OIOPUB_LOAD_LITE') || defined('OIOPUB_JS')) {
	$oiopub_cron = new oiopub_cron();
}

//include platform
include_once($oiopub_set->platform_file);

//load modules class
$oiopub_module = new oiopub_modules();

//output functions
oiopub_output_files();

//init hook
$oiopub_hook->fire('init');

//check install?
if(!defined('OIOPUB_LOAD_LITE')) {
	oiopub_install_redirect();
}

//read only?
if($oiopub_set->demo && oiopub_is_admin()) {
	unset($_POST, $_REQUEST);
}

//debug (end)
if(isset($_GET['oio-debug']) && $_GET['oio-debug'] == 1) {
	echo "<div style='padding:10px; border:1px solid #999;'>\n";
	if(function_exists('microtime')) {
		echo "Execution Time: " . number_format((microtime(true) - OIOPUB_TIME_START), 5) . " seconds\n";
		echo "<br />\n";
	}
	if(function_exists('memory_get_usage')) {
		echo "Memory Used: " . number_format(memory_get_usage() - OIOPUB_MEM_START, 0, '.', ',') . " bytes\n";
		echo "<br />\n";
	}
	if(function_exists('memory_get_peak_usage')) {
		echo "Peak Memory: " . number_format(memory_get_peak_usage() - OIOPUB_MEM_START, 0, '.', ',') . " bytes\n";
		echo "<br />\n";
	}
	echo "</div>\n";
}

?>