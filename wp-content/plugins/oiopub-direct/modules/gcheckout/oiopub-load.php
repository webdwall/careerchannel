<?php

/*
Module: Google Checkout v2.00
Developer: http://www.simonemery.co.uk

Module constructed for OIOpublisher Direct
http://www.oiopublisher.com

Copyright (C) 2008  Simon Emery

This file is part of OIOpublisher Direct.
*/

if(!defined('oiopub')) die();


//module vars
$oio_enabled = 1;
$oio_version = "2.00";
$oio_name = __oio("Google Checkout");
$oio_module = "gcheckout";
$oio_menu = "payment";

//min plugin version
$oio_min_version = "2.10.b1";

//check minimum version
oiopub_module_vcheck($oio_module, $oio_min_version);


//gcheckout class
class oiopub_gcheckout {

	//global vars
	var $name;
	var $folder;
	var $version;
	
	//init
	function oiopub_gcheckout($version='', $name='') {
		///set vars
		$this->name = $name;
		$this->version = $version;
		//call methods
		$this->settings();
		$this->install();
		$this->redirect();
		$this->hooks();
	}
	
	//settings
	function settings() {
		global $oiopub_set;
		//get folder name
		$dir = trim(str_replace('\\', '/', dirname(__FILE__)));
		$exp = explode('/', $dir);
		$this->folder = $exp[count($exp)-1];
		//misc settings
		$oiopub_set->gcheckout_folder = $this->folder;
		$oiopub_set->gcheckout_ipn = $oiopub_set->plugin_url . '/modules/' . $this->folder . '/gcheckout-ipn.php';
		$oiopub_set->gcheckout_form = $oiopub_set->plugin_url . '/modules/' . $this->folder . '/gcheckout-form.php';
		$oiopub_set->gcheckout_success = $oiopub_set->plugin_url . '/payment.php?do=success';
		$oiopub_set->gcheckout_failed = $oiopub_set->plugin_url . '/payment.php?do=failed';
		$oiopub_set->gcheckout_subscription = 0;
		//payment arrays
		if(isset($oiopub_set->gcheckout)) {
			if($oiopub_set->gcheckout['enable'] == 1 && $oiopub_set->gcheckout['valid'] == 1) {
				$oiopub_set->arr_payment['gcheckout'] = $this->name;
			}
		}
	}

	//install
	function install() {
		global $oiopub_set;
		if(!isset($oiopub_set->gcheckout) || $oiopub_set->gcheckout['install'] < $this->version) {
			if(empty($oiopub_set->gcheckout['install'])) {
				include_once($oiopub_set->modules_dir . '/' . $this->folder . '/install/install.php');
			} else {
				include_once($oiopub_set->modules_dir . '/' . $this->folder . '/install/upgrade.php');
			}
			$oiopub_set->gcheckout['install'] = $this->version;
			oiopub_update_config('gcheckout', $oiopub_set->gcheckout);
		}
	}
	
	//uninstall
	function uninstall() {
		global $oiopub_set;
		include_once($oiopub_set->modules_dir . '/' . $this->folder . '/install/uninstall.php');
	}
	
	//page redirect
	function redirect() {
		if(oiopub_is_admin()) {
			if(isset($_GET['page']) && $_GET['page'] == 'oiopub-gcheckout.php') {
				header("Location: admin.php?page=oiopub-opts.php&popup=gcheckout");
				exit();
			}
		}
	}
	
	//add actions
	function hooks() {
		global $oiopub_hook;
		if(oiopub_is_admin()) {
			if(isset($_GET['page']) && $_GET['page'] == "oiopub-opts.php") {
				$oiopub_hook->add('admin_payment', array(&$this, 'admin_options'));
				if(isset($_GET['popup']) && $_GET['popup'] == "gcheckout") {
					$oiopub_hook->add('settings_popup', array(&$this, 'admin_popup'));
				}
			}
			if(isset($_REQUEST['do']) && $_REQUEST['do'] == "oiopub-remove") {
				$oiopub_hook->add('delete_modules', array(&$this, 'uninstall'));
			}
		}
	}
	
	//admin options
	function admin_options() {
		global $oiopub_set;
		if($oiopub_set->gcheckout['valid'] == 1) {
			if($oiopub_set->gcheckout['enable'] == 1) {
				echo "<font color='green'><b>Google Checkout Payments Active</b></font> - ";
			} else {
				echo "<font color='blue'><b>Google Checkout Payments Disabled</b></font> - ";
			}
		} else {
			echo "<font color='red'><b>Google Checkout Setup Incomplete</b></font> - ";
		}
		echo "[<a href='admin.php?page=oiopub-opts.php&popup=gcheckout'>edit settings</a>]\n";
		echo "<br /><br />\n";
	}
	
	//admin popup
	function admin_popup() {
		global $oiopub_set;
		$info = ""; $checked = "";
		if(isset($_POST['oiopub_gcheckout_mid'])) {
			$enable = intval($_POST['oiopub_gcheckout_enable']);
			$merchant_id = oiopub_clean($_POST['oiopub_gcheckout_mid']);
			//$merchant_key = oiopub_clean($_POST['oiopub_gcheckout_mkey']);
			if(!empty($merchant_id)) {
				$oiopub_set->gcheckout['enable'] = $enable;
				$oiopub_set->gcheckout['valid'] = 1;
				$oiopub_set->gcheckout['merchant_id'] = $merchant_id;
				//$oiopub_set->gcheckout['merchant_key'] = $merchant_key;
				oiopub_update_config('gcheckout', $oiopub_set->gcheckout);
				$info = "<font color='green'><i><b>Information successfully updated!</b></i></font>";
			} else {
				$oiopub_set->gcheckout['valid'] = 0;
				oiopub_update_config('gcheckout', $oiopub_set->gcheckout);
				$info = "<font color='red'><i><b>Information not validated. Please try again!</b></i></font>";
			}
		}
		if($oiopub_set->gcheckout['enable'] == 1) {
			$checked = " checked=\"checked\"";
		}
		echo "<div style='border-top:1px solid #D8D8D8; border-bottom:1px solid #D8D8D8; padding:5px 0 5px 0; margin:5px 0 20px 0;'>\n";
		if(!empty($info)) {
			echo $info . "\n";
		} else {
			echo "<i><b>Integration type:</b> you will need to update the status of purchases manually.</i>\n";
			echo "<br />\n";
			echo "<i><b>Subscriptions Supported:</b> " . ($oiopub_set->gcheckout_subscription == 1 ? 'yes' : 'no') . ".</i>\n";		
		}
		echo "</div>\n";
		echo "<form method='post' action='" . $oiopub_set->request_uri . "'>\n";
		echo "<input type='hidden' name='csrf' value='" . oiopub_csrf_token() . "' />\n";
		echo "<b>Enable Google Checkout?</b>\n";
		echo "<br /><br />\n";
		echo "<input type=\"checkbox\" name=\"oiopub_gcheckout_enable\" value=\"1\"" . $checked . " />\n";
		echo "&nbsp;&nbsp;<i>enabling this will option will let advertisers pay using <a href='http://checkout.google.com' target='_blank'>google checkout</a></i>\n";
		echo "<br /><br />\n";
		echo "<b>Merchant ID:</b>\n";
		echo "<br /><br />\n";
		echo "<input type=\"text\" name=\"oiopub_gcheckout_mid\" value=\"" . $oiopub_set->gcheckout['merchant_id'] . "\" size=\"30\" />\n";
		echo "&nbsp;&nbsp;<i>your google checkout merchant ID</i>\n";
		echo "<br /><br />\n";
		//echo "<b>Merchant Key:</b>\n";
		//echo "<br /><br />\n";
		//echo "<input type=\"text\" name=\"oiopub_gcheckout_mkey\" value=\"" . $oiopub_set->gcheckout['merchant_key'] . "\" size=\"30\" />\n";
		//echo "&nbsp;&nbsp;<i>your google checkout merchant Key</i>\n";
		//echo "<br /><br />\n";
		echo "<input type='submit' value='Update Settings' />\n";
		echo "</form>\n";
	}
	
	//paypal form
	function form($rand_id) {
		global $oiopub_set;
		echo '<form id="gcheckout" action="' . $oiopub_set->gcheckout_form . '" method="post">';
		echo '<input type="hidden" name="rand" value="' . $rand_id . '" />';
		echo '<input type="image" name="Checkout" alt="Checkout" src="https://sandbox.google.com/checkout/buttons/checkout.gif?merchant_id=' . $oiopub_set->gcheckout['merchant_id'] . '&w=180&h=46&&style=white&variant=text&loc=en_US" height="46"  width="180" />';
		echo '</form>';
	}
	
}


//execute class
$oiopub_plugin[$oio_module] = new oiopub_gcheckout($oio_version, $oio_name);

?>