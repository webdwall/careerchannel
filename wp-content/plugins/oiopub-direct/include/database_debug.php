<?php

/*
Copyright (C) 2008  Simon Emery

This file is part of OIOpublisher Direct.
*/

if(!defined('oiopub')) die();

//database
class oiopub_db {

	var $dbh;
	var $connect;
	var $db_args;
	var $cache;
	var $query_num = 0;
	var $num_rows = 0;
	var $insert_id = 0;
	var $rows_affected = 0;
	var $errors = array();
	var $stop = 0;

	var $default_charset = '';
	var $check_charset = false;

	var $log_file = 'errors/sql.txt';

	//init
	function oiopub_db($db_args=array(), $dbh='', $cache_obj='') {
		if(empty($dbh) || $dbh === false) {
			$this->connect = false;
			$this->dbh = "";
		} else {
			$this->connect = true;
			$this->dbh = $dbh;
		}
		$this->db_args = $db_args;
		$this->cache = $cache_obj;
	}
	
	//collect errors
	function error($string='') {
		$this->errors[] = $string;
		$this->stop = 1;
	}
	
	//connect
	function connect($db_host, $db_user, $db_pass, $db_name) {
		if($this->stop == 1) return false;
		$this->dbh = mysql_connect($db_host, $db_user, $db_pass) or die(mysql_error());
		$selected = mysql_select_db($db_name, $this->dbh) or die(mysql_error());
		if($selected) {
			$this->default_charset();
			$this->connect = true;
			return true;
		}
		$this->error("Cannot Connect to database - " . mysql_error() . " - check settings in the OIOpublisher 'config.php' file");
		return false;
	}

	//basic query
	function query($sql=null) {
		if(!$sql || $this->stop == 1) return false;
		if(!$this->connect) {
			$this->connect($this->db_args['db_host'], $this->db_args['db_user'], $this->db_args['db_pass'], $this->db_args['db_name']);
		}
		if($this->stop != 1) {
			if(!empty($this->dbh)) {
				$res = mysql_query($sql, $this->dbh) or die(mysql_error());
			} else {
				$res = mysql_query($sql) or die(mysql_error());
			}
			if($error = @mysql_error()) {
				if($this->log_file) {
					$file = str_replace("\\", "/", dirname(__FILE__)) . "/" . $this->log_file;
					if(function_exists('file_put_contents')) {
						@file_put_contents($file, "Query: " . $sql . "\nError: " . $error . "\n\n", FILE_APPEND | LOCK_EX);
					}
				}
			}
			if($res) {
				$this->query_num++;
				$this->insert_id = mysql_insert_id($this->dbh);
				$this->rows_affected = mysql_affected_rows($this->dbh);
				$this->num_rows = mysql_num_rows($res);
				return $res;
			} else {
				$this->insert_id = $this->rows_affected = $this->num_rows = 0;
			}
		}
		return false;
	}

	//get single value
	function GetOne($sql=null) {
		if(!$sql) return false;
		if($res = $this->query($sql)) {
			$row = mysql_fetch_row($res);
			mysql_free_result($res);
			return $row[0];
		}
		return false;
	}
	
	//cache single value
	function CacheGetOne($sql=null, $seconds=7200) {
		if(!$sql || !$this->cache) return false;
		if($this->cache) {
			$res = $this->cache->get($sql, $seconds);
		}
		if(empty($res) && !is_array($res)) {
			$res = $this->GetOne($sql);
			if($this->cache) {
				$this->cache->write($sql, $res, $seconds);
			}
		} else {
			$this->num_rows = 1;
		}
		return $res;
	}

	//get single row
	function GetRow($sql=null) {
		if(!$sql) return false;
		if($res = $this->query($sql)) {
			$row = mysql_fetch_object($res);
			mysql_free_result($res);
			return $row;
		}
		return false;
	}
	
	//cache single row
	function CacheGetRow($sql=null, $seconds=7200) {
		if(!$sql) return false;
		if($this->cache) {
			$res = $this->cache->get($sql, $seconds);
		}
		if(empty($res) && !is_array($res)) {
			$res = $this->GetRow($sql);
			if($this->cache) {
				$this->cache->write($sql, $res, $seconds);
			}
		} else {
			$this->num_rows = 1;
		}
		return $res;
	}

	//get all rows
	function GetAll($sql=null) {
		if(!$sql) return false;
		if($res = $this->query($sql)) {
			$obj = array();
			while($row = mysql_fetch_object($res)) {
				$obj[] = $row;
			}
			mysql_free_result($res);
			return $obj;
		}	
	}
	
	//cache all rows
	function CacheGetAll($sql=null, $seconds=7200) {
		if(!$sql) return false;
		if($this->cache) {
			$res = $this->cache->get($sql, $seconds);
		}
		if(empty($res) && !is_array($res)) {
			$res = $this->GetAll($sql);
			if($this->cache) {
				$this->cache->write($sql, $res, $seconds);
			}
		} else {
			$this->num_rows = empty($res) ? 0 : count($res);
		}
		return $res;
	}
	
	//cache flush
	function CacheFlush($sql=null) {
		if(!$sql) return false;
		if($this->cache) {
			return $this->cache->delete($sql);
		}
		return false;
	}

	//get default charset
	function default_charset() {
		global $oiopub_set;
		//charset check?
		if(!$this->check_charset) {
			//update flag
			$this->check_charset = true;
			//query db
			$table = mysql_query("SHOW CREATE TABLE " . $oiopub_set->prefix . "oiopub_purchases", $this->dbh) or die(mysql_error());
			//get array?
			if($table) {
				$table = mysql_fetch_array($table, MYSQL_NUM);
			}
			//is utf8?
			if(!$table || strpos($table[1], "DEFAULT CHARSET=utf8") !== false) {
				$this->default_charset = ' DEFAULT CHARACTER SET utf8';
				mysql_query("SET character_set_results='utf8', character_set_client='utf8', character_set_connection='utf8', character_set_database='utf8', character_set_server='utf8'", $this->dbh) or die(mysql_error());
			}
		}
		//return charset
		return $this->default_charset;
	}

}

?>