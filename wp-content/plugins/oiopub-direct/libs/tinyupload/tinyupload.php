<?php

/*
Tiny Upload

upload.php

This file does the uploading

Modifications made in order to integrate with OIOpub Direct
*/


//###### Config ######

//The Absolute path (from the clients POV) to this file.
$absPthToSlf = htmlspecialchars($_SERVER['REQUEST_URI'], ENT_QUOTES);

//The Absolute path (from the clients POV) to the destination folder.
$absPthToDst = explode("/libs/", $absPthToSlf, 2);
$absPthToDst = $absPthToDst[0] . "/uploads/";

//The Absolute path (from the servers POV) to the destination folder.
$absPthToDstSvr = dirname(dirname(dirname(__FILE__))) . "/uploads/";
$absPthToDstSvr = str_replace("\\", "/", $absPthToDstSvr);

//define OIO
if(!defined('oiopub')) define('oiopub', 1);


//###### You should not need to edit past this point ######

if(isset($_GET['poll']) && $_GET['poll']) {
	echo '[]';
} else {
	if(isset($_POST['process']) && $_POST['process'] == "upload") {
		if($_FILES['tuUploadFile']['tmp_name']) {
			//load OIO uploading class
			include_once(str_replace("/uploads/", "/include/", $absPthToDstSvr) . "upload.php");
			//create object
			$upload = new file_upload();
			$upload->SetFileName($_FILES['tuUploadFile']['name']);
			$upload->SetFileSize($_FILES['tuUploadFile']['size']);
			$upload->SetTempName($_FILES['tuUploadFile']['tmp_name']);
			$upload->SetUploadDirectory($absPthToDstSvr);
			$upload->SetValidExtensions(array( 'jpg', 'jpeg', 'gif', 'png' ));
			$upload->SetIsImage(true);
			//upload
			$upload->UploadFile();
		}
	}
?>
<html>
<head>
<style type="text/css">
body {
	font-size:10px;
	margin:0px;
	padding:0px;
	height:20px;
	overflow:hidden;
}
</style>
<script type="text/javascript">
window.onload = function() {
	parent.tuIframeLoaded();
}
function tuSmt(){
	filePath = '<?php echo $absPthToDst; ?>' + document.getElementById('tuUploadFile').value;
	if(parent.tuFileUploadStarted(filePath, document.getElementById('tuUploadFile').value)) {
		window.document.body.style.cssText = 'border:none;padding-top:100px';
		document.getElementById('tuUploadFrm').submit();
	}
}
</script>
</head>
<body style="border:none;">
	<form enctype="multipart/form-data" method="post" action="<?php echo $absPthToSlf; ?>" id="tuUploadFrm">
	<input type="hidden" name="process" value="upload" />
		<table border="0" cellspacing="0" cellpadding="0" style="height:22px; vertical-align:top;">
			<tr>
				<td>
					<input type="file" size="24" style="height:22px;" id="tuUploadFile" name="tuUploadFile" />
				</td>
				<td style="padding-left:2px;">
					<input type="submit" value="Go" onclick="tuSmt();" style="border:1px solid #808080; background:#fff; height:20px;"/>
				</td>
			</tr>
		</table>
	</form>
</body>
</html>
<?php } ?>