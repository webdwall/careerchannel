<?php
/*
Plugin Name: AVChat Video Chat Plugin PRO for WordPress
Plugin URI: http://avchat.net/integrations/wordpress
Description: This plugin integrates <a href="http://avchat.net" target="_blank">AVChat 3</a> into any WordPress website.
Author: NuSoft Software
Version: 2.0 (build 513)
Author URI: http://nusofthq.com/



Copyright (C) 2009-3013 AVChat Software, avchat.net

This WordPress Plugin is distributed under the terms of the GNU General Public License.
You can redistribute it and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation, either version 3 of the License, or any later version.

You should have received a copy of the GNU General Public License
along with this plugin.  If not, see <http://www.gnu.org/licenses/>.
*/

if(session_id() == "") { session_start(); }
require_once(ABSPATH . 'wp-content/plugins/avchat-3-pro/insert_tables.php');

// function get_buddypress_info() {
// 	global $current_user;
// 	if(session_id() == "") { session_start(); }
	
// 	if(in_array( 'buddypress/bp-loader.php', apply_filters('active_plugins', get_option('active_plugins')))) {
// 		$_SESSION['bdp']["active"] = '1fhg';
		
// 		//avatar
// 		$_SESSION['bdp']["avatar"] = bp_core_fetch_avatar(array( 'item_id' => $current_user->ID, 'type' => 'thumb', 'alt' => '', 'css_id' => '', 'class' => 'avatar', 'width' => '40', 'height' => '40', 'email' => $current_user->user_email, 'html' => 'false'));
		
// 		//User profile page
// 		$_SESSION['bdp']["profile-url"] = get_bloginfo('wpurl') . '/members/'.$current_user->user_login.'/profile/';
// 	}
// 	print_r($_SESSION['bdp']);
// }

// function avchat3_pro_clear_session() {
// 	session_destroy();
// }

function avchat3_pro_get_user_chat($content){
	//get wordpress and user info
	global $current_user;
	global $wpdb;
	global $blog_id;
	get_currentuserinfo();
	
	//keep blog_id in session
	$_SESSION['avc3']['blog_id'] = $blog_id;
	
	//check if buddypress is active, if it's the we set the info into session
	//get_buddypress_info();
	
	//select general settings from the database
	$settings = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."avchat3_general_settings");
	
	$av3_current_blog_capabilities = $wpdb->prefix.'capabilities';
	//check if multisite is active
	if (is_multisite()) {
		//this is a multisite WP installation
	
		//this is the main sub site in the multisite WP
		$av3_current_blog_capabilities = $wpdb->prefix.'capabilities';
	
		if ($blog_id > 1) {
			//these are other sub sites
			$av3_current_blog_capabilities = $wpdb->prefix.$blog_id.'_capabilities';
		}
	}
	
	if (($current_user->ID != NULL) && ($current_user->ID != "") && ($current_user->ID > 0)) {
		if ($current_user->roles[0] != "") {
			$userRole = $current_user->roles[0];
		} else {
			$userRole = "networkuser";
		}
	} else {
		$userRole = "visitors";
	}
	
	//we select the permissions for that specific user role
	$query = "SELECT * FROM ".$wpdb->prefix . "avchat3_permissions"." WHERE user_role = '".$userRole."'";
	$userPermissions = $wpdb->get_results($query);
	
	$swf = 'index.swf';
	if ($userPermissions[0]->can_access_admin_chat == '1') {
		$swf = 'admin.swf';
	}
	
	//check if we have options from wp page
	//example: [chat dropInRoom:r0,r1 showOnlyRooms:r1 roomsListEnabled:1]
	$contentVals = array();
	if  (preg_match('/\b(dropInRoom|showOnlyRooms|roomsListEnabled)\b/', $content)) {
		preg_match_all('/\[chat.*?\]/', $content, $result);
		if (!empty($result)) {
			$contentVals = explode(" ", str_replace(array('[',']'), "", $result[0][0]));
			$contentReplace = '[chat';
			foreach ($contentVals AS $contentVal) {
				//echo $contentVal.'-';
				if ($contentVal == 'chat') { continue; }
				$contentReplace .= ' ' . $contentVal;
			}
			$contentReplace .= ']';
		}
	}
	
	$currentPath = 'http://'.$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	$pluginPath = get_bloginfo('wpurl') . '/wp-content/plugins/avchat-3-pro/';
	if (is_multisite()) {
		$pluginPath = get_site_url(1) . '/wp-content/plugins/avchat-3-pro/';
	}

	
	if ($settings[0]->swf_width == '') { $settings[0]->swf_width = '100%'; }
	if ($settings[0]->swf_height == '') { $settings[0]->swf_height = '600'; }
	
	$page_content .=  <<<EOF
<script type="text/javascript">
	var chat_path = "{$pluginPath}";
	var embed = "{$settings[0]->display_mode}";
	var chatPathTwitter = "{$currentPath}";
	var twitterKey = "{$settings[0]->twitter_key}";
	var twitterSecret = "{$settings[0]->twitter_secret}";
	var facebookAppID = "{$settings[0]->FB_appId}";
	var flashvars = {
		lstext : "Loading Settings...",
		sscode : "php"
EOF;
	foreach ($contentVals AS $contentVal) {
		if ($contentVal == 'chat') { continue; }
		$cvals = explode(":", $contentVal);
		if ($cvals[0] == 'roomsListEnabled' || strlen($cvals[1]) == 2) {
			$page_content .= ',' . $cvals[0] . ' : "' . $cvals[1] . '"';
			continue;
		}
		$page_content .= ',' . $cvals[0] . ' : "[' . $cvals[1] . ']"';
	}
	$page_content .=  <<<EOF
		,userId : ""
	};
	var params = {
		quality : "high",
		bgcolor : "#272727",
		play : "true",
		loop : "false",
		allowFullScreen : "true",
		base : chat_path
	};
	var attributes = {
		name : "index_embed",
		id :   "index_embed",
		align : "middle"
	};
</script><script src="{$pluginPath}/tinycon.min.js"></script><script type="text/javascript" src="{$pluginPath}/codebird-js/sha1.js"></script><script type="text/javascript" src="{$pluginPath}/codebird-js/codebird.js"></script><script type="text/javascript" src="{$pluginPath}/facebook_integration.js"></script><script type="text/javascript" src="{$pluginPath}/twitter_integration.js"></script><script type="text/javascript" src="{$pluginPath}/swfobject.js"></script><script type="text/javascript" src="{$pluginPath}/new_message.js"></script>
<script type="text/javascript">
	var current_url = location.toString();
	if(current_url.indexOf("?twitter=true") != -1){
		document.body.innerHTML = "<img src='{$pluginPath}ajax-loading.gif'></img>";
		swfobject.embedSWF("index.swf", "myContent", "100%", "100%", "10.3.0", "", flashvars, params, attributes);
	}
</script>	
<div id="myContent" style="padding:0px;margin:0px;">
<div id="av_message" style="color:#ff0000;padding:0px;margin:0px;">
EOF;
	
	if($settings[0]->display_mode == 'popup') {
		if ($settings[0]->popup_button_image_url != '') {
			$popUpLink = "<a href=\"javascript:void(0);\" onclick=\"window.open('".$pluginPath."index_popup.php?bid=".$blog_id."', 'VideoChat', 'height=".$settings[0]->popup_height.", width=".$settings[0]->popup_width."')\"><img src=\"".$settings[0]->popup_button_image_url."\" alt=\"popup\"/></a>";
		} else {
			$popUpLink = "<a href=\"javascript:void(0);\" onclick=\"window.open('".$pluginPath."index_popup.php?bid=".$blog_id."', 'VideoChat', 'height=".$settings[0]->popup_height.", width=".$settings[0]->popup_width."')\"  style=\"display:block;background:#f0f0f0; border:1px solid #ccc;text-align:center; padding:5px;\">Open chat in popup</a>";
		}
		$page_content .= $popUpLink;
	} else {
		$page_content .= '<script type="text/javascript">
				swfobject.embedSWF("'.$pluginPath.$swf.'", "myContent", "'.$settings[0]->swf_width.'", "'.$settings[0]->swf_height.'", "11.2.0", "", flashvars, params, attributes);
			</script>';
	}
	
	$page_content .= '</div></div><script type="text/javascript" src="'.$pluginPath.'/find_player.js"></script>';
	
	if  (preg_match('/\b(dropInRoom|showOnlyRooms|roomsListEnabled)\b/', $content)) {
		return str_replace($contentReplace, $page_content, $content);
	} else {
		return str_replace('[chat]', $page_content, $content);
	}
}

function avchat3_pro_admin_config(){
	add_options_page('AVChat3 Permissions & Config', 'AVChat 3 Video Chat PRO',  'manage_options', 'avchat-3-pro/avchat3-settings.php');
}

register_activation_hook(__FILE__,'avchat3_pro_install');
//add_action('wp_logout', 'avchat3_pro_clear_session');
add_action('admin_menu', 'avchat3_pro_admin_config');
add_filter('the_content', 'avchat3_pro_get_user_chat', 7);
?>