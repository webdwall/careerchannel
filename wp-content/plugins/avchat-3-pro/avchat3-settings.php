<?php
/*

Copyright (C) 2009-2012 AVChat Software, avchat.net

This WordPress Plugin is distributed under the terms of the GNU General Public License.
You can redistribute it and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of the License, or any later version.

You should have received a copy of the GNU General Public License
along with this plugin.  If not, see <http://www.gnu.org/licenses/>.
*/

global $wp_roles;
global $wpdb;

require_once(ABSPATH . 'wp-content/plugins/avchat-3-pro/insert_tables.php');
//in a multisite scenario we might have to create the tables when this page is acessed because when the plugin is NETWORK ACTIVATED the tables are not created for all websitess
if (is_multisite()) {
	avchat3_pro_install(true);
}

$table_permissions = $wpdb->prefix . "avchat3_permissions";
$table_general_settings = $wpdb->prefix . "avchat3_general_settings";

$permissions = array(
				'can_access_chat' => 'Can access chat',
				'can_access_admin_chat' => 'Can access AVChat admin',
				'can_publish_audio_video' =>  'Can publish audio & video stream',
				'only_broadcasters_can_view_cams' =>  'Can view a webcam if only they are broadcasting their own',
				'invite_to_view_stream' =>  'Can invite other users to view their stream',
				'can_stream_private' => 'Can stream private',
				'can_send_files_to_rooms' => 'Can send files to rooms',
				'can_send_files_to_users' => 'Can send files to users',
				'can_pm' => 'Can send private messages',
				'can_create_rooms' => 'Can create rooms',
				'can_moderate_rooms' => 'Can be moderators for the rooms that they created',
				'invite_to_rooms' => 'Can invite other users to join the room they created',
				'can_watch_other_people_streams' => 'Can watch other people streams',
				'can_join_other_rooms' => 'Can join other rooms',
				'show_users_online_stay' => 'Show users how much they stayed online',
				'view_who_is_watching_me' => 'Ability for the users to see who is watching them',
				'can_block_other_users' => 'Can block other users',
				'can_buzz' => 'Can buzz',
				'can_stop_viewer' => 'Can stop viewer',
				'can_ignore_pm' => 'Can ignore private messages',
				'typing_enabled' => 'Typing enabled',
				'allow_urls_text_chat' => 'Allow users to insert urls in text chat',
				'allow_emails_text_chat' => 'Allow users to insert emails in text chat',
				'admin_can_kick' => 'AVChat admins can kick?',
				'admin_can_ban' => 'AVChat admins can ban?',
				'admin_can_view_ips' => 'AVChat admins can view IPs?',
				'admin_can_silence' => 'AVChat admins can silence users?',
				'admin_can_view_pms' => 'AVChat admins can view PMs ?',
				'admin_can_access_sett' => 'AVChat admins can access the [Settings] panel?',
				'admin_can_hide' => 'AVChat admins can login as hidden?',
				'admin_can_view_hiden_admins' => 'AVChat admins can view hidden admins?',
				'admin_can_delete_rooms' => 'AVChat admins can delete rooms?',
				'admin_can_remove_ban' => 'AVChat admins can remove ban?',
				'admin_can_view_private_streams' => 'AVChat admins can view private streams without permission?',
				'admin_can_join_private_rooms' => 'AVChat admins can join private rooms without permission?',
				'admin_can_access_bann_panel' => 'AVChat admins can access bann panel?',
				'admin_can_view_extra_info' => 'AVChat admins can view extra info?'
);

$settings = array(
				'country_flag' => 'Badge URL:',
				'drop_in_room' => 'Auto drop in room with id:',
				'view_only_rooms' => 'Can view only these rooms',
				'cant_view_rooms' => 'Can not view these rooms',
				'max_streams' => 'Max streams a user can watch',
				'max_rooms' => 'Max rooms a user can join',
				'username_prefix' => 'Auto add this prefix to username:',
);

$general_settings = array(
				'connection_string' => 'Connection string*',
				'allow_facebook_login' => 'Allow Facebook and Twitter login',
				'FB_appId' => 'Facebook application ID*',
				'twitter_key' => 'Twitter Consumer Key',
				'twitter_secret' => 'Twitter Consumer Secret',
				'display_mode' => 'Where the chat is embedded',
				'popup_button_image_url' => 'Popup Button Image URL',
				'popup_width' => 'Popup Width',
				'popup_height' => 'Popup Height',
				'swf_width' => 'Chat SWF Width',
				'swf_height' => 'Chat SWF Height',
				'invite_link' => 'Invite URL',
				'disconnect_link' => 'Disconnect button URL',
				'login_page_url' => 'Login page URL',
				'register_page_url' => 'Register page URL' ,
				'ban_page_url' => 'Ban page URL' ,
				'kick_page_url' => 'Kick page URL' ,
				'text_char_limit' => 'Text chat character limit',
				'history_lenght' => 'History length',
				'admin_history_lenght' => 'History length for admin',
				'max_upload' => 'Upload Max File Size',
				'flip_tab_menu' => 'Position of rooms tabbed menu',
				'hide_left_side' => 'Hide left side of chat',
				'p2t_default' => 'Push 2 talk used by default',
				'user_becomes_idle' => 'User Becomes Idle after these seconds',
				'img_preview_area_height_width' => 'Img preview area height and width',
				'show_youtube_videos_preview' => 'Preview YouTube videos and pictures',
				'right_to_left' => 'Right to left',
				'show_avatars' => 'Show avatars in text chat',
				'background_image' => 'Background Image URL',
				'webcam_docking' => 'Enable Web Cam Docking',
				'display_username_realname' => 'Display username or real name',
				'users_list_type' => 'Users list type',
);


if(isset($_POST) && !empty($_POST)){
	foreach ($_POST as $key=>$avconfs){
		if (strpos($key, "-")){
			$avconf_arrtemp = explode("-", $key);
			if($avconfs == 'on')$avconfs = '1';
			$avconf_arr[$avconf_arrtemp[0]][substr($avconf_arrtemp[1],4)] = $avconfs;
		}else{
			$av_general_confs[substr($key,11)] = $avconfs;
		}
	}
	
	
	foreach ($avconf_arr as $key=>$vals){
		
		$updateString = "";
		foreach($permissions as $pkey => $pvalue){
			if($vals[$pkey] == ""){
				$vals[$pkey] = 0;
			}
			$updateString.= $pkey." = '".$vals[$pkey]."', ";
		}
		
		$i=1;
		foreach($settings as $skey=>$svalue){
			$updateString.= $skey." = '".stripslashes(trim($vals[$skey]))."'";
			if(count($settings) != $i) $updateString.= ', ';
			$i++;
		}
		
		$check = $wpdb->get_var( "SELECT COUNT(*) FROM ".$wpdb->prefix."avchat3_permissions WHERE user_role='".$key."'");
		if ($check > 0) {
			$query = "UPDATE ".$table_permissions." SET ".$updateString." WHERE user_role = '".$key."'";
			$wpdb->query($query);
		} else {
			//echo '<pre>';
			//var_dump($vals);
			$data = array(
				'user_role' => $key,
				'can_access_chat' => $vals['can_access_chat'],
				'can_access_admin_chat' => $vals['can_access_admin_chat'],
				'can_publish_audio_video' => $vals['can_publish_audio_video'],
				'can_stream_private' => $vals['can_stream_private'],
				'can_send_files_to_rooms' => $vals['can_send_files_to_rooms'],
				'can_send_files_to_users' => $vals['can_send_files_to_users'],
				'can_pm' => $vals['can_pm'],
				'can_create_rooms' => $vals['can_create_rooms'],
				'can_watch_other_people_streams' => $vals['can_watch_other_people_streams'],
				'can_join_other_rooms' => $vals['can_join_other_rooms'],
				'show_users_online_stay' => $vals['show_users_online_stay'],
				'view_who_is_watching_me' => $vals['view_who_is_watching_me'],
				'can_block_other_users' => $vals['can_block_other_users'],
				'can_buzz' => $vals['can_buzz'],
				'can_stop_viewer' => $vals['can_stop_viewer'],
				'can_ignore_pm' => $vals['can_ignore_pm'],
				'typing_enabled' => $vals['typing_enabled'],
				'drop_in_room' => $vals['drop_in_room'],
				'max_streams' => $vals['max_streams'],
				'max_rooms' => $vals['max_rooms'],
				'username_prefix' => $vals['username_prefix'],
				'admin_can_kick' => $vals['admin_can_kick'],
				'admin_can_ban' => $vals['admin_can_ban'],
				'admin_can_view_ips' => $vals['admin_can_view_ips'],
				'admin_can_silence' => $vals['admin_can_silence'],
				'admin_can_view_pms' => $vals['admin_can_view_pms'],
				'admin_can_access_sett' => $vals['admin_can_access_sett'],
				'admin_can_hide' => $vals['admin_can_hide'],
				'admin_can_view_hiden_admins' => $vals['admin_can_view_hiden_admins'],
				'country_flag' => $vals['country_flag'],
				'view_only_rooms' => $vals['view_only_rooms'],
				'cant_view_rooms' => $vals['cant_view_rooms'],
				'only_broadcasters_can_view_cams' => $vals['only_broadcasters_can_view_cams'],
				'can_moderate_rooms' => $vals['can_moderate_rooms'],
				'invite_to_view_stream' => $vals['invite_to_view_stream'],
				'invite_to_rooms' => $vals['invite_to_rooms'],
				'allow_urls_text_chat' => $vals['allow_urls_text_chat'],
				'allow_emails_text_chat' => $vals['allow_emails_text_chat'],
				'admin_can_delete_rooms' => $vals['admin_can_delete_rooms'],
				'admin_can_remove_ban' => $vals['admin_can_remove_ban'],
				'admin_can_view_private_streams' => $vals['admin_can_view_private_streams'],
				'admin_can_join_private_rooms' => $vals['admin_can_join_private_rooms'],
				'admin_can_access_bann_panel' => $vals['admin_can_access_bann_panel'],
				'admin_can_view_extra_info' => $vals['admin_can_view_extra_info'],
			);
			$wpdb->insert($wpdb->prefix."avchat3_permissions", $data);
		}
	}
	
	$updateString="";
	$p=1;
	foreach($av_general_confs as $gkey=>$gvalue){
		$updateString.= $gkey." = '".stripslashes(trim($gvalue))."'";
		if(count($av_general_confs) != $p) $updateString.= ', ';
		$p++;
	}
	
	$query = "UPDATE ".$table_general_settings." SET ".$updateString;
	//var_dump($query);
	$wpdb->query($query);
}


$location = get_option('siteurl') . '/wp-admin/admin.php?page=avchat-3-pro/avchat3-settings.php'; 
$user_roles = array();

foreach($wp_roles->roles as $role => $details){
	$user_roles[$role] = $details["name"];
}

//unset($user_roles['administrator']);

$user_roles['visitors'] = "Visitors";
if (is_multisite()){
	$user_roles['networkuser'] = "Network user";
}
//var_dump($user_roles);
	
?>

<div class="wrap">
	<h2>AVChat 3 Settings & Permissions</h2>
</div>
<form name="form1" method="post" action="<?php echo $location; ?>">
	<table style="text-align:center">
		
		<tr>
			<th></th>
			<?php foreach($user_roles as $role => $name){?>
				<th style="padding:0 1px !important"><?php echo $name;?></th>
			<?php } ?>
		</tr>
		
		<tr><td colspan="5" style="text-align:left"><h3>Permissions</h3></td>
		<?php foreach($permissions as $key=>$value){ ?>
			
			<tr>
				<td style="text-align:left"><?php echo $value;?></td>
				<?php 
					foreach ($user_roles as $user_role => $name){
						$user_permissions = $wpdb->get_results( "SELECT * FROM ".$wpdb->prefix . "avchat3_permissions WHERE user_role = '".$user_role."'" );
				?>
					<td style="padding:0 1px !important">
						<input type="checkbox" 
							<?php  if($user_permissions[0]->$key){ echo 'checked="checked"';} ?> 
							name="<?php echo strtolower($user_role);?>-avp_<?php echo $key;?>" />	
					</td>
				<?php }?>
			</tr>
		<?php }?>
		
		<tr><td colspan="5" style="text-align:left"><h3>Settings</h3></td>
		<?php foreach($settings as $key=>$value){?>
		<tr>
			<td style="text-align:left"><?php echo $value;?></td>
			<?php 
				foreach ($user_roles as $user_role => $name){
					$user_settings = $wpdb->get_results( "SELECT * FROM ".$wpdb->prefix . "avchat3_permissions WHERE user_role = '".$user_role."'" );
			?>
				<td style="padding:0 1px !important"><input type="text" name="<?php echo strtolower($user_role);?>-avs_<?php echo $key;?>" style="width:80px" value="<?php echo $user_settings[0]->$key;?>" /></td>
			<?php }?>
		</tr>
		<?php }?>
		
		<tr><td colspan="5" style="text-align:left"><h3>General settings</h3></td>
		<?php 
			foreach($general_settings as $key=>$value){
				$av_general_settings = $wpdb->get_results( "SELECT * FROM ".$wpdb->prefix . "avchat3_general_settings" );	
		?>
		<tr>
			<td style="text-align:left"><?php echo $value;?></td>
			<td style="padding:0 1px !important;text-align:left;" colspan="4">
				<?php 
				switch ($key) {
					case 'display_mode':
					?>
						<select name="avgsetting_<?php echo $key?>" >
							<option <?php if ($av_general_settings[0]->$key == 'popup') {echo 'selected="selected"';}?> value="popup">Popup</option>
							<option <?php if ($av_general_settings[0]->$key == 'embed') {echo 'selected="selected"';}?> value="embed">Embed</option>
						</select>
					<?php
						break;
					case ($key == 'allow_facebook_login' || $key == 'hide_left_side' || $key == 'p2t_default' || $key == 'show_youtube_videos_preview' || $key == 'right_to_left' || $key == 'show_avatars' || $key == 'webcam_docking'):
					?>
						<select name="avgsetting_<?php echo $key?>" >
							<option <?php if ($av_general_settings[0]->$key == 'yes') {echo 'selected="selected"';}?> value="yes">Yes</option>
							<option <?php if ($av_general_settings[0]->$key == 'no') {echo 'selected="selected"';}?> value="no">No</option>
						</select> 
					<?php
						break;
					case 'flip_tab_menu':
					?>
						<select name="avgsetting_<?php echo $key?>" >
							<option <?php if ($av_general_settings[0]->$key == 'top') {echo 'selected="selected"';}?> value="top">Top</option>
							<option <?php if ($av_general_settings[0]->$key == 'bottom') {echo 'selected="selected"';}?> value="bottom">Bottom</option>
						</select> 
					<?php
						break;
					case 'display_username_realname':
					?>
						<select name="avgsetting_<?php echo $key?>" >
							<option <?php if ($av_general_settings[0]->$key == 'username') {echo 'selected="selected"';}?> value="username">Username</option>
							<option <?php if ($av_general_settings[0]->$key == 'real-name') {echo 'selected="selected"';}?> value="real-name">Real name</option>
						</select>
					<?php 
						break;
					case 'users_list_type':
					?>
						<select name="avgsetting_<?php echo $key?>" >
							<option <?php if ($av_general_settings[0]->$key == 'thumbnail') {echo 'selected="selected"';}?> value="thumbnail">Thumbnail</option>
							<option <?php if ($av_general_settings[0]->$key == 'smallthumbnail') {echo 'selected="selected"';}?> value="smallthumbnail">Small Thumbnail</option>
							<option <?php if ($av_general_settings[0]->$key == 'small') {echo 'selected="selected"';}?> value="small">Small</option>
						</select>
					<?php 
						break;
					default :
					?>
						<input size="50" type="text" name="avgsetting_<?php echo $key;?>" value="<?php echo $av_general_settings[0]->$key; ?>" />
				<?php }?>
			</td>
		</tr>
		<?php } ?>
	</table>
	<p class="submit"><input type="submit" value="Update Options" class="button-primary" /></p>
</form>