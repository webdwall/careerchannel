<?php
/**
 * Popup file for AVChat 3 with Wordpress
 *
 * @category	Group Video Chat
 * @package		AVChat 3 Plugin for Wordpress
 * @author		Radu Patron <radu@nusofthq.com>
 * @copyright	2013 Nusofthq.com
 * @license		This Wordpress Plugin is distributed under the terms of the GNU General Public License V2.0.
 * 				you can redistribute it and/or modify it under the terms of the GNU General Public License V2.0
 * 				as published by the Free Software Foundation, either version 3 of the License, or any later version.
 * @link		http://www.avchat.net
 */

//include wordpress functions
define('WP_USE_THEMES', false);
require_once dirname(dirname(dirname(dirname(__FILE__)))).'/wp-load.php';

//get wordpress and user info
global $current_user;
global $wpdb;
global $blog_id;
get_currentuserinfo();

if ($_GET['bid'] != $blog_id) {
	switch_to_blog($_GET['bid']);
}

//select general settings from the database
$settings = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."avchat3_general_settings");

//$av3_current_blog_capabilities = $wpdb->prefix.'capabilities';
// //check if multisite is active
// if (is_multisite()) {
// 	//this is a multisite WP installation

// 	//this is the main sub site in the multisite WP
// 	$av3_current_blog_capabilities = $wpdb->prefix.'capabilities';

// 	if ($blog_id > 1) {
// 		//these are other sub sites
// 		$av3_current_blog_capabilities = $wpdb->prefix.$blog_id.'_capabilities';
// 	}
// }

if (($current_user->ID != NULL) && ($current_user->ID != "") && ($current_user->ID > 0)) {
	if ($current_user->roles[0] != "") {
		$userRole = $current_user->roles[0];
	} else {
		$userRole = "networkuser";
	}
} else {
	$userRole = "visitors";
}

//we select the permissions for that specific user role
$query = "SELECT * FROM ".$wpdb->prefix . "avchat3_permissions"." WHERE user_role = '".$userRole."'";
$userPermissions = $wpdb->get_results($query);

restore_current_blog();

$swf = 'index.swf';
if ($userPermissions[0]->can_access_admin_chat == '1') {
	$swf = 'admin.swf';
}

$currentPath = 'http://'.$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$pluginPath = get_bloginfo('wpurl') . '/wp-content/plugins/avchat-3-pro/';
if (is_multisite()) {
	$pluginPath = get_site_url(1) . '/wp-content/plugins/avchat-3-pro/';
}

$widthPopUP = $settings[0]->popup_width;
$heightPopUP = $settings[0]->popup_height;
if ($widthPopUP == '') { $widthPopUP='960'; }
if ($heightPopUP == '') { $heightPopUP='700'; }
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Video Chat</title>
	<script type="text/javascript" src="<?php echo $pluginPath;?>tinycon.min.js"></script>
	<script type="text/javascript" src="<?php echo $pluginPath;?>facebook_integration.js"></script>
	<script type="text/javascript" src="<?php echo $pluginPath;?>swfobject.js"></script>
	<script type="text/javascript">
		var chat_path = "<?php echo $pluginPath;?>";
		var chatPathTwitter = "<?php echo $pluginPath;?>";
		var twitterKey = "<?php echo $settings[0]->twitter_key?>";
		var twitterSecret = "<?php echo $settings[0]->twitter_secret?>";
		var embed = 'popup';
		var facebookAppID = "<?php echo $settings[0]->FB_appId?>";
		var flashvars = {
			lstext : "Loading Settings...",
			sscode : "php",
			userId : ""
		};
		var params = {
			quality : "high",
			bgcolor : "#272727",
			play : "true",
			loop : "false",
			allowFullScreen : "true",
			base : chat_path
		};
		var attributes = {
			name : "index_embed",
			id :   "index_embed",
			align : "middle"
		};
	</script>
	<script type="text/javascript" src="<?php echo $pluginPath;?>new_message.js"></script>
	<script type="text/javascript" src="<?php echo $pluginPath;?>codebird-js/sha1.js"></script>
	<script type="text/javascript" src="<?php echo $pluginPath;?>codebird-js/codebird.js"></script>
	<script type="text/javascript" src="<?php echo $pluginPath;?>twitter_integration.js"></script>
	<style type="text/css">
	html {
		height:100%;
		overflow:hidden;
	}
	body {
		padding:0px; margin:0px;
		width:100%;
		height:100%;
	}
	#wrapper {
		display:block;
		width:100%;
		height:100%;
	}
	#index_embed {
		height:100%;
	}
	</style>
</head>
<body>
<div id="wrapper">
	<script type="text/javascript">
		swfobject.embedSWF("<?php echo $pluginPath.$swf;?>", "myContent", "100%", "100%", "11.2.0", "", flashvars, params, attributes);
	</script>
	<div id="myContent">
		<div id="av_message" style="color:#ff0000"> </div>
	</div>
	<div id="fb-root"></div>
	<script type="text/javascript" src="./find_player.js"></script>
</div> 
</body>
</html>