<?php
	require "RtmpClient.class.php";
	require "debug.php";
	require "connectionDetails.php";
	require "utils.php";
	@session_start(); 

	$host=HOST;
	$appName=APPNAME;
	$port=PORT;
	
	$data = array($_SESSION["gender"],$_SESSION["username"],"","","user",0,0,$_SESSION["thumbnailUrl"],$_SESSION["theToken"],$_SESSION["profileUrl"],$_SESSION["profileCountryFlag"],$_SESSION["clientUniqueIdentifier"],$revision,$_COOKIE["userId"],getRealIpAddr());
		
		//echo "Inainte de conexiune<br/>";
		$client = new RtmpClient();
		$client->connect($host,$appName,$port,$data);
		//die("Connected");
		
		//echo "Dupa conexiune";
		//checking if the client is licensed
		$licenseKey = $client->checkIfLicensed();
		//checking if the client is banned
		//$bannedStatus = $client->checkIfBanned();
		
		
		//Creating the rooms list 
		$roomsList = $client->getRoomsList($_COOKIE["userId"]);
		//var_dump($roomsList);
			
		//sorting the roomList by number of users
		$rlArray = array();
				
		foreach($roomsList as $j){
				array_push($rlArray, $j);
		}
		
		function sortRooms($a, $b){
			return $b["users"] - $a["users"];
		}
		
		usort($rlArray, "sortRooms");
		
		for ($i=0; $i<count($rlArray); $i++){
				if($rlArray[$i]["password"] != ""){
					echo"<li  data-theme='b'>
						<div style='margin-top:-10px;'><img src='img/lock.png' class='ui-li-icon'/></div>
						<div style='margin-left:10px;'><h1>".$rlArray[$i]["name"]."</h1></div>
						<p>".$_SESSION["AVC_inRoom"]." <b>".$rlArray[$i]["users"]."</b> ".$_SESSION["AVC_roomUsers"]."</p>
						<p>".$_SESSION["AVC_owner"]." <b>".$rlArray[$i]["ownerName"]."</b></p>
						<p>".$_SESSION["AVC_isPrivate"]." <b>".$_SESSION["AVC_isPrivateYes"]."</b></p>
						
					</li>";
				}else{
					echo"<li>
					 <a href='joinRoom.php?rid=".$rlArray[$i]["id"]."&roomName=".$rlArray[$i]["name"]."' data-transition='slide'>
						<h1>".$rlArray[$i]["name"]."</h1>
						<p>".$_SESSION["AVC_inRoom"]." <b>".$rlArray[$i]["users"]."</b> ".$_SESSION["AVC_roomUsers"]."</p>
						<p>".$_SESSION["AVC_owner"]." <b>".$rlArray[$i]["ownerName"]."</b></p>
						<p>".$_SESSION["AVC_isPrivate"]." <b>".$_SESSION["AVC_isPrivateNo"]."</b></p>
					 </a>
					</li>";
				}
				
			}
?>