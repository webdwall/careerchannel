<?php
/**
 * Integration file for AVChat 3 with Wordpress
 *
 * @category	Group Video Chat
 * @package		AVChat 3 Plugin for Wordpress
 * @author		Radu Patron <radu@nusofthq.com>
 * @copyright	2013 Nusofthq.com
 * @license		This Wordpress Plugin is distributed under the terms of the GNU General Public License V2.0.
 * 				you can redistribute it and/or modify it under the terms of the GNU General Public License V2.0
 * 				as published by the Free Software Foundation, either version 3 of the License, or any later version.
 * @link		http://www.avchat.net
 */

//include wordpress functions
define('WP_USE_THEMES', false);
$rootPath = dirname(dirname(dirname(dirname(__FILE__))));
require_once $rootPath . '/wp-load.php';

//get wordpress and user info
global $current_user;
global $wpdb;
global $blog_id;
global $bp;
get_currentuserinfo();

//echo $blog_id.'-';

if ($_SESSION['avc3']['blog_id'] != $blog_id) {
	//switch_to_blog($_SESSION['avc3']['blog_id']);
}
//echo $_SESSION['avc3']['blog_id'].'- '.$wpdb->prefix.'<br />';

//var_dump($bp);

//echo '<pre>';
//var_dump($current_user);

// $av3_current_blog_capabilities = $wpdb->prefix.'capabilities';
// //check if multisite is active
// if (is_multisite()) {
// 	//this is a multisite WP installation
	
// 	//this is the main sub site in the multisite WP
// 	$av3_current_blog_capabilities = $wpdb->prefix.'capabilities';
	
// 	if ($blog_id > 1) {
// 		//these are other sub sites
// 		$av3_current_blog_capabilities = $wpdb->prefix.$_SESSION['avc3']['blog_id'].'_capabilities';
// 		echo $av3_current_blog_capabilities.'test';
// 	}
// }


/**
 * AVChat 3 General Settings
 */

$avconfig['enableRegisterTab'] = 1;
$avconfig['changeuser'] = 0;
$avconfig['changegender'] = 0;

/**
 * AVChat 3 Plugin settings
 */
 


//select general settings from the database
$query = "SELECT * FROM ".$wpdb->prefix."avchat3_general_settings";
$settings = $wpdb->get_results($query);

// custom
if($current_user->roles[0]=="subscriber"){
   // $avconfig['roomsListEnabled']=0;
}

//RTMP connection string
$avconfig['connectionstring'] = $settings[0]->connection_string;

//invite page URL
$avconfig['inviteLink'] = $settings[0]->invite_link;

//disconect link
$avconfig['disconnectButtonLink'] = $settings[0]->disconnect_link;

//login page URL
$avconfig['loginPageURL'] = $settings[0]->login_page_url;

//register page URL
$avconfig['registerPageURL'] = $settings[0]->register_page_url;

//facebook and twitter connect
$avconfig['enableOtherAccountOptions'] = 0;
if($settings[0]->allow_facebook_login == 'yes'){
	$avconfig['enableOtherAccountOptions'] = 1;
}

//the maximum number of characters a text message can have (protection against long spam messages and flooding)
$avconfig['textChatCharLimit'] = $settings[0]->text_char_limit;

if ($settings[0]->history_lenght != "") {
	$avconfig['historyLength'] = $settings[0]->history_lenght;
}

$avconfig['flipTabMenu'] = 0;
if ($settings[0]->flip_tab_menu == 'bottom') {
	$avconfig['flipTabMenu'] = 1;
}

$avconfig['hideLeftSide'] = 0;
if ($settings[0]->hide_left_side == 'yes') {
	$avconfig['hideLeftSide'] = 1;
}

$avconfig['pushToTalkDefault'] = 0;
if ($settings[0]->p2t_default == 'yes') {
	$avconfig['pushToTalkDefault'] = 1;
}

//ban page url
$avconfig['banURL'] = $settings[0]->ban_page_url;

//kick page url
$avconfig['kickURL'] = $settings[0]->kick_page_url;

//max upload file size
$avconfig['maxUploadFileSize'] = (int)$settings[0]->max_upload * 1024;

//user becomes idle after x seconds
$avconfig['userBecomesIdleAfterXSeconds'] = $settings[0]->user_becomes_idle;

//image preview width and height
$avconfig['imagePreviewAreaWidthAndHeight'] = $settings[0]->img_preview_area_height_width;

//show youtube & image preview
$avconfig['showYTVideosPreview'] = 0;
if ($settings[0]->show_youtube_videos_preview == 'yes') {
	$avconfig['showYTVideosPreview'] = 1;
}

//admin chat history length
$avconfig['historyLengthForAdmin'] = $settings[0]->admin_history_lenght;

//right to left
$avconfig['rightToLeft'] = 0;
if ($settings[0]->right_to_left == 'yes') {
	$avconfig['rightToLeft'] = 1;
}

//show avatar in text chat
$avconfig['showAvatarsInTextChat'] = 0;
if ($settings[0]->show_avatars == 'yes') {
	$avconfig['showAvatarsInTextChat'] = 1;
}

//background image url
$avconfig['backgroundImageUrl'] = $settings[0]->background_image;

//webcam docking
$avconfig['enableWebcamDocking'] = 0;
if ($settings[0]->webcam_docking == 'yes') {
	$avconfig['enableWebcamDocking'] = 1;
}

//users list type
$avconfig['usersListType'] = $settings[0]->users_list_type;

/**
 * AVChat 3 user permissions
 */

if (($current_user->ID != NULL) && ($current_user->ID != "") && ($current_user->ID > 0)) {
	if ($current_user->roles[0] != "") {
		$userRole = $current_user->roles[0];
	} else {
		$userRole = "networkuser";
	}
} else {
	$userRole = "visitors";
}

//echo $userRole;
//var_dump($current_user->roles);
//we select the permissions for that specific user role
$query = "SELECT * FROM ".$wpdb->prefix . "avchat3_permissions"." WHERE user_role = '".$userRole."'";
$userPermissions = $wpdb->get_results($query);

//restore_current_blog();

if ($userPermissions[0]->can_access_chat == '0') {
	if ($userRole != 'visitors') {
		$avconfig['showUserLevelError'] = 1;
	} else {
		$avconfig['showLoginError'] = 1;
	}
}

$avconfig['allowVideoStreaming'] = 0;
$avconfig['allowAudioStreaming'] = 0;
if ($userPermissions[0]->can_publish_audio_video == '1') {
	$avconfig['allowVideoStreaming'] = 1;
	$avconfig['allowAudioStreaming'] = 1;
}

$avconfig['allowPrivateStreaming'] = 0;
if ($userPermissions[0]->can_stream_private == '1') {
	$avconfig['allowPrivateStreaming'] = 1;
}

$avconfig['sendFileToRoomsEnabled'] = 0;
if ($userPermissions[0]->can_send_files_to_rooms == '1') {
	$avconfig['sendFileToRoomsEnabled'] = 1;
}

$avconfig['sendFileToUserEnabled'] = 0;
if ($userPermissions[0]->can_send_files_to_users == '1') {
	$avconfig['sendFileToUserEnabled'] = 1;
}

$avconfig['pmEnabled'] = 0;
if ($userPermissions[0]->can_pm == '1') {
	$avconfig['pmEnabled'] = 1;
}

$avconfig['createRoomsEnabled'] = 0;
if ($userPermissions[0]->can_create_rooms == '1') {
	$avconfig['createRoomsEnabled'] = 1;
}

$avconfig['maxStreams'] = 0;
if ($userPermissions[0]->can_watch_other_people_streams == '1') {
	$avconfig['maxStreams'] = $userPermissions[0]->max_streams;
}

$avconfig['joinRoomsEnabled'] = 0;
if ($userPermissions[0]->can_join_other_rooms == '1') {
	$avconfig['joinRoomsEnabled'] = 1;
}

$avconfig['showOnlineTime'] = 0;
if ($userPermissions[0]->show_users_online_stay == '1') {
	$avconfig['showOnlineTime'] = 1;
}

$avconfig['userCanSeeWhoIsWatchingHim'] = 0;
if ($userPermissions[0]->view_who_is_watching_me == '1') {
	$avconfig['userCanSeeWhoIsWatchingHim'] = 1;
}

$avconfig['userCanBlockOtherUsers'] = 0;
if ($userPermissions[0]->can_block_other_users == '1') {
	$avconfig['userCanBlockOtherUsers'] = 1;
}

$avconfig['buzzButtonEnabled'] = 0;
if ($userPermissions[0]->can_buzz == '1') {
	$avconfig['buzzButtonEnabled'] = 1;
}

$avconfig['stopViewerButtonEnabled'] = 0;
if ($userPermissions[0]->can_stop_viewer == '1') {
	$avconfig['stopViewerButtonEnabled'] = 1;
}

$avconfig['showIgnorePMsButton'] = 0;
if ($userPermissions[0]->can_ignore_pm == '1') {
	$avconfig['showIgnorePMsButton'] = 1;
}

$avconfig['typingEnabled'] = 0;
if ($userPermissions[0]->typing_enabled == '1') {
	$avconfig['typingEnabled'] = 1;
}

$avconfig['dropInRoom'] = $userPermissions[0]->drop_in_room;

$avconfig["maxRoomsOneCanBeIn"] = $userPermissions[0]->max_rooms;

$avconfig["userNamePrefix"] = $userPermissions[0]->username_prefix;

$avconfig['profileCountryFlag'] = $userPermissions[0]->country_flag;

$avconfig['showOnlyRooms'] = $userPermissions[0]->view_only_rooms;

$avconfig['hideTheseRooms'] = $userPermissions[0]->cant_view_rooms;

$avconfig['onlyBroadcastersCanViewOtherCams'] = 0;
if ($userPermissions[0]->only_broadcasters_can_view_cams == '1') {
	$avconfig['onlyBroadcastersCanViewOtherCams'] = 0;
}

$avconfig['enableModeratorForRooms'] = 0;
if ($userPermissions[0]->can_moderate_rooms == '1') {
	$avconfig['enableModeratorForRooms'] = 0;
}

$avconfig['usersCanInviteToViewCam'] = 0;
if ($userPermissions[0]->invite_to_view_stream == '1') {
	$avconfig['usersCanInviteToViewCam'] = 0;
}

$avconfig['usersCanInviteToRoom'] = 0;
if ($userPermissions[0]->invite_to_rooms == '1') {
	$avconfig['usersCanInviteToRoom'] = 1;
}

$avconfig['allowUrls'] = 0;
if ($userPermissions[0]->allow_urls_text_chat == '1') {
	$avconfig['allowUrls'] = 1;
}

$avconfig['allowEmails'] = 0;
if ($userPermissions[0]->allow_emails_text_chat == '1') {
	$avconfig['allowEmails'] = 1;
}


//Admin permissions
if ($userPermissions[0]->can_access_admin_chat == '1') {
	$avconfig['adminCanKick'] = '0';
	if ($userPermissions[0]->admin_can_kick == '1') {
		$avconfig['adminCanKick'] = '1';
	}
	
	$avconfig['adminCanBan'] = '0';
	if ($userPermissions[0]->admin_can_ban == '1') {
		$avconfig['adminCanBan'] = '1';
	}
	
	$avconfig['adminCanViewIps'] = '0';
	if ($userPermissions[0]->admin_can_view_ips == '1') {
		$avconfig['adminCanViewIps'] = '1';
	}
	
	$avconfig['adminCanSilenceFromRoom'] = '0';
	if ($userPermissions[0]->admin_can_silence == '1') {
		$avconfig['adminCanSilenceFromRoom'] = '1';
	}
	
	$avconfig['adminCanSilenceFromRoom'] = '0';
	if ($userPermissions[0]->admin_can_view_pms == '1') {
		$avconfig['adminCanSilenceFromRoom'] = '1';
	}
	
	$avconfig['adminCanAccessSettings'] = '0';
	if ($userPermissions[0]->admin_can_access_sett == '1') {
		$avconfig['adminCanAccessSettings'] = '1';
	}
	
	$avconfig['hiddenGenderEnabled'] = '0';
	if ($userPermissions[0]->admin_can_hide == '1') {
		$avconfig['hiddenGenderEnabled'] = '1';
	}
	
	$avconfig['adminCanViewHiddenAdmins'] = '0';
	if ($userPermissions[0]->admin_can_view_hiden_admins == '1') {
		$avconfig['adminCanViewHiddenAdmins'] = '1';
	}
	
	$avconfig['adminCanDeleteRooms'] = '0';
	if ($userPermissions[0]->admin_can_delete_rooms == '1') {
		$avconfig['adminCanDeleteRooms'] = '1';
	}
	
	$avconfig['adminCanRemoveBan'] = '0';
	if ($userPermissions[0]->admin_can_remove_ban == '1') {
		$avconfig['adminCanRemoveBan'] = '1';
	}
	
	$avconfig['adminCanViewPrivateStreamsWithoutPermission'] = '0';
	if ($userPermissions[0]->admin_can_view_private_streams == '1') {
		$avconfig['adminCanViewPrivateStreamsWithoutPermission'] = '1';
	}
	
	$avconfig['adminCanJoinPrivateRoomsWithoutPermission'] = '0';
	if ($userPermissions[0]->admin_can_join_private_rooms == '1') {
		$avconfig['adminCanJoinPrivateRoomsWithoutPermission'] = '1';
	}
	
	$avconfig['adminCanAccessBannPanel'] = '0';
	if ($userPermissions[0]->admin_can_access_bann_panel == '1') {
		$avconfig['adminCanAccessBannPanel'] = '1';
	}
	
	$avconfig['adminCanViewExtraInfo'] = '0';
	if ($userPermissions[0]->admin_can_view_extra_info == '1') {
		$avconfig['adminCanViewExtraInfo'] = '1';
	}

}

//echo '<pre>';
//var_dump($settings[0]);
//var_dump($userPermissions[0]);
//var_dump($current_user);

if (($current_user->ID != NULL) && ($current_user->ID != "") && ($current_user->ID > 0)) {
	//user session exist
	$avconfig['enableOtherAccountOptions'] = 0;
	$avconfig['enableRegisterTab'] = 0;
	$avconfig['changeuser'] = 0;
	$avconfig['changegender'] = 1;
	
	$avconfig['username'] = $current_user->user_login;
	if ($settings[0]->display_username_realname == 'real-name') {
		$avconfig['username'] = $current_user->display_name;
	}
	
	//default wordpress avatar
	$imgString = get_avatar($current_user->user_email, 50);
	if (preg_match('# src=\'(.*)\' #U', $imgString, $defaultAvatar)) {
		if ($defaultAvatar[1] != '') {
			$avconfig["thumbnailUrl"] = $defaultAvatar[1];
		}
	}
	
	//WP User Avatar Plugin   http://wordpress.org/plugins/wp-user-avatar/
	if (in_array( 'wp-user-avatar/wp-user-avatar.php', apply_filters( 'active_plugins', get_option( 'active_plugins' )))) {
		$avconfig["thumbnailUrl"]= get_wp_user_avatar_src($current_user->ID, 'thumbnail');
	}
	
	//budypress
	if(in_array( 'buddypress/bp-loader.php', apply_filters('active_plugins', get_option('active_plugins')))) {
		//avatar
		$avconfig["thumbnailUrl"] = bp_core_fetch_avatar(array( 'item_id' => $current_user->ID, 'type' => 'thumb', 'alt' => '', 'css_id' => '', 'class' => 'avatar', 'width' => '40', 'height' => '40', 'email' => $current_user->user_email, 'html' => 'false'));
			
		//User profile page
		$avconfig['profileUrl'] = get_bloginfo('wpurl') . '/members/'.$current_user->user_login.'/profile/';
		//$userClass = new BP_Core_User($user_info['user_id']);
	}
	
} else {
	//visitator
	$avconfig['username'] = 'Visitor_'.rand(1,4);
	$avconfig['profileUrl'] = '';
	$avconfig['changegender'] = 1;
}
?>