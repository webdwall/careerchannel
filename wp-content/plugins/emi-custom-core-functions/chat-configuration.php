<?php 
add_shortcode( 'avchat', 'cc_avchat_shortcode_wrapper' );
/**
 * a workaround of the drawback that avchat plugin doesn't provide a shortcode.
 * It does however applies a filter on content and does a str_replace of [chat] with the generated html of chat iterface.
 * So lets use that function directly to provide support for a shortcode
 */
function cc_avchat_shortcode_wrapper(){
	$retval = '';
	if(function_exists('avchat3_pro_get_user_chat') ){
		$retval = avchat3_pro_get_user_chat( '[chat]' );
	}
	return $retval;
}