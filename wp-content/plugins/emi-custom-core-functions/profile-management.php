<?php 
/**
 * Checks if the given user is a 'Candidate'.
 * A 'Candidate' is a normal 'subscriber' member with no other wordpress roles.
 * 
 * @param int|object $user : the user(id) or user(object). Default: false(loggedin user)
 * @return boolean
 */
function is_user_a_candidate( $user=false ){
	$is_user_a_candidate = false;
	
	if( !$user ){
		if( !is_user_logged_in() ){
			return false;
		}
		$user = wp_get_current_user();
	}
	else{
		if( !is_object($user) ){
			$user = new WP_User($user);
		}
	}
	
	//if the user is subscriber
	if( !user_can($user, 'edit_posts') ){
		//we have an additional subscriber-level role 'employer'
		//so lets confirm that the user is not an 'employer'
		if ( !empty( $user->roles ) && is_array( $user->roles ) ) {
			$is_user_a_candidate = true;
			foreach ( $user->roles as $role ){
				if( 'subscriber'!=  strtolower($role) ){
					$is_user_a_candidate = false;
					break;
				}
			}
		}
	}
	
	return $is_user_a_candidate;
}

/*
 * this function will add custom fields in the your-profile page of theme-my-login
 * $param $profileuser - WP_User object having all the information of current user
 * @return void
 */
function add_custom_fields_in_your_profile_page( $profileuser ){
    $user_id = $profileuser->data->ID;
    
    $roles = $profileuser->roles[0];
    $can_show_extra_fields = check_can_we_show_extra_fields( $roles );
    if( !$can_show_extra_fields ){
	return false;
    }
    /* if return true that means registeration type of current user is not specified */
    $have_to_configure_RT_setting = check_current_user_type_configuration();
    
    $show_extra_custom_fields = true;
    if( $have_to_configure_RT_setting ){

	$show_extra_custom_fields = false;
	?>
	<tr>
	    <th>
		<label>Registered as</label>
	    </th>
	    <td>
		<select name="user_type" id="user_type">
		    <option>--member type --</option>
		    <option value="candidate" <?php if( $roles == "subscriber" ){	echo 'selected'; } ?> >Candidate</option>
		    <option value="employer" <?php if( $roles == "employer" ){ echo 'selected'; } ?> >Employer</option>
		</select>
	    </td>
	</tr>
	<?php
    }
    
    if( $show_extra_custom_fields ){
	show_extra_custom_fields_in_your_profile_page( $profileuser );
    }
    
}

function check_can_we_show_extra_fields( $roles ){
    if( $roles == "employer" || $roles == "subscriber" ){
		return true;
    }
    return false;
}

function show_extra_custom_fields_in_your_profile_page( $profileuser ){
    $user_id = $profileuser->data->ID;
    $roles = $profileuser->roles[0];
    
    global $wpdb;
    $table_name = $wpdb->prefix . "country_cat";
    $countries = $wpdb->get_results("select * from $table_name");
    
    $nationality = get_user_meta($user_id,"nationality",true);
    $country_of_residence = get_user_meta($user_id,"user_cor",true);
    
    $preferred_location1 = get_user_meta($user_id,"preferred_location1",true);
    
    $jobstreet = get_user_meta( $user_id, "jobstreet", true );
    
    $salary = get_user_meta( $user_id, "salary", true );
    $expected_salary = get_user_meta( $user_id, "expected_salary", true );
    $experience = get_user_meta( $user_id, "experience_in_months", true );
    $experience_months = $experience%12 > 0 ? $experience%12 : "";
    $experience_years = ($experience - $experience%12 )/12;
    $position = get_user_meta( $user_id, "position", true );
    
    $skill_arr_val = get_user_meta( $user_id, "skill", true );
    
    $education_val = get_user_meta( $user_id, "education", true );
    $webcam_check = get_user_meta( $user_id, "webcam_check", true );
    $motor_license_val = get_user_meta( $user_id, "motor_license", true );
    
    $date_of_birth = get_user_meta( $user_id, "member_dob", true );
    $residential_region = get_user_meta( $user_id, "member_residential_region", true );
    $career_objective = get_user_meta( $user_id, "candidate_career_objectives", true );
    $hide_profile_option = get_user_meta($user_id, "cc_hide_profile", true);
    ?>
    <?php if( $roles != "employer" ) : ?>
	<?php
	$hide_field_message = get_option("c_account_hide_message");
	?>
	<tr>
	    <th>
		<label>Hide Account ?</label>
	    </th>
	    <td>
		<label><input type="radio" name="hide_account_radio" value="yes" <?php if( $hide_profile_option == "yes" ){ echo 'checked'; } ?> /> Yes</label>
		<label><input type="radio" name="hide_account_radio" value="no" <?php if( $hide_profile_option == "no" || $hide_profile_option == "" ){ echo 'checked'; } ?> /> No</label>
		<small><i><?php echo $hide_field_message; ?></i></small>
	    </td>
	</tr>
	<tr>
	    
	<th>
	    <label for="salary"> Date of Birth </label>
	</th>
	<td>
	    <div class="select" >
		<?php 
		$content = get_month_year_day_content();
		
		if( $date_of_birth && !empty( $date_of_birth ) ){
		    $dob_arr = explode("-", $date_of_birth);
		}else{
		    /* get rid of warnings using the statement */
		    $dob_arr = array("","","");
		}
		?>
		<select name="member_dob_year" >
		    <option>-Year-</option>
		    <?php foreach( $content["year"] as $year ) :
			$select_attr = $dob_arr[0] == $year ? "selected" : "";
			?>
			<option <?php echo $select_attr; ?> value="<?php echo $year; ?>" ><?php echo $year; ?></option>
		    <?php endforeach; ?>
		</select>
		<select name="member_dob_month" >
		    <option>-Month-</option>
		    <?php foreach( $content["month"] as $month ) :
			$select_attr = $dob_arr[1] == $month ? "selected" : "";
			?>
		    <option <?php echo $select_attr; ?> value="<?php echo $month; ?>" ><?php echo $month; ?></option>
		    <?php endforeach; ?>
		</select>
		<select name="member_dob_day" >
		    <option>-Day-</option>
		    <?php foreach( $content["day"] as $day ) :
			$select_attr = $dob_arr[2] == $day ? "selected" : "";
			?>
		    <option <?php echo $select_attr; ?> value="<?php echo $day; ?>" ><?php echo $day; ?></option>
		    <?php endforeach; ?>
		</select>
	    </div>
	</td>
    </tr>
    <?php endif; ?>
    <tr>
	<th>
	    <label for="user_nationality_profile"> Nationality </label>
	</th>
	<td>
	    <select name="nationality" id="user_nationality_profile" >
		<?php populateCountries( $nationality ); ?>
	    </select>
	</td>
    </tr>
    <tr>
	<th>
	    <label for="user_cor"> Country of Residence </label>
	</th>
	<td>
	    <select name="user_cor" id="user_cor" onchange="getworklocations( this.selectedIndex );" >
		<?php populateCountries( $country_of_residence ); ?>
	    </select>
	</td>
    </tr>
    <?php if( $roles != "employer" ) : ?>
    <tr>
	<th>
	    <label for="">Where do you want to work</label>
	</th>
	<td>
	    <div id="locations" >
		<?php echo $preferred_location1; ?>
	    </div>
	</td>
    </tr>
    <?php endif; ?>
    <?php if( $roles != "employer" ) : ?>
    <tr>
	<th>
	    <label for="salary"> Motor License </label>
	</th>
	<td>
	    <div class="select" >
		<select name="motor_license" id="motor_license" >
		    <option value="">-select Motor License-</option>
		    <?php
		    $motor_licenses = get_motor_license();
		    foreach ( $motor_licenses as $motor_license ){
			if( $motor_license_val == $motor_license  ){
			    echo '<option value="'.$motor_license.'" selected >'.$motor_license.'</option>';
			}else{
			    echo '<option value="'.$motor_license.'" >'.$motor_license.'</option>';
			}
		    }
		    ?>
		</select>
	    </div>
	</td>
    </tr>
    <?php
    global $wpdb;
    $table_name = $wpdb->prefix . "educational_qualification";
    $educational = $wpdb->get_results("select * from $table_name");
    ?>
    <tr>
	<th>
	    <label>Educational Qualification</label>
	</th>
	<td>
	    <div class="field">
		<select name="education" id="education" value="">
		    <option value="">Select option</option>
		    <?php foreach ($educational as $education) : ?>
		    <?php if( $education->name == $education_val ) : ?>
		    	<option  value="<?php echo $education->name; ?>" selected ><?php echo $education->name; ?></option>
			<?php else: ?>
		    	<option  value="<?php echo $education->name; ?>" ><?php echo $education->name; ?></option>
			<?php endif; ?>
		    <?php endforeach; ?>
		</select>
	    </div>
	</td>
    </tr>
    <tr>
	<th>
	    <label>Do You Have Web Cam?</label>
	</th>
	<td>
	    <label><input type="radio" name="webcam_check" value="yes" <?php if( $webcam_check == "yes" ){ echo 'checked'; } ?> /> YES</label>
	    <label><input type="radio" name="webcam_check" value="no" <?php if( $webcam_check == "no" ){ echo 'checked'; } ?> /> No</label>
	</td>
    </tr>
    <tr>
	    <th><label for="description"><?php _e( 'About Yourself' ); ?></label></th>
	    <td><textarea name="description" id="about_yourself" rows="5" cols="30"><?php echo esc_html( $profileuser->description ); ?></textarea><br />
	    <span class="description"><?php _e( 'Share a little biographical information to fill out your profile. This may be shown publicly.' ); ?></span></td>
    </tr>
    <tr>
	<th>
	    <label for="salary"> Expected Monthly Salary </label>
	</th>
	<td>
	    <div class="field has-select" style="width: 47%;">
		<div class="select" style="width:100%;">
		    <select name="salary" id="salary" value="">
				<option value="">-Select One-</option>
				<option value="SGD" <?php if( $salary == 'SGD' ){ echo 'selected'; } ?> >Singapore Dollars</option>
				<option value="USD" <?php if( $salary == 'USD' ){ echo 'selected'; } ?> >US Dollars</option>
		    </select>
		</div>
	    </div>
	    <input type="text" class="input-text" name="expected_salary" id="expected_salary" value="<?php if( isset( $expected_salary ) ){ echo $expected_salary; } ?>" style="width: 46%;float: right;margin-top: -38px;">
	</td>
    </tr>
    <tr>
	<th>
	    <label> Experience </label>
	</th>
	<td>
	    <input type="number" name="experience_years_field" id="experience_years_field"  placeholder="years" value="<?php echo $experience_years; ?>" min="0" />
	    <input type="number" name="experience_months_field" id="experience_months_field" placeholder="months"  value="<?php echo $experience_months; ?>" min="0" max="11" />
	</td>
    </tr>
    <tr>
	<th>
	    <label>Level of position you want to work in</label>
	</th>
	<td>
	    <div class="fields">
		<label><input type="checkbox" name="position[]" <?php if( !empty($position) && in_array("Senior Manager", $position ) ){ echo "checked"; } ?> id="user_position" value="Senior Manager" >Senior Manager</label>
		<label><input type="checkbox" name="position[]" <?php if( !empty($position) && in_array("Manager", $position ) ){ echo "checked"; } ?> id="user_position" value="Manager" >Manager</label>
		<label><input type="checkbox" name="position[]" <?php if( !empty($position) && in_array("Senior Executive", $position ) ){ echo "checked"; } ?> id="user_position" value="Senior Executive" >Senior Executive</label>
		<label><input type="checkbox" name="position[]" <?php if( !empty($position) && in_array("Junior Executive", $position ) ){ echo "checked"; } ?> id="user_position" value="Junior Executive" >Junior Executive</label>
		<label><input type="checkbox" name="position[]" <?php if( !empty($position) && in_array("Fresh Entry/Level", $position ) ){ echo "checked"; } ?> id="user_position" value="Fresh Entry/Level" >Fresh Entry/Level</label>
		<label><input type="checkbox" name="position[]" <?php if( !empty($position) && in_array("Non Executive", $position ) ){ echo "checked"; } ?> id="user_position" value="Non Executive" >Non Executive</label>
		<label><input type="checkbox" name="position[]" <?php if( !empty($position) && in_array("Student Seeking Intership", $position ) ){ echo "checked"; } ?> id="user_position" value="Student Seeking Intership" >Student Seeking Intership</label>
	    </div>
	</td>
    </tr>
    <?php
	global $wpdb;
	$table_name = $wpdb->prefix . "skills_cat";
	$skills = $wpdb->get_results("select * from $table_name");
    ?>
    <tr class="user_type_candidate_fields" >
	<th>
	    <label for="">What do you want to specialize in <span style="color:#C9C7C7;">(Maximum 5)&nbsp;</span></label>
	</th>
	<td>
	    <div class="field specialization_field_container" style="padding: 14px 19px 41px 21px;border: solid 1px #b0b8bb;">
		<?php foreach ($skills as $skill) : ?>
		    <label for=""><?php echo $skill->cat_name; ?></label>
		    <?php
		    global $wpdb;
		    $table_name = $wpdb->prefix . "skills";
		    $cat = $wpdb->get_results("select * from $table_name where skill_cat='$skill->id'");
		    ?>
		    <?php foreach ($cat as $cats) : ?>
			<label>
			    <input type="checkbox" name="skill[]" <?php if( !empty( $skill_arr_val ) &&  in_array( $cats->skill_name ,$skill_arr_val ) ){ echo "checked"; } ?> value="<?php echo $cats->skill_name; ?>" > <?php echo $cats->skill_name; ?>
			</label>
		    <?php endforeach; ?>
		<?php endforeach; ?>
	    </div>    
	</td>
    </tr>
    
    <?php endif; ?>
    
    <?php if( $roles == 'employer' ): ?>
    <?php
	$member_position_title = get_user_meta( $user_id, 'member_position_title', true );
	$member_contact_number = get_user_meta( $user_id, 'member_contact_number', true );
	$emp_company_name = get_user_meta( $user_id, 'emp_company_name', true );
	$emp_company_location = get_user_meta( $user_id, 'emp_company_location', true );
	$emp_office_number = get_user_meta( $user_id, 'emp_office_number', true );
	$emp_company_email = get_user_meta( $user_id, 'emp_company_email', true );
	$emp_company_address = get_user_meta( $user_id, 'emp_company_address', true );
	$emp_company_postal_code = get_user_meta( $user_id, 'emp_company_postal_code', true );
	$emp_company_registration_number = get_user_meta( $user_id, 'emp_company_registration_number', true );
	$emp_about_company = get_user_meta( $user_id, 'emp_about_company', true );
	
    ?>
    <tr>
	<th><label>Company Name</label></th>
	<td>
	    <input type="text" name="emp_company_name" id="emp_company_name"  value="<?php echo $emp_company_name; ?>" />
	</td>
    </tr>
    <tr>
	<th><label>Location of Company</label></th>
	<td>
	    <input type="text" name="emp_company_location" id="emp_company_location"  value="<?php echo $emp_company_location; ?>" />
	</td>
    </tr>
    <tr>
	<th><label>Contact Number</label></th>
	<td>
	    <input type="text" name="member_contact_number" id="member_contact_number"  value="<?php echo $member_contact_number; ?>"  />
	</td>
    </tr>
    <tr>
	<th><label>Company Email</label></th>
	<td>
	    <input type="text" name="emp_company_email" id="emp_company_email" value="<?php echo $emp_company_email; ?>" />
	</td>
    </tr>

    <tr>
	<th><label>Company Address</label></th>
	<td>
	    <input type="text" name="emp_company_address" id="emp_company_address" value="<?php echo $emp_company_address; ?>" />
	</td>
    </tr>


    <tr>
	<th><label>Company Postal Code</label></th>
	<td>
	    <input type="text" name="emp_company_postal_code" id="emp_company_postal_code" value="<?php echo $emp_company_postal_code; ?>" />
	</td>
    </tr>


    <tr>
	<th><label>Company Registration Number</label></th>
	<td>
	    <input type="text" name="emp_company_registration_number" id="emp_company_registration_number" value="<?php echo $emp_company_registration_number; ?>" />
	</td>
    </tr>
    <tr>
	<th>
	    <label>Do You Have Web Cam?</label>
	</th>
	<td>
	    <label><input type="radio" name="webcam_check" value="yes" <?php if( $webcam_check == "yes" ){ echo 'checked'; } ?> />YES</label>
	    <label><input type="radio" name="webcam_check" value="no" <?php if( $webcam_check == "no" ){ echo 'checked'; } ?> />No</label>
	</td>
    </tr>
    
    <tr>
	<th>
	    <label for="">About the company</label>
	</th>
	<td>
	    <div id="locations" >
		<textarea name="emp_about_company" id="emp_about_company" ><?php echo $emp_about_company; ?></textarea>
	    </div>
	</td>
    </tr>

    <?php endif; ?>
    <?php if( $roles != 'employer' ): ?>
    <tr>
	<th>
	    <label>I learned about Career Channel from</label>
	</th>
	<td>
	    <div class="field">
		<select name="jobstreet" id="jobstreet" value="">
		    <option value="">-Select One-</option>
		    <option value="tv" <?php if( $jobstreet == 'tv' ){ echo "selected"; } ?> >TV</option>
		    <option value="radio" <?php if( $jobstreet == 'radio' ){ echo "selected"; } ?> >Radio</option>
		    <option value="google search engine" <?php if( $jobstreet == 'google search engine' ){ echo "selected"; } ?> >Google Search Engine</option>
		    <option value="yahoo search engine" <?php if( $jobstreet == 'yahoo search engine' ){ echo "selected"; } ?> >Yahoo Search Engine</option>
		    <option value="website banner" <?php if( $jobstreet == 'website banner' ){ echo "selected"; } ?> >Website Banner</option>
		    <option value="magzine" <?php if( $jobstreet == 'magzine' ){ echo "selected"; } ?> >Magazine</option>
		    <option value="newspaper" <?php if( $jobstreet == 'newspaper' ){ echo "selected"; } ?> >Newspaper</option>
		    <option value="exhibition" <?php if( $jobstreet == 'exhibition' ){ echo "selected"; } ?> >Exhibition</option>
		    <option value="bus" <?php if( $jobstreet == 'bus' ){ echo "selected"; } ?> >Bus</option>
		    <option value="lrt/mrt/train" <?php if( $jobstreet == 'lrt/mrt/train' ){ echo "selected"; } ?> >LRT/MRT/Train</option>
		    <option value="bus shelter" <?php if( $jobstreet == 'bus shelter' ){ echo "selected"; } ?> >Bus Shelter</option>
		    <option value="texi" <?php if( $jobstreet == 'texi' ){ echo "selected"; } ?> >Texi</option>
		    <option value="friend" <?php if( $jobstreet == 'friend' ){ echo "selected"; } ?> >Friend</option>  
		    <option value="others" <?php if( $jobstreet == 'others' ){ echo "selected"; } ?> >Others</option>
		</select>
	    </div>
	</td>
    </tr>
    <?php endif; ?>
    <?php
}

add_action( 'profile_update', 'save_TML_your_profile_custom_fields', 10, 2 );

function save_TML_your_profile_custom_fields( $user_id, $old_user_data ){

    if( check_current_user_type_configuration() && isset( $_POST["user_type"] ) && $_POST["user_type"] != "" ){
	if( $_POST["user_type"] == "employer" ){
	    $wp_user_object = new WP_User( $user_id );
	    $wp_user_object->set_role('employer');
	    
	}else{
	    $wp_user_object = new WP_User( $user_id );
	    $wp_user_object->set_role('subscriber');
	    
	}
	update_user_meta( $user_id, 'registration_type', "fb" );
    }else{
	$user_details = get_userdata($user_id);
	save_your_profile_common_fields( $user_id );
	
	if( $user_details->roles[0] == "employer" ){
	    
	    save_your_profile_employer_common_fields( $user_id );
	    
	}else{
	    save_your_profile_candidates_common_fields( $user_id );
	}
    }
}

function save_your_profile_common_fields( $user_id ){
    
    if( isset( $_POST["nationality"] ) && $_POST["nationality"] != "" ){
	update_user_meta( $user_id, 'nationality', $_POST['nationality'] );
    }

    if( isset( $_POST["user_cor"] ) && $_POST["user_cor"] != "" ){
	update_user_meta( $user_id, 'user_cor', $_POST['user_cor'] );
    }
    
    if( isset( $_POST["preferred_location1"] ) && $_POST["preferred_location1"] != "" ){
	update_user_meta( $user_id, 'preferred_location1', $_POST['preferred_location1'] );
    }
    
    if( isset( $_POST["jobstreet"] ) && $_POST["jobstreet"] != "" ){
	update_user_meta( $user_id, 'jobstreet', $_POST['jobstreet'] );
    }
    
    if( isset( $_POST["webcam_check"] ) ){
	update_user_meta( $user_id, 'webcam_check', $_POST['webcam_check'] );
    }
    $user_detials = get_userdata($user_id);
    $user_email = $user_detials->data->user_email;
    $subscribe_to_maillist = $_POST['cmmi_optin_checkbox'] != "" ? true : false;
    update_user_meta( $user_id, 'subscribe_to_maillist', $subscribe_to_maillist );
    if( $subscribe_to_maillist ){
	cmmi_subscribe($user_id, $user_email);
    }else{
	cmmi_unsubscribe($user_id, $user_email);
    }
}

function save_your_profile_candidates_common_fields( $user_id ){
    
    if( $_POST["hide_account_radio"] == "yes" ){
	update_user_meta($user_id, "cc_hide_profile", $_POST["hide_account_radio"] );
    }else if( $_POST["hide_account_radio"] == "no" ){
	delete_user_meta($user_id, "cc_hide_profile");
    }
    if( isset( $_POST["salary"] ) && $_POST["salary"] != "" ){
	update_user_meta( $user_id, 'salary', $_POST['salary'] );
    }

    if( isset( $_POST["expected_salary"] ) && $_POST["expected_salary"] != "" ){
	update_user_meta( $user_id, 'expected_salary', $_POST['expected_salary'] );
    }
    
    if( (isset( $_POST["experience_years_field"] ) && $_POST["experience_years_field"]) ||  (isset( $_POST["experience_months_field"] ) && $_POST["experience_months_field"])  ){
	$years = ($_POST["experience_years_field"] > 0)?$_POST["experience_years_field"]:0;
	$months = ($_POST["experience_months_field"] > 0)?$_POST["experience_months_field"]:0;
	$experience_in_months = ($years * 12) + $months;
	update_user_meta( $user_id , 'experience_in_months', $experience_in_months );
    }

    if( isset( $_POST["position"] ) ){
	update_user_meta( $user_id, 'position', $_POST['position'] );
    }

    if( isset( $_POST["skill"] ) ){
	update_user_meta( $user_id, 'skill', $_POST['skill'] );
    }

    if( isset( $_POST["education"] ) ){
	update_user_meta( $user_id, 'education', $_POST['education'] );
    }
    
    
    if( isset( $_POST["motor_license"] ) ){
	update_user_meta( $user_id, 'motor_license', $_POST['motor_license'] );
    }
    
    if( isset( $_POST["member_dob_year"] ) && $_POST["member_dob_year"] != "" && isset( $_POST["member_dob_month"] ) && $_POST["member_dob_month"] != "" && isset( $_POST["member_dob_day"] ) && $_POST["member_dob_day"] != "" ){
	$dob = $_POST["member_dob_year"]."-".$_POST["member_dob_month"]."-".$_POST["member_dob_day"];
	update_user_meta( $user_id, 'member_dob', $dob );
    }
    if( isset( $_POST["member_residential_region"] ) && $_POST["member_residential_region"] != "" ){
	update_user_meta( $user_id, 'member_residential_region', $_POST['member_residential_region'] );
    }
    
    if( isset( $_POST["candidate_career_objectives"] ) && $_POST["candidate_career_objectives"] != "" ){
	update_user_meta( $user_id, 'candidate_career_objectives', $_POST['candidate_career_objectives'] );
    }
    
}


function save_your_profile_employer_common_fields( $user_id ){
    if( isset( $_POST["member_position_title"] ) ){
	update_user_meta( $user_id, 'member_position_title', $_POST['member_position_title'] );
    }
    if( isset( $_POST["member_contact_number"] ) ){
	update_user_meta( $user_id, 'member_contact_number', $_POST['member_contact_number'] );
    }
    if( isset( $_POST["emp_company_name"] ) ){
	update_user_meta( $user_id, 'emp_company_name', $_POST['emp_company_name'] );
    }
    if( isset( $_POST["emp_company_location"] ) ){
	update_user_meta( $user_id, 'emp_company_location', $_POST['emp_company_name'] );
    }
    if( isset( $_POST["emp_office_number"] ) ){
	update_user_meta( $user_id, 'emp_office_number', $_POST['emp_office_number'] );
    }
    if( isset( $_POST["emp_company_email"] ) ){
	update_user_meta( $user_id, 'emp_company_email', $_POST['emp_company_email'] );
    }
    if( isset( $_POST["emp_company_address"] ) ){
	update_user_meta( $user_id, 'emp_company_address', $_POST['emp_company_address'] );
    }
    if( isset( $_POST["emp_company_postal_code"] ) ){
	update_user_meta( $user_id, 'emp_company_postal_code', $_POST['emp_company_postal_code'] );
    }
    if( isset( $_POST["emp_company_registration_number"] ) ){
	update_user_meta( $user_id, 'emp_company_registration_number', $_POST['emp_company_registration_number'] );
    }
    if( isset( $_POST["emp_about_company"] ) ){
	update_user_meta( $user_id, 'emp_about_company', $_POST['emp_about_company'] );
    }
}