jQuery(document).ready(function() {
	//validate_user_login_details();
	/* Extend job listing code */
	extend_job_listing_setup();
	save_extend_job_listing();
	
	/* Reders Calender on Employer dashboard */
	employer_calender_script();
	
	employer_add_appointment_script();
	
	save_candidate_tags_details();
	
	search_candidate_auto_suggest_script();
	
	change_image_upload_container_on_top();
	
	post_job_listing_repositioning();
	
	post_job_listing_repositioning_select_tags();
	
	cc_delete_appointment();
	
	/* Registration default value */
	jQuery("#registerform #user_cor").trigger("change");
});

function validate_user_login_details(){

	jQuery("#loginform .login-submit #wp-submit").click(function(){
		
		var user_login = jQuery("#loginform #user_login").val();
		var user_pass = jQuery("#loginform #user_pass").val();
		var error = false;

		jQuery("#loginform .login_custom_error ").remove();

		if ( user_login == "" ){

			jQuery("#loginform #user_login").after("<p class='login_custom_error' >Empty field is not allowed...</p>");
			error = true;

		}
		if ( user_pass == "" ){

			jQuery("#loginform #user_pass").after("<p class='login_custom_error' >Empty field is not allowed...</p>");
			error = true;

		}
		
		if( error ){
			return false;
		}

		var data = {
			action: "emi_custom_login",
			name: user_login,
			pswd: user_pass
		}

		jQuery.ajax({
			type: 'POST',
			url: ajaxurl,
			data: data,
			success:function( response ){

				if( jQuery.trim(response) == "ok" ){

					location.reload();

				}else{
					response = jQuery.parseJSON( response );
					jQuery("#loginform").prepend( "<p class='login_custom_error'>"+response+"</p>" );

				}
				
				
			},
			error:function( response ){
				
			}
		});
	    return false;
	});

}

function extend_job_listing_setup(){
    var html = jQuery("#extend_job_listing_modal").parent().html();
    jQuery("#extend_job_listing_modal").parent().remove();
    jQuery("body").append( html );
}

function save_extend_job_listing(){
    var doing_ajax = false;
    jQuery("#extend_job_listing_modal .extend_job_listing_save_link").click(function(){
	
	if( doing_ajax ){
	    alert("please wait your request is processing...");
	    return false;
	}
	
	jQuery("#extend_job_listing_modal .warning_message").remove();
	if( jQuery("#extend_job_listing_modal #job_package").val() == "" ){
	    jQuery("#extend_job_listing_modal #job_package").after("<p class='text-warning warning_message' >Field is required</p>");
	    return false;
	}
	
	var j_id = jQuery(this).attr("data-job_id");
	var o_id = jQuery("#extend_job_listing_modal #job_package").val();
	var data = {
	    action: "extend_current_job_listing",
	    _wpnonce: _ecs.nonce,
	    job_id: j_id,
	    order_id: o_id
	};
	
	doing_ajax = true;
	
	jQuery.ajax({
	    type: "POST",
	    url: _ecs.ajax_url,
	    data: data,
	    success:function( response ){
		doing_ajax = false;
		response = jQuery.parseJSON( response );
		
		if( response.result == "ok" ){
		    jQuery("#extend_job_listing_modal #job_package").parent().replaceWith( response.drop_down_html );
		    alert( "Successfully extended current job listing period... ");
		    jQuery('#extend_job_listing_modal').modal('hide');
		}else{
		    if( typeof( response.message!=='undefined' ) && response.message!=='' ){
			alert( response.message );
		    }
		    else{
			alert("some problem occured with the request.. Please try again!!");
		    }
		}
	    },
	    error:function( response ){
		doing_ajax = false;
	    }
	});
    });
}

function getworklocations(country_id) {
    
    if( typeof(country_id)!=='undefined'){
	var data = {
	    action: 'emi_work_location_states',
	    country_id: country_id
	};
	jQuery.ajax({
	    type: "POST",
	    url: _ecs.ajax_url,
	    data: data,
	    success: function (response) {
		jQuery('#locations').html(response);
	    }
	});
    }
}

function add_first_location(){
    document.getElementById('second_location_dropdown').style.display = 'block';
    document.getElementById('first_location').style.display = 'none';
    document.getElementById('second_location').style.display = 'block';
}

function add_second_location() {
    document.getElementById('third_location_dropdown').style.display = 'block';
    document.getElementById('second_location').style.display = 'none';
    document.getElementById('third_location').style.display = 'none';
}

function employer_calender_script(){
    
    jQuery('#wp-calendar').datepicker();
    
    var appointment_dates = jQuery(".employer_dashboard_calender_container #can_appointments_date_list_hidden").val();
    
    if( appointment_dates == "" || appointment_dates == null ){
	return false;
    }
    var appointment_dates_arr = jQuery.parseJSON( appointment_dates );
    
    var c_date = new Date();
    var c_month = (parseInt(c_date.getMonth()) + 1);
    var c_year = c_date.getFullYear();
    WP_Cal_mark_dates_by_month( appointment_dates_arr ,c_month, c_year );
    
    jQuery('.ui-datepicker-next, .ui-datepicker-prev, .wp-cal-prev, .wp-cal-next').live('click', function (){
	
        var month_num = jQuery('#wp-calendar .ui-datepicker-month').text();
        var Year = jQuery('#wp-calendar .ui-datepicker-year').text();
        month_num = WP_Cal_convertMonth( month_num );
	WP_Cal_mark_dates_by_month( appointment_dates_arr, month_num, Year);
	employer_appointment_handler();
    });
    
    employer_appointment_handler();
}

function WP_Cal_mark_dates_by_month( appointment_dates_arr, Month, Year ){
    
    jQuery.each( appointment_dates_arr, function ( key, value ){
	
	//alert( value );
	curr_date = key.split('-');
	d = curr_date[2];
	
	m = curr_date[1];
	y = curr_date[1];
	
	if( m != Month && y != Year ){
	    return;
	}
	
	
	
	//var element = jQuery('#wp-calendar a.ui-state-default:contains("'+d+'")');
	jQuery("#wp-calendar a.ui-state-default").filter(function() {
	    return jQuery(this).text() === d;
	}).css("box-shadow","0px 0px 25px 0px #FFB2B2 inset").attr("title",value+" Apointment(s)").addClass("show-tooltip").tooltip();
    });
}


function employer_appointment_handler(){
    jQuery("#wp-calendar .ui-datepicker-calendar td a").click(function(){
	var month_num = jQuery('#wp-calendar .ui-datepicker-month').text();
        var Year = jQuery('#wp-calendar .ui-datepicker-year').text();
        month_num = WP_Cal_convertMonth( month_num );
	var month_day = jQuery(this).text();
	
	var location = window.location.href;
	var date = Year+"-"+month_num +"-"+month_day;
	location = updateQueryStringParameter("date", date,location)
	window.location.href = location;
    });
}

function updateQueryStringParameter(key, value, url) {
    if (!url) url = window.location.href;
    var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi");

    if (re.test(url)) {
        if (typeof value !== 'undefined' && value !== null)
            return url.replace(re, '$1' + key + "=" + value + '$2$3');
        else {
            var hash = url.split('#');
            url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
            if (typeof hash[1] !== 'undefined' && hash[1] !== null) 
                url += '#' + hash[1];
            return url;
        }
    }
    else {
        if (typeof value !== 'undefined' && value !== null) {
            var separator = url.indexOf('?') !== -1 ? '&' : '?',
                hash = url.split('#');
            url = hash[0] + separator + key + '=' + value;
            if (typeof hash[1] !== 'undefined' && hash[1] !== null) 
                url += '#' + hash[1];
            return url;
        }
        else
            return url;
    }
}

function WP_Cal_convertMonth(month) {
    var m = new Array(7);
    m["January"] = 1;
    m["February"] = 2;
    m["March"] = 3;
    m["April"] = 4;
    m["May"] = 5;
    m["June"] = 6;
    m["July"] = 7;
    m["August"] = 8;
    m["September"] = 9;
    m["October"] = 10;
    m["November"] = 11;
    m["December"] = 12;
    return m[month];
}

function employer_add_appointment_script(){
    jQuery('.add_appointment_form #appointment_time').ptTimeSelect();
    jQuery('.add_appointment_form #appointment_date').datepicker( {
	dateFormat:"yy-m-d",
	minDate: 0
    } );
    
    
    jQuery(".add_appointment_form #add_appointment_to_list").click(function(){
	
	jQuery(".add_appointment_form .appointment_form_error").remove();
	var error = false;
	if( jQuery(".add_appointment_form #appointment_date").val() == "" ){
	    jQuery(".add_appointment_form #appointment_date").after("<p class='text-error appointment_form_error' >Field is required!!</p>");
	    error = true;
	}
	if( jQuery(".add_appointment_form #appointment_time").val() == "" ){
	    jQuery(".add_appointment_form #appointment_time").after("<p class='text-error appointment_form_error' >Field is required!!</p>");
	    error = true;
	}
	if( error ){
	    return false;
	}
	
    });
    
}

jQuery(document).ready(function($){ 
    /* ++++++++++++++++++++++++++++++++++++++
     * applying to job - on single job page 
     * +++++++++++++++++++++++++++++++++++ */
    $(".application-content .candidates_data").click(function(){
	jQuery(".application-content .apply_warning_message").remove();
        var candidate_message = $("#apply_job_message_txtarea").val();
        var sender_id = $(this).attr('data-sender');
        var reciver_id = $(this).attr('data-reciver');
        var primary_obj = $(this).attr('data-job');
        
	
	if( candidate_message == "" ){
	    jQuery(".application-content #apply_job_message_txtarea").after("<p class='text-error apply_warning_message' >Please provide a short message for employer!!</p>");
	    return false;
	}
	
	$(".show_status").addClass('loading');
	
        jQuery.ajax({ 
	    type: 'POST',  
	    url: _ecs.ajax_url,  
	    data: {  
		sender_id : sender_id,
		reciver_id :reciver_id,
		c_message : candidate_message,
		primary_obj : primary_obj,
		action: 'cc_apply_to_job'
	    },
	    success:function(response){
		response = jQuery.parseJSON(response);
		if( response.status===true ){
		    jQuery('.job_apply_alternate').html("<p class='text-success'>"+response.message+"</p>");
		}
		else{
		    jQuery('.job_apply_alternate').html("<p class='text-error'>"+response.message+"</p>");
		}
	    }
        });
    });
    /* ============ apply to jobs ========== */
    
    /* ++++++++++++++++++++++++++++++++++++++
     * send message click on compose message screen
     * +++++++++++++++++++++++++++++++++++ */
    $(".sent_notification").click(function(){
        var sender_id = $(this).attr('data-senderid'); 
        var subject = $("#message_subj").val();
        var reciver_id =$(this).attr('data-reciverid'); 
        var content = $("#message_content").val();
        $(".show_status").addClass('statusloading');
        jQuery.ajax({ 
	    type: 'POST',  
	    url: _ecs.ajax_url,
	    data: {  
		sender_id : sender_id,
		reciver_id :reciver_id,
		subject : subject,
		reciver_id : reciver_id,
		content : content,
		action: 'compose_message_ajax'
	    },
	    success:function(response){
		$(".show_status").removeClass('statusloading');
		response = jQuery.parseJSON(response);
		if( response.status===true ){
		   jQuery('.message_status').html("<p class='text-success'>"+response.message+"</p>");
		   $("#message_subj").val("");
		   $("#message_content").val("");
		}
		else{
		    jQuery('.job_apply_alternate').html("<p class='text-error'>"+response.message+"</p>");
		}
	    }
	});
    });
    /* ======== compose message =============== */
    
    /* ++++++++++++++++++++++++++++++++++
     * delete message
     * +++++++++++++++++++++++++++++++ */
    $(".delete_msg").click(function(){
        var id = $(this).attr('data_id');
        if (confirm("Are you sure?")) {
            jQuery.ajax({ 
		type: 'POST',  
		url: _ecs.ajax_url,  
		data: {  
		    msg_id :id,
		    action: 'delete_message_ajax'
		},
		success:function(response){
		    $("#delete_msg-"+id).parent().parent().fadeOut();
		    //alert("message deleted successfully.");
		}
            });
        }
        return false;
    });
    /* ======== delete message ============ */
});

function save_candidate_tags_details(){
    var doing_ajax = false;
    jQuery(".panel-heading .tag_candidate_radio_btn, .btn-group .tag_candidate_radio_btn").click(function(){
	
	if( doing_ajax ){
	    alert("please wait your request is getting processed....");
	    return false;
	}
	
	var curr_obj = jQuery(this);
	var tag = jQuery(this).val()
	var can_id = jQuery(this).attr("can_id")
	var job_id = jQuery(this).attr("job_id")
	
	if( can_id == "" && job_id == "" ){
	    alert("there some problem here.. please try again...");
	    return false;
	}
	
	var data = {
	    tag: tag,
	    can_id: can_id,
	    job_id: job_id,
	    action: 'tag_candidate_to_list'
	};
	doing_ajax = true;
	jQuery.ajax({
	    type: 'POST',
	    url: _ecs.ajax_url,
	    data: data,
	    success:function( response ){
		doing_ajax = false;
		alert("candidate tagged successfuly as "+tag);
		curr_obj.parents().eq(7).find(".candidate_tag").html("");
		curr_obj.parents().eq(7).find(".candidate_tag").html('<span class="label label-primary label-'+tag+'">'+tag+'</span>');
	    },
	    error:function( response ){
		doing_ajax = false;
	    }
	});
    });
}

function search_candidate_auto_suggest_script(){
    
    if( projects != "" ){
	
	var projects = jQuery.parseJSON( jQuery(".employer_candidae_searchbox_container #candidate_details_arr").val() );
	jQuery( ".employer_candidae_searchbox_container #candidate_search_text_box" ).autocomplete({
	    minLength: 0,
	    source: projects,
	    focus: function( event, ui ) {
		jQuery( "#project" ).val( ui.item.label );
		return false;
	    },
	    select: function( event, ui ) {
		jQuery( ".employer_candidae_searchbox_container #candidate_search_text_box" ).val( ui.item.label );
		jQuery( ".employer_candidae_searchbox_container #candidate_search_member_id" ).val( ui.item.value );

		return false;
	    }
	})
    }
}

function change_image_upload_container_on_top(){
    var count = jQuery( "#theme-my-login #userphoto" ).length;
    var count2 = jQuery( "#theme-my-login #member_contact_number" ).length;
    if( count > 0 && count2 > 0 ){
	
	var p_html = jQuery( "#theme-my-login #userphoto td" ).html();
	jQuery("#user_login").parents("tr").before( "<tr><th>Upload Company Logo</th><td>"+p_html+"</td></tr>" );
	jQuery( "#theme-my-login #userphoto" ).remove();
	
	jQuery("#userphoto_image_file_control").parents("tr").before("<tr><th><p class='text-info' >Particular</p></th></tr>");
	jQuery( "#nickname" ).parents("tr").remove();
	
    }else if( count > 0 ){
	var p_html = jQuery( "#theme-my-login #userphoto td" ).html();
	jQuery("#email").parents("tr").after( "<tr><th>Upload Profile Picture</th><td>"+p_html+"</td></tr>" );
	jQuery( "#theme-my-login #userphoto" ).remove();
	
	jQuery("#theme-my-login #user_login").parents("tr").before( jQuery("#theme-my-login input[name='hide_account_radio']").parents("tr") );
	
    }
}

function post_job_listing_repositioning(){
    if( jQuery("#submit-job-form").length > 0 ){
	
	var logo_html = jQuery("#submit-job-form .fieldset-company_logo").clone();
	jQuery("#submit-job-form .fieldset-company_logo").remove();
	//console.log( logo_html );
	jQuery("#submit-job-form .fieldset-job_title").before( logo_html );
    }
}

function post_job_listing_repositioning_select_tags(){
    
    var sequenced_option = [
	[ "north", ''],
	[ "south", 'north'],
	[ "east", 'south'],
	[ "west", 'east'],
	[ "central", 'west'],
	[ "senoko", 'central'],
	[ "sentosa", 'senoko'],
	[ "jurong-island", 'sentosa']
    ];
    sequenced_option.forEach(function(entry) {
	reposition_select_options( entry[0], entry[1], "job_region");
    });
    
    var sequenced_option2 = [
	
	[ "full-time", ''],
	[ "part-time", 'full-time'],
	[ "temporary", 'part-time'],
	[ "internship", 'temporary'],
	[ "freelance", 'internship']
	
    ];
    sequenced_option2.forEach(function(entry) {
	reposition_select_options( entry[0], entry[1], "job_type" );
    });
}

/**
 * This function will reposition specified option to specified posititon
 * @param name - this is the option name to reposition
 * @param cselector - option tag after which specifiled option will be placed
 * @return void
 */
function reposition_select_options( name, cselector, selectid ){
    var test = jQuery("#submit-job-form #"+selectid+" option[value='"+name+"']").clone();
    jQuery("#submit-job-form #"+selectid+" option[value='"+name+"']").remove();
    jQuery("#submit-job-form #"+selectid+" option[value='"+cselector+"']").after( test );
}

function cc_delete_appointment(){
    
    var doing_ajax = false;
    jQuery(".delete_appointment_link").click(function(){
	
	if( doing_ajax ){
	    alert("Please wait your request is getting processed...");
	    return false;
	}
	
	if( !confirm( "Are you sure you want to delete this appointment??" ) ){
	    return false;
	}
	
	var curr_obj = jQuery(this);
	var aid = curr_obj.attr("id");
	
	var data = {
	    action: 'cc_delete_appoinment',
	    aid: aid
	}
	doing_ajax = true;
	jQuery.ajax({
	    type: 'POST',
	    url: _ecs.ajax_url,
	    data: data,
	    success:function( response ){
		doing_ajax = false;
		curr_obj.parents("tr").remove();
	    },
	    error:function( response ){
		doing_ajax = false;
	    }
	});
	
	return false;
    });
    
}

jQuery(window).load(function(){
    jQuery(".woocommerce-tabs #tab-description").show();
});