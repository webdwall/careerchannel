jQuery(window).load(function() {
    
    if( !_reg.data ){
	return false;
    }
    
    jQuery("#dialog").dialog({ modal: true, draggable: false, title: "Complete Registration Details", height: 270, width: 400  });

    handle_user_registeration_details();

});

function handle_user_registeration_details(){
    var doing_ajax = false;
    jQuery("#dialog #reg_form_save_btn").click(function(){
	
	if( doing_ajax ){
	    alert("please wait.. your request is processing..");
	    return false;
	}
	
	jQuery("#dialog .reg_form_warning_msge").remove();
	var selected_val = jQuery("#dialog #user_type").val();
	if( selected_val == null || selected_val.trim() == "" ){
	    jQuery("#dialog #user_type").after("<p class='reg_form_warning_msge text-warning' >Field is required!!</p>");
	    return false;
	}
	var data = {
	    action:"save_reg_form_user_details",
	    val:selected_val
	};
	
	doing_ajax = true;
	jQuery("#dialog #reg_form_save_btn").after("<p class='reg_form_info_msge text-warning' >processing request...</p>");
	jQuery.ajax({
	    type: 'POST',
	    url: _reg.ajaxurl,
	    data: data,
	    success:function( response ){
		
		doing_ajax = false;
		jQuery("#dialog .reg_form_info_msge").remove();
		alert( "setting saved sucessfully..." );
		jQuery("#dialog").dialog("close");
	    },
	    error:function( response ){
		doing_ajax = false;
		alert( "some problem occured.. Please try again." );
		jQuery("#dialog").dialog("close");
	    }
	});
	
    });
    
}