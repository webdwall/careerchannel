jQuery(document).ready(function() {
    /* reorder the fields in registration form */
    cc_registration_reorder_fields();
    cc_profile_reorder_fields();
    
    /* show or hide some content of the registration form according to the user type selected */
    member_type_selection_setting();
    
    /* validating fields in registration form */
    validate_reg_form_required_fields();
    
    specializtion_checkbox_customization();

});

/*
 * Changes the order of dislay of some fields in registraion form
 * to group them together logically.
 * 
 * @author: emicc
 */
function cc_registration_reorder_fields(){
    //membership type should be the first
    jQuery('form#registerform').prepend( jQuery( '#member_role_type' ) );
    jQuery( '.job_street_container' ).before( jQuery('#pass1').parent() );
    jQuery( '.job_street_container' ).before( jQuery('#pass2').parent() );
    jQuery( '.job_street_container' ).before( jQuery('.registeration-captcha-field') );
    
}

function cc_profile_reorder_fields(){
    /* Particular, Company Information , credentials */
    
    jQuery("#emp_company_name").parents("tr").before("<tr><th><p class='text-info' >Company Information</p></th></tr>");
    jQuery("#pass1").parents("tr").before("<tr><th><p class='text-info' >Credentials</p></th></tr>");
    
    
    jQuery("#expected_salary").parents("tr").before("<tr><th><p class='text-info' >Career Information</p></th></tr>");
    jQuery("#theme-my-login input[name='hide_account_radio']").parents("tr").before("<tr><th><p class='text-info' >Particular</p></th></tr>");
    
    /*Hide employer extra fields*/
    var count = jQuery( "#theme-my-login #userphoto_image_file_control" ).length;
    var count2 = jQuery( "#theme-my-login #member_contact_number" ).length;
    if( count2 > 0 ){
	jQuery("#email").parents("tr").hide();
	jQuery("#description").parents("tr").hide();
	jQuery("#email").parents("table").siblings("h3").hide();
	
	jQuery("#emp_company_registration_number").parents("tr").after( jQuery("#url").parents("tr") );
	//jQuery("#url").parents("table").remove();
    }else if( count > 0 ){
	jQuery("#about_yourself").parents("tr").after( jQuery("#userphoto_image_file").parents("tr") );
	jQuery("#about_yourself").parents("tr").after( jQuery("#email").parents("tr") );
	
	jQuery("#url").parents("table").siblings("h3").hide();
	jQuery("#url").parents("table").hide();
	
	jQuery("#description").parents("tr").remove();
	
	jQuery("#pass1").parents("tr").after( jQuery("#jobstreet").parents("tr") );
	jQuery("#nickname").parents("tr").remove();
	
    }
    
}

function member_type_selection_setting(){
    
    /* This condition is to check if 'ragister as drop down' exist if exist that means we are on the registeration page */
    if ( jQuery( "#theme-my-login #user_type" ).length > 0 ){
	
	jQuery("#registerform #user_login").parent().before("<h3 class='register_form_divider first_divider' >Personal Details</h3>");
	jQuery("#registerform .user_type_candidate_fields").before("<h3 class='register_form_divider second_divider' >What Kind Of Jobs Are you Looking For?</h3>");
	
	jQuery("#registerform .user_type_employer_fields").hide();
	jQuery("#user_description").before( jQuery('#webcam_check_container') );
	
	jQuery( "#theme-my-login #user_type" ).change(function(){
	    var selected_option = jQuery( this ).val();
	    if( selected_option == 'candidate' ){
		
		jQuery("#registerform .user_type_candidate_fields").siblings(".second_divider").html("What Kind Of Jobs Are you Looking For?");
		//jQuery( "#theme-my-login .user_type_candidate_fields" ).stop().slideDown(500);
		jQuery( "#theme-my-login .candidate_field_container" ).stop().slideDown(500);
		
		jQuery("#registerform .user_type_employer_fields").stop().slideUp(500);
		//jQuery(".motor_license_container").before( jQuery('#webcam_check_container') );
		jQuery("#user_description").before( jQuery('#webcam_check_container') );
		
		
	    }else{
		
		jQuery("#registerform .user_type_candidate_fields").siblings(".second_divider").html("Company Detail");
		jQuery( "#theme-my-login .candidate_field_container" ).stop().slideUp(500);
		
		jQuery("#registerform .user_type_employer_fields").stop().slideDown(500);
		
		jQuery("#emp_company_registration_number").after( jQuery('#webcam_check_container') );
		
	    }
	});
	
	if ( jQuery( "#theme-my-login #user_type" ).val() != "" ){
	    jQuery( "#theme-my-login #user_type" ).trigger("change");
	}
    }
}

function validate_reg_form_required_fields(){
    
    jQuery("#theme-my-login #wp-submit").click( function(){
	
	jQuery("#theme-my-login .register_form_warning").remove();
	
	var is_input_valid = true;
	var user_type = jQuery("#theme-my-login #user_type").val();
	var user_login = jQuery("#theme-my-login #user_login").val();
	var user_email = jQuery("#theme-my-login #user_email").val();
	var user_pass = jQuery("#theme-my-login #pass1").val();
	var agree = jQuery("#theme-my-login .register_form_bottom_message #agreement_optin_checkbox:checked").length;
	
	
	if( user_type.trim() === "" || user_type === null ){
	    jQuery("#theme-my-login #user_type").after("<p class='register_form_warning text-error'  >Fields is required</p>");
	    is_input_valid = false;
	}
	
	
	
	if( user_login.trim() === "" || user_login === null ){
	    jQuery("#theme-my-login #user_login").after("<p class='register_form_warning text-error' >Fields is required</p>");
	    is_input_valid = false;
	}else{
	    
	    var patt   = /[^0-9a-z]/i;
	    if( patt.test(user_login) ){
		jQuery("#theme-my-login #user_login").after("<p class='register_form_warning text-error'  >Username is not in proper format.. Please avoid spaces and special character.</p>");
		is_input_valid = false;
	    }
	    else{
		is_input_valid = true;
	    }
	}
	
	if( user_email.trim() === "" || user_email === null ){
	    jQuery("#theme-my-login #user_email").after("<p class='register_form_warning text-error' >Fields is required</p>");
	    is_input_valid = false;
	}
	
	if( user_pass.trim() === "" || user_pass === null ){
	    jQuery("#theme-my-login #pass1").after("<p class='register_form_warning text-error' >Fields is required</p>");
	    is_input_valid = false;
	}
	
	if( agree <= 0 ){
	    jQuery("#theme-my-login .register_form_bottom_message input").parents(".register_form_bottom_message").after("<p class='register_form_warning text-error' >Fields is required</p>");
	    is_input_valid = false;
	}
	
	
	if( !is_input_valid ){
	    jQuery(this).after("<p class='register_form_warning text-error' >please correct the errors on this form...</p>");
	    return  false;
	}
    });
    
}

function specializtion_checkbox_customization(){
    
    jQuery(".user_type_candidate_fields .clear_spec_checkbox").click(function(){
	jQuery(".specialization_field_container input[type='checkbox']").each(function(){
	    jQuery(this).prop("checked",false);
	    jQuery(this).parent().attr("class","");
	});
	jQuery(".chk_count_warning").remove();
	return false;
    });
    
    jQuery(".specialization_field_container input[type='checkbox']").click(function(){
	var chk_count = jQuery(".specialization_field_container input[type='checkbox']:checked").length;
	if( chk_count > 5 ){
	    jQuery(".chk_count_warning").remove();
	    jQuery(".specialization_field_container").before("<span class='chk_count_warning' >Please select a maximum of 5 specializations only.</span>");
	    return false;
	}else{
	    jQuery(".chk_count_warning").remove();
	}
	jQuery(this).parent().toggleClass("active");
    });
    
    
}