<?php

/*###################################################################
 * Candidate Account Setting module
###################################################################*/

add_action("pmsc_admin_general_setting_hook","pmsc_candidate_accont_setting");
function pmsc_candidate_accont_setting(){
    $message = get_option("c_account_hide_message");
    ?>
    <li class="devider" >
	<p>
	    <label>Hide Candidate Profile Content: </label>
	    <textarea class="account_setting_messagte" name="account_setting_message" cols="50" rows="5" ><?php echo $message; ?></textarea>
	    <br/>
	    <small><i>content will be shown on the candidate account setting where candidate can hide their account all over.</i></small>
	</p>
    </li>
    <?php
}

add_action("pmsc_admin_setting_save_button_hook","pmsc_candidate_accont_setting_handler");
function pmsc_candidate_accont_setting_handler(){
    if( isset( $_POST["save_admin_general_setting_button"] ) ){
	if( isset( $_POST["account_setting_message"] ) && $_POST["account_setting_message"] != "" ){
	    update_option( "c_account_hide_message", trim( $_POST["account_setting_message"] ) );
	}
    }
}


//add_action( 'cc_dashboard_nav',						'candidate_account_setting_nav',17 );
//add_action( 'cc_dashboard_title',					'candidate_account_setting_title' );
//add_action( 'cc_dashboard_content',					'candidate_account_setting_content' );

function candidate_account_setting_nav( $user_type ){
    if( $user_type != 'employer' ){
	    $active_class = '';
	    if( cc_user_dashboard_is_current_component('account-settings') ){
		    $active_class = 'active';
	    }
	    
	    echo "<li class='$active_class'><a href='". cc_user_dashboard_url( $user_type, 'account-settings') ."'>Account Settings</a></li>";
    }
}

function candidate_account_setting_title( $user_type ){
    if( $user_type != 'employer' && cc_user_dashboard_is_current_component('account-settings') ){
	//must be all small letters, capitalization will be done with css
	echo cc_user_dashboard_current_component();
    }
}


function candidate_account_setting_content( $user_type ){
    if( $user_type != 'employer' && cc_user_dashboard_is_current_component('account-settings') ){
	
	$user_id = get_current_user_id();
	if( isset( $_POST["account_submit_btn"] ) ){
	    if( isset( $_POST["hide_account_radio"] ) && $_POST["hide_account_radio"] != "" ){
		if( $_POST["hide_account_radio"] == "yes" ){
		    update_user_meta($user_id, "cc_hide_profile", $_POST["hide_account_radio"] );
		}else{
		    delete_user_meta($user_id, "cc_hide_profile");
		}
	    }
	}
	$selected_option = get_user_meta($user_id, "cc_hide_profile", true);
	$message = get_option("c_account_hide_message");
	?>
	<form method="post" action="" >
	    <div class="candidate_account_setting_container" >
		<label>Hide Account ?</label><br/>
		<label><input type="radio" name="hide_account_radio" value="yes" <?php if( $selected_option == "yes" ){ echo 'checked'; } ?> /> Yes</label><br/>
		<label><input type="radio" name="hide_account_radio" value="no" <?php if( $selected_option == "no" || $selected_option == "" ){ echo 'checked'; } ?> /> No</label><br/>
		<small><i><?php echo $message; ?></i></small><br/><br/>
		<input type="submit" class="account_submit_btn button button-small" name="account_submit_btn" value="Save" />
	    </div>
	</form>
	<?php
    }
}