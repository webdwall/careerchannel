<?php
/*
Plugin name: EMI Custom Core Functions
Description: E - media identity - custom core funstions
version: 0.2
Author: E Media Identity
Author URI: http://emediaidentity.com/
Network: true
Text Domain: emi-core
*/

/* the time for transient cache */
define( 'CC_TRANSIENT_TIME', 5 * DAY_IN_SECONDS );
$include_base_path = plugin_dir_path( __FILE__ );
include( $include_base_path . 'functions.php' );
include( $include_base_path . 'apply-to-jobs.php' );
include( $include_base_path . 'apply-to-jobs-admin.php' );
include( $include_base_path . 'member-custom-registration.php' );
include( $include_base_path . 'profile-management.php' );
include( $include_base_path . "membership.php" );
include( $include_base_path . "shop-filters.php" );
include( $include_base_path . "appointments.php" );
include( $include_base_path . "membership-admin-settings.php" );
include( $include_base_path . "chat-configuration.php" );
include( $include_base_path . "tag-candidates.php" );
include( $include_base_path . "candidate-account-setting.php" );

add_action('init','emi_custom_init_core_functions');
function emi_custom_init_core_functions(){
	$instance = EMI_Custom_Common_function_Core::get_instance();	
}

class EMI_Custom_Common_function_Core{

	// static function only use static function hence this var is used in  get_instance() method.
	private static $instance;
	/* this is just a random hardcoded value */
	private $noncekey = '41HF2s';
	
	private function __construct() {
	    
		add_action( 'wp_enqueue_scripts',  array( $this, 'emi_enqueue_cssjs' ) );

		add_action("wp_ajax_nopriv_emi_custom_login", array( $this, "emi_custom_login_ajax" ) );
		
		add_action("wp_ajax_save_reg_form_user_details", array( $this,"save_reg_form_user_details_ajax") );
		
		add_action("wp_ajax_extend_current_job_listing", array( $this,"extend_current_job_listing_ajax") );
		
		add_action('wp_ajax_nopriv_emi_work_location_states', array( $this, 'emi_work_location_states' ) );
		
		add_action('wp_ajax_emi_work_location_states', array( $this, 'emi_work_location_states' ) );
		
		add_action('wp_ajax_tag_candidate_to_list', array( $this, 'tag_candidate_to_list_ajax' ) );
		
		add_action('wp_ajax_cc_delete_appoinment', array( $this, 'cc_delete_appoinment_ajax' ) );
	}

    /*
     * this function will check if the object is already present or not
     * this function is static therefore no need to create the object of it.
    */
    public static function get_instance(){
        if(!isset(self::$instance)){
            self::$instance=new self();
        }
        return self::$instance;
    }

    function emi_enqueue_cssjs(){
    	

	wp_enqueue_script(
    		"emi-common-script",
    		path_join( WP_PLUGIN_URL, basename( dirname( __FILE__ ) )."/js/custom-core-script.js" ),
    		array('jquery')
	);
	
	wp_enqueue_script(
    		"emi-register-form-script",
    		path_join( WP_PLUGIN_URL, basename( dirname( __FILE__ ) )."/js/registration-form-validation-script.js" ),
    		array('jquery')
	);
	
	wp_enqueue_script(
    		"full-reg-popup-script",
    		path_join( WP_PLUGIN_URL, basename( dirname( __FILE__ ) )."/js/full-reg-popup-script.js" ),
    		array('jquery')
	);
	
	
	//@todo: add nonce for ajax request
	$data = array( 
	    'ajax_url'	    =>  admin_url('/admin-ajax.php'),
	    'nonce'	    => wp_create_nonce( $this->noncekey."extend_job_listing" )
	);
	wp_localize_script( "emi-common-script", '_ecs', $data);
	
	//@todo: add nonce for ajax request
	$data_reg = array( 'data' => check_current_user_type_configuration(), 'ajaxurl'	=> admin_url('/admin-ajax.php') );
	wp_localize_script( "full-reg-popup-script", '_reg', $data_reg);
	
	/* jQuery-ui css and script which will be used in Displaying dialog box and for calender  */
	wp_enqueue_script(
	    "jquery-style",
	    path_join( WP_PLUGIN_URL, basename( dirname( __FILE__ ) )."/jqueryui/jquery-ui-1.10.3.custom.min.js" ),
	    array('jquery')
	);
	wp_enqueue_style(
	    "jquery-script",
	    path_join( WP_PLUGIN_URL, basename( dirname( __FILE__ ) )."/jqueryui/jquery-ui-1.10.3.custom.min.css" )
	);
	wp_enqueue_script(
	    "pttime-select-script",
	    path_join( WP_PLUGIN_URL, basename( dirname( __FILE__ ) )."/jqueryui/jquery.ptTimeSelect.js" ),
	    array('jquery')
	);
	wp_enqueue_style(
	    "pttime-select-style",
	    path_join( WP_PLUGIN_URL, basename( dirname( __FILE__ ) )."/jqueryui/jquery.ptTimeSelect.css" )
	);
	
    }

    function emi_custom_login_ajax(){
    	
    	$user_login = $_POST["name"];
    	$user_pass = $_POST["pswd"];

		$creds = array();
		$creds['user_login'] = $user_login;
		$creds['user_password'] = $user_pass;
		$creds['remember'] = true;
		$user = wp_signon( $creds, false );
		if ( is_wp_error($user) ){
			die( json_encode( $user->get_error_message() ) );
		}else{
			die("ok");
		}
    }
    
    function save_reg_form_user_details_ajax(){
	
	$selected_value = $_POST["val"];
	$user_id = get_current_user_id();
	
	update_user_meta( $user_id, 'registration_type', "fb" );
	if( $selected_value == "employer" ){
	    $wp_user_object = new WP_User( $user_id );
	    $wp_user_object->set_role('employer');
	}
	die("ok");
    }
    
    function extend_current_job_listing_ajax(){
		check_admin_referer( $this->noncekey."extend_job_listing" );

		$job_id = $_POST["job_id"];
		$order_id = $_POST["order_id"];
		$user_id = get_current_user_id();

		if( $order_id===false ){
			die( json_encode( array('result'=>'notok') ) );
		}
		
		/* check if current user having job listing remaining  */
		$free_quota_remaining = get_free_joblisting_allowed_for_current_user($user_id);
		
		$premium_quota_details = get_premium_joblisting_allowed_for_current_user($user_id);
		$premium_quota_remaining = $premium_quota_details["no_of_job_listing"];
		
		//die( 'premium quota remaining is ' . $premium_quota_remaining . ' orderid is ' . $order_id );
		
		if( $order_id > 0 && (int)$premium_quota_remaining<1 ){
			//employer is trying to use one of his paid listing quota, lets check if any available
			die( json_encode( array('result'=>'notok', 'message'=>'You can not use premium listing quota, as your quota has exhausted!') ) );
		}
		
		if( ($free_quota_remaining + $premium_quota_remaining) <= 0  ){
			$return_arr = array(
				"result"	=> "notok",
				"message"	=> "You can not use reposting feautre, as your quota has exhausted! Consider upgrading your membership."
			);
			die( json_encode( $return_arr ) );
		}

		global $wpdb;
		$table_name = $wpdb->prefix."job_listing_quota_details";
		$columns = array(
			"job_id"    => $job_id,
			"curr_date" => current_time("mysql"),
			"user_id"   => $user_id,
			"order_id"  => $order_id,
			"type"	=> "extension"
		);
		
		$datatypes = array(
			"%d",
			"%s",
			"%d",
			"%d",
			"%s"
		);
		$result = $wpdb->insert(
			$table_name,
			$columns,
			$datatypes
		);
	
		if( $result > 0 ){
			/* after successfully adding entry in the quota table will extend the expiry date by 30 days */
			$job_expiry_date = get_post_meta( $job_id , "_job_expires", true);
			$new_expiry_date = date( "Y-m-d", strtotime( $job_expiry_date. ' + 30 days') );
			update_post_meta( $job_id , "_job_expires", $new_expiry_date );

			ob_start();
			generate_package_selection_drop_down();
			$drop_down_html = ob_get_contents();
			ob_end_clean();

			$return_arr = array(
			"result"	    => "ok",
			"drop_down_html"    => $drop_down_html
			);

			die( json_encode( $return_arr ) );
		} else {
			$return_arr = array(
				"result"	    => "ok"
			);
			die( json_encode( $return_arr ) );
		}
	
    }
    
    function emi_work_location_states() {
    $country_id = $_POST['country_id'];
    if ($country_id) {

	$selectedCountryIndex = (int)$country_id;
	
	$countries_states_details = get_countires_states_list();
	$states_arr = $countries_states_details["state_arr"];
	$states = $states_arr[$selectedCountryIndex];
	$state_arr = array();
	$state_arr = split("\|", $states  );
	?>
	
	<fieldset>
	    
	    <div class="field has-select">
		<div class="select">
		    <select name="preferred_location1" >
			<option>Select First Preferred Location</option>
			<?php foreach ( $state_arr as $state ) { ?>
			    <option value="<?php echo $state; ?>"><?php echo $state; ?></option>
			<?php } ?>
		    </select>
		</div>
	    </div>
	    <span style="float:right;cursor:pointer;" id="first_location" onclick="add_first_location();">Add Another Location</span>	
	</fieldset>

	<fieldset>
	    <div class="field has-select"  id="second_location_dropdown" style="display:none;">
		<div class="select">
		    <select name="preferred_location2" >
			<option>Select Second Preferred Location(Optional)</option>
			<?php foreach ( $state_arr as $state ) { ?>
			    <option value="<?php echo $state; ?>"><?php echo $state; ?></option>
			<?php } ?>
		    </select>
		</div>
	    </div>
	    <span style="float:right;display:none;cursor:pointer;" id="second_location" onclick="add_second_location();">Add Another Location</span>
	</fieldset>

	<fieldset>
	    <div class="field has-select" id="third_location_dropdown" style="display:none;">
		<div class="select">
		    <select name="preferred_location3" >
			<option>Select Third Preferred Location(Optional)</option>
			<?php foreach ( $state_arr as $state ) { ?>
			    <option value="<?php echo $state; ?>"><?php echo $state; ?></option>
			<?php } ?>
		    </select>
		</div>
	    </div>
	</fieldset>
	<?php 
    }
    die();
}


    function tag_candidate_to_list_ajax(){
	$emp_id = get_current_user_id();
	$can_id = $_POST["can_id"];
	$job_id = $_POST["job_id"];
	$tag = $_POST["tag"];
	if( empty( $emp_id ) || empty( $job_id ) || empty( $tag ) ){
	    die("notok");
	}
	$tagges_candidates = get_user_meta( $emp_id , "tagged_candidates7", true);
	$temp_tag_det = array(
	    "tag"   => $tag,
	    "job"   => $job_id
	);
	$flag = false;
	if( empty( $tagges_candidates ) ){
	    $tagges_candidates = array();
	}else{
	    foreach ( $tagges_candidates as $cand_id => &$detail ){
		if( $can_id == $cand_id ){
		    echo 'Level 1';

		    foreach ( $detail as &$det ){
			if( $det[ "job" ] == $job_id ){
			    $det[ "tag" ] = $tag;
			    echo 'Level 2';
			    $flag = true;
			}
		    }
		}
	    }
	}
	if( !$flag ){
	    $tagges_candidates[$can_id][] = $temp_tag_det;
	}
	update_user_meta( $emp_id , "tagged_candidates7", $tagges_candidates );
	die("ok");
    }
    
    function cc_delete_appoinment_ajax(){
	$id = $_POST["aid"];
	global $wpdb;

	$delete_query = "delete from ".$wpdb->prefix."employer_appointment_details where id = $id";

	$results = $wpdb->query($delete_query);
	
	die("ok");
    }
    
}/* EMI_Common_function_Core */

register_activation_hook(__FILE__, 'emi_register_custom_role' );
function emi_register_custom_role(){
    /* Adding employer role here on plugin activation */
    $capabilities =  array(
		'read'         => true,
		'edit_posts'   => false,
		'delete_posts' => false,
    );
    $result = add_role('employer' ,'Employer', $capabilities );
    
    /* Job listing quota details table  */
    global $wpdb;
    $table_name = $wpdb->prefix."job_listing_quota_details";
    $table_name2 = $wpdb->prefix."employer_appointment_details";
    
    $table_sql1 = "CREATE TABLE $table_name (
	    id bigint(20) NOT NULL AUTO_INCREMENT,
	    job_id bigint(20) NOT NULL,
	    curr_date datetime NOT NULL,
	    user_id bigint(20) NOT NULL,
	    order_id bigint(20) NOT NULL,
	    type varchar(100) NOT NULL,
	    package varchar(200) NOT NULL,
	    UNIQUE KEY id (id)
    );";

    $table_sql2 = "CREATE TABLE $table_name2 (
	 id bigint(20) NOT NULL AUTO_INCREMENT,
	 employer_id bigint(20) NOT NULL,
	 candidate_id bigint(20) NOT NULL,
	 job_id bigint(20) NULL,
	 app_date datetime NOT NULL,
	 app_time varchar(20) NOT NULL,
	 self_message text NULL,
	 candidate_message text NULL,
	 UNIQUE KEY id (id)
    );";
    
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $table_sql1 );
    
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $table_sql2 );
    
}

/**
 * SETTING CRONJOB
 *  Expire jobs posted by free members if they've received more than 10 applications  
 */
register_activation_hook( __FILE__, 'cc_expire_free_member_jobs_activate_crons' );
register_deactivation_hook( __FILE__, 'cc_expire_free_member_jobs_deactivate_crons');

function cc_expire_free_member_jobs_activate_crons(){
    wp_schedule_event( time(),'hourly','action_cc_expire_free_member_jobs' );
}

function cc_expire_free_member_jobs_deactivate_crons(){
    wp_clear_scheduled_hook('action_cc_expire_free_member_jobs');
}

/* this is the filter used to give custom duration in cron */
/* add a new time interval for cron jobs  - for debugging */
//add_filter( 'cron_schedules', 'cron_add_5_min' );
function cron_add_5_min( $schedules ) {
    // Adds once weekly to the existing schedules.
    $schedules['fifteenminutes'] = array(
	    'interval' => 30,
	    'display' => __( 'fifteen minutes' )
    );
    return $schedules;
}

add_action('action_cc_expire_free_member_jobs','cc_crons_handler');
?>