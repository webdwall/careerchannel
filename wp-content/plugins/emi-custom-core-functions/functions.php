<?php 
if( !function_exists('emi_get_option') ):
/**
 * A wrapper for the get_option() method.
 * All the requests to get_option call through this method are cached for later retrieval.
 * 
 * @param string $option_name name of the option
 * @param mixed $default_value default false
 * @param bool $bypass_cache whether to refresh cache and retrieve again from database
 * @return mixed option value
 */
function emi_get_option( $option_name, $default_value=false, $bypass_cache=false ){
	$option_value = wp_cache_get( 'emi' . $option_name  );
	if( $option_value ===false || $bypass_cache ){
		$option_value = get_option($option_name, $default_value);
		wp_cache_set( 'emi'.$option_name, $option_value);
	}
	return $option_value;
}
endif;

if( !function_exists('emi_update_option') ):
/**
 * A wrapper for the update_option() method.
 * A call to update_option is made and cache is updated.
 * 
 * @param string $option_name name of the option
 * @param mixed $option_value 
 * 
 * @return void
 */
function emi_update_option( $option_name, $option_value ){
	update_option($option_name, $option_value);
	wp_cache_set( 'emi'.$option_name, $option_value);
}
endif;

/* Save job listing motor_license and job_status in post meta */
add_action( 'save_post', 'save_and_update_job_listing_details' );
function save_and_update_job_listing_details( $post_id ){
    if( isset($_POST["motor_license"]) && $_POST["motor_license"] != "" ){
		update_post_meta($post_id, "_motor_license", $_POST["motor_license"] );
    }
    if( isset($_POST["job_status"]) && $_POST["job_status"] != "" ){
		update_post_meta($post_id, "_job_status", $_POST["job_status"] );
    }
}

function get_job_listing_status( $job_id ){
    $value = get_post_meta($job_id, "_job_status", true);
    $value = ( $value != "" ) ? $value : "";
    return $value;
}

function get_candidate_tag_dropdown( $candidate_id, $job_id ){
    
    if( empty( $candidate_id ) || empty( $job_id ) ){
	return "<i>INVALID MESSAGE</i>";
    }
    
    $html  = "";
    $html .= "<ul class='dropdown-menu tag_candidates'>";

    $html .= "<li><label><input type='radio' can_id = '$candidate_id' job_id = '$job_id' name='tag_candidate_radio_btn' class='tag_candidate_radio_btn' value='None'/> None </label></li>";
    $html .= "<li><label><input type='radio' can_id = '$candidate_id' job_id = '$job_id' name='tag_candidate_radio_btn' class='tag_candidate_radio_btn' value='Pending'/> Pending </label></li>";
    $html .= "<li><label><input type='radio' can_id = '$candidate_id' job_id = '$job_id' name='tag_candidate_radio_btn' class='tag_candidate_radio_btn' value='Rejected'/> Rejected </label></li>";
    $html .= "<li><label><input type='radio' can_id = '$candidate_id' job_id = '$job_id' name='tag_candidate_radio_btn' class='tag_candidate_radio_btn' value='Shortlisted'/> Shortlisted </label></li>";
    $html .= "<li><label><input type='radio' can_id = '$candidate_id' job_id = '$job_id' name='tag_candidate_radio_btn' class='tag_candidate_radio_btn' value='Screened'/> Screened </label></li>";
    $html .= "<li><label><input type='radio' can_id = '$candidate_id' job_id = '$job_id' name='tag_candidate_radio_btn' class='tag_candidate_radio_btn' value='Hired'/> Hired </label></li>";

    $html .= "</ul>";
    
    $button  =    '<button type="button" class="btn btn-default dropdown-toggle show-tooltip" data-toggle="dropdown" title="Tag Candidate" >';
    //$button .=        "<span class='glyphicon glyphicon-cog'></span>";
    $button .=        "<span class=''>Tag as</span>";
    $button .=    "</button>";

    return "<div class='btn-group'>".$html.$button."</div>";
}

function get_current_candidate_tag( $candidate_id, $job_id ){
    if( empty( $candidate_id ) || empty( $job_id ) ){
	return false;
    }
    $employer_id = get_current_user_id();
    $candidate_tags = get_user_meta( $employer_id , "tagged_candidates7", true );
    if( $candidate_tags && !empty( $candidate_tags ) ){
	$tag_detail = $candidate_tags[$candidate_id];
	if( $tag_detail && !empty($tag_detail) ){
	    
	    $replace_count = 100;
	    $tag_name = "";
	    foreach ( $tag_detail as $tags_details ):
		
		if( $tags_details["job"] == $job_id ){
		    $tag_name = $tags_details["tag"];
		    break;
		}
	    endforeach;
	    
	    $tag_name_attr = str_replace(" ", "", $tag_name, $replace_count);
	    if( $tag_name != "" ):
		?>
		<span class="label label-primary label-<?php echo esc_attr( $tag_name_attr );?>" ><?php echo $tag_name; ?></span>
		<?php
	    endif;
	}
    }
}
/**
 * this function will change default worpress mail name
 * @param type $email - default wordpress email
 * @return $email new email id
 */
function cc_mail_from_email( $email ) {
    return 'Secure@CareerChannel.sg';
}
add_filter( 'wp_mail_from', 'cc_mail_from_email' );

function cc_mail_from_name( $name ) {
    return 'CareerChannel';
}
add_filter( 'wp_mail_from_name', 'cc_mail_from_name' );

function filter_user_pass_registered( $pass ){
    $user_pass = $_POST{"pass1"};
    return $user_pass;
}
add_filter( 'tml_user_registration_pass', 'filter_user_pass_registered' );

function show_custom_specialization_select_tag( $type = false, $key = false, $value = false ){
    
    /* First get all parent key value pair in one array */
    /* get all the childs key value pair in second array */
    $args = array(
	'type'	    => 'job_listing',
	'taxonomy'	    => 'job_listing_category',
	'hide_empty'    => false
    );
    $categories = get_categories( $args );

    $parent_categories = array();
    $child_categories = array();

    foreach ( $categories as $category ){
	if( $category->parent == 0 ){
	    $parent_categories[] = $category;
	}else{
	    $child_categories[] = $category;
	}
    }
    ?>
    <select  name="<?php if( $key != "" ){ echo $key; }else{ echo "search_categories"; } ?>" id="<?php if( $key != "" ){ echo $key; }else{ echo "search_categories"; } ?>" class="postform" >
	<option value="" ><?php if( $key == "specialization" ){ echo "All"; }else{ echo "--select industries--"; } ?></option>
	<?php
	foreach ( $parent_categories as $parent_category ){

	    $temp_child_category = array();
	    $curr_term_id = $parent_category->term_id;
	    $curr_term_key_arr = array();
	    foreach ( $child_categories as $child_category ){
		if( $child_category->parent == $curr_term_id ){

		    $curr_term_key_arr[] = $child_category->term_id;

		    $cat["id"] = ( $type != "" ) ? $child_category->slug : $child_category->term_id;
		    $cat["name"] = $child_category->name;
		    $temp_child_category[] = $cat;
		}
	    }
	    $curr_term_key_arr_val = implode(",", $curr_term_key_arr);

	    echo '<optgroup class="optgroup" label="'.$parent_category->name.'" >';
	    if( $type != "post_job" ):
		echo '<option value="'.$curr_term_key_arr_val.'" class="opt-indent"  > All '.$parent_category->name.'</option>';
	    endif;
	    foreach ( $temp_child_category as $child_cat ){
		$select_attr = $child_cat["id"] == $value ? "selected" : "";
		echo '<option value="'.$child_cat["id"].'" class="opt-indent" '.$select_attr.' > '.$child_cat['name'].'</option>';
	    }
	    echo '</optgroup>';

	}
	?>
    </select>
    <?php
}