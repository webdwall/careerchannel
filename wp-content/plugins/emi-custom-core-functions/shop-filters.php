<?php 
/**
 * remove billing and shipping address fileds from checkout page
 */
//add_filter( 'woocommerce_checkout_fields' , 'cc_remove_billing_shipping_checkout_fields' );
// Our hooked in function - $fields is passed via the filter!
function cc_remove_billing_shipping_checkout_fields( $fields ) {
	unset($fields['order']['order_comments']);
	//unset($fields['billing']);
	$fields['billing'] = array();
	//unset($fields['shipping']);
	$fields['shipping'] = array();

	return $fields;
}
/**
 * returns the id's of all add-on products
 * @return array array of ids
 */
function cc_get_addons_ids(){
	$emi_shop_packages_settings = emi_get_option( 'emi_jl_package_info' );
	
	return array( 
		'bronze'	=> isset( $emi_shop_packages_settings['bronze_addon'] ) ? $emi_shop_packages_settings['bronze_addon'] : false,
		'silver'	=> isset( $emi_shop_packages_settings['silver_addon'] ) ? $emi_shop_packages_settings['silver_addon'] : false,
		'gold'		=> isset( $emi_shop_packages_settings['gold_addon'] ) ? $emi_shop_packages_settings['gold_addon'] : false,
		'platinum'	=> isset( $emi_shop_packages_settings['platinum_addon'] ) ? $emi_shop_packages_settings['platinum_addon'] : false
	);
}

/**
 * Adds the argument to exclude add-ons from wp_query which shouldn't be accesible to logged in user.
 * E.g: if loggedin user has silver package active, he/she should be allowed to purchase
 * silver addon only.
 *  
 * @param array $args wp_query arguments
 * 
 * @return array $args modified arguments
 */
function add_relevant_addons_ids_query_param( $args ){
	$add_ons_ids = cc_get_addons_ids();
	/*
	 * array( package => '', quota_exceeded => true|false, days_remaining => '',purchase_date => )
	 */
	$logged_in_user_package_details = member_package_details();
	$active_package = $logged_in_user_package_details['package'];
	if( $active_package && $active_package!='free' ){
		//user has some package active, so display only related add-on 
		$args['post__in'] = array( $add_ons_ids[$active_package] );
	}
	else{
		//user is a free user, so no add-on
		$posts_to_exclude = array();
		foreach ($add_ons_ids as $key => $value) {
			$posts_to_exclude[] = $value;
		}
		$args['post__not_in'] = $posts_to_exclude;
	}
	
	return $args;
}

//add_filter( 'emi_wcpl_products_query_args', 'cc_only_addons_or_only_packages' );
function cc_only_addons_or_only_packages( $args ) {
	/*
	 * 1. add-ons shouldn't be displayed anywhere except the add-ons page
	 * 2. on the add-ons page only add-ons should be displayed and not the package
	*/
	$queried_object = get_queried_object();
	$operator = 'NOT IN';
	if( $queried_object->taxonomy=='product_cat' && $queried_object->slug=='add-ons' ){
		$operator = 'IN';
		//only addons according to logged in user's package should be displayed
		$args = add_relevant_addons_ids_query_param( $args );
	}
	
	$args['tax_query'] = array(
		array(
			'taxonomy' => 'product_type',
			'field'    => 'slug',
			'terms'    => 'job_package'
		),
		array(
			'taxonomy' => 'product_cat',
			'field' => 'slug',
			'terms' => array( 'add-ons' ),
			'operator' => $operator
		)
	);
	
	return $args;
}

/**
 * Prints the title on the shop page.
 * Checks if it is a category archive and generates the title accordingly.
 * 
 * @return void
 */
function emi_wcpl_shop_title(){
	$queried_object = get_queried_object();
	if( $queried_object->taxonomy=='product_cat' ){
		echo $queried_object->name;
	}
	else{
		_e( 'Job Packages', 'jobify' );
	}
}

/**
 * Prints the description/information on the shop page.
 * Basically prints the description of the product category if its a product category archive.
 */
function emi_wcpl_shop_description(){
	$queried_object = get_queried_object();
	if( $queried_object->taxonomy=='product_cat' ){
		$term = get_term_by('slug', $queried_object->slug, 'product_cat');
		if( $term  ){
			echo "<p class='text-info'>";
				echo $term->description;
			echo "</p>";
		}
	}
}

add_action( 'pre_get_posts', 'remove_memebrships_from_shop' );
function remove_memebrships_from_shop( $query ){
	if ( ! $query->is_main_query() ) return;
	if ( ! $query->is_post_type_archive() ) return;
	
	if ( ! is_admin() && is_shop() ) {
	    $query->set( 'tax_query', array( array(
			    'taxonomy' => 'product_type',
			    'field'    => 'slug',
			    'terms'    => 'job_package',
			    'operator' => 'NOT IN'
	    ) ) );
	}
	remove_action( 'pre_get_posts', 'remove_memebrships_from_shop' );
}

add_filter('the_content', 'cc_membership_page_content');
function cc_membership_page_content($content) {
    //featured job
    if (is_page()) {
	$selected_package_value = get_option("emi_jl_package_info");
	$mem_options_page = ( isset( $selected_package_value['membership_page'] ) ? $selected_package_value['membership_page'] : 0 );
	if ($mem_options_page && is_page($mem_options_page)) {
	    $content .= '<div style="clear:both;"></div>';
	    ob_start();

	    cc_membership_page_content_html();

	    $content .= ob_get_contents();
	    ob_end_clean();
	}
    }
    return $content;
}

function cc_membership_page_content_html(){
    
    /* This code snippet is to restrict user to purchase multiple packages at a time */
    global $woocommerce;
    if( !empty( $woocommerce->cart->cart_contents ) ){
	foreach( $woocommerce->cart->cart_contents as $temp_data ){
	    if( $temp_data["data"]->product_type == "job_package" ){
		echo '<p class="text-info" > You already added one package in the cart.. Only one package is allowed at a time <a href="'.get_site_url().'/cart/" > Return </a></p>';
		return false;
	    }
	}
    }
    /* ############################################################################# */
    
    
    $user_id = get_current_user_id();
    
    $user_details = get_userdata($user_id);
    if( $user_details->roles[0] != "employer" ){
		echo '<p class="text-error" >You not allowed to access this page..</p>';
		return false;
    }
    
    $member_details = member_package_details( $user_id );
    $package_details = get_option( "emi_jl_package_info"  );
    $curr_package = ( $member_details["package"] != ''  ) ? $member_details["package"] : '';
    
	/* ++++++++++++++++++++++++
	 * first display all the packages.
	 * All the membership packages will be displayed, irrespective of what is the current membership of current user
	 */
    $package_args = array(
	    'post_type'  => 'product',
	    'limit'      => -1,
	    'tax_query'  => array(
		    array(
			    'taxonomy' => 'product_cat',
			    'field'    => 'slug',
			    'terms'    => 'memberships',
			    'operator' => 'IN'
		    )
	    )
    );
    
    $packages = new WP_Query( apply_filters( 'emi_wcpl_products_query_args', $package_args ) );

    if( $packages->have_posts() ):

	echo '<div class="job-packages pricing-table-widget-'.$packages->found_posts.' ">';
	while ( $packages->have_posts() ) : 

	    $packages->the_post(); 
	    $package = get_product( get_post()->ID );

	    ?>
	    <div class="pricing-table-widget woocommerce">
		<div class="pricing-table-widget-title">
		    <?php the_title(); ?>
		</div>

		<div class="pricing-table-widget-description">
		    <h2><?php echo $package->get_price_html(); ?></h2>

		    <!--<p>
			<span class="rcp_level_duration">
			<?php/*
			    printf( _n( '%d job', '%s jobs', $package->get_limit(), 'jobify' ) . ' ', $package->get_limit() );

			    printf( _n( 'for %s day', 'for %s days', $package->get_duration(), 'jobify' ), $package->get_duration() );
			*/?>
			</span>
		    </p>-->
		    <p>
			<span class="rcp_level_duration" >
			    
			    <?php echo get_the_content(); ?>
			    
			</span>
		    </p>

		    <p>
			<?php
			    //$link = $package->add_to_cart_url();
			    $link = get_site_url()."/shop?add-to-cart=".get_the_ID();
			    $label = apply_filters( 'add_to_cart_text', __( 'Add to cart', 'jobify' ) );
			?>
			<a href="<?php echo esc_url( $link ); ?>" class="button-secondary"><?php echo $label; ?></a>
		    </p>
		</div>
	    </div>
	    <?php

	endwhile;
	echo '</div>';
    endif;

    wp_reset_postdata();
	
	/* ================================== */
	/* then display only the relevant add-on */
	if( $curr_package != '' && $curr_package != 'free' ){
	
		$emi_shop_packages_settings = emi_get_option( 'emi_jl_package_info' );

		$add_on_id = "";
		if( $curr_package == 'bronze' ){
			$add_on_id = $package_details[ 'bronze_addon' ];
		}else if( $curr_package == 'silver' ){
			$add_on_id = $package_details[ 'silver_addon' ];
		}else if( $curr_package == 'gold' ){
			$add_on_id = $package_details[ 'gold_addon' ];
		}else if( $curr_package == 'platinum' ){
			$add_on_id = $package_details[ 'platinum_addon' ];
		}

		if( $add_on_id != "" ){

			$package_args["post__in"] = array( $add_on_id );
			$package_args["tax_query"] = array(
						array(
							'taxonomy' => 'product_cat',
							'field'    => 'slug',
							'terms'    => 'add-ons',
							'operator' => 'IN'
						)
			);
			
			$addons_query = new WP_Query( apply_filters( 'emi_wcpl_products_query_args', $package_args ) );

			if( $addons_query->have_posts() ):
				?>
				<div class="panel panel-default">
					<div class="panel-heading"><h3>Additional listing add-on's</h3></div>
					<div class="panel-body job-packages pricing-table-widget-<?php echo $addons_query->found_posts;?> addons">
						<?php 
						while ( $addons_query->have_posts() ) : 

							$addons_query->the_post(); 
							$package = get_product( get_the_ID() );

							?>
							<div class="pricing-table-widget woocommerce">
								<div class="pricing-table-widget-title">
									<?php the_title(); ?>
								</div>

								<div class="pricing-table-widget-description">
									<h2><?php echo $package->get_price_html(); ?></h2>
									<p>
									<span class="rcp_level_duration" >
										<?php echo get_the_content(); ?>
									</span>
									</p>

									<p>
									<?php
										//$link = $package->add_to_cart_url();
										$link = get_site_url()."/shop?add-to-cart=".get_the_ID();
										$label = apply_filters( 'add_to_cart_text', __( 'Add to cart', 'jobify' ) );
									?>
									<a href="<?php echo esc_url( $link ); ?>" class="button-secondary"><?php echo $label; ?></a>
									</p>
								</div>
							</div>
							<?php

						endwhile;
						?>
						    <div class="pricing-table-widget woocommerce">
							    <div class="pricing-table-widget-title">
								    Package Details
							    </div>

							    <div class="pricing-table-widget-description">
								    

								    <p>
								    <span class="rcp_level_duration" >
									    Slider<br/>
									    Spotlight<br/>
									    Featured<br/>
									    Company Logo
								    </span>
								    </p>

								    <p>
								    <?php
									    //$link = $package->add_to_cart_url();
									    $link = get_site_url()."/contact-us/";
									    
								    ?>
								    <a href="<?php echo esc_url( $link ); ?>" class="button-secondary"><?php echo "Contact Us"; ?></a>
								    </p>
							    </div>
						    </div>
					</div>
				</div><!-- .panel -->
				<?php 
			endif;

			wp_reset_postdata();
		}
    }
}