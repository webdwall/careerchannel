<?php 
add_action( 'admin_menu', 'cc_apptj_settings_menu' );
function cc_apptj_settings_menu(){
	add_options_page('Application Message Config', 'App Message Config', 'manage_options', 'app-message-config', 'cc_apptj_settings_menu_page' );
}

function cc_apptj_settings_menu_page(){
	if( isset( $_POST["save_app_message_setting_button"] ) ){

		if( isset( $_POST["email_content_txtarea"] ) ){
		update_option("cc_app_message_format", $_POST["email_content_txtarea"]);
		}
		if( isset( $_POST["job_application_allowed"] ) ){
		update_option("cc_app_free_user_application_allowed", $_POST["job_application_allowed"]);
		}

	}
	$default_format = "Hello %employername%,
				%candidatename%( %candidateprofile% ) applied to your job %joblink%.
				Please look into it.
				Here's the message from candidate:
				%candidatemessage%";
	$email_content_format = stripslashes( get_option("cc_app_message_format", $default_format ) );

	$free_user_application_allowed = get_option("cc_app_free_user_application_allowed",10);
	?>
	<div class="admin_setting_main_page_wrapper">
		<form name="admin_general_setting_form" method="post" action="" >
		<h3>Application Message settings:</h3>
		<table style="text-align: left;" >
			<tr>
			<th>
				<label>Email content structure : </label>
			</th>
			<td>
				<small><em>Use &lt;br /&gt; to insert line breaks.</em></small><br/>
				<textarea placeholder="Message format" name="email_content_txtarea" id="email_content_txtarea" rows="8" cols="60" ><?php echo $email_content_format; ?></textarea>
			</td>
			</tr>
			<tr>
			<th>Instructions</th>
			<td>
				<ul>
				<li>These are the dynamic placeholders you can specify in the mail content which will be, later replaced with respective details.</li>
				<li><strong>%employername%</strong> - Name of the employer whose job the candidate has applied for. (Receivers name).</li>
				<li><strong>%candidatename%</strong> - Name of the candidate who has applied for the job.</li>
				<li><strong>%candidateprofile%</strong> - The link to candidate's profile.</li>
				<li><strong>%joblink%</strong> - The link to the details of job applied for.</li>
				<li><strong>%candidatemessage%</strong> - The message which candidate has entered while applying for the job.</li>
				</ul>
			</td>
			</tr>
			<tr>
			<th>
				<label>Free employer max job application : </label>
			</th>
			<td>
				<small><em>The maximum number of job application a free employer can receive 'per job'.<br/>If the number of applications received exceed this value,
						the job listing will be automatically deactivated.<br/>This rule is only applicable for employers who were a free member at the time of posting 
						the job AND are a free employer right now.
					</em></small><br/>
				<input type="number" min="0" name="job_application_allowed" id="job_application_allowed" value="<?php echo $free_user_application_allowed; ?>" />
			</td>
			</tr>
			<tr>
			<td>
				<input type="submit" name="save_app_message_setting_button" value="Save Settings" class="button-primary" />
			</td>
			</tr>
		</table>
		</form>
	</div>
	<?php
}

add_filter( 'cc_user_notifications', 'cc_applications_notifications', 10, 3 );
function cc_applications_notifications( $notifications, $user_id, $last_time_checked ){
    $user_details = get_userdata($user_id);
    if( $user_details->roles[0] == "employer" ){
	
	global $wpdb;
	$query = "SELECT * FROM " . get_cc_threads_table() . " WHERE reciver_id=%d AND msg_date>%s AND type='jobapplication'";
	$query = $wpdb->prepare( $query, $user_id, $last_time_checked );

	$applications = $wpdb->get_results( $query );

	if( $applications && !empty( $applications ) ){
	    foreach( $applications as $application ){
		$notification = array(
		    'type'	=> 'job-application',
		    'url'	=> get_site_url()."/employer-dashboard/?component=messages&subcomponent=job-application&tid=".$application->ID,
		    'text'	=> 'You have a new job applcaition message.'
		);
		$notifications[] = $notification;
	    }
	}
    }
    return $notifications;
}