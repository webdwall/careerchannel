<?php

/*###################################################################
 * Employer Pending Tab
###################################################################*/

add_action( 'cc_dashboard_nav',						'employer_tag_candidates_pending_nav',20 );
add_action( 'cc_dashboard_title',					'employer_tag_candidates_pending_title' );
add_action( 'cc_dashboard_content',					'employer_tag_candidates_pending_content' );

function employer_tag_candidates_pending_nav( $user_type ){
    if( $user_type=='employer' ){
	    $active_class = '';
	    if( cc_user_dashboard_is_current_component('pending') ){
		    $active_class = 'active';
	    }

	    //must be all small letters, capitalization will be done with css 
		echo "</ul><div class='devider'></div><h4>Candidates</h4><ul class='nav nav-pills nav-stacked'>";
	    echo "<li class='$active_class'><a href='". cc_user_dashboard_url( 'employer', 'pending') ."'>Pending</a></li>";
    }
}

function employer_tag_candidates_pending_title( $user_type ){
    if( $user_type=='employer' && cc_user_dashboard_is_current_component('pending') ){
	    //must be all small letters, capitalization will be done with css
	    echo 'Pending Candidates';
    }
}

function employer_tag_candidates_pending_content( $user_type ){
    if( $user_type=='employer' && cc_user_dashboard_is_current_component('pending') ){
	list_candidates_of_specific_tags( "Pending" );
    }
}

/*###################################################################
 * Employer Rejected Tab
###################################################################*/

add_action( 'cc_dashboard_nav',						'employer_tag_candidates_rejected_nav',20 );
add_action( 'cc_dashboard_title',					'employer_tag_candidates_rejected_title' );
add_action( 'cc_dashboard_content',					'employer_tag_candidates_rejected_content' );

function employer_tag_candidates_rejected_nav( $user_type ){
    if( $user_type=='employer' ){
	    $active_class = '';
	    if( cc_user_dashboard_is_current_component('rejected') ){
		    $active_class = 'active';
	    }

	    //must be all small letters, capitalization will be done with css
	    echo "<li class='$active_class'><a href='". cc_user_dashboard_url( 'employer', 'rejected') ."'>Rejected</a></li>";
    }
}

function employer_tag_candidates_rejected_title( $user_type ){
    if( $user_type=='employer' && cc_user_dashboard_is_current_component('rejected') ){
	    //must be all small letters, capitalization will be done with css
	    echo 'Rejected Candidates';
    }
}

function employer_tag_candidates_rejected_content( $user_type ){
    if( $user_type=='employer' && cc_user_dashboard_is_current_component('rejected') ){
	list_candidates_of_specific_tags( "Rejected" );
    }
}

/*###################################################################
 * Employer Short Listed Tab
###################################################################*/

add_action( 'cc_dashboard_nav',						'employer_tag_candidates_shortlisted_nav',20 );
add_action( 'cc_dashboard_title',					'employer_tag_candidates_shortlisted_title' );
add_action( 'cc_dashboard_content',					'employer_tag_candidates_shortlisted_content' );

function employer_tag_candidates_shortlisted_nav( $user_type ){
    if( $user_type=='employer' ){
	    $active_class = '';
	    if( cc_user_dashboard_is_current_component('shortlisted') ){
		    $active_class = 'active';
	    }

	    //must be all small letters, capitalization will be done with css
	    echo "<li class='$active_class'><a href='". cc_user_dashboard_url( 'employer', 'shortlisted') ."'>Shortlisted</a></li>";
    }
}

function employer_tag_candidates_shortlisted_title( $user_type ){
    if( $user_type=='employer' && cc_user_dashboard_is_current_component('shortlisted') ){
	    //must be all small letters, capitalization will be done with css
	    echo 'Shortlisted Candidates';
    }
}

function employer_tag_candidates_shortlisted_content( $user_type ){
    if( $user_type=='employer' && cc_user_dashboard_is_current_component('shortlisted') ){
	list_candidates_of_specific_tags( "Shortlisted" );
    }
}

/*###################################################################
 * Employer Screened Tab
###################################################################*/

add_action( 'cc_dashboard_nav',						'employer_tag_candidates_screened_nav',20 );
add_action( 'cc_dashboard_title',					'employer_tag_candidates_screened_title' );
add_action( 'cc_dashboard_content',					'employer_tag_candidates_screened_content' );

function employer_tag_candidates_screened_nav( $user_type ){
    if( $user_type=='employer' ){
	    $active_class = '';
	    if( cc_user_dashboard_is_current_component('screened') ){
		    $active_class = 'active';
	    }

	    //must be all small letters, capitalization will be done with css
	    echo "<li class='$active_class'><a href='". cc_user_dashboard_url( 'employer', 'screened') ."'>Screened</a></li>";
    }
}

function employer_tag_candidates_screened_title( $user_type ){
    if( $user_type=='employer' && cc_user_dashboard_is_current_component('screened') ){
	    //must be all small letters, capitalization will be done with css
	    echo 'Screened Candidates';
    }
}

function employer_tag_candidates_screened_content( $user_type ){
    if( $user_type=='employer' && cc_user_dashboard_is_current_component('screened') ){
	list_candidates_of_specific_tags( "Screened" );
    }
}

/*###################################################################
 * Employer Hired Tab
###################################################################*/

add_action( 'cc_dashboard_nav',						'employer_tag_candidates_hired_nav',20 );
add_action( 'cc_dashboard_title',					'employer_tag_candidates_hired_title' );
add_action( 'cc_dashboard_content',					'employer_tag_candidates_hired_content' );

function employer_tag_candidates_hired_nav( $user_type ){
    if( $user_type=='employer' ){
	    $active_class = '';
	    if( cc_user_dashboard_is_current_component('hired') ){
		    $active_class = 'active';
	    }

	    //must be all small letters, capitalization will be done with css
	    echo "<li class='$active_class'><a href='". cc_user_dashboard_url( 'employer', 'hired') ."'>Hired</a></li>";
    }
}

function employer_tag_candidates_hired_title( $user_type ){
    if( $user_type=='employer' && cc_user_dashboard_is_current_component('hired') ){
	    //must be all small letters, capitalization will be done with css
	    echo 'Hired Candidates';
    }
}

function employer_tag_candidates_hired_content( $user_type ){
    if( $user_type=='employer' && cc_user_dashboard_is_current_component('hired') ){
	list_candidates_of_specific_tags( "Hired" );
    }
}

/**
 * list all the candidates of current employer added to specific tag
 * @param String $tag_name - Name of the tag
 */
function list_candidates_of_specific_tags( $tag_name ){
    
    $user_id = get_current_user_id();
    $candidates_tag_detials = get_user_meta( $user_id, "tagged_candidates7", true);
    
    if( empty( $candidates_tag_detials ) ){
	echo 'No users found.';
	return false;
    }
    
    $tagged_user_ids = array();
    $associated_job = array();
    foreach ( $candidates_tag_detials as $candidate_id => $can_tag ){
	foreach ( $can_tag as  $can ){
	    if( $can["tag"] == $tag_name ){
		$tagged_user_ids[] = $candidate_id;
		$associated_job[$candidate_id][] =  $can["job"];
	    }
	}
	
    }
    
    if( $tagged_user_ids && !empty( $tagged_user_ids ) ){
	
	$args = array(
	    "include"   => $tagged_user_ids,
	    "meta_query" => array(
		    array(
			'key'     => "cc_hide_profile",
			'value'   => "yes",
			'compare' => "NOT EXISTS"
		)
	    )
	);
	$candidate_query = new WP_User_Query( $args );

	if ( ! empty( $candidate_query->results ) ) {
		foreach ( $candidate_query->results as $user ) {
		    global $candidate;
		    $candidate = $user;
		    
		    foreach ( $associated_job[ $candidate->ID ] as $temp_val ):
			$job_id = $temp_val;
			$job_detail = get_post( $job_id );

			echo '<div class="tagged_candidate_container" style="position:relative;" >';
			    echo '<div class="tagged_candidate_job" style="position:absolute;right: 50px;top: -15px;" >';
				echo '<h6>Tagged for: <a href='.$job_detail->guid.' >'.$job_detail->post_title.'</a></h6>';
			    echo '</div>';
			    get_template_part('members/candidate', 'loop');
			echo '</div>';
		    endforeach;
		}
	}else{
	    echo 'No users found.';
	} 
    }else {
	echo 'No users found.';
    }
}