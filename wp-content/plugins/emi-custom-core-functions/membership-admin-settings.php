<?php
add_action( 'pmsc_admin_general_setting_hook', 'emi_free_listing_custom_config' );

function emi_free_listing_custom_config() {
    $selected_value = get_option("emi_free_listing_per_month",3);
    $product_lists = get_products_list();
    $selected_package_value = get_option("emi_jl_package_info");
    
    ?>
	<li>
		<p><strong>Membership Settings:</strong></p>
		
		<p>
			<label for='mem_options_page'>Membership options page </label>
			<?php 
			$mem_options_page = ( isset( $selected_package_value['membership_page'] ) ? $selected_package_value['membership_page'] : 0 );
			
			$args = array(
				'selected' => $mem_options_page,
				'name' => 'mem_options_page',
				'id' => 'mem_options_page'
			);
			echo pmsc_admin_settings_pages_list($args);
			?><br/>
			<small><i>The (woocommerce)Product which corresponds to Silver membership package.</i></small>
		</p>
		
		<p>
			<label for='bronze_package'>Bronze Package </label>
			<select name="bronze_package" id="bronze_package" >
				<option value=''>select..</option>
				<?php foreach( $product_lists as $title ): ?>
				<?php if( $title["id"] == $selected_package_value["bronze"] ): ?>
					<option value="<?php echo $title["id"]; ?>" selected > <?php echo $title["title"]; ?> </option>
				<?php else: ?>
					<option value="<?php echo $title["id"]; ?>" > <?php echo $title["title"]; ?> </option>
				<?php endif; ?>
				<?php endforeach; ?>
			</select><br/>
			<small><i>The (woocommerce)Product which corresponds to Bronze membership package.</i></small>
		</p>
		<p>
			<label for='bronze_package_addon'>Bronze Package Add-on</label>
			<select name="bronze_package_addon" id="bronze_package_addon" >
				<option value=''>select..</option>
				<?php foreach( $product_lists as $title ): ?>
				<?php if( $title["id"] == $selected_package_value["bronze_addon"] ): ?>
					<option value="<?php echo $title["id"]; ?>" selected > <?php echo $title["title"]; ?> </option>
				<?php else: ?>
					<option value="<?php echo $title["id"]; ?>" > <?php echo $title["title"]; ?> </option>
				<?php endif; ?>
				<?php endforeach; ?>
			</select><br/>
			<small><i>The (woocommerce)Product which corresponds to Bronze package add-on.</i></small>
		</p>
		<p>
			<label for='silver_package'>Silver Package </label>
			<select name="silver_package" id="silver_package" >
				<option value=''>select..</option>
				<?php foreach( $product_lists as $title ): ?>
				<?php if( $title["id"] == $selected_package_value["silver"] ): ?>
					<option value="<?php echo $title["id"]; ?>" selected > <?php echo $title["title"]; ?> </option>
				<?php else: ?>
					<option value="<?php echo $title["id"]; ?>" > <?php echo $title["title"]; ?> </option>
				<?php endif; ?>
				<?php endforeach; ?>
			</select><br/>
			<small><i>The (woocommerce)Product which corresponds to Silver membership package.</i></small>
		</p>
		<p>
			<label for='silver_package_addon'>Silver Package Add-on</label>
			<select name="silver_package_addon" id="silver_package_addon" >
				<option value=''>select..</option>
				<?php foreach( $product_lists as $title ): ?>
				<?php if( $title["id"] == $selected_package_value["silver_addon"] ): ?>
					<option value="<?php echo $title["id"]; ?>" selected > <?php echo $title["title"]; ?> </option>
				<?php else: ?>
					<option value="<?php echo $title["id"]; ?>" > <?php echo $title["title"]; ?> </option>
				<?php endif; ?>
				<?php endforeach; ?>
			</select><br/>
			<small><i>The (woocommerce)Product which corresponds to Silver package add-on.</i></small>
		</p>
		<p>
			<label for='gold_package'>Gold package </label>
			<select name="gold_package" id="gold_package" >
				<option value=''>select..</option>
				<?php foreach( $product_lists as $id => $title ): ?>
				<?php if( $title["id"] == $selected_package_value["gold"] ): ?>
					<option value="<?php echo $title["id"]; ?>" selected > <?php echo $title["title"]; ?> </option>
				<?php else: ?>
					<option value="<?php echo $title["id"]; ?>" > <?php echo $title["title"]; ?> </option>
				<?php endif; ?>
				<?php endforeach; ?>
			</select><br/>
			<small><i>The (woocommerce)Product which corresponds to Gold membership package.</i></small>
        </p>
		<p>
			<label for='gold_package_addon'>Gold package Add-on</label>
			<select name="gold_package_addon" id="gold_package_addon" >
				<option value=''>select..</option>
				<?php foreach( $product_lists as $id => $title ): ?>
				<?php if( $title["id"] == $selected_package_value["gold_addon"] ): ?>
					<option value="<?php echo $title["id"]; ?>" selected > <?php echo $title["title"]; ?> </option>
				<?php else: ?>
					<option value="<?php echo $title["id"]; ?>" > <?php echo $title["title"]; ?> </option>
				<?php endif; ?>
				<?php endforeach; ?>
			</select><br/>
			<small><i>The (woocommerce)Product which corresponds to Gold package Add-on.</i></small>
        </p>
		<p>
			<label for='platinum_package'>Platinum package </label>
			<select name="platinum_package" id="platinum_package" >
				<option value=''>select..</option>
				<?php foreach( $product_lists as $id => $title ): ?>
				<?php if( $title["id"] == $selected_package_value["platinum"] ): ?>
					<option value="<?php echo $title["id"]; ?>" selected > <?php echo $title["title"]; ?> </option>
				<?php else: ?>
					<option value="<?php echo $title["id"]; ?>" > <?php echo $title["title"]; ?> </option>
				<?php endif; ?>
				<?php endforeach; ?>
			</select><br/>
			<small><i>The (woocommerce)Product which corresponds to Platinum membership package.</i></small>
		</p>
		<p>
			<label for='platinum_package_addon'>Platinum package Add-on</label>
			<select name="platinum_package_addon" id="platinum_package_addon" >
				<option value=''>select..</option>
				<?php foreach( $product_lists as $id => $title ): ?>
				<?php if( $title["id"] == $selected_package_value["platinum_addon"] ): ?>
					<option value="<?php echo $title["id"]; ?>" selected > <?php echo $title["title"]; ?> </option>
				<?php else: ?>
					<option value="<?php echo $title["id"]; ?>" > <?php echo $title["title"]; ?> </option>
				<?php endif; ?>
				<?php endforeach; ?>
			</select><br/>
			<small><i>The (woocommerce)Product which corresponds to Platimum package Add-on.</i></small>
		</p>
		<p>
			<label for='free_listing_txt'>Free Listing per month </label>
			<input type="number" name="free_listing_txt" id="free_listing_txt" value="<?php echo $selected_value; ?>" /><br/>
			<small><i>Maximum number of free listings allowed to all employers(free members as well as premium members).</i></small>
		</p>
    </li>
	<li class="devider"></li>
    <?php
}

//save/update the settins option
add_action('pmsc_admin_setting_save_button_hook', 'emi_save_free_listing_custom_config');

function emi_save_free_listing_custom_config() {
    $selected_value = get_option('emi_free_listing_per_month', 3);
    //update only if it's been changed
    
    $new_selection = ( $_POST['free_listing_txt'] != "" && $_POST['free_listing_txt'] != null ? $_POST['free_listing_txt'] : 3 );
    if ( $new_selection != $selected_page ) {
	update_option('emi_free_listing_per_month', $new_selection);
    }
    
    $bronze_value = ( $_POST['bronze_package'] != "" && $_POST['bronze_package'] != null ? $_POST['bronze_package'] : 0 );
    $bronze_value_addon = ( $_POST['bronze_package_addon'] != "" && $_POST['bronze_package_addon'] != null ? $_POST['bronze_package_addon'] : 0 );
    
    $silver_value = ( $_POST['silver_package'] != "" && $_POST['silver_package'] != null ? $_POST['silver_package'] : 0 );
    $silver_value_addon = ( $_POST['silver_package_addon'] != "" && $_POST['silver_package_addon'] != null ? $_POST['silver_package_addon'] : 0 );
    
    $gold_value = ( $_POST['gold_package'] != "" && $_POST['gold_package'] != null ? $_POST['gold_package'] : 0 );
    $gold_value_addon = ( $_POST['gold_package_addon'] != "" && $_POST['gold_package_addon'] != null ? $_POST['gold_package_addon'] : 0 );
	
    $platinum_value = ( $_POST['platinum_package'] != "" && $_POST['platinum_package'] != null ? $_POST['platinum_package'] : 0 );
    $platinum_value_addon = ( $_POST['platinum_package_addon'] != "" && $_POST['platinum_package_addon'] != null ? $_POST['platinum_package_addon'] : 0 );
    
    $memebrship_page = ( $_POST['mem_options_page'] != "" && $_POST['mem_options_page'] != null ? $_POST['mem_options_page'] : 0 );
	
    $package_details = array(
		"bronze"    => $bronze_value,
		"bronze_addon" => $bronze_value_addon,
		"silver"    => $silver_value,
		"silver_addon" => $silver_value_addon,
		"gold"	    => $gold_value,
		"gold_addon"=> $gold_value_addon,
		"platinum"  => $platinum_value,
		"platinum_addon"=> $platinum_value_addon,
		"membership_page"=> $memebrship_page
    );
    update_option( "emi_jl_package_info" , $package_details );
}

function get_products_list(){
    $args = array(
	"post_type"	    => "product",
	"post_status"	    => "publish",
	"posts_per_page"    => -1
    );
    $products = new WP_Query( $args );
    $product_lists = array();
    if( $products->have_posts() ){
	while ( $products->have_posts() ){
	    $products->the_post();
	    $product_lists_temp["title"] = get_the_title();
	    $product_lists_temp["id"] = get_the_ID();
	    $product_lists[] = $product_lists_temp;
	}
    }
    return $product_lists;
}