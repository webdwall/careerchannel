<?php 
/*
 * Displays the UI to send pm to the job poster.
 * Appears on the modal box after click of 'apply' button on single job page.
 * 
 * @since: 0.2
 * 
 * @param: void
 * @return : void
 */
function cc_job_apply_alternate_method(){
	/*
	 * Do nothing if not a single job page
	 * Do nothing if user is not logged In.
	 * Do nothing if logged in user is not a 'candidate'
	 * Check if user has already sent a 'application' message to job-posted
	 * regarding this job
	*/
	
    
	if( !is_user_logged_in() || !is_user_a_candidate() || !is_single() || 'job_listing' != get_post_type( get_the_ID() ) ){
		return false;
	}
	
	$current_user = wp_get_current_user();
	$args = array(
		'sender_id'			=> $current_user->ID,
		'receiver_id'		=> get_the_author_meta('ID'),
		'primary_object'	=> get_the_ID(),
		'type'				=> 'job_application',
		'include_deleted'	=> true
	);
	
	if( cc_messages_does_message_exist( $args ) ){
		/*
		 * the candidate has already applied for this job once, 
		 * shouldn't be able to send the pm again.
		 */
		echo "<div class='job_apply_alternate'>";
		echo	"<p class='text-info'>It appears that you've already applied for this job. If not, you can contact the employer again.</p>";
		echo "</div>";
	}else{
	    $post_detials = get_post(get_the_ID());
		echo "<div class='job_apply_alternate'>";
			$html = '<p><textarea id="apply_job_message_txtarea" placeholder = "write your message here" ></textarea></p>';

			$html .= "<p><center><button class='button button-info candidates_data' data-sender=".get_current_user_id()." data-reciver=".$post_detials->post_author." data-job=".get_the_ID()." >Apply</button></center></p>";
			$html .= "<span class='show_status'></span>";

			echo $html;
		echo "</div>";
	}
}

/**
 * this function will send an email to the employer specified in parameter
 * @param array $args: mixed array
 * @return boolean : true on success
 */
function cc_job_application_email_employer( $args ){
    /*
    Hello %employername%,
    %candidatename%( %candidateprofile% ) applied to your job %joblink%.
    Please look into it.
    Here\'s the message from candidate:
    %candidatemessage%
     */
    $email_format = get_option("cc_app_message_format","");
    $employer_id = $args["reciver_id"];
    $candidate_id = $args["sender_id"];
    $job_id = $args["primary_obj"];
    $candidate_message = $args["content"];
    $subject = ( $args["subject"] ? $args["subject"] : 'New job application' );
    
    $employer_details = get_userdata( $employer_id );
    $employer_name = $employer_details->data->user_login;
    $receiver_email_id = $employer_details->data->user_email;
    
    $candidate_details = get_userdata( $candidate_id );
    $candidate_name = $candidate_details->data->user_login;
    $candidate_profile = "<a href='".get_site_url()."/candidates/".$candidate_id."' >$candidate_name</a>";
    
    $job_details = get_post( $job_id );
    $job_link = "<a href=".get_permalink( $job_id )." >".$job_details->post_name."</a>";
    
    $email_format = str_replace( "%employername%" , $employer_name, $email_format );
    $email_format = str_replace( "%candidatename%" , $candidate_name, $email_format );
    $email_format = str_replace( "%candidateprofile%" , $candidate_profile, $email_format );
    $email_format = str_replace( "%joblink%" , $job_link, $email_format );
    $email_format = str_replace( "%candidatemessage%" , $candidate_message, $email_format );
	
	$email_format = stripslashes( $email_format );
	
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers .= 'From: careerchannel.sg <no-reply@careerchannel.sg' . "\r\n";
    
    $result = wp_mail( $receiver_email_id, $subject, $email_format, $headers );
    if( $result ){
		return true;
    } else {
		//update_option( 'emi_debug', 'mail for job application sent to '.$receiver_email_id. '. The email content is : ' . $email_format );
		//if wp_mail fails, try php mail function
		mail( $receiver_email_id, $subject, $email_format, $headers );
		return false;
    }
}

add_action( 'wp_ajax_cc_apply_to_job', 'ajax_cc_apply_to_job');
function ajax_cc_apply_to_job(){
    $content = $_POST["c_message"];
    $args = array (
		'content'       => $content,
		'sender_id'     => $_POST['sender_id'],
		'reciver_id'    => $_POST['reciver_id'],
		'reply_of'      => 0,
		'type'          => 'jobapplication',
		'primary_obj'   => $_POST['primary_obj'],
		'secondry_obj'  => false
	);
	$instance = CCMembersMessages::get_instance();
    $response = $instance->cc_messages_send($args);
    if( $response['status'] ){
        $response['message'] = "successfully notified the employer. you will soon get response of your notification";
    }
    $send_email = check_employer_email_credentails( $args["reciver_id"], $args["primary_obj"]);
    if( $send_email ){
		cc_job_application_email_employer( $args );
    }
    die(json_encode($response));
}