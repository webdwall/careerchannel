<?php

/*###################################################################
 * Employer Dashboard Module
###################################################################*/

add_action( 'cc_dashboard_nav',						'employer_appointment_calender_nav',11 );
add_action( 'cc_dashboard_subnav',					'employer_appointment_calender_subnav' );
add_action( 'cc_dashboard_title',					'employer_appointment_calender_title' );
add_action( 'cc_dashboard_content',					'employer_appointment_calender_content' );
add_filter( 'cc_dashboard_default_subcomponent',	'employer_appointment_calender_default_subcomponent', 10, 2 );

function employer_appointment_calender_nav( $user_type ){
    if( $user_type=='employer' ){
	    $active_class = '';
	    if( cc_user_dashboard_is_current_component('appointments') ){
		    $active_class = 'active';
	    }

	    //must be all small letters, capitalization will be done with css
	    echo "<li class='$active_class'><a href='". cc_user_dashboard_url( 'employer', 'appointments') ."'>calendar</a></li>";
    }
}

function employer_appointment_calender_subnav( $user_type ){
	$my_component = 'appointments';
	if( $user_type=='employer' && cc_user_dashboard_is_current_component($my_component) ){
		$subcomponents = array( 'list', 'add' );
 		foreach( $subcomponents as $subcomponent ){
 			$active_class = '';
			if( cc_user_dashboard_is_current_subcomponent($subcomponent) ){
				$active_class = 'active';
			}

			//must be all small letters, capitalization will be done with css
			echo "<li class='$active_class'><a href='". cc_user_dashboard_url($user_type, $my_component, $subcomponent) ."'>$subcomponent</a></li>";
 		}
	}
}

function employer_appointment_calender_default_subcomponent( $default_subcomponent, $component ){
	if( $component=='appointments' ){
		$default_subcomponent = 'list';
 	}
	return $default_subcomponent;
}

function employer_appointment_calender_title( $user_type ){
    if( $user_type=='employer' && cc_user_dashboard_is_current_component('appointments') ){
	    //must be all small letters, capitalization will be done with css
	    echo 'Appointment - ' . cc_user_dashboard_current_subcomponent();
    }
}

function employer_appointment_calender_content( $user_type ){
    if( $user_type=='employer' && cc_user_dashboard_is_current_component('appointments') ){
	switch( cc_user_dashboard_current_subcomponent() ){ 
	    case 'add':
		    employer_appointment_add_appointment_content();
		    break;
	    default:
	    case 'list':
		    employer_appointment_list_appointment_content();
		    break;
	 }
    }
}

function employer_appointment_add_appointment_content(){
    $member_id = $_GET['member_id'];
    $select_jobs = array();
    $employer_id = get_current_user_id();
    $args = array(
	'post_type'	=> 'job_listing',
	'author'	=> $employer_id,
	'post_per_page'	=> -1,
	'post_status'	=> 'publish'
    );
    $job_list = new WP_Query( $args );
    while ( $job_list->have_posts() ){
	$job_list->the_post();
	$job_temp = array();
	$job_temp["id"] = get_the_ID();
	$job_temp["title"] = get_the_title();
	$select_jobs[] = $job_temp;
    }
    wp_reset_postdata();
    
    if (isset($member_id) && !empty($member_id)) {
	$user = new WP_User($member_id);
	//is it a valid user 
	if (isset($user->data) && !empty($user->data)) {
	    
	    if( is_user_a_candidate( $user ) ){
		//$candidate_name = 'Candidate ' . $user->ID;
		//only premium users should see candidate's name
		//if( can_view_candidate_info( $user, 'name' ) ){
		$candidate_name .= $user->display_name;
		//}
		$member_name = $candidate_name;
		/* Here will save the appointment detials */
		save_appointment_form_details( $employer_id, $member_id, $member_name );
	    }
	    ?>
	    <div class = "add_appointment_form">
		
		<form class="add_appointment_form" action="" method="post" >
		
		    <label>Appointment To : <span class="label label-default"><?php echo $member_name;?></span></label><br/>
			<p>&nbsp;</p>
			
			<p>
				<label for="appointment_job">Appointment for job <small><em>(optional)</em></small></label>
				<select name="appointment_job" id="appointment_job">
					<option value=''>--select job--</option>
					<?php foreach( $select_jobs as $job_detail ) : ?>
					<option value='<?php echo $job_detail["id"]; ?>'><?php echo $job_detail["title"]; ?></option>
					<?php endforeach; ?>
				</select>
			</p>
			
			<div class="row clear">
				<div class="col span_6">
					<p><label for="appointment_date" >Date</label>
					<input type="text" name="appointment_date" id="appointment_date" placeholder="Appointment Date" readonly /></p>
				</div>
				<div class="col span_6">
					<p><label for="appointment_time" >Time</label>
					<input type="text" name="appointment_time" id="appointment_time" placeholder="Appointment Time" readonly /></p>
				</div>
			</div>
		    <p><br/>
			<label for="appointment_self_message" >Self Notes <small><i>( not visible to candidate)</i></small></label>
		    <textarea name="appointment_self_message" id="appointment_self_message" placeholder="Write Message for Self Reference" ></textarea>
			</p>

			<p>
			<label for="appointment_candidate_message" >Message for Candidate</label>
		    <textarea name="appointment_candidate_message" id="appointment_candidate_message" placeholder="Write Message for Candidate" ></textarea>
			</p>
			
			<p><br/>
		    <input type="submit" name="add_appointment_to_list" class="add_appointment_btn button button-small" id="add_appointment_to_list" value = "Add Appointment" />
		    
			</p>
		
		</form>
		
	    </div>
	    <?php

	}else{
	    echo "<p class='text-error'>Not a valid member!</p>";
	}
    }else{
	$package_details = member_package_details( get_current_user_id() );
	if( $package_details && !empty( $package_details ) &&  $package_details["package"] != "free" ){

	    get_candidate_search_box("appointments","add","add appointment");

	}else{
	    echo "<p class='text-info'>Please select a member first!</p>";
	}
    }
    
}

function save_appointment_form_details( $employer_id, $candidate_id, $candidate_name ){
    if( isset( $_POST["add_appointment_to_list"] ) ){
	if( isset( $_POST["appointment_date"] ) && $_POST["appointment_date"] != "" && isset( $_POST["appointment_time"] ) && $_POST["appointment_time"] != "" ){
	    
	    /* this is the default format of private message to the candidate */
	    $app_date = trim( $_POST["appointment_date"] );
	    $app_time = trim( $_POST["appointment_time"] );

	    $employer_details = get_userdata($employer_id);
	    
	    
	    $send_p_m = true;
	    
	    $candidate_message = trim( $_POST["appointment_candidate_message"] );
	    $job_id = $_POST["appointment_job"];
	    
	    
	    global $wpdb;
	    $table_name = $wpdb->prefix."employer_appointment_details";
	    $add_appointment_details = array(
		"employer_id"		=> $employer_id,
		"candidate_id"		=> $candidate_id,
		"app_date"	        => $app_date,
		"app_time"		=> $app_time,
		"self_message"	        => trim( $_POST["appointment_self_message"] ),
		"candidate_message"	=> $candidate_message,
		"job_id"		=> $job_id
	    );
	    $data_types = array(
		"%d",
		"%d",
		"%s",
		"%s",
		"%s",
		"%s",
		"%d"
	    );
	    $result = $wpdb->insert(
		   $table_name,
		   $add_appointment_details,
		   $data_types
	    );
	    if( $result ){
		$appointment_id = $wpdb->insert_id;

		if( !$send_p_m ){
			echo "<p class='text-info' >Appointment Scheduled Successfully!!</p>";
		}else{
			/* Here will send personal message to the candidate */
			$args = array (
				'subject'       =>  "",
				'content'       =>  $candidate_message ,
				'sender_id'     =>  $employer_id,
				'reciver_id'    =>  $candidate_id,
				'type'          => 'jobappointment',
				'primary_obj'   => $appointment_id,
				'secondry_obj'  => false
			);
			$instance = CCMembersMessages::get_instance();
			$response = $instance->cc_messages_send($args);
			
			//also send an email to the candidate
			send_appointment_email( $add_appointment_details, $job_id, 'candidate' );
			
			if( $response['status'] ){
				echo "<p class='text-info' >Appointment Scheduled Successfully!!</p>";
			}
		}
	    }
	    
	}else{
	    echo "<p class='text-error' >Appointment Date and Time Field is required!!</p>";
	}
    }
    
}

/**
 * Sends an email informing the user about the schedules appointment.
 * 
 * @param array $appointment_details all the details about the appointment, like cnadidate id, employer id, message for candiate etc.
 * @param int $job_id the id of the job which the appointment is for
 * @param string $sendto whether it should be sent to candidate or employer. Default 'candidate'
 * 
 * @return void
 */
function send_appointment_email( $appointment_details, $job_id, $sendto='candidate' ){
	$appointments_link = cc_user_dashboard_url($sendto, 'appointments');
	
	$reciver_id = $appointment_details['candidate_id'];
	if( $sendto=='employer' ){
		$reciver_id = $appointment_details['employer_id'];
	}
	
	$receiver_details = get_userdata( $reciver_id );
	
	$subject = 'New appointment scheduled on CareerChannel';
	$message  = "<center><strong>Hi " . $receiver_details->data->display_name . ", a new appointment has been scheduled for you on "
			. "<a href='". esc_url($appointments_link) ."'>CareerChannel</a></strong></center><br/>"
			. "<p>Below are the details of the appointment:</p>"
			. "Appointment date and time : " . date( 'd M \'y', strtotime($appointment_details['app_date']) ) . ' ' . $appointment_details['app_time'] . "<br/>";
	
	if( isset( $job_id ) && (int)$job_id != 0 ){
		$job_detail_query = new WP_Query( 'p='.$job_id );
		if( $job_detail_query->have_posts() ){
			while( $job_detail_query->have_posts() ){
				$job_detail_query->the_post();
				$message .= "Regarding (job) : <a href='" . get_permalink() . "' title='view details'>" . get_the_title() . "</a><br/>" ;
				break;
			}
		}
		wp_reset_postdata();
		//$message .= "Regarding (job) : <a href='" . get_permalink() 
	}
	
	if( isset( $appointment_details['candidate_message'] ) ){
		$message .= "<p><strong>Message from employer:</strong><br/>"
				. stripslashes( $appointment_details['candidate_message'] )
				. "</p>";
	}
	
	$message .= "<p>-----------------------<br/>"
			. "<a href='". esc_url($appointments_link) ."'>CLICK HERE</a> to view all your appointments.<br/>"
			. "<a href='". wp_login_url($appointments_link) ."'>CLICK HERE</a> to login to your account."
			. "</p>";
	
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	$headers .= 'From: careerchannel.sg <no-reply@careerchannel.sg' . "\r\n";

	/* **********************************
	 * wp_mail doesn't seem to work on some servers. If wp_mail fails, try with php mail() function 
	 *********************************** */
	if( !wp_mail( $receiver_details->data->user_email, $subject, $message, $headers ) ){
		mail( $receiver_details->data->user_email, $subject, $message, $headers );
	}
}

function employer_appointment_list_appointment_content(){
    
    
    $employer_id = get_current_user_id();

    if( isset( $_GET["date"] ) ){
	list_employer_scheduled_appointment( $employer_id, $_GET["date"] );
	return false;
    }
    show_employer_dashboard_calender( $employer_id );
    
}

function list_employer_scheduled_appointment( $employer_id, $date ){
    
    global $wpdb;
    $query = "SELECT * FROM ".$wpdb->prefix."employer_appointment_details WHERE app_date = '$date' and employer_id = $employer_id";
    $appointment_details = $wpdb->get_results( $query );
    echo '<div class="appointment_list_container" >';
	echo '<table class="appointment_list_table table">';
	echo '<tr>';
	echo '<th>Candidate</th><th>Appointment date</th><th>Appointment Time</th><th>Self Ref Message</th><th>Candidate Message</th><th></th>';
	echo '</tr>';
	if( $appointment_details ){
	    foreach( $appointment_details as $appointment ){
		
		$candidate_name = '';
		//only premium users should see candidate's name
		$user = new WP_User( $appointment->candidate_id );
		if( can_view_candidate_info( $user, 'name' ) ){
			$candidate_name .= $user->display_name;
		}
		
		echo '<tr>';
		    echo '<td>'.$candidate_name.'</td>';
		    echo '<td>'.date("d-m-Y", strtotime( $appointment->app_date ) ).'</td>';
		    echo '<td>'.$appointment->app_time.'</td>';
		    echo '<td rowspan=2  >'.$appointment->self_message.'</td>';
		    echo '<td rowspan=2  >'.$appointment->candidate_message.'</td>';
		    echo '<td><a id="'.$appointment->id.'" class="glyphicon glyphicon-remove show-tooltip delete_appointment_link" title="delete"></a></td>';
		echo '</tr>';
		echo '<tr>';
		    $tid = get_appointment_message_id( $appointment->id );
		    $tid_attr = $tid > 0 ? "&tid=$tid" : "";
		    echo '<td><span ><a href="'.  get_site_url().'/employer-dashboard?component=messages&subcomponent=job-appointment'.$tid_attr.'#dnav" >View</a></span></td>';
		    echo '<td><span class="badge" >option2</span></td>';
		    echo '<td><span class="badge" >option3</span></td>';
		echo '</tr>';
	    }
	}else{
	    echo '<tr>';
	    echo '<td>No Appointments found!</td>';
	    echo '</tr>';
	}
	echo '</table>';
	echo "<div class='return_link' ><a href='".get_site_url()."/employer-dashboard?component=appointments&subcomponent=list#dnav' >Return to all appointments</a></div>";
    echo '</div>';
}

function show_employer_dashboard_calender( $employer_id ){
    
    global $wpdb;
    $query = "SELECT CONCAT(Year(app_date),'-',Month(app_date),'-',Day(app_date)) as 'dates', count(id) as 'count' FROM ".$wpdb->prefix."employer_appointment_details WHERE employer_id = $employer_id group by app_date";
    $appointment_details = $wpdb->get_results( $query );
    $appointment_dates = array();
    foreach( $appointment_details as $date ){
	$appointment_dates[$date->dates] = $date->count;
    }
    ?>
    <div class="employer_dashboard_calender_container">
	<div id="calendar_wrap">
	    <div id="wp-calendar" style="font-size:25px;" ></div>
	    <?php
	    if( $appointment_dates ){
		$hidden_appointments_date = json_encode($appointment_dates);
	    }
	    ?>
	    <input type="hidden" name="can_appointments_date_list_hidden" id="can_appointments_date_list_hidden" value="<?php echo esc_attr($hidden_appointments_date) ?>" />
	</div>
    </div>
    <?php
    
}

function get_appointment_message_id( $id ){
    global $wpdb;
    $tid = $wpdb->get_var("SELECT ID FROM demo_cc_messages_threads WHERE primary_obj = ".$id );
    $tid = $tid > 0 ? $tid : 0;
    return $tid;
}

/*###################################################################
 * Candidate Dashboard Module
###################################################################*/
add_action( 'cc_dashboard_nav',						'candidate_appointment_calender_nav',15 );
add_action( 'cc_dashboard_title',					'candidate_appointment_calender_title' );
add_action( 'cc_dashboard_content',					'candidate_appointment_calender_content' );

function candidate_appointment_calender_nav( $user_type ){
    if( $user_type != 'employer' ){
	    $active_class = '';
	    if( cc_user_dashboard_is_current_component('appointments') ){
		    $active_class = 'active';
	    }

	    //must be all small letters, capitalization will be done with css
	    echo "<li class='$active_class'><a href='". cc_user_dashboard_url( $user_type, 'appointments') ."'>Calendar <span class='badge'>". get_appointments_for_current_user() ."</span></a></li>";
    }
}

function candidate_appointment_calender_title( $user_type ){
    if( $user_type != 'employer' && cc_user_dashboard_is_current_component('appointments') ){
	//must be all small letters, capitalization will be done with css
	echo cc_user_dashboard_current_component()." <span class='badge'>".get_appointments_for_current_user()."</span>";
    }
}


function candidate_appointment_calender_content( $user_type ){
    if( $user_type != 'employer' && cc_user_dashboard_is_current_component('appointments') ){
	$candidate_id = get_current_user_id();
    
	if( isset( $_GET["date"] ) ){
	    list_candidate_appointment( $candidate_id, $_GET["date"] );
	    return false;
	}
	show_candidate_dashboard_calender( $candidate_id );
    }
}

function show_candidate_dashboard_calender( $candidate_id ){
    
    global $wpdb;
    $query = "SELECT CONCAT(Year(app_date),'-',Month(app_date),'-',Day(app_date)) as 'dates', count(id) as 'count' FROM ".$wpdb->prefix."employer_appointment_details WHERE candidate_id = $candidate_id group by app_date";
    $appointment_details = $wpdb->get_results( $query );
    $appointment_dates = array();
    foreach( $appointment_details as $date ){
	$appointment_dates[$date->dates] = $date->count;
    }
    ?>
    <div class="employer_dashboard_calender_container">
	<div id="calendar_wrap">
	    <div id="wp-calendar" style="font-size:25px;" ></div>
	    <?php
	    if( $appointment_dates ){
		$hidden_appointments_date = json_encode($appointment_dates);
	    }
	    ?>
	    <input type="hidden" name="can_appointments_date_list_hidden" id="can_appointments_date_list_hidden" value="<?php echo esc_attr($hidden_appointments_date) ?>" />
	</div>
    </div>
    <?php
    
}

function list_candidate_appointment( $employer_id, $date ){
    
    global $wpdb;
    $query = "SELECT * FROM ".$wpdb->prefix."employer_appointment_details WHERE app_date = '$date' and candidate_id = $employer_id";
    $appointment_details = $wpdb->get_results( $query );
    echo '<div class="appointment_list_container" >';
	echo '<table class="appointment_list_table table">';
	echo '<tr>';
	echo '<th>Employer</th><th>Appointment date</th><th>Appointment Time</th><th>Employer Message</th><th></th>';
	echo '</tr>';
	if( $appointment_details ){
	    foreach( $appointment_details as $appointment ){
		
		//$employer_name = 'Employer ' . $appointment->employer_id;
		//only premium users should see candidate's name
		$user = new WP_User( $appointment->employer_id );
		//if( can_view_candidate_info( $user, 'name' ) ){
		$employer_name = $user->display_name;
		//}
		
		echo '<tr>';
		echo '<td>'.$employer_name.'</td><td>'.date("Y-m-d", strtotime( $appointment->app_date ) ).'</td><td>'.$appointment->app_time.'</td><td>'.$appointment->candidate_message.'</td>';
		echo '<td><a id="'.$appointment->id.'" class="glyphicon glyphicon-remove show-tooltip delete_appointment_link" title="delete"></a></td>';
		echo '</tr>';
	    }
	}else{
	    echo '<tr>';
	    echo '<td>No Appointments found!</td>';
	    echo '</tr>';
	}
	
	echo '</table>';
	echo "<div class='return_link' ><a href='".get_site_url()."/candidate-dashboard?component=appointments#dnav' >Return to all appointments</a></div>";
    echo '</div>';
}

function get_appointments_for_current_user(){
    global $wpdb;
    
    $candidate_id = get_current_user_id();
	$appointments = wp_cache_get( 'canappnums' . $candidate_id );
	if( $appointments === false ){
		$appointments = $wpdb->get_var(" SELECT count(*) FROM ".$wpdb->prefix."employer_appointment_details WHERE candidate_id = $candidate_id and app_date >= DATE_FORMAT(now(),'%Y-%m-%d') ");
		$appointments = ($appointments > 0) ? $appointments : 0;
		wp_cache_set( 'canappnums' . $candidate_id, $appointments );
	}
    return $appointments;
}

/*###################################################################
 * Notifications
###################################################################*/
add_filter( 'cc_user_notifications', 'cc_appointments_notifications', 10, 3 );
function cc_appointments_notifications( $notifications, $user_id, $last_time_checked ){
	//appointment notifications are only meant for candidates.
	//notification for replies to appointment messages are already taken care of in messages plugin
	if( is_user_a_candidate( $user_id ) ){
		global $wpdb;
		$query = "SELECT a.*, t.msg_date "
				. "FROM ". $wpdb->prefix."employer_appointment_details a JOIN " . get_cc_threads_table() . " t ON t.primary_obj=a.id "
				. "WHERE t.type='jobappointment' AND a.candidate_id=%d AND t.msg_date>%s ";
		$query = $wpdb->prepare( $query, $user_id, $last_time_checked );
		
		$appointments = $wpdb->get_results( $query );
		
		if( $appointments && !empty( $appointments ) ){
			foreach( $appointments as $appointment ){
				$notification = array(
					'type'	=> 'job-appointment',
					'url'	=> add_query_arg( array( 'date'=>date("Y-m-d", strtotime( $appointment->app_date ) ) ), cc_user_dashboard_url('candidate', 'appointments') ),
					'text'	=> 'You have a new appointment.'
				);
				
				$notifications[] = $notification;
			}
		}
	}
	return $notifications;
}