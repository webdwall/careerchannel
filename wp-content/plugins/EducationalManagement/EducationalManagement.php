<?php
/*
Plugin Name: Manage Educational Management Plugin
Plugin URI: http://eminsoftwaresolutions.com
Description: This plugin is to manage Educational Qualification of site . 
Version: 2.1
Author: Gurmeet Badwal .
Author URI: gbadwal@eminsoftwaresolutions.com 
*/
add_action('admin_menu', 'EducationalManagement');
// Table creation code start here
register_activation_hook(__FILE__,'table_educational_management_install');

function table_educational_management_install ()
 	{
   		 educational_management();
	}
function educational_management()
  {
  global $wpdb;
   $table_name = $wpdb->prefix ."educational_qualification";
   if($wpdb->get_var("show tables like '$table_name'") != $table_name) {
      
      $sql = "CREATE TABLE " . $table_name . " (
	  id int(11) NOT NULL AUTO_INCREMENT,
	  name VARCHAR(255) NOT NULL,
	  added_on TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
	  status BOOLEAN NOT NULL ,
	  PRIMARY KEY id (id)
	);";
	
      require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
      dbDelta($sql);
  	}
  }
  
// Table creation code end here

function EducationalManagement() {
add_options_page('Educational Management', __('Educational Management'), 0,  __FILE__, 'management');
}

////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////
function management()
{

   global $wpdb;
   		$wpu = $_GET['wpu'];
		switch($wpu) {
			case 'add_education':
				add_education();
			break;
			case 'add_education_query':
				add_education_query();
			break;
			case 'status':
				update_status_education();
			break;
			case 'delete_education':
				delete_education();
			break; 
			default:
				show_education_adminside();
			break; 
		}
      }
	  ?>
<?php function show_education_adminside($msg="")
{
global $wpdb;
$message = $msg ;
if($_GET['msg'] != "")
{
	$message = $_GET['msg'];
}


			
$resultsno = 10;

$table_name = $wpdb->prefix ."educational_qualification";
$query = "select * from $table_name order by added_on desc ";
$row_count = $wpdb->get_results($query);
// pagination code //
	$page = $_REQUEST['page_num']; 
	if ($_REQUEST["page_num"]) { $page  = $_REQUEST["page_num"]; } else { $page=1; };
	$start_from = ($page-1) * $resultsno; 
	$query.=" LIMIT $start_from, $resultsno";
	
$row = $wpdb->get_results($query);
$res_count = sizeof($row_count);
?>

  <table border="0" align="center" cellpadding="10" cellspacing="10" style="width:600px;margin-left:20px; border:solid 1px #999999; margin-top:50px;">
      
     <tr>
	 <td colspan="3"><b> <?php echo __('Manage Educational Qualification ');  ?> </b> </td> 
	 <td align="right"><a href="?page=<?php echo dirname(plugin_basename(__FILE__)); ?>/EducationalManagement.php&wpu=add_education"><b>Add New</b></a>&nbsp;
	 <a href="?page=<?php echo dirname(plugin_basename(__FILE__)); ?>/EducationalManagement.php"><b>< Go Back</b></a></td>
      </tr>
	 <tr><td colspan="4">&nbsp;</td></tr>  
	 <?php if($message!="") { ?>    
	 <tr>
	 <td colspan="4"><b> <?php echo $message ; ?> </b> </td> 
	
	 </tr>
	 <tr><td colspan="4">&nbsp;</td></tr>  
	 <?php } ?>	  
	 <?php
   
	  if(!empty($row))
      {
	 ?>
	 <tr>
	 	<td> <b>Name</b> </td>
		<td> <b>Status</b> </td>
		<td> <b>Option</b> </td>
	 </tr>
	 <?php
	 								
	 foreach($row as $data)
	   {   ?>
	 <tr>
	 	<td><?php echo $data->name; ?></td>
	<td ><?php  if($data->status == 1) { echo "Published |"." <a href='?page=".dirname(plugin_basename(__FILE__))."/EducationalManagement.php&wpu=status&set=unpublish&id=$id&pid=".$data->id."'>Unpublish</a>"; } else { echo "Unpublished |"." <a href='?page=".dirname(plugin_basename(__FILE__))."/EducationalManagement.php&wpu=status&set=publish&id=$id&pid=".$data->id."'>Publish</a>"; } ?></td>
		<td><a href="?page=<?php echo dirname(plugin_basename(__FILE__)); ?>/EducationalManagement.php&wpu=delete_education&pid=<?php echo $data->id ;?>">Delete</a>
  </td>
	</tr>
	<?php
														
	    }
		?>
	<tr><td height="10"></td></tr>
	<tr>
		<td colspan="4" align="center">
		<?php 	
				
                       $total_rows = ceil($res_count / $resultsno); 
 
                     for ($i=1; $i<=$total_rows; $i++) {
			         if($page == $i)
			           {
				          echo "<b style='color:#999999;'>[".$i."] </b>";
			           }
                         else
			           {
				          ?><a href="?page=<?php echo dirname(plugin_basename(__FILE__)); ?>/MusicUpload.php&page_num=<?php echo $i; ?>" style="text-decoration:none;" >[<?php echo $i; ?>]</a>
		<?php 
				        }
                }?>
		</td>
	</tr>
	<?php
	  }
	  else
	  {
	?>
	  <tr>
	 	<td colspan="4" align="center"> <b>No Records Found.</b> </td>
		
	 </tr>
	<?php
	  }
	?>
	       
</table>
<?php
}
function add_education_query()
{

global $wpdb;
if(isset($_POST['submit']))
{
		$location_title = $_POST['location_title'];
		
		$table_name = $wpdb->prefix ."educational_qualification";

		$get_old_files = "select * from $table_name where name = '".$location_title."'";
			
		$row_count = mysql_num_rows(mysql_query($get_old_files));
			
		if($row_count > 0)
		{
			$msg =  "<p><strong>"." Error: Education already exist. "."</strong></p>";
			show_education_adminside($msg) ;
		}
		else
		{
			$insert =  "INSERT INTO ".$table_name." (name, status) VALUES ('".$location_title."', '1')";
					
			if($wpdb->query($insert)){
			$msg =  "<p><strong>"." New Education Added Successfully "."</strong></p>";
			show_education_adminside($msg) ;
			}
		}
		
}
}
function add_education()
{
?>

<script>
function show_loading()
{
 
  if(document.getElementById('location_title').value=='')
  {
   alert("Enter Education");
   return false;
  }
  
}
</script>

  <table border="0" cellpadding="10" cellspacing="10" align="center" style="width:600px;margin-left:20px; border:solid 1px #999999; margin-top:50px;">
     <tr>
	 <td colspan="3"><b> <?php echo __('Add Educational Qualification'); ?> </b> </td> 
	    <td align="right"><a href="?page=<?php echo dirname(plugin_basename(__FILE__)); ?>/EducationalManagement.php"><b>< Go Back</b></a></td>
      </tr> 
     <tr><td colspan="4">&nbsp;</td></tr>  
	 <?php if($message!="") { ?>    
	 <tr>
	 <td colspan="4"><b> <?php echo $message ; ?> </b> </td> 
	
	 </tr>
	 <tr><td colspan="4">&nbsp;</td></tr>  
	 <?php } ?>
	 <form method="post" action="?page=<?php echo dirname(plugin_basename(__FILE__)); ?>/EducationalManagement.php&wpu=add_education_query" name="form1" id="form1">
	 <tr>
	 	<td colspan="2" align="right" width="30%">Education Name</td>
		<td colspan="2" align="left"><input type="text" name="location_title" id="location_title" style="width:195px;" /></td>
	 </tr>
	

	 <tr>
	 	<td colspan="4" align="center"><input type="submit" name="submit" value="Submit" onclick="return show_loading();" /></td>
	 </tr>
	 </form>
	
	       
</table>
<?php
}
function delete_education() //function is used to delete records
{
   global $wpdb;
   $pid=$_GET['pid'];
   $id=$_GET['id'];
   
  $table_name = $wpdb->prefix ."educational_qualification";
  
    if($wpdb->query("DELETE FROM $table_name WHERE id = '$pid'"))
    {
    $msg = "<p><strong>".__('Record deleted successfully')."</strong></p>" ;
	show_education_adminside($msg) ;
    }	
  
  else 
  {
  echo  "<p><strong>".__('There is an error in deleting record. Please check the data.')."</strong></p>" ;
  show_education_adminside($msg) ;
     
  }
 
}

function update_status_education() //function is used to update record status
{
   global $wpdb;
   $table_name = $wpdb->prefix ."educational_qualification";
   
   $query="select * from $table_name where status = 1";
   $affected_query = mysql_query($query);
   $total_rows = mysql_affected_rows();
   //echo $total_rows ;
  
   $pid=$_GET['pid'];
   if($_GET['set'] == "publish")
   {
   		$status = 1;
   }
   else
   {
   		$status = 0;
   }
  	if($wpdb->query("update $table_name set status = '$status' WHERE id = $pid"))
    {
    $msg = "<p><strong>".__('Record updated successfully')."</strong></p>" ;
	show_education_adminside($msg) ;
    }	
   	else 
  	{
  
	  echo  "<p><strong>".__('There is an error in Updating record.')."</strong></p>" ;
	  show_education_adminside($msg) ;
     }
 
}
////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////cat////////////////////////////////////////////////////////////
?>