<?php
/*
Plugin Name: Manage Skills Management Plugin
Plugin URI: http://eminsoftwaresolutions.com
Description: This plugin is to manage Skills of site . 
Version: 2.1
Author: Gurmeet Badwal .
Author URI: gbadwal@eminsoftwaresolutions.com 
*/
add_action('admin_menu', 'SkillsManagement');
// Table creation code start here
register_activation_hook(__FILE__,'table_skills_management_install');

function table_skills_management_install ()
 	{
   		 category_skills();
  			 skills();
	}
function category_skills()
  {
  global $wpdb;
   $table_name = $wpdb->prefix ."skills_cat";
   if($wpdb->get_var("show tables like '$table_name'") != $table_name) {
      
      $sql = "CREATE TABLE " . $table_name . " (
	  id int(11) NOT NULL AUTO_INCREMENT,
	  cat_name VARCHAR(255) NOT NULL,
	  added_on TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
	  PRIMARY KEY id (id)
	);";
	
      require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
      dbDelta($sql);
  	}
  }
  function skills() {
   global $wpdb;

   $table_name = $wpdb->prefix ."skills";
   if($wpdb->get_var("show tables like '$table_name'") != $table_name) {
      
      $sql = "CREATE TABLE " . $table_name . " (
	  id int(11) NOT NULL AUTO_INCREMENT,
	  skill_name VARCHAR(255) NOT NULL,
	  skill_cat varchar(255) NOT NULL ,
	  added_on TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
	  status BOOLEAN NOT NULL ,
	  PRIMARY KEY id (id)
	);";
	
      require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
      dbDelta($sql);
	}
	
   }
// Table creation code end here

function SkillsManagement() {
add_options_page('Skills Management', __('Skills Management'), 0,  __FILE__, 'skills_management');
}

////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////
function skills_management()
{

   global $wpdb;
   		$wpu = $_GET['wpu'];
		switch($wpu) {
			case 'add_skill':
				add_skill();
			break;
			case 'add_skill_query':
				add_skill_query();
			break;
			case 'status':
				update_status_skill();
			break;
			case 'delete_skill':
				delete_skill();
			break; 
			case 'add_cat_skill':
				add_cat_skill();
			break;
			case 'delete_skill_cat':
				delete_skill_cat();
			break; 
			case 'skill_admin':
				showskill_adminside();
			break;
			default:
				showcat_skill_adminside();
			break; 
		}
      }
	  ?>
<?php function showskill_adminside($msg="")
{
global $wpdb;
$message = $msg ;
if($_GET['msg'] != "")
{
	$message = $_GET['msg'];
}
if($_GET['id'] != "")
{
	 $id = $_GET['id'];
}

			
$resultsno = 10;
$table_name_cat = $wpdb->prefix ."skills_cat";
$query_cat = "select * from $table_name_cat where id=$id  ";
$sqlquery=mysql_query($query_cat); 
$fecth_query=mysql_fetch_array($sqlquery);
$table_name = $wpdb->prefix ."skills";
$query = "select * from $table_name where skill_cat=$id order by added_on desc ";
$row_count = $wpdb->get_results($query);
// pagination code //
	$page = $_REQUEST['page_num']; 
	if ($_REQUEST["page_num"]) { $page  = $_REQUEST["page_num"]; } else { $page=1; };
	$start_from = ($page-1) * $resultsno; 
	$query.=" LIMIT $start_from, $resultsno";
	
$row = $wpdb->get_results($query);
$res_count = sizeof($row_count);
?>

  <table border="0" align="center" cellpadding="10" cellspacing="10" style="width:600px;margin-left:20px; border:solid 1px #999999; margin-top:50px;">
      
     <tr>
	 <td colspan="3"><b> <?php echo __('Manage skills of ' .$fecth_query['cat_name'].'');  ?> </b> </td> 
	 <td align="right"><a href="?page=<?php echo dirname(plugin_basename(__FILE__)); ?>/SkillsManagement.php&wpu=add_skill&id=<?php echo"$id"?>"><b>Add New</b></a>&nbsp;
	 <a href="?page=<?php echo dirname(plugin_basename(__FILE__)); ?>/SkillsManagement.php"><b>< Go Back</b></a></td>
      </tr>
	 <tr><td colspan="4">&nbsp;</td></tr>  
	 <?php if($message!="") { ?>    
	 <tr>
	 <td colspan="4"><b> <?php echo $message ; ?> </b> </td> 
	
	 </tr>
	 <tr><td colspan="4">&nbsp;</td></tr>  
	 <?php } ?>	  
	 <?php
   
	  if(!empty($row))
      {
	 ?>
	 <tr>
	 	<td> <b>Skills</b> </td>
		<td> <b>Status</b> </td>
		<td> <b>Option</b> </td>
	 </tr>
	 <?php
	 								
	 foreach($row as $data)
	   {   ?>
	 <tr>
	 	<td><?php echo $data->skill_name; ?></td>
	<td ><?php  if($data->status == 1) { echo "Published |"." <a href='?page=".dirname(plugin_basename(__FILE__))."/SkillsManagement.php&wpu=status&set=unpublish&id=$id&pid=".$data->id."'>Unpublish</a>"; } else { echo "Unpublished |"." <a href='?page=".dirname(plugin_basename(__FILE__))."/SkillsManagement.php&wpu=status&set=publish&id=$id&pid=".$data->id."'>Publish</a>"; } ?></td>
		<td><a href="?page=<?php echo dirname(plugin_basename(__FILE__)); ?>/SkillsManagement.php.php&wpu=delete_skill&pid=<?php echo $data->id ;?>&id=<?php echo $id; ?>">Delete</a>
  </td>
	</tr>
	<?php
														
	    }
		?>
	<tr><td height="10"></td></tr>
	<tr>
		<td colspan="4" align="center">
		<?php 	
				
                       $total_rows = ceil($res_count / $resultsno); 
 
                     for ($i=1; $i<=$total_rows; $i++) {
			         if($page == $i)
			           {
				          echo "<b style='color:#999999;'>[".$i."] </b>";
			           }
                         else
			           {
				          ?><a href="?page=<?php echo dirname(plugin_basename(__FILE__)); ?>/MusicUpload.php&page_num=<?php echo $i; ?>" style="text-decoration:none;" >[<?php echo $i; ?>]</a>
		<?php 
				        }
                }?>
		</td>
	</tr>
	<?php
	  }
	  else
	  {
	?>
	  <tr>
	 	<td colspan="4" align="center"> <b>No Records Found.</b> </td>
		
	 </tr>
	<?php
	  }
	?>
	       
</table>
<?php
}
function add_skill_query()
{
if($_GET['id'] != "")
{
	 $id = $_GET['id'];
}
global $wpdb;
if(isset($_POST['submit']))
{
		$location_title = $_POST['location_title'];
		
		$table_name = $wpdb->prefix ."skills";

		$get_old_files = "select * from $table_name where skill_name = '".$location_title."'";
			
		$row_count = mysql_num_rows(mysql_query($get_old_files));
			
		if($row_count > 0)
		{
			$msg =  "<p><strong>"." Error: Skill already exist. "."</strong></p>";
			Showskill_adminside($msg) ;
		}
		else
		{
			$insert =  "INSERT INTO ".$table_name." (skill_name,skill_cat, status) VALUES ('".$location_title."','".$id."', '1')";
					
			if($wpdb->query($insert)){
			$msg =  "<p><strong>"." New Skill Added Successfully "."</strong></p>";
			Showskill_adminside($msg) ;
			}
		}
		
}
}
function add_skill()
{
if($_GET['id'] != "")
{
	 $id = $_GET['id'];
}
?>

<script>
function show_loading()
{
 
  if(document.getElementById('location_title').value=='')
  {
   alert("Enter Skill");
   return false;
  }
  
}
</script>

  <table border="0" cellpadding="10" cellspacing="10" align="center" style="width:600px;margin-left:20px; border:solid 1px #999999; margin-top:50px;">
     <tr>
	 <td colspan="3"><b> <?php echo __('Add Skill'); ?> </b> </td> 
	    <td align="right"><a href="?page=<?php echo dirname(plugin_basename(__FILE__)); ?>/SkillsManagement.php"><b>< Go Back</b></a></td>
      </tr> 
     <tr><td colspan="4">&nbsp;</td></tr>  
	 <?php if($message!="") { ?>    
	 <tr>
	 <td colspan="4"><b> <?php echo $message ; ?> </b> </td> 
	
	 </tr>
	 <tr><td colspan="4">&nbsp;</td></tr>  
	 <?php } ?>
	 <form method="post" action="?page=<?php echo dirname(plugin_basename(__FILE__)); ?>/SkillsManagement.php&wpu=add_skill_query&id=<?php echo $id; ?>" name="form1" id="form1">
	 <tr>
	 	<td colspan="2" align="right" width="30%">Skill</td>
		<td colspan="2" align="left"><input type="text" name="location_title" id="location_title" style="width:195px;" /></td>
	 </tr>
	

	 <tr>
	 	<td colspan="4" align="center"><input type="submit" name="submit" value="Submit" onclick="return show_loading();" /></td>
	 </tr>
	 </form>
	
	       
</table>
<?php
}
function delete_skill() //function is used to delete records
{
   global $wpdb;
   $pid=$_GET['pid'];
   $id=$_GET['id'];
   
  $table_name = $wpdb->prefix ."skills";
  
    if($wpdb->query("DELETE FROM $table_name WHERE id = '$pid'"))
    {
    $msg = "<p><strong>".__('Record deleted successfully')."</strong></p>" ;
	showskill_adminside($msg) ;
    }	
  
  else 
  {
  echo  "<p><strong>".__('There is an error in deleting record. Please check the data.')."</strong></p>" ;
  showskill_adminside($msg) ;
     
  }
 
}

function update_status_skill() //function is used to update record status
{
   global $wpdb;
   $table_name = $wpdb->prefix ."skills";
   
   $query="select * from $table_name where status = 1";
   $affected_query = mysql_query($query);
   $total_rows = mysql_affected_rows();
   //echo $total_rows ;
  
   $pid=$_GET['pid'];
   if($_GET['set'] == "publish")
   {
   		$status = 1;
   }
   else
   {
   		$status = 0;
   }
  	if($wpdb->query("update $table_name set status = '$status' WHERE id = $pid"))
    {
    $msg = "<p><strong>".__('Record updated successfully')."</strong></p>" ;
	showskill_adminside($msg) ;
    }	
   	else 
  	{
  
	  echo  "<p><strong>".__('There is an error in Updating record.')."</strong></p>" ;
	  showskill_adminside($msg) ;
     }
 
}
////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////cat////////////////////////////////////////////////////////////
?>
<?php function showcat_skill_adminside($msg="")
{

global $wpdb;

$message = $msg ;
if($_GET['msg'] != "")
{
	$message = $_GET['msg'];
}
//results per page;
$resultsno = 10;
$table_name = $wpdb->prefix ."skills_cat";
$query = "select * from $table_name order by added_on desc ";
$row_count = $wpdb->get_results($query);
// pagination code //

	$page = $_REQUEST['page_num']; 
	if ($_REQUEST["page_num"]) { $page  = $_REQUEST["page_num"]; } else { $page=1; };
	$start_from = ($page-1) * $resultsno; 
	$query.=" LIMIT $start_from, $resultsno";
	
$row = $wpdb->get_results($query);
$res_count = sizeof($row_count);
?>

  <table border="0" align="center" cellpadding="10" cellspacing="10" style="width:600px;margin-left:20px; border:solid 1px #999999; margin-top:50px;">
      
      <tr>
	 <td colspan="3"><b> <?php echo __('Manage Skills Category '); ?> </b> </td> 
	 <td align="right"><a href="?page=<?php echo dirname(plugin_basename(__FILE__)); ?>/SkillsManagement.php&wpu=add_cat_skill"><b>Add New</b></a></td>
	    
      </tr>
	 <tr><td colspan="4">&nbsp;</td></tr>  
	 <?php if($message!="") { ?>    
	 <tr>
	 <td colspan="4"><b> <?php echo $message ; ?> </b> </td> 
	
	 </tr>
	 <tr><td colspan="4">&nbsp;</td></tr>  
	 <?php } ?>
	 <?php
   
	  if(!empty($row))
      {
	 ?>
	 <tr>
	 	<td> <b>Skills Category</b> </td>
		<td> <b>View Skills</b> </td>
		<td> <b>Option</b> </td>
	 </tr>
	 <?php
	 								
	 foreach($row as $data)
	   {   ?>
	 <tr>
	 	<td><?php echo $data->cat_name; ?></td>
		<td><a href="?page=<?php echo dirname(plugin_basename(__FILE__)); ?>/SkillsManagement.php&wpu=skill_admin&id=<?php echo $data->id ;?>">View skills of <?php echo $data->cat_name; ?></a></td>
		<td><a href="?page=<?php echo dirname(plugin_basename(__FILE__)); ?>/SkillsManagement.php&wpu=delete_skill_cat&id=<?php echo $data->id ;?>">Delete</a>
 		</td>
	</tr>
	<?php
	   }
		?>
	<tr><td height="10"></td></tr>
	<tr>
		<td colspan="4" align="center">
		<?php 	
				
                       $total_rows = ceil($res_count / $resultsno); 
 
                     for ($i=1; $i<=$total_rows; $i++) {
			         if($page == $i)
			           {
				          echo "<b style='color:#999999;'>[".$i."] </b>";
			           }
                         else
			           {
				          ?><a href="?page=<?php echo dirname(plugin_basename(__FILE__)); ?>/SkillsManagement.php&page_num=<?php echo $i; ?>" style="text-decoration:none;" >[<?php echo $i; ?>]</a>
		<?php 
				        }
                }?>
		</td>
	</tr>
	<?php
	  }
	  else
	  {
	?>
	  <tr>
	 	<td colspan="4" align="center"> <b>No Records Found.</b> </td>
		
	 </tr>
	<?php
	  }
	?>
	       
</table>
<?php
}


function add_cat_skill()
{
?>
<script>
function show_loading()
{
 
  if(document.getElementById('cat_name').value=='')
  {
   alert("Enter Skill Category");
   return false;
  }
  
}
</script>
<?php
global $wpdb;
if(isset($_POST['submit']))
{
		$cat_name = $_POST['cat_name'];
		$table_name = $wpdb->prefix ."skills_cat";
        
		$get_old_files = "select * from $table_name where cat_name = '$cat_name'";
	
		$row_count = mysql_num_rows(mysql_query($get_old_files));
			
		if($row_count > 0)
		{
			$message = "Error: Skill Category already exist.";
		}
		else
		{
			$insert =  "INSERT INTO ".$table_name." (cat_name) VALUES ('".$cat_name."')";
			if($wpdb->query($insert)){
			$msg =  "<p><strong>"." Skill Category Added Successfully "."</strong></p>";
			Showcat_skill_adminside($msg) ;			
		}
}
		
					
}

?>
  <table border="0" cellpadding="10" cellspacing="10" align="center" style="width:600px;margin-left:20px; border:solid 1px #999999; margin-top:50px;">
     <tr>
	 <td colspan="3"><b> <?php echo __('Add Skill Category '); ?> </b> </td> 
	 <td align="right"><a href="?page=<?php echo dirname(plugin_basename(__FILE__)); ?>/SkillsManagement.php"><b>< Go Back</b></a></td>
      </tr> 
     <tr><td colspan="4">&nbsp;</td></tr>  
	 <?php if($message!="") { ?>    
	 <tr>
	 <td colspan="4"><b> <?php echo $message ; ?> </b> </td> 
	 </tr>
	 <tr><td colspan="4">&nbsp;</td></tr>  
	 <?php } ?>
	 <form method="post" action="" enctype="multipart/form-data" name="form1" id="form1">
	 <tr>
	 	<td colspan="2" align="right" width="30%">Category Name </td>
		<td colspan="2" align="left"><input type="text" name="cat_name" id="cat_name" style="width:195px;" /></td>
	 </tr>
	  
	 <tr>
	 	<td colspan="4" align="center"><input type="submit" name="submit" value="Submit" onclick="return show_loading();" /></td>
	 </tr>
	 </form>
</table>
<?php
}

function delete_skill_cat() //function is used to delete records
{
  global $wpdb;
  $id=$_GET['id'];   
  $table_name = $wpdb->prefix ."skills_cat";
  $row = $wpdb->get_results("select * from $table_name where id = '$id'");
		  
			  
if($wpdb->query("DELETE FROM $table_name WHERE id = '$id'"))
    {
// delete and unset all the music files for category
				  
	$wpdb->query("delete from wp_skills where skill_cat='$id'");			
	$msg = "<p><strong>".__('Record deleted successfully')."</strong></p>" ;
	showcat_skill_adminside($msg) ;
    }	
  else 
  {
  echo  "<p><strong>".__('There is an error in deleting record. Please check the data.')."</strong></p>" ;
  showcat_skill_adminside($msg) ;
  }
}
?>