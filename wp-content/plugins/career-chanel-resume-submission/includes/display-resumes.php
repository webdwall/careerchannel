<?php 
// this is a resume setting page for candiate.
cc_get_member_resume(get_current_user_ID());
//this is to show resumne link in display resume page.
function cc_get_member_resume($member_id){
	global $wpdb;
	$query = 'SELECT * FROM ' . SUBTABLE . ' ' . $condition . 'where member_id='.$member_id.' ORDER BY pubDate DESC' ;
	
	$submissionsQuery = $wpdb->get_results($query);
    if(isset($submissionsQuery ) && !empty($submissionsQuery )){
    	?>
	<div id="resumeDisplay">
	    <table class="resume_display table table-striped">
	    	<thead>
	    		<th>Resume Title</th>
	    		<th>Last Update </th>
	    		<th>Settings</th>
	    		<th>Status</th>
			</thead>
			<tbody>
		    	<?php
		    	$form_lnk = get_option('resume_form_page');
		    	foreach ( $submissionsQuery as $submission ){
					$pdfLink = @exportSubToPDF( $submission->id );
					?>
					
					<tr id='resume-<?php echo $submission->id ;?>'>
						<td><a href="<?php echo WP_CONTENT_URL . '/uploads/rsjp/pdfs/' . $pdfLink; ?>" target="_blank"><?php echo $submission->resume_title; ?></a></td>
						<td><?php echo $submission->pubdate; ?></td>
						<td>
							<?php 
							$edit_base_url = cc_user_dashboard_url('candidate', 'resume', 'edit');
							$edit_complete_url = add_query_arg( array( 'type'=>'edit', 'resume_id'=>$submission->id ), $edit_base_url );
							?>
							<a href="<?php echo esc_attr( $edit_complete_url );?>" class="edit_resume">
								Edit
							</a> /
							<a href="#" class="delet_resume" data-resume_id ="<?php echo $submission->id ;?>" >
								Delete
							</a>
						</td>
						<td>
							<?php	
									$checkbox = ' ' ;
									
									if($submission->status == "active"){
									    $checkbox ="checked";
									}
									else{
									    $checkbox = "unchecked";
									}
									echo '<input type="radio" class="change_status" name="change_status" title= "change status active to make it visible." data-id ="'.$submission->id.'"  data-status ="'.$submission->status.'" '.$checkbox.' value=""><label for=".change_status">'.$submission->status.'</label></input>';
							?>
								<span class="status_react"/>
						</td>
					</tr>
					<?php
				}
				?>
				</tbody>
		</table>
	</div>
			<?php
    }
    else{
    	echo "<p class='text-error'>You've not added any resume yet!</p>";
    }
    
}
?> 