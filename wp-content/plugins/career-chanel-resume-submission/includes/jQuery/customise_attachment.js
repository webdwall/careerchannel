jQuery(document).ready(function($){

	var ajaxurl = RSJP_RESUME.ajaxurl;

	var  type = jQuery.trim(getParameterByName('type'));
	var resume_id = jQuery.trim(getParameterByName('resume_id'));

	$(".cc_remove_pdf").click(function(){
		var curr_obj = $(this);
		curr_obj.siblings('.remove_pdf_react').addClass("remove_pdf_loading");
		jQuery.ajax({ 
	        type: 'POST',  
	        url: ajaxurl,  
	        data: {  
	            link : $(this).attr("data-file"),
	            resume_id : resume_id,
	            action: 'remove_attachment_ajax'

	        },
	        success:function(response){
	         	if(response="success"){
	         		curr_obj.parent().remove();
	         	}
	         	else{
	         		alert("access denied..!");
	         	}
	         	curr_obj.siblings('.remove_pdf_react').removeClass("remove_pdf_loading");
	        }
	    });
		return false;
	});


	$(".delet_resume").click(function(e){
	    e.preventDefault();
	    var resume_id = $(this).attr("data-resume_id");
	    $parent = $(this).parent().parent();
	    $parent.addClass('danger');
	    jQuery.ajax({ 
	        type: 'POST',  
	        url: ajaxurl,  
	        data: {  
	            type : type,
	            resume_id : resume_id,
	            action: 'delet_resume_ajax'
	        },
	        success:function(response){
		    alert(response);
		    $parent.remove();
		},
		error: function(){
		    alert( 'Your request could not be processed. Please try again later' )
		}
	    });
	});


	
	$(".change_status").click(function(){
		var curr_obj = $(this);
		var status =" ";
		/*
		 if($(this).attr("checked")=='checked'){
		     status = "active";
		 }
		 else{
		     status = "inactive";
		 }
		 */
		var resume_id = $(this).attr("data-id");		
		curr_obj.siblings('.status_react').addClass("status_loading");
	   	jQuery.ajax({ 
	        type: 'POST',  
	        url: ajaxurl,  
	        data: {  
	            resume_id : resume_id,
	            status:status,
	            action: 'change_status_resume_ajax'

	        },
	        success:function(response){
	         	if(response == "error"){
	         		alert("access denied.");
	         	}
	         	else{
	         		curr_obj.attr('data-status',response);
	         		curr_obj.children().text(response);
	         	}
				window.location.reload();
	         	curr_obj.siblings('.status_react').removeClass("status_loading");
	        }
	    });
	
	});

});

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

/*	var curr_obj = $(this);
		var resume_id = $(this).attr("data-id");
		var status = $(this).attr("data-status");
		curr_obj.siblings('.status_react').addClass("status_loading");
	   	jQuery.ajax({ 
	        type: 'POST',  
	        url: ajaxurl,  
	        data: {  
	            resume_id : resume_id,
	            status:status,
	            action: 'change_status_resume_ajax'

	        },
	        success:function(response){
	         	console.log(response);
	         	if(response == "error"){
	         		alert("access denied.");
	         	}
	         	else{
	         		curr_obj.attr('data-status',response);
	         		curr_obj.text(response);
	         	}
	         	curr_obj.siblings('.status_react').removeClass("status_loading");
	        }
	    });
	    return false;
	    */
